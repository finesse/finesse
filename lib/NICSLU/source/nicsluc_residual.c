/*calculate the residual error, i.e. error = |Ax-b|*/
/*last modified: aug 17, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "math.h"
#include "complex.h"

int NicsLUc_Residual(uint__t n, complex__t *ax, uint__t *ai, uint__t *ap, complex__t *x, complex__t *b, \
					real__t *err, int mode)
{
	uint__t i, j, end;
	complex__t sum, t, s;
	real__t n2;

	if (0 == n || NULL == ax || NULL == ai || NULL == ap || NULL == x || NULL == b || NULL == err)
	{
		return NICSLU_ARGUMENT_ERROR;
	}

	*err = -1.;
	n2 = 0.;

	if (mode == 0)
	{
		for (i=0; i<n; ++i)
		{
			C_CLR(sum);
			end = ap[i+1];
			for (j=ap[i]; j<end; ++j)
			{
				C_MUL(t, ax[j], x[ai[j]]);
				C_ADDSELF(sum, t);
			}
			C_SUB(t, sum, b[i]);
			n2 += C_SQSUM(t);
		}
	}
	else
	{
		complex__t *r = (complex__t *)malloc(sizeof(complex__t)*n);
		if (r == NULL) return NICSLU_MEMORY_OVERFLOW;
		memset(r, 0, sizeof(complex__t)*n);

		for (i=0; i<n; ++i)
		{
			C_CPY(t, x[i]);
			end = ap[i+1];
			for (j=ap[i]; j<end; ++j)
			{
				C_MUL(s, ax[j], t);
				C_ADDSELF(r[ai[j]], s);
			}
		}

		for (i=0; i<n; ++i)
		{
			C_SUB(t, r[i], b[i]);
			n2 += C_SQSUM(t);
		}

		free(r);
	}

	*err = sqrt(n2);

	return NICS_OK;
}

/*Ax-b, vector*/
void _I_NicsLUc_Residual(SNicsLUc *nicslu, complex__t *sol, complex__t *b, complex__t *rerror)
{
	uint__t i, j;
	uint__t *cp, *cpi;
	uint__t *ai, *ap;
	complex__t *ax;
	uint__t n, end;
	complex__t sum;
	real__t *rs, *cs;
	uint__t col;
	complex__t t, s;
	uint__t mc64_scale;
	real__t rrs;
	complex__t xi;

	mc64_scale = nicslu->cfgi[1];
	n = nicslu->n;

	memset(rerror, 0, sizeof(complex__t)*n);

	if (nicslu->cfgi[0] == 0)
	{
		cp = nicslu->col_perm;
		ax = nicslu->ax;
		ai = nicslu->ai;
		ap = nicslu->ap;	

		if (mc64_scale)
		{
			rs = nicslu->row_scale;
			cs = nicslu->col_scale_perm;

			for (i=0; i<n; ++i)
			{
				C_CLR(sum);
				end = ap[i+1];
				for (j=ap[i]; j<end; ++j)
				{
					col = ai[j];
					C_MUL(s, ax[j], sol[cp[col]]);
					C_DIVSELFR(s, cs[col]);
					C_ADDSELF(sum, s);
				}
				C_DIVR(s, sum, rs[i]);
				C_SUB(t, s, b[i]);
				C_CPY(rerror[i], t);
			}
		}
		else
		{
			for (i=0; i<n; ++i)
			{
				C_CLR(sum);
				end = ap[i+1];
				for (j=ap[i]; j<end; ++j)
				{
					col = ai[j];
					C_MUL(s, ax[j], sol[cp[col]]);
					C_ADDSELF(sum, s);
				}
				C_SUB(t, sum, b[i]);
				C_CPY(rerror[i], t);
			}
		}
	}
	else/*CSC*/
	{
		cp = nicslu->col_perm;
		cpi = nicslu->col_perm_inv;
		ax = nicslu->ax;
		ai = nicslu->ai;
		ap = nicslu->ap;

		memset(rerror, 0, sizeof(complex__t)*n);

		if (mc64_scale)
		{
			rs = nicslu->row_scale;
			cs = nicslu->col_scale_perm;

			for (i=0; i<n; ++i)
			{
				rrs = rs[i];
				C_CPY(xi, sol[i]);
				C_DIVSELFR(xi, rrs);
				end = ap[i+1];
				for (j=ap[i]; j<end; ++j)
				{
					col = ai[j];
					C_MUL(s, ax[j], xi);
					C_ADDSELF(rerror[cp[col]], s);
				}
			}

			for (i=0; i<n; ++i)
			{
				C_DIVR(s, rerror[i], cs[cpi[i]]);
				C_SUB(t, s, b[i]);
				C_CPY(rerror[i], t);
			}
		}
		else
		{
			for (i=0; i<n; ++i)
			{
				C_CPY(xi, sol[i]);
				end = ap[i+1];
				for (j=ap[i]; j<end; ++j)
				{
					col = ai[j];
					C_MUL(s, ax[j], xi);
					C_ADDSELF(rerror[cp[col]], s);
				}
			}

			for (i=0; i<n; ++i)
			{
				C_SUB(t, rerror[i], b[i]);
				C_CPY(rerror[i], t);
			}
		}
	}
}

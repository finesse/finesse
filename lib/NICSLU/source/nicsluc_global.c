/*global parameters*/

#include "nicsluc.h"

size_t cg_sd = sizeof(complex__t);
size_t cg_si = sizeof(int__t);
size_t cg_sp = sizeof(complex__t) + sizeof(int__t);
size_t cg_sis1 = sizeof(int__t) - 1;
size_t cg_sps1 = sizeof(complex__t) + sizeof(int__t) - 1;

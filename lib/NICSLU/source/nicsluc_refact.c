/*re-factorize another matrix with the same structure, without partial pivoting*/
/*last modified: aug 14, 2013*/
/*author: Chen, Xiaoming*/

#include "nicsluc.h"
#include "nicsluc_internal.h"
#include "timer_c.h"
#include "math.h"
#include "complex.h"

extern size_t cg_si, cg_sd, cg_sp;

int NicsLUc_ReFactorize(SNicsLUc *nicslu, complex__t *ax0)
{
	complex__t *ax;
	uint__t *ai, *ap;
	uint__t cstart, cend, oldrow;
	uint__t *rowperm;
	int err;
	uint__t n;
	void *lu_array;
	complex__t *ldiag;
	size_t *up;
	uint__t *ulen, *llen;
	complex__t *x;
	uint__t i, k, p, j;
	uint__t *pinv;
	uint__t lcol;
	complex__t xj, t;
	uint__t scale;
	complex__t *cscale;
	uint__t *u_row_index_j;
	complex__t *u_row_data_j;
	uint__t u_row_len_j;
	uint__t *l_row_index_i;
	complex__t *l_row_data_i;
	uint__t l_row_len_i;

	/*check flags*/
	if (NULL == nicslu || NULL == ax0)
	{
		return NICSLU_ARGUMENT_ERROR;
	}
	if (!nicslu->flag[2])
	{
		return NICSLU_MATRIX_NOT_FACTORIZED;
	}

	ax = nicslu->ax;
	ai = nicslu->ai;
	ap = nicslu->ap;
	rowperm = nicslu->row_perm;
	n = nicslu->n;
	lu_array = nicslu->lu_array;
	ldiag = nicslu->ldiag;
	up = nicslu->up;
	ulen = nicslu->ulen;
	llen = nicslu->llen;
	x = (complex__t *)(nicslu->workspace);
	pinv = (uint__t *)(nicslu->pivot_inv);
	scale = nicslu->cfgi[2];
	cscale = nicslu->cscale;

	/*begin*/
	TimerStart((STimer *)(nicslu->timer));

	/*mc64_scale*/
	_I_NicsLUc_MC64ScaleForRefact(nicslu, ax0);

	/*scale*/
	err = _I_NicsLUc_Scale(nicslu);
	if (FAIL(err)) return err;

	memset(x, 0, sizeof(complex__t)*n);

	for (i=0; i<n; ++i)
	{
		oldrow = rowperm[i];
		cstart = ap[oldrow];
		cend = ap[oldrow+1];

		/*numeric*/
		if (scale == 1 || scale == 2)
		{
			for (k=cstart; k<cend; ++k)
			{
				j = ai[k];
				C_DIV(x[pinv[j]], ax[k], cscale[j]);
			}
		}
		else
		{
			for (k=cstart; k<cend; ++k)
			{
				C_CPY(x[pinv[ai[k]]], ax[k]);
			}
		}

		u_row_len_j = ulen[i];
		l_row_len_i = llen[i];
		l_row_index_i = (uint__t *)(((byte__t *)lu_array) + up[i] + u_row_len_j*cg_sp);
		l_row_data_i = (complex__t *)(l_row_index_i + l_row_len_i);

		for (k=0; k<l_row_len_i; ++k)
		{
			lcol = l_row_index_i[k];
			u_row_len_j = ulen[lcol];
			u_row_index_j = (uint__t *)(((byte__t *)lu_array) + up[lcol]);
			u_row_data_j = (complex__t *)(u_row_index_j + u_row_len_j);

			C_CPY(xj, x[lcol]);
			for (p=0; p<u_row_len_j; ++p)
			{
				C_MUL(t, xj, u_row_data_j[p]);
				C_SUBSELF(x[u_row_index_j[p]], t);
			}
		}

		u_row_len_j = ulen[i];
		u_row_index_j = (uint__t *)(((byte__t *)lu_array) + up[i]);
		u_row_data_j = (complex__t *)(u_row_index_j + u_row_len_j);

		C_CPY(xj, x[i]);
		C_CLR(x[i]);

		/*check the diag*/
		if (C_EQZ(xj))
		{
			return NICSLU_MATRIX_NUMERIC_SINGULAR;
		}
		if (C_NAN(xj))
		{
			return NICSLU_NUMERIC_OVERFLOW;
		}

		/*put data into L and U*/
		for (k=0; k<u_row_len_j; ++k)
		{
			lcol = u_row_index_j[k];
			C_DIV(u_row_data_j[k], x[lcol], xj);
			C_CLR(x[lcol]);
		}

		for (k=0; k<l_row_len_i; ++k)
		{
			lcol = l_row_index_i[k];
			C_CPY(l_row_data_i[k], x[lcol]);
			C_CLR(x[lcol]);
		}

		C_CPY(ldiag[i], xj);
	}

	/*finish*/
	TimerStop((STimer *)(nicslu->timer));
	nicslu->stat[2] = TimerGetRuntime((STimer *)(nicslu->timer));

	return NICS_OK;
}

/* ! A collection of functions to communicate with the socket connection
 * 
 * $Id: net.c 184 2006-03-31 08:54:07Z hewitson $ */


#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

#include "mnet.h"

#define DEBUG 0


/* ! Open a stream on the socket descriptor and initialise it.
 * 
 * \retval 0 - succesful initialisation 
 * \retval -1 - Couldn't open a stream on the socket. 
 */
int netinit (mstream * s, int sd)
{

	// open a read and write stream on socket descriptor
	if (((s->in = fdopen (sd, "r")) == NULL)
			|| ((s->out = fdopen (sd, "w")) == NULL))
	{
		perror ("fdopen");
		return -1;
	}

	// intilialise last read
	s->last = 0;

	return 0;
}


/* ! read data from socket straight into a file descriptor 
 *
 * \retval 0 - succesful 
 * \retval -1 - Couldn't write to file. 
 */
int freadToFile (mstream * s, int fd, int nbytes)
{
	int            read;
	char          *buffer;

	// allocate memory for read in the data
	if ((buffer = (char *) calloc (nbytes, sizeof (char))) == NULL)
	{
		fprintf (stderr, "### freadToFile: couldn't allocate buffer memory\n");
		return -1;
	}

	// read the data
	read = fread (buffer, sizeof (char), nbytes, s->in);

	// write data to file descriptor
	if ((write (fd, buffer, nbytes)) < 0)
	{
		perror ("freadToFile:");
		return -1;
	}

	s->last = READ_EVENT;													 // set stream last event
	free (buffer);																 // free buffer memory

	return 0;
}


/* ! send a block of data as bytes 
 *
 * \retval sent - number of bytes sent 
 */
int fsendData (mstream * s, char *data, int nbytes)
{
	int            sent;

#if DEBUG
  printf ("sending %d bytes\n", nbytes);
#endif
  
	sent = fwrite (data, sizeof (char), nbytes, s->out);

	s->last = WRITE_EVENT;

#if DEBUG
  printf ("sent %d bytes\n", sent * sizeof (short int));
#endif

	return sent;
}


/* ! send a block of data as bytes 
 *
 * \retval read - number of bytes read 
 */
int freadData (mstream * s, char **data, int nbytes)
{
	int            read;

#if DEBUG
  printf ("sending %d bytes\n", nbytes);
#endif

	read = fread (*data, sizeof (char), nbytes, s->in);

	s->last = WRITE_EVENT;

	return read;
}

/* ! receive a block of short data from stream 
 * 
 * \retval read - number of bytes read 
 */
int freadDataS (mstream * s, short int **data, int nsamples)
{
	int            read;
	int            i;
	short int     *incoming;

#if DEBUG
	printf ("reading %d bytes\n", nsamples * sizeof (short int));
#endif

	// check whether we need to flush, i.e. if there was a write event 
	// on this stream before this

	if (s->last)
		fflush (s->out);

	incoming = (short int *) malloc (sizeof (short int) * nsamples);
  if(incoming==NULL)
  {
    fprintf(stderr, "### freadDataS: error allocating memory.\n");
    return -1;
  }

	read = fread (incoming, sizeof (short int), nsamples, s->in);
	// now convert this to host order
	for (i = 0; i < nsamples; i++)
		(*data)[i] = ntohs (incoming[i]);

#if DEBUG
	printf ("read %d bytes\n", read * sizeof (short int));
#endif
  
	s->last = READ_EVENT;
	free (incoming);

	return read * sizeof (short int);
}


/* ! send a block of n shorts to stream 
 *
 * \retval sent - number of bytes sent 
 */
int fsendDataS (mstream * s, short int *data, int nsamples)
{
	int            sent;
	int            i;
	short int     *outgoing;

#if DEBUG
	printf ("sending %d shorts\n", nsamples);
#endif
	outgoing = (short int *) malloc (sizeof (short int) * nsamples);
  if(outgoing == NULL)
  {
    fprintf(stderr, "### fsendDataS: error allocating memory.\n");
    return -1;    
  }

	// need to convert to network order first
	for (i = 0; i < nsamples; i++)
		outgoing[i] = htons (data[i]);

	sent = fwrite (outgoing, sizeof (short int), nsamples, s->out);

	s->last = WRITE_EVENT;

#if DEBUG
	printf ("sent %d shorts\n", sent);
#endif

	free (outgoing);

	return sent * sizeof (short int);
}


/* ! receive a block of long data from stream 
 *
 * \retval read - number of bytes read 
 */
int freadDataL (mstream * s, long int **data, int nsamples)
{
	int            read;
	int            i;
	long int      *incoming;

#if DEBUG
	printf ("reading %d samples\n", nsamples);
#endif

	if (s->last)
		fflush (s->out);

	incoming = (long int *) malloc (sizeof (long int) * nsamples);
  if(incoming==NULL)
  {
    fprintf(stderr, "### freadDataL: error allocating memory.\n");
    return -1;
  }

	read = fread (incoming, sizeof (long int), nsamples, s->in);
	// now convert this to host order
	for (i = 0; i < nsamples; i++)
	{
		(*data)[i] = ntohl (incoming[i]);
	}

#if DEBUG
	printf ("read %d samples\n", read);
#endif


	s->last = READ_EVENT;
	free (incoming);

	return read * sizeof (long int);
}


/* ! send a block of nsamples longs to stream 
 * 
 * \retval sent - number of bytes sent 
 */
int fsendDataL (mstream * s, long int *data, int nsamples)
{
	int            sent;
	int            i;
	long int      *outgoing;

#if DEBUG
	printf ("sending %d longs\n", nsamples);
#endif

	outgoing = (long int *) malloc (sizeof (long int) * nsamples);
  if(outgoing == NULL)
  {
    fprintf(stderr, "### fsendDataL: error allocating memory.\n");
    return -1;    
  }

	// need to convert to network order first
	for (i = 0; i < nsamples; i++)
		outgoing[i] = htonl (data[i]);

	sent = fwrite (outgoing, sizeof (long int), nsamples, s->out);

#if DEBUG
	printf ("sent %d longs\n", sent);
#endif

	s->last = WRITE_EVENT;
	free (outgoing);

	return sent * sizeof (long int);
}

/* ! receive a block of floats from stream \retval read - number of bytes read */
int freadDataF (mstream * s, float **data, int nsamples)
{
	int            i;

#if DEBUG
	printf ("reading %d samples\n", nsamples);
#endif

	if (s->last)
		fflush (s->out);

	// now convert this to host order
	for (i = 0; i < nsamples; i++)
		freadFloat (s, &(*data)[i]);

	s->last = READ_EVENT;

	return nsamples * sizeof (float);
}


/* ! send a block of nsamples floats to stream \retval sent - number of bytes
 * sent */
int fsendDataF (mstream * s, float *data, int nsamples)
{
	int            i;

#if DEBUG
	printf ("sending %d floats\n", nsamples);
#endif

	// need to convert to network order first
	for (i = 0; i < nsamples; i++)
		fsendFloat (s, data[i]);

	s->last = WRITE_EVENT;


	return nsamples * sizeof (float);
}

/*! 
 *  Receive a block of doubles from stream 
 *
 * \retval read - number of bytes read 
 */
int freadDataD (mstream * s, double **data, int nsamples)
{
	int            i;

#if DEBUG
	printf ("reading %d samples\n", nsamples);
#endif

	if (s->last)
		fflush (s->out);

	// now convert this to host order
	for (i = 0; i < nsamples; i++)
		freadDouble (s, &(*data)[i]);

	s->last = READ_EVENT;

	return nsamples * sizeof (double);
}


/* ! 
 *  Send a block of nsamples floats to stream 
 * 
 * \retval sent - number of bytes sent 
 */
int fsendDataD (mstream * s, double *data, int nsamples)
{
	int            i;

#if DEBUG
	printf ("sending %d doubles\n", nsamples);
#endif

	// need to convert to network order first
	for (i = 0; i < nsamples; i++)
		fsendDouble (s, data[i]);

	s->last = WRITE_EVENT;

	return nsamples * sizeof (double);
}


/* ! send a string to file stream \retval sent - number of bytes sent */
int fsendString (mstream * s, char *str)
{
	int            sent;

#if DEBUG
	printf ("size of string to send is %d\n", strlen (str));
	printf ("sending %s\n", str);
#endif

	fsendLong (s, strlen (str));

	sent = fwrite (str, sizeof (char), strlen (str), s->out);

	s->last = WRITE_EVENT;

#if DEBUG
	printf ("sent %d chars\n", sent);
#endif

	return sent * sizeof (char);
}

/* ! read a string from the stream \retval read - number of bytes read */
int freadString (mstream * s, char **buff, int *buff_size)
{
	int            read = 0;
	long int       len;

	(*buff)[0] = 0;																 // clear buffer

	// do we need to flush?
	if (s->last)
		fflush (s->out);

	read = freadLong (s, &len);

#if DEBUG
	printf ("read %d bytes as a long\n", read);
	printf ("going to receive %ld bytes\n", len);
#endif

	// check for EOF - if so, exit since client has left
	if (fgets (*buff, len + 1, s->in) == NULL)
	{
		printf ("### fgets returned NULL\n");
		return (-1);
	}
	// get num chars read
	read = strlen (*buff);

	//if (read > 0)
  //		*buff_size = read;

#if DEBUG
	printf ("read %d from stream\n", read);
#endif

	s->last = READ_EVENT;

#if DEBUG
	printf ("read %s\n", *buff);
#endif

	return read;
}

/* ! send a float
 * 
 */
int fsendFloat (mstream * s, float f)
{
	int            sent;
	long int       exp, expout;
	long int       mant, mantout;
	double         fmant;
  int            texp;
  
	// get mantissa and exponant
	fmant = frexp (f, &texp);
  exp = texp;
	// get int version of mantissa
	mant = (long int) (fmant * pow (2, 24));
	// convert to network order
	expout = htonl (exp);
	mantout = htonl (mant);

	// send mantissa to socket
	sent = fwrite (&mantout, sizeof (long int), 1, s->out);
	// send exponant to socket
	sent = fwrite (&expout, sizeof (long int), 1, s->out);

	s->last = WRITE_EVENT;

#if DEBUG
	printf ("sent %f as float.\n", f);
#endif

	return sent * sizeof (float);
}

/* ! read a float from a file stream 
 * 
 * \retval read - number of bytes read 
 */
int freadFloat (mstream * s, float *f)
{
	long int       mantin, expin;
	long int       mant, exp;
	double         fmant;
	int            received;

	if (s->last)
		fflush (s->out);

	// read mantissa
	received = fread (&mantin, sizeof (long int), 1, s->in);
	// read exponant
	received = fread (&expin, sizeof (long int), 1, s->in);

	// convert to host order
	mant = ntohl (mantin);
	exp = ntohl (expin);
  
	// convert mantissa back to a float
	fmant = mant / pow (2, 24);

	// convert back to a float
	*f = (float) (fmant * pow (2, exp));

	if (received == 0)
	{
		if (ferror (s->in) != 0)
			printf ("### error on input stream\n");
		if (feof (s->in) != 0)
			printf ("### eof on input stream\n");
	}

#if DEBUG
	printf ("received %f as a long\n", *f);
#endif

	s->last = READ_EVENT;

	return received * sizeof (float);
}


/* ! send a double
 * 
 */
int fsendDouble (mstream * s, double d)
{
	int            sent;
  char           packed[8];
  
  pack_double(d,&packed[0],1);
  
	// send mantissa to socket
	sent = fwrite (packed, sizeof (char), 8, s->out);
	
	s->last = WRITE_EVENT;

	return sent * sizeof (double);
}

/*! 
 *  Read a double from a file stream 
 *  
 *  \retval read - number of bytes read 
 */
int freadDouble (mstream * s, double *d)
{
	int            received;
  char           packed[8];
  
	if (s->last)
		fflush (s->out);

	// read data
	received = fread (packed, sizeof (char), 8, s->in);
  
  *d = unpack_double(&packed[0], 1);
    
	s->last = READ_EVENT;

	return received * sizeof (double);
}


/* ! send a long int to file stream 
* 
* \retval sent - number of bytes sent 
*/
int fsendLong (mstream * s, long int li)
{
	int            sent;
	long int       lout;

	lout = htonl (li);														 // convert to network order

	// send a short to socket
	sent = fwrite (&lout, sizeof (long int), 1, s->out);
	s->last = WRITE_EVENT;

#if DEBUG
	printf ("sent %ld as long.\n", li);
#endif

	return sent * sizeof (long int);
}

/* ! send a short int to file stream \retval sent - number of bytes sent */
int fsendShort (mstream * s, short int si)
{
	int            sent;
	short int      sout;

	sout = htons (si);														 // convert to network order

	// send a short to socket
	sent = fwrite (&sout, sizeof (short int), 1, s->out);
	s->last = WRITE_EVENT;

	return sent * sizeof (short int);
}


/* ! read a long int from a file stream 
 * 
 * \retval read - number of bytes read 
 */
int freadLong (mstream * s, long int *li)
{
	long int       lint = 0;
	int            received;

	if (s->last)
		fflush (s->out);

	received = fread (&lint, sizeof (long int), 1, s->in);

	if (received == 0)
	{
		if (ferror (s->in) != 0)
			printf ("### error on input stream\n");
		if (feof (s->in) != 0)
			printf ("### eof on input stream\n");
	}

	*li = ntohl (lint);

#if DEBUG
	printf ("received %ld as a long\n", *li);
#endif

	s->last = READ_EVENT;

	return received * sizeof (long int);
}

/* ! read a long int from a file stream 
 * 
 * \retval read - number of bytes read 
 */
int freadShort (mstream * s, short int *si)
{
	short int      sint = 0;
	int            received = -1;

	if (s->last)
		fflush (s->out);

	received = fread (&sint, sizeof (short int), 1, s->in);

	*si = ntohs (sint);
	s->last = READ_EVENT;

	return received * sizeof (short int);
}


/* ! send a string to the socket descriptor 
 *
 * \retval sent - number of bytes sent 
 */
int sendString (int socketfd, char *s)
{
	int            sent;

#if DEBUG
  printf ("sending %s\n", s);
#endif

	sent = send (socketfd, s, strlen (s), 0);

	// check to see if a new line was sent
	if (strlen (s) > 0)
		if (s[strlen (s) - 1] != '\n')
			send (socketfd, "\n", strlen ("\n"), 0);

	return sent;
}

/* ! read a string 
 *
 * \retval read - number of bytes read 
 */
int readString (int socketfd, char *s, int len)
{
	int            read;

	read = recv (socketfd, s, len, 0);

	return read;
}

/* ! send a short int to network socket 
 * 
 * \retval sent - number of bytes sent
 * \retval -1 - error sending short 
 */
int sendShort (int socketfd, short int si)
{
	int            sent;
	short int      out;

	out = htons (si);															 // convert to network order

	// send a short to socket
	sent = send (socketfd, &out, sizeof (si), 0);

	return sent;
}

/* ! read a short int from the network socket 
 * 
 * \retval read - number of bytes
 * read \retval -1 - error reading short 
 */
int readShort (int socketfd, short int *si)
{
	short int      in;
	int            received;

	// read a short int from socket
	received = recv (socketfd, &in, sizeof (short int), 0);
	*si = ntohs (in);															 // convert to host order

	return received;															 // return num bytes
}

/* ! send a long int to network socket 
 *
 * \retval sent - number of bytes sent
 * \retval -1 - error sending short 
 */
int sendLong (int socketfd, long int si)
{
	int            sent;
	long int       out;

	out = htonl (si);															 // convert to network order
	// send a short to socket
	sent = send (socketfd, &out, sizeof (si), 0);

	// printf("sent %d bytes\n", sent);

	return sent;
}

/* ! read a long int from the network socket 
 *
 * \retval read - number of bytes read 
 * \retval -1 - error reading short 
 */
int readLong (int socketfd, long int *li)
{
	long int       in;
	int            received = -1;

	received = recv (socketfd, &in, sizeof (long int), 0);
	*li = ntohl (in);															 // convert to host order

	return received;															 // return num bytes read
}


/* ! read n bytes from fd */
int readn (int fd, void *vptr, size_t n)
{
	size_t         nleft;
	ssize_t        nread;
	char          *ptr;

	ptr = (char *) vptr;
	nleft = n;

	while (nleft > 0)
	{
		if ((nread = read (fd, ptr, nleft)) < 0)
		{
			if (errno == EINTR)
				nread = 0;
			else
				return -1;
		}
		else if (nread == 0)
			break;

		nleft -= nread;
		ptr += nread;
	}

	return n - nleft;
}

/* ! write n bytes to fd */
int writen (int fd, const void *vptr, size_t n)
{
	size_t         nleft;
	ssize_t        nwritten;
	const char    *ptr;

	ptr = (char *) vptr;
	nleft = n;

	while (nleft > 0)
	{
		if ((nwritten = write (fd, ptr, nleft)) <= 0)
		{
			if (errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}

		nleft -= nwritten;
		ptr += nwritten;
	}

	return n;
}

double unpack_double(const char *p,  /* Where the high order byte is */
              int incr)       /* 1 for big-endian; -1 for little-endian */
{
	int s;
	int e;
	long fhi, flo;
	double x;

	/* First byte */
	s = (*p>>7) & 1;
	e = (*p & 0x7F) << 4;
	p += incr;

	/* Second byte */
	e |= (*p>>4) & 0xF;
	fhi = (*p & 0xF) << 24;
	p += incr;

	/* Third byte */
	fhi |= (*p & 0xFF) << 16;
	p += incr;

	/* Fourth byte */
	fhi |= (*p & 0xFF) << 8;
	p += incr;

	/* Fifth byte */
	fhi |= *p & 0xFF;
	p += incr;

	/* Sixth byte */
	flo = (*p & 0xFF) << 16;
	p += incr;

	/* Seventh byte */
	flo |= (*p & 0xFF) << 8;
	p += incr;

	/* Eighth byte */
	flo |= *p & 0xFF;
	p += incr;

	x = (double)fhi + (double)flo / 16777216.0; /* 2**24 */
	x /= 268435456.0; /* 2**28 */

	/* XXX This sadly ignores Inf/NaN */
	if (e == 0)
		e = -1022;
	else {
		x += 1.0;
		e -= 1023;
	}
	x = ldexp(x, e);

	if (s)
		x = -x;

	return x;
}


int pack_double( double x, /* The number to pack */
                        char *p,  /* Where to pack the high order byte */
                        int incr) /* 1 for big-endian; -1 for little-endian */
{
	int s;
	int e;
	double f;
	long fhi, flo;

	if (x < 0) {
		s = 1;
		x = -x;
	}
	else
		s = 0;

	f = frexp(x, &e);

	/* Normalize f to be in the range [1.0, 2.0) */
	if (0.5 <= f && f < 1.0) {
		f *= 2.0;
		e--;
	}
	else if (f == 0.0) {
		e = 0;
	}
	else {
		fprintf(stderr, "frexp() result out of range\n");
		return -1;
	}

	if (e >= 1024) {
		/* XXX 1024 itself is reserved for Inf/NaN */
		fprintf(stderr, "float too large to pack with d format\n");
		return -1;
	}
	else if (e < -1022) {
		/* Gradual underflow */
		f = ldexp(f, 1022 + e);
		e = 0;
	}
	else if (!(e == 0 && f == 0.0)) {
		e += 1023;
		f -= 1.0; /* Get rid of leading 1 */
	}

	/* fhi receives the high 28 bits; flo the low 24 bits (== 52 bits)
*/
	f *= 268435456.0; /* 2**28 */
	fhi = (long) floor(f); /* Truncate */
	f -= (double)fhi;
	f *= 16777216.0; /* 2**24 */
	flo = (long) floor(f + 0.5); /* Round */

	/* First byte */
	*p = (s<<7) | (e>>4);
	p += incr;

	/* Second byte */
	*p = (char) (((e&0xF)<<4) | (fhi>>24));
	p += incr;

	/* Third byte */
	*p = (fhi>>16) & 0xFF;
	p += incr;

	/* Fourth byte */
	*p = (fhi>>8) & 0xFF;
	p += incr;

	/* Fifth byte */
	*p = fhi & 0xFF;
	p += incr;

	/* Sixth byte */
	*p = (flo>>16) & 0xFF;
	p += incr;

	/* Seventh byte */
	*p = (flo>>8) & 0xFF;
	p += incr;

	/* Eighth byte */
	*p = flo & 0xFF;
	p += incr;

	/* Done */
	return 0;
}

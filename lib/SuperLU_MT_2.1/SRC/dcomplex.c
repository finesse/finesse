
/*
 * -- SuperLU routine (version 2.0) --
 * Lawrence Berkeley National Lab, Univ. of California Berkeley,
 * and Xerox Palo Alto Research Center.
 * September 10, 2007
 *
 */
/*
 * This file defines common arithmetic operations for complex type.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "slu_dcomplex.h"


/* Complex Division c = a/b */
void z_div(doublecomplex *c, doublecomplex *a, doublecomplex *b)
{
    double ratio, den;
    double abr, abi, cr, ci;
  
    if( (abr = b->re) < 0.)
	abr = - abr;
    if( (abi = b->im) < 0.)
	abi = - abi;
    if( abr <= abi ) {
	if (abi == 0) {
	    fprintf(stderr, "z_div: division by zero\n");
            exit(-1);
	}	  
	ratio = b->re / b->im ;
	den = b->im * (1 + ratio*ratio);
	cr = (a->re*ratio + a->im) / den;
	ci = (a->im*ratio - a->re) / den;
    } else {
	ratio = b->im / b->re ;
	den = b->re * (1 + ratio*ratio);
	cr = (a->re + a->im*ratio) / den;
	ci = (a->im - a->re*ratio) / den;
    }
    c->re = cr;
    c->im = ci;
}


/* Returns sqrt(z.r^2 + z.i^2) */
double z_abs(doublecomplex *z)
{
    double temp;
    double real = z->re;
    double imag = z->im;

    if (real < 0) real = -real;
    if (imag < 0) imag = -imag;
    if (imag > real) {
	temp = real;
	real = imag;
	imag = temp;
    }
    if ((real+imag) == real) return(real);
  
    temp = imag/real;
    temp = real*sqrt(1.0 + temp*temp);  /*overflow!!*/
    return (temp);
}


/* Approximates the abs */
/* Returns abs(z.r) + abs(z.i) */
double z_abs1(doublecomplex *z)
{
    double real = z->re;
    double imag = z->im;
  
    if (real < 0) real = -real;
    if (imag < 0) imag = -imag;

    return (real + imag);
}

/* Return the exponentiation */
void z_exp(doublecomplex *r, doublecomplex *z)
{
    double expx;

    expx = exp(z->re);
    r->re = expx * cos(z->im);
    r->im = expx * sin(z->im);
}

/* Return the complex conjugate */
void d_cnjg(doublecomplex *r, doublecomplex *z)
{
    r->re = z->re;
    r->im = -z->im;
}

/* Return the imaginary part */
double d_imag(doublecomplex *z)
{
    return (z->im);
}



/* STANDALONE.C */
/*FORMULC 2.22           as of 2/19/98        */
/*A byte-compiler for mathematical functions */
/*Copyright (c) 1993-98 by Harald Helfgott        */
/* 	This program is provided "as is", without any explicit or */
/* implicit warranty. */
/* This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.*/
/* Programmer's Address:
	    Harald Helfgott
	    MB 1807, Brandeis University
	    P.O. Box 9110
	    Waltham, MA 02254-9110
	    U.S.A.
	    hhelf@cs.brandeis.edu */
/**************************************************************/
/* Here begins the stand-alone part */
/* Many thanks to Dr. Ralf Grosse-Kunstleve (ralf@kristall.erdw.ethz.ch)*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "formulc.h"
static char *progn = "formulc";


static void progerror(const char *msg)
{
  fprintf(stderr, "%s: %s\n", progn, msg);
  exit(1);
}


static void NotEnoughCore(void)
{
  fprintf(stderr, "%s: Not enough core\n", progn);
  exit(1);
}


static double xtal(double x)
{
  const double EPS = 1.e-5;


  x -= (long) x;
  for (;;) if (x >= 1.) x -= 1.; else break;
  for (;;) if (x <  0.) x += 1.; else break;
  if      ( 1. - EPS <= x && x <=  1. + EPS) x = 0.;
  else if (-1. - EPS <= x && x <= -1. + EPS) x = 0.;
  else if (    - EPS <= x && x <=       EPS) x = 0.;

  return x;
}


static int fgetline(FILE *fpin, char s[], int size_s)
{
  int         last_s, c, i;


  last_s = size_s - 1;

  i = 0;

  while ((c = getc(fpin)) != EOF && c != '\n')
  {
#ifdef __MSDOS__
    if (c == 0x1A /* CtrlZ */) {
      ungetc(c, fpin);
      c = EOF;
      break;
    }
#endif

    if (i < last_s) s[i++] = (char) c;
  }

  s[i] = '\0';

  if (i == 0 && c == EOF)
    return 0;

  if (i > 0 && s[i - 1] == '\r') /* remove CR if we have CR-LF */
    s[i - 1] = '\0';

  return 1;
}


static int strarg(char *arg, char *s, int n)
                                      /* - string argument -            */
{                                     /* return n-th argument of string */
  char *a = arg;                      /* s in arg.                      */
                                      /* n = 0 for the first argument.  */
  while (*s == ' ' || *s == '\t') s++;   /* Blank and Tab are seperators.*/

  while (n--)
  { while (*s != '\0' && *s != ' ' && *s != '\t') s++;
    while (*s == ' ' || *s == '\t') s++;
  }

  while (*s != '\0' && *s != ' ' && *s != '\t') *a++ = *s++;
  *a = '\0';

  return (a != arg);
}


static int str_icmp(const char *s, const char *t)
{
  char  cs, ct;


  while (*s || *t)
  {
    cs = toupper(*s++);
    ct = toupper(*t++);
    if (cs < ct) return -1;
    if (cs > ct) return  1;
  }

  return 0;
}


static char firstnonblank(const char *s)
{
  while (*s && isspace(*s)) s++;
  return *s;
}


static int is_legal_c_format(const char *cfmt)
{
  int         count, fmt_on, fmt_width;
  const char  *start_digits;


  count = 0;
  start_digits = NULL;

  for (fmt_on = fmt_width = 0; *cfmt; cfmt++)
  {
    if (isdigit(*cfmt))
    {
      if (start_digits == NULL)
          start_digits = cfmt;
    }

    if (fmt_on == 0)
    {
      if (*cfmt == '%')
      {
        fmt_on = 1;
        fmt_width = 0;
      }
    }
    else
    {
      if      (*cfmt == '%')
      {
        if (fmt_width) return 0;
        fmt_on = 0;
      }
      else if (*cfmt == '$')
      {
        if (start_digits == NULL) return 0;
        if (cfmt - start_digits != 1) return 0;
        if (*start_digits != '1') return 0;
      }
      else if (*cfmt == '*')
        return 0;
      else if (isalpha(*cfmt))
      {
        if (strchr("feEgG", *cfmt) == NULL) return 0;
        fmt_on = 0;
        count++;
      }

      fmt_width++;
    }

    if (isdigit(*cfmt) == 0)
      start_digits = NULL;
  }

  if (fmt_on || count != 1) return 0;

  return 1;
}


static const char LegalVarNames[] = "abcdefghijklmnopqrstuvwxyz";
#define mLegalVarNames (sizeof LegalVarNames / sizeof (*LegalVarNames))


static int BuildVarTab(const char *VarDecl, char *VarTab, int *FldIdx)
{
  int         iVar, iFld, i;
  char        VarName;
  const char  *VD;


  iFld = 0;
  iVar = 0;

  for (VD = VarDecl; *VD; VD++, iFld++)
  {
    VarName = tolower(*VD);

    for (i = 0; LegalVarNames[i]; i++)
      if (VarName == LegalVarNames[i])
        break;

    if (LegalVarNames[i])
    {
      VarTab[iVar] = VarName;
      FldIdx[iVar] = iFld;
             iVar++;
    }
  }

  VarTab[iVar] = '\0';
  return iVar;
}


static formu *MultipleTranslate(const char *formulae, char *VarTab,
                                int *nListFormu, int delim)
{
  int         n, i, pass, error;
  char        *buf;
  const char  *fo;
  int         ibuf, iFld;
  formu       *ListFormu;


      buf = malloc((strlen(formulae) + 1) * sizeof (*buf));
  if (buf == NULL)
    NotEnoughCore();

  if (delim < 0) delim = ';';

  ListFormu = NULL;

  for (pass = 0; pass <= 1; pass++)
  {
    iFld = 0;
    ibuf = 0;

    for (fo = formulae;; fo++)
    {
      if (*fo == '\0' || *fo == delim)
      {
        if (ibuf == 0) return NULL;

        buf[ibuf] = '\0';

        if (pass == 1)
        {
          ListFormu[iFld] = translate(buf, VarTab, &n, &error);

          if (n == 0)
          {
            fprintf(stderr, "   %s\n", buf);
            for (i = 0; i < error; i++) putc('-', stderr);
            fprintf(stderr, "---^\n");
            fprintf(stderr, "%s: %s\n", progn, fget_error());
            exit(1);
          }
        }

        iFld++;
        ibuf = 0;

        if (*fo == '\0') break;
      }
      else if (isspace(*fo) == 0)
        buf[ibuf++] = tolower(*fo);
    }

    if (pass == 0)
    {
          ListFormu = malloc(iFld * sizeof (*ListFormu));
      if (ListFormu == NULL)
        NotEnoughCore();
    }
  }

  free(buf);

       *nListFormu = iFld;
  return ListFormu;
}


static int iDigit(char c)
{
   switch (c)
   {
     case '0': return 0;
     case '1': return 1;
     case '2': return 2;
     case '3': return 3;
     case '4': return 4;
     case '5': return 5;
     case '6': return 6;
     case '7': return 7;
     case '8': return 8;
     case '9': return 9;
     default:
       break;
   }

   return -1;
}


static int GetRange(const char *s, int *f, int *l)
{
  int  active, i;


  (*f) = 0;
  (*l) = 0;

  active = 0;

  for (;; s++)
  {
    if (*s == '\0')
    {
      (*l) = (*f);
      return 0;
    }

        i = iDigit(*s);
    if (i < 0) break;

    (*f) *= 10;
    (*f) += i;

    active = 1;
  }

  if (active && (*f) == 0) return -1;
  if (*s++ != '-') return -1;

  active = 0;

  for (;; s++)
  {
    if (*s == '\0')
    {
      if (active && (*l) == 0) return -1;
      if ((*l) && (*l) < (*f)) return -1;
      return 0;
    }

        i = iDigit(*s);
    if (i < 0) break;

    (*l) *= 10;
    (*l) += i;

    active = 1;
  }

  return -1;
}


typedef struct T_oFmts
  {
    const char      *Fmt;
    int             iFld;
    int             jFld;
    struct T_oFmts  *Next;
  }
  T_oFmts;


static int Set_oFmt(T_oFmts *oFmt, const char *arg)
{
  if (arg[0] == '@')
  {
    oFmt->Fmt  = NULL;

    if (GetRange(&arg[1], &oFmt->iFld, &oFmt->jFld) != 0)
      return -1;
  }
  else if (is_legal_c_format(arg))
  {
    oFmt->Fmt  = arg;
    oFmt->iFld = 0;
    oFmt->jFld = 0;
  }
  else
    return -1;

  return 0;
}


static int EchoRange(char *s, int f, int l, int ExtraBlk)
{
  int         active, i;
  const char  *p;


  active = 0;

  for (i = 1; i < f; i++)
  {
    while (*s &&   isspace(*s)) s++;
    while (*s && ! isspace(*s)) s++;
  }

  while (*s && isspace(*s)) s++;

  if (*s && ExtraBlk) putc(' ', stdout);

  for (;;)
  {
    while (*s && ! isspace(*s)) { putc(*s++, stdout), active = 1; }
    if (*s == '\0') break;

    if (l && i >= l) break;
    i++;

    p = s;
    while (*p &&   isspace(*p)) p++;
    if (*p == '\0') break;

    while (*s &&   isspace(*s)) putc(*s++, stdout);
  }

  return active;
}


static void usage(void)
{
  fprintf(stderr,
    "usage: %s [-xtal] [-d\"x\"] \"function(s)\" \"arguments\""
    " [\"c-format\"|@# ...] [file]\n",
    progn);
  exit(1);
}


int main(int argc, char *argv[])
{
  int   iarg, n;
  int   F_xtal, F_Delim;
  char  *F_functions;
  char  buf[256], svar[sizeof buf], xtrac;
  long  linec;
  char  *fnin;
  FILE  *fpin;

  int    nVarTab, iVar;
  char    VarTab[mLegalVarNames * sizeof (char)];
  int     FldIdx[mLegalVarNames * sizeof (int)];
  int     nListFormu, iLF;
  formu   *ListFormu;
  double  var, *Results;

  int         ExtraBlk;
  const char  *LastFmt;
  T_oFmts     *Root_oFmts, *oFmt;


  if (fnew("mod", (Func) fmod, 2, 0) != 1)
    progerror("Internal Error: fnew() failed");

  if (fnew("xtal", xtal, 1, 0) != 1)
    progerror("Internal Error: fnew() failed");

  fnin        = NULL;
  fpin        = NULL;
  F_xtal      = 0;
  F_Delim     = -1;
  F_functions = NULL;
  nVarTab     = -1;
  Root_oFmts  = NULL;

  for (iarg = 1; iarg < argc; iarg++)
  {
    if      (str_icmp(argv[iarg], "-xtal") == 0)
    {
      if (F_xtal) usage();
      F_xtal = 1;
    }
    else if (    argv[iarg][0] == '-'
             && (argv[iarg][1] == 'd' || argv[iarg][1] == 't')
             && (argv[iarg][2] > 1 || argv[iarg][2] == 0)
             &&  argv[iarg][3] == '\0'
            )
    {
      if (F_Delim >= 0) usage();
      F_Delim = argv[iarg][2];
    }
    else if (F_functions == NULL)
    {
      F_functions = argv[iarg];
      if (strlen(F_functions) == 0) usage();
    }
    else if (nVarTab == -1)
    {
      nVarTab = BuildVarTab(argv[iarg], VarTab, FldIdx);
    }
    else if (   fnin == NULL
             && argv[iarg][0] != '@'
             && strchr(argv[iarg], '%') == NULL)
    {
      fnin = argv[iarg];
    }
    else
    {
      if (Root_oFmts == NULL) {
        Root_oFmts = malloc(sizeof (*oFmt));
        oFmt = Root_oFmts;
      }
      else {
        oFmt->Next = malloc(sizeof (*oFmt));
        oFmt = oFmt->Next;
      }

      oFmt->Next = NULL;

      if (Set_oFmt(oFmt, argv[iarg]) != 0)
      {
        fprintf(stderr, "%s: Invalid format string \"%s\"\n",
          progn, argv[iarg]);
        exit(1);
      }
    }
  }

  if (F_functions == NULL || nVarTab < 1)
    usage();

  if (F_xtal && F_Delim < 0) F_Delim = ',';

      ListFormu = MultipleTranslate(F_functions, VarTab, &nListFormu, F_Delim);
  if (ListFormu == NULL)
    usage();

      Results = malloc(nListFormu * sizeof (*Results));
  if (Results == NULL)
    NotEnoughCore();

  if (fnin == NULL)
    fpin = stdin;
  else
  {
        fpin = fopen(fnin, "r");
    if (fpin == NULL)
    {
      fprintf(stderr, "%s: Can't open %s\n", progn, fnin);
      exit(1);
    }
  }

  rnd_init();
  linec = 0;

  while (fgetline(fpin, buf, sizeof buf))
  {
    linec++;

    if (firstnonblank(buf) == '#')
    {
      fprintf(stdout, "%s\n", buf);
      continue;
    }

    for (iVar = 0; iVar < nVarTab; iVar++)
    {
      if (strarg(svar, buf, FldIdx[iVar]) == 0)
        break;

          n = sscanf(svar, "%lf%c", &var, &xtrac);
      if (n != 1)
        break;

      if (F_xtal && (var < -2. || var > 2.))
        break;

      make_var(VarTab[iVar], var);
    }

    if (iVar < nVarTab)
    {
      fprintf(stdout, "%s\n", buf);
      continue;
    }

    for (iLF = 0; iLF < nListFormu; iLF++)
    {
      Results[iLF] = fval_at(ListFormu[iLF]);

      if (F_xtal)
        Results[iLF] = xtal(Results[iLF]);
    }

    ExtraBlk = 0;
    LastFmt  = NULL;

    iLF = 0;

    for (oFmt = Root_oFmts; oFmt; oFmt = oFmt->Next)
    {
      if (oFmt->Fmt)
      {
        if (iLF < nListFormu)
        {
          fprintf(stdout, oFmt->Fmt, Results[iLF++]);
          LastFmt = oFmt->Fmt;
          ExtraBlk = 1;
        }
      }
      else
        if (EchoRange(buf, oFmt->iFld, oFmt->jFld, ExtraBlk))
          ExtraBlk = 1;
    }

    if (LastFmt)
      ExtraBlk = -1;
    else
      LastFmt  = "%.6g";

    for (; iLF < nListFormu; iLF++)
    {
      if (ExtraBlk == 1) putc(' ', stdout);
      fprintf(stdout, LastFmt, Results[iLF]);
      if (ExtraBlk == 0) ExtraBlk = 1;
    }

    putc('\n', stdout);
  }

  exit(0);
  return 0;
}


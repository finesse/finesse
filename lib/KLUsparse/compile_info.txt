
How to compile:

Set Macintosh flags in  UFconfig
Current:

CC = cc
CFLAGS = -Wall -W -pipe -O3 -arch ppc -arch i686  -isysroot /Developer/SDKs/MacOSX10.4u.sdk -fno-common -no-cpp-precomp -fexceptions
LIB = -lstdc++
BLAS = -framework Accelerate
LAPACK = -framework Accelerate
AR = libtool -o

in AMD     make lib
in BTF     make
in COLAMD  make lib
in KLU:    make


FIX FIRST:

On OS X :

- bug in klu_version.h for ppc architecure. Path:

43 	+diff -Nru ../SuiteSparse/KLU/Include/klu_version.h ./KLU/Include/klu_version.h
44 	+--- ../SuiteSparse/KLU/Include/klu_version.h   2006-05-23 22:32:28.000000000 +0300
45 	++++ ./KLU/Include/klu_version.h        2006-11-20 09:18:27.000000000 +0200
46 	+@@ -203,6 +203,8 @@
47 	+
48 	+ */
49 	+
50 	++#include <math.h>
51 	++#if !defined(__complex_t) /* Mac OS X PowerPC defines this */
52 	+ typedef struct
53 	+ {
54 	+     double component [2] ;    /* real and imaginary parts */
55 	+@@ -213,6 +215,10 @@
56 	+ #define Entry DoubleComplex
57 	+ #define Real component [0]
58 	+ #define Imag component [1]
59 	++#else
60 	++typedef struct __complex_s Unit ;
61 	++#define Entry __complex_t
62 	++#endif
63 	+

- for 'fat' libraries (i.e. those with both ppc and i386)
  one has to use libtool rather than ar. Replace AR in UFconfig

- in KLU in Makefile, target library: remove demo
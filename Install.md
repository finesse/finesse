# Installing FINESSE

This is a short guide for installing and running FINESSE.  Please see
http://www.gwoptics.org/finesse/ for more information on FINESSE.   
Last updated 1st April 2020.

Table of contents:

1. Introduction
2. FINESSE and PYKAT
3. Try it without any installation
4. Installation using Anaconda (recommended)
5. Running FINESSE from a Jupyter notebook 
6. Running FINESSE from the command line 
7. Installing pre-compiled binaries
8. Getting the source

## 1. Introduction

FINESSE is an interferometer simulation program written in C. It calculates
light amplitudes in a user-specified interferometer configuration. It can 
generate output signals for various photo detector types. All calculations are
done in the frequency domain. Gaussian beams can be used to model spatial
effects.

FINESSE can be installed in different ways. We strongly recommend using the
Anaconda installer, but other methods are listed below. 

You can contact us at: `finesse-support@nikhef.nl`.

## 2. FINESSE and PYKAT

FINESSE is written in C and is compiled into a single binary executable called
`kat`. Direct interaction with FINESSE can be performed via a command-line 
interface from a system terminal.

PYKAT is a Python wrapper for FINESSE. It enables a user to easily perform
complex sets of modelling tasks efficiently, enables FINESSE to be used in
conjunction with the wide variety of Python packages, and makes it easy to use
FINESSE from Jupyter notebooks. This has become the preferred method of 
using FINESSE now for many users. We support PYKAT for Python 3.x.

## 3. Try it online before installing

You can try FINESSE and PYKAT in an online Jupyter notebook before you install 
it on your own computer. The link http://www.gwoptics.org/finesse/binder
will forward you to an interactive notebook, hosted on https://mybinder.org/,
in which we have pre-installed FINESSE and PYKAT. The initial loading of that
page will be slow, but then you can play with FINESSE in a full Jupyter
environment (note that you cannot save your work).

[![Binder](https://mybinder.org/badge_logo.svg)](http://www.gwoptics.org/finesse/binder)


## 4. Installing and using Anaconda (recommended)

Using the `conda` package manager from Anaconda is the recommended and 
supported way of installing FINESSE. You need to download and install 
Anaconda for Python 3.x from https://www.anaconda.com/.


We recommend creating a dedicated environment, for example with: 

```
conda create --name finesse 
conda activate finesse
``` 

(see https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)

To install FINESSE and PYKAT together (recommended) use:  
```conda install -c gwoptics pykat```  

If instead you *only* want to install FINESSE, you can use:  
```conda install -c gwoptics finesse```

We recommend Jupyter notebooks for using FINESSE, you can install Jupyter via:  
```conda install -c anaconda jupyter```


We have created a more detailed installation guide for teaching 
FINESSE in workshops and summer schools:  
http://www.gwoptics.org/learn/schools/SoftwareInstall.pdf

Remember that installing FINESSE via Conda will not make FINESSE accessible
system wide. It will only be accessible from the Conda environment you install
it in. This typically isn't an issue, as you can set your Conda environment to
activate when you start your terminal (on OSX and Linux). On Windows, you can
run everything from the Anaconda Prompt as this correctly sets up your
environment.

Note that FINESSE is developed primarily for Linux and macOS, and while it does 
work on Windows, some features are not available in that system. In case this
is a problem, you might consider using the Windows Subsystem for Linux (WSL)
and then install FINESSE for Linux. For more information on WSL, see
https://docs.microsoft.com/en-us/windows/wsl/about


## 5. Running FINESSE from a Jupyter notebook 

For this you need to have PYKAT, FINESSE and Jupyter installed, and you must open the
Jupyter notebook in the environment that PYKAT and FINESSE have been installed
in.

Below is a simple example program to show how FINESSE can be used in a notebook. 
For further examples and for guidance on getting started with FINESSE, we 
encourage you to explore  
http://www.gwoptics.org/learn/01_Introduction/01_Getting_Started/.  


```
from pykat import finesse
from pykat.commands import * 

kat = finesse.kat()
code = """
l laser 1 0 n1
s space 1 n1 n2
pd pout n2
xaxis laser P lin 1 10 100
"""
kat.parse(code)
out = kat.run()
out.plot()
```

## 6. Running FINESSE from the command line

Without PYKAT, you can interact with FINESSE from a console window. If you 
call the program by typing `kat` in the console without any options or
filenames, a short message on the usage will be displayed. Using the option
`-h` a help screen with a syntax reference is printed (i.e. `./kat -h` or 
`kat.exe -h`). Page 2 of the syntax reference is shown using `-hh`. 

## 7. Installing pre-compiled binaries 

You can also download the pre-compiled binaries of FINESSE from
http://www.gwoptics.org/finesse/#download.

After downloading the appropriate package for your operating system,  you can
install FINESSE simply by unpacking the zip (or tar.gz) file. This will create
a directory called `Finesse` containing all the necessary files for FINESSE
(this does not provide PYKAT or Jupyter).

To run FINESSE you need to change into the working directory which contains all 
required files (the FINESSE binary 'kat', the init file 'kat.ini' and an 
interferometer input file). 


If you want to automatically plot the results you need to have Gnuplot or
Python installed. If you do not use Anaconda you might need to tell FINESSE 
where to find the Gnuplot or Python executable.

### Unix/OS X:
You can easily find the Gnuplot executable with the command:  
```which gnuplot```  
This should show the full path, e.g. '/usr/bin/gnuplot'. Next open 
'kat.ini' with a text editor and change the line beginning with 
`GNUCOMMAND' to (in this example):  

```GNUCOMMAND "/usr/local/bin/gnuplot -persist"```

### Windows:
First, you must move the FINESSE folder to a location on your computer where 
you want to store it. Be careful if putting it in a system-wide directory such
as 'Program Files', as you will require administrator rights to write to such a
location. Once it has been moved to the desired location, you should double
click the 'install.bat' file. Running this will bring up a command window to
update your PATH variables. After this has been run you must *not* move this
Finesse folder, as the system PATH variable has been set for this directory
only. If you do move the folder then please re-run `install.bat`.

Running `install.bat` will also attempt to find a Gnuplot installation on your
computer and set up FINESSE to work with it. If it does not find one you will
either have to install it first, and re-run `install.bat`, or manually se tup the
`kat.ini` file to use your gnuplot installation. Please install Gnuplot from:
http://sourceforge.net/projects/gnuplot/files/latest/download  

If you have a space in the path to `gnuplot.exe`, you need to use `'` *and* `"` to
encapsulate the path and GNUCOMMAND:  

```
    GNUCOMMAND '"C:\Program Files\gnuplot\bin\Wgnuplot.exe" --persist $s'
```

else use:  

```
    GNUCOMMAND 'C:\gnuplot\bin\Wgnuplot.exe --persist $s'
```

Starting from FINESSE version 2.1, the Windows version should show Gnuplot
plots, and allow those plots to be kept open while still using the terminal.
FINESSE will now internally use a combination of both `wgnuplot.exe` and
`gnuplot.exe`, which it expects to be in the same path as stated in the
GNUCOMMAND; this is irrespective of which binary is actually stated in the
GNUCOMMAND variable.


## 8. Getting the source

If you want access to the latest development version of FINESSE you will have
to set up up the development environment. This is significantly easier on Linux
and macOS. On Windows we recommend the Windows Subsystem for Linux (WSL).

The FINESSE source is available at:  
https://git.ligo.org/finesse/finesse

After installing all required dependencies, FINESSE can be compiled using the build script `finesse.sh`.

The PYKAT source code can be obtained from:
https://git.ligo.org/finesse/pykat

#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_mem.h"
#include "kat_fortran.h"
#include "kat_optics.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_check.h"
#include "kat_aa.h"
#include "kat_knm_int.h"
#include "kat_knm_mirror.h"
#include "kat_knm_aperture.h"
#if INCLUDE_CUBA == 1
#include "cuba.h"
#endif
#include <gsl/gsl_cblas.h>

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern local_var_t vlocal;
extern FILE *fp_log;

mirror_knm_t mrtmap;

extern FILE * ipfile, * idfile;

extern const complex_t complex_i; //!< sqrt(-1) or 0 + i
extern const complex_t complex_1; //!< 1 but in complex space: 1 + 0i
extern const complex_t complex_m1; //!< -1 but in complex space: -1 + 0i
extern const complex_t complex_0; //!< 0 but in complex space: 0 + 0i

static u_nm_accel_t *acc_11_nr1_1, *acc_11_nr1_2;
static u_nm_accel_t *acc_22_nr2_1, *acc_22_nr2_2;
static u_nm_accel_t *acc_21_nr2_1, *acc_21_nr1_2;
static u_nm_accel_t *acc_12_nr1_1, *acc_12_nr2_2;

void alloc_knm_accel_mirror_mem(long *bytes) {

    // need to pick out the maximum number order for allocating memory
    int max_m=mem.hg_mode_order, max_n=mem.hg_mode_order;

    // make sure they are null before allocating new memory
    assert(acc_11_nr1_1 == NULL);
    assert(acc_11_nr1_2 == NULL);
    assert(acc_12_nr1_1 == NULL);
    assert(acc_12_nr2_2 == NULL);
    assert(acc_21_nr2_1 == NULL);
    assert(acc_21_nr1_2 == NULL);
    assert(acc_22_nr2_1 == NULL);
    assert(acc_22_nr2_2 == NULL);

    // allocate memory for all the accelerators
    acc_11_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_11_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_22_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_22_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_12_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_12_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_21_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_21_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
}

void mirror_knm_free(mirror_knm_t* knm) {
    assert(knm != NULL);

    if (!IS_MIRROR_KNM_ALLOCD(knm))
        bug_error("We have tried to free memory that has not been allocated");

    int num_fields = mem.num_fields;
    int field_index;

    for (field_index = 0; field_index < num_fields; field_index++) {
        free(knm->k11[field_index]);
        free(knm->k12[field_index]);
        free(knm->k21[field_index]);
        free(knm->k22[field_index]);
        // remembering to NULL any loose pointers...
        knm->k11[field_index] = NULL;
        knm->k21[field_index] = NULL;
        knm->k12[field_index] = NULL;
        knm->k22[field_index] = NULL;
    }

    free(knm->k11);
    free(knm->k12);
    free(knm->k21);
    free(knm->k22);

    knm->k11 = NULL;
    knm->k21 = NULL;
    knm->k12 = NULL;
    knm->k22 = NULL;
}

void mirror_knm_alloc(mirror_knm_t* knm, long *bytes) {
    if (knm == NULL)
        bug_error("mirror_knm_t pointer is nulled");

    if (IS_MIRROR_KNM_ALLOCD(knm))
        bug_error("We have tried to allocate memory to a mirror_knm when it is already allocated, that or we have not initialised the pointers to be NULL");

    allocate_zmatrix(&knm->k11, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k12, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k21, mem.num_fields, bytes);
    allocate_zmatrix(&knm->k22, mem.num_fields, bytes);
}

// multiply A*B then put result in new matrix

void mirror_knm_matrix_mult(mirror_knm_t* A, mirror_knm_t* B, mirror_knm_t *result) {
    assert(result != NULL && A != NULL && B != NULL);
    assert(result->k11 != NULL && A->k11 != NULL && B->k11 != NULL);
    assert(result->k12 != NULL && A->k12 != NULL && B->k12 != NULL);
    assert(result->k21 != NULL && A->k21 != NULL && B->k21 != NULL);
    assert(result->k22 != NULL && A->k22 != NULL && B->k22 != NULL);

    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int l, n, m;

    mirror_knm_t *tmp;

    // if A or B are just identity matrices then just copy them into result
    if (A->IsIdentities) {
        if (B != result)
            mirror_knm_matrix_copy(B, result);

    } else if (B->IsIdentities) {
        if (A != result)
            mirror_knm_matrix_copy(A, result);

    } else {
        // if result != A | B then no need to use a temporary matrix to store result 
        // just bung it in the result matrix
        if ((result == A) || (result == B))
            tmp = &mrtmap;
        else
            tmp = result;

        for (n = 0; n < num_fields; n++) {
            for (m = 0; m < num_fields; m++) {
                // initialise to 0 here just incase some other data is there
                tmp->k11[n][m] = complex_0;
                tmp->k12[n][m] = complex_0;
                tmp->k21[n][m] = complex_0;
                tmp->k22[n][m] = complex_0;

                for (l = 0; l < num_fields; l++) {
                    tmp->k11[n][m] = z_pl_z(tmp->k11[n][m], z_by_z(A->k11[n][l], B->k11[l][m]));
                    tmp->k12[n][m] = z_pl_z(tmp->k12[n][m], z_by_z(A->k12[n][l], B->k12[l][m]));
                    tmp->k21[n][m] = z_pl_z(tmp->k21[n][m], z_by_z(A->k21[n][l], B->k21[l][m]));
                    tmp->k22[n][m] = z_pl_z(tmp->k22[n][m], z_by_z(A->k22[n][l], B->k22[l][m]));
                }
            }
        }

        if ((result == A) || (result == B))
            mirror_knm_matrix_copy(&mrtmap, result);
    }
}

void mirror_knm_matrix_mult_scale(double scale, mirror_knm_t* A, mirror_knm_t* B, mirror_knm_t *result) {
    /**
     * This function scales A and multiplies it with B. The scaling is unusual here
     * as we interpolate between A and I, so that with zero scaling we just get B.
     * 
     * This was implemented mainly to scale maps for lock dragging. 
     * 
     * Computes (A*scale + I*(scale-1)) * B and puts it into result.
     */
    assert(result != NULL && A != NULL && B != NULL);
    assert(result->k11 != NULL && A->k11 != NULL && B->k11 != NULL);
    assert(result->k12 != NULL && A->k12 != NULL && B->k12 != NULL);
    assert(result->k21 != NULL && A->k21 != NULL && B->k21 != NULL);
    assert(result->k22 != NULL && A->k22 != NULL && B->k22 != NULL);

    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int l, n, m;

    mirror_knm_t *tmp;

    // if A or B are just identity matrices then just copy them into result
    if (A->IsIdentities) {
        if (B != result)
            mirror_knm_matrix_copy(B, result);

    } else if (B->IsIdentities) {
        if (A != result)
            mirror_knm_matrix_copy(A, result);

    } else {
        // if result != A | B then no need to use a temporary matrix to store result 
        // just bung it in the result matrix
        if ((result == A) || (result == B))
            tmp = &mrtmap;
        else
            tmp = result;

        for (n = 0; n < num_fields; n++) {
            for (m = 0; m < num_fields; m++) {
                // initialise to 0 here just incase some other data is there
                tmp->k11[n][m] = complex_0;
                tmp->k12[n][m] = complex_0;
                tmp->k21[n][m] = complex_0;
                tmp->k22[n][m] = complex_0;

                if(n != m) {
                    for (l = 0; l < num_fields; l++) {
                        tmp->k11[n][m] = z_pl_z(tmp->k11[n][m], z_by_z(z_by_x(A->k11[n][l], scale), B->k11[l][m]));
                        tmp->k12[n][m] = z_pl_z(tmp->k12[n][m], z_by_z(z_by_x(A->k12[n][l], scale), B->k12[l][m]));
                        tmp->k21[n][m] = z_pl_z(tmp->k21[n][m], z_by_z(z_by_x(A->k21[n][l], scale), B->k21[l][m]));
                        tmp->k22[n][m] = z_pl_z(tmp->k22[n][m], z_by_z(z_by_x(A->k22[n][l], scale), B->k22[l][m]));
                    }
                } else {
                    for (l = 0; l < num_fields; l++) {
                        tmp->k11[n][m] = z_pl_z(tmp->k11[n][m], z_by_z(z_pl_z(co(1-scale, 0), z_by_x(A->k11[n][l], scale)), B->k11[l][m]));
                        tmp->k12[n][m] = z_pl_z(tmp->k12[n][m], z_by_z(z_pl_z(co(1-scale, 0), z_by_x(A->k12[n][l], scale)), B->k12[l][m]));
                        tmp->k21[n][m] = z_pl_z(tmp->k21[n][m], z_by_z(z_pl_z(co(1-scale, 0), z_by_x(A->k21[n][l], scale)), B->k21[l][m]));
                        tmp->k22[n][m] = z_pl_z(tmp->k22[n][m], z_by_z(z_pl_z(co(1-scale, 0), z_by_x(A->k22[n][l], scale)), B->k22[l][m]));
                    }
                }
            }
        }

        if ((result == A) || (result == B))
            mirror_knm_matrix_copy(&mrtmap, result);
    }
}

// Should copy one matrix into another

void mirror_knm_matrix_copy(mirror_knm_t* src, mirror_knm_t* dest) {
    assert(src != NULL);
    assert(dest != NULL);

    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    int i;
    
    assert(dest->k11 != NULL);
    assert(dest->k12 != NULL);
    assert(dest->k21 != NULL);
    assert(dest->k22 != NULL);
    assert(src->k11 != NULL);
    assert(src->k12 != NULL);
    assert(src->k21 != NULL);
    assert(src->k22 != NULL);
    
    for (i = 0; i < num_fields; i++) {
        memcpy(dest->k11[i], src->k11[i], num_fields * sizeof (complex_t));
        memcpy(dest->k12[i], src->k12[i], num_fields * sizeof (complex_t));
        memcpy(dest->k21[i], src->k21[i], num_fields * sizeof (complex_t));
        memcpy(dest->k22[i], src->k22[i], num_fields * sizeof (complex_t));
    }
}

// sets matrix to identity matrix

void mirror_knm_matrix_ident(mirror_knm_t* M) {
    assert(M != NULL);
    
    int i;
    
    // zero the matrices, this can be done in a memset call because
    // knm matrices are stored in a 1D array
    memset(&(M->k11[0][0]), 0, inter.num_fields * inter.num_fields * sizeof(complex_t));
    memset(&(M->k12[0][0]), 0, inter.num_fields * inter.num_fields * sizeof(complex_t));
    memset(&(M->k21[0][0]), 0, inter.num_fields * inter.num_fields * sizeof(complex_t));
    memset(&(M->k22[0][0]), 0, inter.num_fields * inter.num_fields * sizeof(complex_t));
    
    for (i = 0; i < inter.num_fields; i++) {
        M->k11[i][i] = complex_1;
        M->k12[i][i] = complex_1;
        M->k21[i][i] = complex_1;
        M->k22[i][i] = complex_1;
    }
}

bool knm_q_cmp(mirror_knm_q_t *q1, mirror_knm_q_t *q2) {
   if(!ceq(q1->qxt1_11, q2->qxt1_11)) return false;
   if(!ceq(q1->qxt1_12, q2->qxt1_12)) return false;
   if(!ceq(q1->qxt1_21, q2->qxt1_21)) return false;
   if(!ceq(q1->qxt1_22, q2->qxt1_22)) return false;
   if(!ceq(q1->qxt2_11, q2->qxt2_11)) return false;
   if(!ceq(q1->qxt2_12, q2->qxt2_12)) return false;
   if(!ceq(q1->qxt2_21, q2->qxt2_21)) return false;
   if(!ceq(q1->qxt2_22, q2->qxt2_22)) return false;
   if(!ceq(q1->qyt1_11, q2->qyt1_11)) return false;
   if(!ceq(q1->qyt1_12, q2->qyt1_12)) return false;
   if(!ceq(q1->qyt1_21, q2->qyt1_21)) return false;
   if(!ceq(q1->qyt1_22, q2->qyt1_22)) return false;
   if(!ceq(q1->qyt2_11, q2->qyt2_11)) return false;
   if(!ceq(q1->qyt2_12, q2->qyt2_12)) return false;
   if(!ceq(q1->qyt2_21, q2->qyt2_21)) return false;
   if(!ceq(q1->qyt2_22, q2->qyt2_22)) return false;
   
   return true;
}

/** q values passed to this should have been turned depending on the direction of
 *  incoming and outgoing beam
 */ 
void calculate_mirror_qt_qt2(KNM_MIRROR_NODE_DIRECTION_t KNM, mirror_knm_q_t *knm_q, complex_t qx1,
        complex_t qy1, complex_t qx2, complex_t qy2, double nr1, double nr2, int n1idx, int n2idx, int cidx) {

    ABCD_t trans1;

    switch (KNM) {
        case MR11:
            component_matrix(&trans1, cidx, n1idx, n1idx, TANGENTIAL);
            knm_q->qxt1_11 = q1_q2(trans1, qx1, nr1, nr1);
            component_matrix(&trans1, cidx, n1idx, n1idx, SAGITTAL);
            knm_q->qyt1_11 = q1_q2(trans1, qy1, nr1, nr1);
            knm_q->qxt2_11 = cminus(cconj(qx1));
            knm_q->qyt2_11 = cminus(cconj(qy1));
            break;
        case MR22:
            component_matrix(&trans1, cidx, n2idx, n2idx, TANGENTIAL);
            knm_q->qxt1_22 = q1_q2(trans1, cminus(cconj(qx2)), nr2, nr2);
            component_matrix(&trans1, cidx, n2idx, n2idx, SAGITTAL);
            knm_q->qyt1_22 = q1_q2(trans1, cminus(cconj(qy2)), nr2, nr2);
            knm_q->qxt2_22 = qx2;
            knm_q->qyt2_22 = qy2;
            break;
        case MR12:
            component_matrix(&trans1, cidx, n1idx, n2idx, TANGENTIAL);
            knm_q->qxt1_12 = q1_q2(trans1, qx1, nr1, nr2);
            component_matrix(&trans1, cidx, n1idx, n2idx, SAGITTAL);
            knm_q->qyt1_12 = q1_q2(trans1, qy1, nr1, nr2);
            knm_q->qxt2_12 = qx2;
            knm_q->qyt2_12 = qy2;
            break;
        case MR21:
            component_matrix(&trans1, cidx, n2idx, n1idx, TANGENTIAL);
            knm_q->qxt1_21 = q1_q2(trans1, cminus(cconj(qx2)), nr2, nr1);
            component_matrix(&trans1, cidx, n2idx, n1idx, SAGITTAL);
            knm_q->qyt1_21 = q1_q2(trans1, cminus(cconj(qy2)), nr2, nr1);
            knm_q->qxt2_21 = cminus(cconj(qx1));
            knm_q->qyt2_21 = cminus(cconj(qy1));
            break;
        default:
            bug_error("calculate_qt_qt2 cannot handle a KNM value of %i\n", KNM);
    }
}

void calc_mirror_limit_ws(double *wx1,double *wx2,double *wy1,double *wy2, KNM_MIRROR_NODE_DIRECTION_t knum, mr_knm_map_int_params_t *p){
    mirror_knm_q_t *kq = p->knm_q;
    
    switch(knum){
        case MR11:
            *wx1 = w_size(kq->qxt1_11, p->nr1);
            *wx2 = w_size(kq->qxt2_11, p->nr1);            
            *wy1 = w_size(kq->qyt1_11, p->nr1);
            *wy2 = w_size(kq->qyt2_11, p->nr1);
            break;
        case MR12:
            *wx1 = w_size(kq->qxt1_12, p->nr1);
            *wx2 = w_size(kq->qxt2_12, p->nr2);            
            *wy1 = w_size(kq->qyt1_12, p->nr1);
            *wy2 = w_size(kq->qyt2_12, p->nr2);
            break;
        case MR21:
            *wx1 = w_size(kq->qxt1_21, p->nr2);
            *wx2 = w_size(kq->qxt2_21, p->nr1);            
            *wy1 = w_size(kq->qyt1_21, p->nr2);
            *wy2 = w_size(kq->qyt2_21, p->nr1);
            break;
        case MR22:
            *wx1 = w_size(kq->qxt1_22, p->nr2);
            *wx2 = w_size(kq->qxt2_22, p->nr2);            
            *wy1 = w_size(kq->qyt1_22, p->nr2);
            *wy2 = w_size(kq->qyt2_22, p->nr2);
            break;
    }
}

/**
 * Returns true of false depending on if the aperture knm should be integrated or not
 * 
 * @param mirror mirror component
 * @param mismatch whether mismatch present
 * @param astigmatism whether beam is astigmatic
 * @return True or false depending on if the aperture should be integrated or analytically calculated
 */
bool should_integrate_mirror_aperture_knm(mirror_t *mirror, int mismatch, int astigmatism) {
    
    //check if an aperture has even been defined
    if (mirror->r_aperture > 0) {
        // check if we should integrate the aperture coupling coefficients rather
        // than computing them analytically
        if (mirror->knm_flags & INT_APERTURE || astigmatism || mismatch) {

            // if using any of the Riemann methods, these currently do not support 
            // aperture integration without using a map
            if ((init.mapintmethod == RIEMANN_SUM_NEW) || (init.mapintmethod == NEWTON_COTES)) {
                
                if(mirror->map_merged.usermessages & APERTURE_RIEMANN ) {
                    warn("Aperture calculation using Riemann integrators is not currently supported.\n"
                         "   Please use an aperture map or a Cuba integrating routine.\n");
                    mirror->map_merged.usermessages |= APERTURE_RIEMANN;
                }              
                                
                // return false as with these methods no aperture coupling coefficient
                // can be integrated.
                return false;
            } else {
                return true;
            }

        } else {
            // otherwise we need to calculate the aperture coupling coefficient matrix here analytically
            if(!(mirror->map_merged.usermessages & APERTURE_NO_ANALYTIC )&& mirror->aperture_type == CIRCULAR){
                warn("Analytic circular aperture coupling coefficient calculation not completely implemented yet, becareful!\n");
                mirror->map_merged.usermessages |= APERTURE_NO_ANALYTIC;
            }

            if (inter.debug && !options.quiet)
               message("* Calculating aperture Knm analytically\n");
            
            // return false to state that no aperture integration is needed
            // as we have done it analytically
            return false;
        }
    }

    return false;
}

void check_mirror_knm_mismatch_astig(bitflag knm_calc_flags, mirror_knm_q_t *knm_q, bitflag *mismatch, bitflag *astigmatism){
    // calculate the individual q values for each KNM and if we should even
    // bother calculating them    
    if ((knm_calc_flags & MR11Calc) == MR11Calc){
        if (!ceq(knm_q->qxt1_11, knm_q->qxt2_11)
                || !ceq(knm_q->qyt1_11, knm_q->qyt2_11)) {
            *mismatch = *mismatch | 1;
        }

        if (!ceq(knm_q->qxt1_11, knm_q->qyt1_11)
                || !ceq(knm_q->qxt2_11, knm_q->qyt2_11))
            *astigmatism = *astigmatism | 1;
    }
    
    if ((knm_calc_flags & MR12Calc) == MR12Calc){
        if (!ceq(knm_q->qxt1_12, knm_q->qxt2_12)
                || !ceq(knm_q->qyt1_12, knm_q->qyt2_12))
            *mismatch = *mismatch | 4;

        if (!ceq(knm_q->qxt1_12, knm_q->qyt1_12)
                || !ceq(knm_q->qxt2_12, knm_q->qyt2_12))
            *astigmatism = *astigmatism | 4;
    }

    if ((knm_calc_flags & MR21Calc) == MR21Calc){
        if (!ceq(knm_q->qxt1_21, knm_q->qxt2_21)
                || !ceq(knm_q->qyt1_21, knm_q->qyt2_21))
            *mismatch = *mismatch | 8;

        if (!ceq(knm_q->qxt1_21, knm_q->qyt1_21)
                || !ceq(knm_q->qxt2_21, knm_q->qyt2_21))
            *astigmatism = *astigmatism | 8;
    }
    
    if ((knm_calc_flags & MR22Calc) == MR22Calc){
        if (!ceq(knm_q->qxt1_22, knm_q->qxt2_22)
                || !ceq(knm_q->qyt1_22, knm_q->qyt2_22))
            *mismatch = *mismatch | 2;

        if (!ceq(knm_q->qxt1_22, knm_q->qyt1_22)
                || !ceq(knm_q->qxt2_22, knm_q->qyt2_22))
            *astigmatism = *astigmatism | 2;
    }
}

// Scale the integration limits slightly depending on the beam mode
// as the beam tends to get larger at higher modes. If a mirror aperture
// has been specified then polar coordinates are used for the integration
// so r and theta limits are set.
//
// If the Riemann integration method is being used then cartesian coords
// are used by default. Polar limits are only set when using the new integration
// methods Cuba parallel and serial.
//
// if the beam is much smaller than the aperture, cartesian coordinates are used.
void get_mirror_int_limit(mr_knm_map_int_params_t *p, int knum) {
    if (p->aperture_radius < 0)
        bug_error("get_int_limit(*p): Mirror aperture cannot be negative\n");

    double wx1=0,wx2=0,wy1=0,wy2=0;
    double _wx1,_wx2,_wy1,_wy2;
    
    switch(knum){
        case 0:
            // used by cuba parallel, find the largest w values out of all the
            // different 
            if(CALC_MR_KNM(p,11)){
                calc_mirror_limit_ws(&_wx1,&_wx2,&_wy1,&_wy2,MR11,p);
                wx1 = max(wx1,_wx1);
                wx2 = max(wx2,_wx2);
                wy1 = max(wy1,_wy1);
                wy2 = max(wy2,_wy2);                
            }
            if(CALC_MR_KNM(p,12)){
                calc_mirror_limit_ws(&_wx1,&_wx2,&_wy1,&_wy2,MR12,p);
                wx1 = max(wx1,_wx1);
                wx2 = max(wx2,_wx2);
                wy1 = max(wy1,_wy1);
                wy2 = max(wy2,_wy2);                
            }
            if(CALC_MR_KNM(p,21)){
                calc_mirror_limit_ws(&_wx1,&_wx2,&_wy1,&_wy2,MR21,p);
                wx1 = max(wx1,_wx1);
                wx2 = max(wx2,_wx2);
                wy1 = max(wy1,_wy1);
                wy2 = max(wy2,_wy2);                
            }
            if(CALC_MR_KNM(p,22)){
                calc_mirror_limit_ws(&_wx1,&_wx2,&_wy1,&_wy2,MR22,p);
                wx1 = max(wx1,_wx1);
                wx2 = max(wx2,_wx2);
                wy1 = max(wy1,_wy1);
                wy2 = max(wy2,_wy2);                
            }
            break;
        case MR11:
            calc_mirror_limit_ws(&wx1,&wx2,&wy1,&wy2,MR11,p);
            break;
        case MR12:
            calc_mirror_limit_ws(&wx1,&wx2,&wy1,&wy2,MR12,p);
            break;
        case MR21:
            calc_mirror_limit_ws(&wx1,&wx2,&wy1,&wy2,MR21,p);
            break;
        case MR22:
            calc_mirror_limit_ws(&wx1,&wx2,&wy1,&wy2,MR22,p);
            break;
        default:
            bug_error("Cannot handle a knum input of %i\n",knum);
    }
    
    p->xmax[0] = 5 * max(sqrt(p->n1 + 0.5) * wx1, sqrt(p->n2 + 0.5) * wx2);
    p->xmin[0] = -p->xmax[0];
    p->xmax[1] = 5 * max(sqrt(p->m1 + 0.5) * wy1, sqrt(p->m2 + 0.5) * wy2);
    p->xmin[1] = -p->xmax[1];

    double r = p->aperture_radius;
    
    // if using the Riemann just stick to the Cartesian coordinates
    // if r == 0 then the mirror is infinitely big
    // if r > max(...) then the beam size is smaller than the aperture so Cartesian will do
    // also check if we should be integrating to calculate the aperture with inter.knm, if we're not
    // then there is no need to use polar coordinates as polar coordinates are only used for aperture
    // calculations.
    if (p->mirror->aperture_type == CIRCULAR) {
        if ((p->merged_map->integration_method == NEWTON_COTES 
                || p->merged_map->integration_method == RIEMANN_SUM_NEW)
                || !(p->mirror->knm_flags & INT_APERTURE)
                || ((r == 0) || (r > min(p->xmax[0], p->xmax[1])))) {
            p->polar_limits_used = false;
        } else {
            //[0] is r, [1] is theta
            p->xmin[0] = 0;
            p->xmin[1] = 0;
            p->xmax[0] = r;
            p->xmax[1] = TWOPI;
            p->polar_limits_used = true;
        }
    } else {
        p->polar_limits_used = false;
        p->xmin[0] = max(-r, p->xmin[0]);
        p->xmin[1] = max(-r, p->xmin[1]);
        p->xmax[0] = min(r, p->xmax[0]);
        p->xmax[1] = min(r, p->xmax[1]);
    }
    
    if (inter.debug & 64){
        if (p->polar_limits_used) {
            message("* Map %s limits, r: %g %g phi: %g %g\n", p->merged_map->name,p->xmin[0],p->xmax[0],p->xmin[1],p->xmax[1]);
        } else{
            message("* Map %s limits, x: %g %g y: %g %g\n", p->merged_map->name,p->xmin[0],p->xmax[0],p->xmin[1],p->xmax[1]);
        }
    }
    
    unsigned int *usermessages = &(p->merged_map->usermessages);
    
    if(inter.debug && !options.quiet && !(*usermessages & USING_POLAR)){
        if(p->polar_limits_used)
           message("* Polar coordinates used to integrate map %s\n",p->merged_map->name);
        else
           message("* Cartesian coordinates used to integrate map %s\n",p->merged_map->name);
            
        *usermessages = *usermessages | USING_POLAR;
    }
    
    if(p->usingMap && !(*usermessages & MAP_TOO_SMALL)){        
        surface_merged_map_t *map = p->merged_map;
        // here we check to see if the map size is smaller than the limits. if so
        // you could probably do with a bigger map
        if(p->polar_limits_used){
            bool a = (map->cols-map->x0) * map->xstep < p->xmax[0]*(1-1e-15);
            bool b = (map->rows-map->y0) * map->ystep < p->xmax[0]*(1-1e-15);
            bool c = (map->x0-map->cols) * map->xstep > -p->xmax[0];
            bool d = (map->y0-map->rows) * map->ystep > -p->xmax[0];
            if( (a || b || c || d) && inter.debug && !options.quiet){
                warn("The map %s is smaller than the dimensions of the integration limits\n"
                     "   You should use a bigger map or a smaller beam for more accurate results,\n"
                     "   and make sure that the map is correctly centred using the x0 and y0\n"
                     "   (aperture diameter:%e, map width:%e, map height: %e)\n"
                        , p->merged_map->filename
                        , 2.0*p->xmax[0] 
                        , (map->cols-1) * map->xstep
                        , (map->rows-1) * map->ystep);
                
                *usermessages = *usermessages | MAP_TOO_SMALL;
            }
        }
        else {
            if( (((map->cols-map->x0) * map->xstep < p->xmax[0]) 
             || ((map->x0-map->cols) * map->xstep < p->xmin[0]) 
             || ((map->rows-map->y0) * map->ystep > p->xmax[1]) 
             || ((map->y0-map->rows) * map->ystep > p->xmin[1])
                ) && inter.debug && !options.quiet){
                warn("The map %s is smaller than the dimensions of the integration limits\n"
                     "   You should use a bigger map or a smaller beam for more accurate results,\n"
                     "   and make sure that the map is correctly centred using the x0 and y0\n"
                     "   (x range=%e, y range=%e, map width=%e map height=%e)\n"
                        , p->merged_map->filename
                        , p->xmax[0] - p->xmin[0]
                        , p->xmax[1] - p->xmin[1]
                        , (map->cols-1) * map->xstep
                        , (map->rows-1) * map->ystep);
            
                
                *usermessages = *usermessages | MAP_TOO_SMALL;
            }
        }
    }
    
    if(p->usingMap && !(*usermessages & MAP_RES_TOO_LOW )
            && p->merged_map->integration_method == RIEMANN_SUM_NEW){
        if((min(wx1,wx2)/p->merged_map->xstep < 5.0) || (min(wy1,wy2)/p->merged_map->ystep < 5.0) ){
            warn("The map %s might not have a high enough resolution. Currently\n"
              "   the beam size samples less than 10 points on the map. This can\n"
              "   cause large errors when using higher orders.\n", p->merged_map->name);
            *usermessages |= MAP_RES_TOO_LOW;
        }
    }
    
}

typedef struct knm_surf_cuba_params {
    surface_map_t *map;
    int n1, n2, m1, m2;
    double xmin, xrange;
    double ymin, yrange;
    double xfac, yfac;
    double constant;
} knm_surf_cuba_params_t;

/** A bilinear interpolation of a maps data, x and y are real values of distance
 * from the maps centre.
 */
double eval_surface_map(surface_map_t *map, double x, double y){
    // get index space position of (x,y) coord
    double px = map->x0 + x/map->xstep;
    double py = map->y0 + y/map->ystep;
    
    if(py < 0 || px < 0 || px > map->cols-1 || py > map->rows-1) return 0;
    
    size_t x1 = (size_t)floor(px);
    size_t y1 = (size_t)floor(py);
    
    if((int)x1 == map->cols-1) x1--;
    if((int)y1 == map->rows-1) y1--;
    
    size_t x2 = x1 + 1;
    size_t y2 = y1 + 1;
    
    double dx1 = px - x1;
    double dy1 = py - y1;
    double dx2 = x2 - px;
    double dy2 = y2 - py;
    
    double f = map->data[y1][x1] * dx2 * dy2;
    f += map->data[y2][x1] * dx2 * dy1;
    f += map->data[y1][x2] * dx1 * dy2;
    f += map->data[y2][x2] * dx1 * dy1;
    
    f /= ((x2-x1)*(y2-y1));
    
    return f;
}

#if INCLUDE_CUBA == 1

static int integrand_cuba_surf(const int *ndim, const double xx[], const int *ncomp, double ff[], void *userdata) {
    assert(*ndim == 2);
    assert(*ncomp == 1);
    
    // get rid of compiler warnings
    (void) ndim;
    (void) ncomp;
    
    knm_surf_cuba_params_t *p = (knm_surf_cuba_params_t*)userdata;
    
    double x = p->xmin + xx[0] * p->xrange;
    double y = p->ymin + xx[1] * p->yrange;
    // use factor to change from sqrt{2}x/w(z) - > x'
    double x1 = p->xfac * x;
    double y1 = p->yfac * y;
    
    double Hn1 = hermite(p->n1, x1);
    double Hn2 = (p->n1==p->n2) ? Hn1 : hermite(p->n2, x1);
    double Hm1 = hermite(p->m1, y1);
    double Hm2 = (p->m1==p->m2) ? Hm1 :hermite(p->m2, y1);
    
    double func = eval_surface_map(p->map, x, y);
    
    ff[0] = Hn1*Hn2 * Hm1*Hm2* exp(-x1*x1 - y1*y1) * func * p->constant;
    
    return 0;
}

#endif

/**
 * Computes the overlap integral or a surface motion. Assumes the coupling
 * is mode matched so that the entire integral is real.
 * 
 * @param result
 * @param p
 */
void integrate_riemann_sum_surf(double *result, knm_surf_cuba_params_t *p) {
    assert(result != NULL);
    assert(p != NULL);
    
    int i=0, j=0;
    
    double x=0, y=0;
    double dx = p->map->xstep;
    double dy = p->map->ystep;
    double da = dx*dy;

    *result = 0.0;
    
    for (i = 0; i < p->map->cols; i++) {
        x = (i - p->map->x0) * dx;    
        // use factor to change from sqrt{2}x/w(z) - > x'
        double x1  = x * p->xfac;
        double Hn1 = hermite(p->n1, x1);
        double Hn2 = (p->n1==p->n2) ? Hn1 : hermite(p->n2, x1);
            
        for (j = 0; j < p->map->rows; j++) {
            y = (j - p->map->y0) * dy;
            double y1  = y * p->yfac;
            double Hm1 = hermite(p->m1, y1);
            double Hm2 = (p->m1==p->m2) ? Hm1 : hermite(p->m2, y1);
            
            double func = p->map->data[j][i];
            *result += Hn1*Hn2 * Hm1*Hm2* exp(- x1*x1 - y1*y1) * func;
        }
    }
   
    // finally multiply by the area of the each pixel in the map to correctly
    // scale the results
    *result *= da * p->constant;
}

void calc_mirror_knm_surf_motions_rom(mirror_t *mirror, double nr1, double nr2,
                                      complex_t qx11, complex_t qy11,
                                      complex_t qx22, complex_t qy22,
                                      bitflag astigmatism) {
   
    assert(mirror != NULL);
    
    int timer = startTimer("ROMHOM");
    
    if(mirror->x_off != 0.0 || mirror->y_off != 0.0)
        warn("ROMHOM can't take into account transverse offsets in mirror position, use normal maps for this or include offset in map when making ROM.", mirror->name);
    
    // boolean value that states whether we should calculate knm using knm without
    // having to do the whole integral and just by applying some phase factor
    int calc_knm_transpose = (init.calc_knm_transpose && (astigmatism == 0));
    
    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2
    
    // K11
    if (CALC_MR_KNM(mirror,11)) {
        u_nm_accel_get(acc_11_nr1_1, max_n, max_m, qx11, qy11, nr1);
        u_nm_accel_get(acc_11_nr1_2, max_n, max_m, qx11, qy11, nr1);
    }
    
    // K22
    if (CALC_MR_KNM(mirror,22)) {
        u_nm_accel_get(acc_22_nr2_1, max_n, max_m, qx22, qy22, nr2);
        u_nm_accel_get(acc_22_nr2_2, max_n, max_m, qx22, qy22, nr2);
    }
    
    int num_coeffs = inter.num_fields * inter.num_fields;
    int current_coeff = 1;
    
    time_t starttime = time(NULL);
    
    int n, m, n1, m1, n2, m2, l;
    
    set_progress_action_text("Calculating ROMHOM surface knm for %s", mirror->name);
    
    for(l=0; l < mirror->num_surface_motions; l++){
        if(!mirror->surface_motions_isROM[l])
            continue;
            
        rom_map_t *rom = &inter.rom_maps[mirror->surface_motions[l]];
        knm_workspace_t *ws11 = &rom->roq11.knm_ws;
        knm_workspace_t *ws22 = &rom->roq22.knm_ws;

        if (CALC_MR_KNM(mirror, 11) && rom->roq11.enabled){
            double wx11 = w0_size(qx11, nr1);
            double wy11 = w0_size(qy11, nr1);
            double zx11 = fabs(qx11.re);
            double zy11 = fabs(qy11.re);
            
            assert(wx11 <= rom->roq11.w0max && wx11 >= rom->roq11.w0min);
            assert(zx11 <= rom->roq11.zmax && zx11 >= rom->roq11.zmin);
            assert(wy11 <= rom->roq11.w0max && wy11 >= rom->roq11.w0min);
            assert(zy11 <= rom->roq11.zmax && zy11 >= rom->roq11.zmin);
            
            (void)wx11; //suppressing compiler warnings
            (void)wy11;
            (void)zx11;
            (void)zy11;

            fill_unn_cache(&(ws11->ux_cache_11), acc_11_nr1_1->acc_n, acc_11_nr1_2->acc_n, true);
            fill_unn_cache(&(ws11->uy_cache_11), acc_11_nr1_1->acc_m, acc_11_nr1_2->acc_m, true);
        }

        if (CALC_MR_KNM(mirror, 22) && rom->roq22.enabled){
            double wx22 = w0_size(qx22, nr2);
            double wy22 = w0_size(qy22, nr2);
            double zx22 =    fabs(qx22.re);
            double zy22 =    fabs(qy22.re);
            (void)wx22; //suppressing compiler warnings
            (void)wy22;
            (void)zx22;
            (void)zy22;
            
            assert(wx22 <= rom->roq22.w0max && wx22 >= rom->roq22.w0min);
            assert(zx22 <= rom->roq22.zmax &&  zx22 >= rom->roq22.zmin);
            assert(wy22 <= rom->roq22.w0max && wy22 >= rom->roq22.w0min);
            assert(zy22 <= rom->roq22.zmax &&  zy22 >= rom->roq22.zmin);
            
            fill_unn_cache(&(ws22->ux_cache_22), acc_22_nr2_1->acc_n, acc_22_nr2_2->acc_n, true);
            fill_unn_cache(&(ws22->uy_cache_22), acc_22_nr2_1->acc_m, acc_22_nr2_2->acc_m, true);
        }
        
        // Iterate over each mode to calculate k_n1,m1,n2,m2
        for (n = 0; n < inter.num_fields; n++) {    
            for (m = 0; m < inter.num_fields; m++) {

                // If we are using the knm to calc knm then we do not need to bother
                // with doing all the integrating for any of the lower triangle.
                if (!calc_knm_transpose || (n >= m)) {

                    //Transform linear mode index system into actual TEM_NM mode
                    get_tem_modes_from_field_index(&n1, &m1, n);
                    get_tem_modes_from_field_index(&n2, &m2, m);
                    
                    // compute some constants that are n1,n2,m1,m2 dependant
                    if (CALC_MR_KNM(mirror, 11) && rom->roq11.enabled){
                        complex_t znm1c1 = z_by_zc(z_by_z(acc_11_nr1_1->acc_n->prefac[n1], acc_11_nr1_1->acc_m->prefac[m1]),
                                                   z_by_z(acc_11_nr1_2->acc_n->prefac[n2], acc_11_nr1_2->acc_m->prefac[m2]));

                        mirror->knm_surf_motion_1o[l][n][m] = z_by_z(do_romhom_real_int(&rom->roq11, ws11->d_u_xy, &ws11->ux_cache_11, &ws11->uy_cache_11, n1, m1, n2, m2), znm1c1);
                        
                        // the only difference between the incoming and outgoing computation
                        // is that q_1 = cminus(cconj(q_2)) which means that the gouy phase is
                        // opposite sign, or just the conjugate. As the knm matrix is hermitian
                        // we can compute everything at once from one integral computation.
                        mirror->knm_surf_motion_1i[l][n][m] = cconj(mirror->knm_surf_motion_1o[l][n][m]);
                    } else {
                        mirror->knm_surf_motion_1o[l][n][m] = (n1==n2 && m1==m2) ? complex_1 : complex_0;
                        mirror->knm_surf_motion_1i[l][n][m] = (n1==n2 && m1==m2) ? complex_1 : complex_0;
                    }

                    if (CALC_MR_KNM(mirror, 22) && rom->roq22.enabled) {
                        complex_t znm2c2 = z_by_zc(z_by_z(acc_22_nr2_1->acc_n->prefac[n1], acc_22_nr2_1->acc_m->prefac[m1]),
                                                   z_by_z(acc_22_nr2_2->acc_n->prefac[n2], acc_22_nr2_2->acc_m->prefac[m2]));

                        mirror->knm_surf_motion_2o[l][n][m] = z_by_z(do_romhom_real_int(&rom->roq22, ws22->d_u_xy, &ws22->ux_cache_22, &ws22->uy_cache_22, n1, m1, n2, m2), (znm2c2));
                        
                        // the only difference between the incoming and outgoing computation
                        // is that q_1 = cminus(cconj(q_2)) which means that the gouy phase is
                        // opposite sign, or just the conjugate. As the knm matrix is hermitian
                        // we can compute everything at once from one integral computation.
                        mirror->knm_surf_motion_2i[l][n][m] = cconj(mirror->knm_surf_motion_2o[l][n][m]);
                    } else {
                        mirror->knm_surf_motion_2o[l][n][m] = (n1==n2 && m1==m2) ? complex_1 : complex_0;
                        mirror->knm_surf_motion_2i[l][n][m] = (n1==n2 && m1==m2) ? complex_1 : complex_0;
                    }

                    if (calc_knm_transpose && !(n1 == n2 && m1 == m2)) {
                        mirror->knm_surf_motion_1i[l][m][n] = cconj(mirror->knm_surf_motion_1i[l][n][m]);
                        mirror->knm_surf_motion_1o[l][m][n] = cconj(mirror->knm_surf_motion_1o[l][n][m]);
                        mirror->knm_surf_motion_2i[l][m][n] = cconj(mirror->knm_surf_motion_2i[l][n][m]);
                        mirror->knm_surf_motion_2o[l][m][n] = cconj(mirror->knm_surf_motion_2o[l][n][m]);
                    }
                }

                current_coeff++;
                print_progress_and_time(num_coeffs, current_coeff, starttime);
            }
        }
    }
    
    endTimer(timer);
}

void calc_mirror_knm_surf_motions_map(mirror_t *m, double nr1, double nr2, complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2) {
    
    assert(m!=NULL);
    assert(m->num_surface_motions > 0);
    assert(m->surface_motions != NULL);
    
    bool compute1 = false, compute2 = false;
    
    if(!ceq(m->last_surf_knm_qx1, qx1)) compute1 |= true;
    if(!ceq(m->last_surf_knm_qy1, qy1)) compute1 |= true;
    
    if(!ceq(m->last_surf_knm_qx2, qx2)) compute2 |= true;
    if(!ceq(m->last_surf_knm_qy2, qy2)) compute2 |= true;
    
    //bool equal_q = ceq(qx1, qx2) && ceq(qy1, qy2);
   
    node_t *n1 = &inter.node_list[m->node1_index];
    node_t *n2 = &inter.node_list[m->node2_index];
    
    if(n1->gnd_node) compute1 = false;
    if(n2->gnd_node) compute2 = false;
    
    if(!compute1 && !compute2) return;
    
    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2
    
    if (!n1->gnd_node) u_nm_accel_get(acc_11_nr1_2, max_n, max_m, qx1, qy1, nr1);
    if (!n2->gnd_node) u_nm_accel_get(acc_22_nr2_2, max_n, max_m, qx2, qy2, nr2);
    
    knm_surf_cuba_params_t p = (knm_surf_cuba_params_t){0};
		// todo: this creates a compiler warning, would be nice to fix that.
    
#if INCLUDE_CUBA == 1
    int nregions, neval, fail;
    double error[1], prob[1];
    // if the q values are equal on both sides then there is no need to compute
    // the same integral twice.
    int NCOMP = 1;
    double integral[1];
#endif
    
    int in = 0, out = 0, k;
    
    double abs_err = (m->knm_cuba_abs_err != 0) ? m->knm_cuba_abs_err : init.abserr;
    double rel_err = (m->knm_cuba_rel_err != 0) ? m->knm_cuba_rel_err : init.relerr;
    
    time_t starttime = time(NULL);
    int num_coeffs = inter.num_fields * inter.num_fields;
    int current_coeff = 0;
        
    for(k=0; k<m->num_surface_motions; k++){
        if(m->surface_motions_isROM[k])
            continue;
        
        p.map = &inter.surface_motion_map_list[m->surface_motions[k]];
        current_coeff = 1;
                
        set_progress_action_text("Integrating surface motion %i/%i on %s", k+1, m->num_surface_motions, m->name);
        print_progress_and_time(num_coeffs, current_coeff, starttime);
        
        for(in=0; in<inter.num_fields; in++){
            get_tem_modes_from_field_index(&p.n1, &p.m1, in);
            
            // only loop over upper half of matrix
            for(out=in; out<inter.num_fields; out++){
                get_tem_modes_from_field_index(&p.n2, &p.m2, out);
                
                double const_fac = 1.0 / (sqrt(pow(2, p.n1+p.n2+p.m1+p.m2-2) * fac(p.n1) * fac(p.n2) * fac(p.m1) * fac(p.m2)) * PI);
                
                double gx11 = gouy(qx1);
                double gy11 = gouy(qy1);
                double gx22 = gouy(qx2);
                double gy22 = gouy(qy2);
                
                double value = 0.0;
                
                if(compute1) {
                    double wx = w_size(qx1, nr1);
                    double wy = w_size(qy1, nr1);
                    p.constant = const_fac / (wx*wy);
                    p.xfac = SQRTTWO / wx;
                    p.yfac = SQRTTWO / wy;
                    
#if INCLUDE_CUBA == 1
                    p.xmin = -5 * sqrt(max(p.n1, p.n2)+1) * wx;
                    p.ymin = -5 * sqrt(max(p.m1, p.m2)+1) * wy;
                    p.xrange = -2 * p.xmin;
                    p.yrange = -2 * p.ymin;
                    
                    integral[0] = 0;
                    Cuhre(2, NCOMP, integrand_cuba_surf, (void*) &p,
                                rel_err, abs_err, 0,
                                0, init.maxintcuba, 13, NULL,
                                &nregions, &neval, &fail, integral, error, prob);
                    // scale the integral value due to -1:1 limits in Cuhre
                    value = integral[0] * p.xrange * p.yrange;
#else
                    integrate_riemann_sum_surf(&value, &p);
#endif
                    // surface integration is just the real kernel of the hermite
                    // polynomials, have to add in the gouy phase here.
                    // Even though we reverse gouy it later it needs to be in 
                    // here for merging with any static distortions
                    complex_t knm = z_by_xphr(complex_1, value, (p.n1 - p.n2) * gx11 + (p.m1 - p.m2) * gy11);
                    
                    //warn("%i%i->%i%i %s\n", p.n1, p.m1, p.n2, p.m2, complex_form15(knm));
                    
                    // the only difference between the incoming and outgoing computation
                    // is that q_1 = cminus(cconj(q_2)) which means that the gouy phase is
                    // opposite sign, or just the conjugate. As the knm matrix is hermitian
                    // we can compute everything at once from one integral computation.
                
                    m->knm_surf_motion_1o[k][in][out] = knm;
                    m->knm_surf_motion_1i[k][in][out] = cconj(knm);
                    
                    //warn("%i%i->%i%i = %.15g\n",p.n1, p.m1, p.n2, p.m2, zabs(knm));
                    
                    if(out!=in) {
                        m->knm_surf_motion_1o[k][out][in] = cconj(m->knm_surf_motion_1o[k][in][out]);
                        m->knm_surf_motion_1i[k][out][in] = cconj(m->knm_surf_motion_1i[k][in][out]);
                    }
                }
                
                if(compute2){
                    double wx = w_size(qx2, nr2);
                    double wy = w_size(qy2, nr2);
                    p.constant = const_fac / (wx*wy);
                    p.xfac = SQRTTWO / wx;
                    p.yfac = SQRTTWO / wy;
                    
#if INCLUDE_CUBA == 1
                    p.xmin = -5 * sqrt(max(p.n1, p.n2)+1) * wx;
                    p.ymin = -5 * sqrt(max(p.m1, p.m2)+1) * wy;
                    p.xrange = -2 * p.xmin;
                    p.yrange = -2 * p.ymin;
                    
                    integral[0] = 0.0;
                    
                    Cuhre(2, NCOMP, integrand_cuba_surf, (void*) &p,
                           rel_err, abs_err, 0,
                            0, init.maxintcuba, 13, NULL,
                            &nregions, &neval, &fail, integral, error, prob);
                    
                    value = integral[0] * p.xrange * p.yrange;
#else
                    integrate_riemann_sum_surf(&value, &p);
#endif
                    
                    complex_t knm = z_by_xphr(complex_1, value, (p.n1 - p.n2) * gx22 + (p.m1 - p.m2) * gy22);
                    
                    // use a minus sign (complex_m1) here because the surface motion will appear
                    // opposite from node 2 side, we also have the msign term here because the coord
                    // system of the outgoing beam has a negative x compared to the map coord system
                    m->knm_surf_motion_2o[k][in][out] = knm;
                    m->knm_surf_motion_2i[k][in][out] = cconj(knm);
                    
                    if(out!=in) {
                        m->knm_surf_motion_2o[k][out][in] = cconj(m->knm_surf_motion_2o[k][in][out]);   
                        m->knm_surf_motion_2i[k][out][in] = cconj(m->knm_surf_motion_2i[k][in][out]);
                    }
                } 
                
                if(out!=in) 
                    current_coeff+=2; // add two because we also compute the transposed elements too
                else
                    current_coeff++;
                
                print_progress_and_time(num_coeffs, current_coeff, starttime);
            }
        }
    }
    
    m->last_surf_knm_qx1 = qx1;
    m->last_surf_knm_qy1 = qy1;
    m->last_surf_knm_qx2 = qx2;
    m->last_surf_knm_qy2 = qy2;
    
    set_progress_action_text("Integrating surface motion done on %s", m->name);
    print_progress_and_time(num_coeffs, num_coeffs+1, starttime);
}


/**
 * Computes the ROMHOM map model coupling matrix for a mirror.
 * 
 * @param mirror
 * @param nr1
 * @param nr2
 * @param integrating
 * @param mismatch
 * @param astigmatism
 */
void compute_mirror_knm_romhom(mirror_t *mirror, double nr1, double nr2, int mismatch, int astigmatism) {
    assert(mirror != NULL);
    
    int timer = startTimer("ROMHOM");
    
    if(mirror->map_rom == NULL){
        endTimer(timer);
        return;
    }
    
    mirror_knm_q_t *kq = &(mirror->knm_q);
    
    // If the q values used are the same then no need to recompute HOM
    if(knm_q_cmp(kq, &(mirror->prev_rom_q)))
        return;
    
    mirror_knm_t *knm = &(mirror->knm_romhom);
    
    if (!IS_MIRROR_KNM_ALLOCD(knm))
        bug_error("mirror romhom knm has not been allocated");
    
    if(mirror->x_off != 0.0 || mirror->y_off != 0.0)
        warn("ROMHOM can't take into account transverse offsets in mirror position, use normal maps to take this into account.", mirror->name);
    
    // boolean value that states whether we should calculate knm using knm without
    // having to do the whole integral and just by applying some phase factor
    int calc_knm_transpose = (init.calc_knm_transpose && (mismatch == 0) && (astigmatism == 0));
    
    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2

    if(!ceq(kq->qxt1_11, kq->qxt2_11) || !ceq(kq->qyt1_11, kq->qyt2_11)) {
        warn("Mismatch on side 1 reflection at %s, can't compute coupling using ROM\n", mirror->name);
        warn("1->1: qx = %s, qx' = %s\n", complex_form15(kq->qxt1_11), complex_form15(kq->qxt2_11));
        warn("1->1: qy = %s, qy' = %s\n", complex_form15(kq->qyt1_11), complex_form15(kq->qyt2_11));
    }
    
    if(!ceq(kq->qxt1_22, kq->qxt2_22) || !ceq(kq->qyt1_22, kq->qyt2_22)) {
        warn("Mismatch on side 2 reflection at %s, can't compute coupling using ROM\n", mirror->name);
        warn("2->2: qx = %s, qx' = %s\n", complex_form15(kq->qxt1_22), complex_form15(kq->qxt2_22));
        warn("2->2: qy = %s, qy' = %s\n", complex_form15(kq->qyt1_22), complex_form15(kq->qyt2_22));
    }
    
    if(!ceq(kq->qxt1_12, kq->qxt2_12) || !ceq(kq->qxt1_21, kq->qxt2_21) 
            || !ceq(kq->qyt1_12, kq->qyt2_12) || !ceq(kq->qyt1_21, kq->qyt2_21)) {
        warn("Mismatch on transmission at %s, can't compute coupling using ROM\n", mirror->name);
        warn("1->2: qx = %s, qx' = %s\n", complex_form15(kq->qxt1_12), complex_form15(kq->qxt2_12));
        warn("1->2: qy = %s, qy' = %s\n", complex_form15(kq->qyt1_12), complex_form15(kq->qyt2_12));
        warn("2->1: qx = %s, qx' = %s\n", complex_form15(kq->qxt1_21), complex_form15(kq->qxt2_21));
        warn("2->1: qy = %s, qy' = %s\n", complex_form15(kq->qyt1_21), complex_form15(kq->qyt2_21));
    }
    
    // K11
    if (CALC_MR_KNM(mirror,11)) {
        u_nm_accel_get(acc_11_nr1_1, max_n, max_m, kq->qxt1_11, kq->qyt1_11, nr1);
        u_nm_accel_get(acc_11_nr1_2, max_n, max_m, kq->qxt2_11, kq->qyt2_11, nr1);
    }
    
    // K22
    if (CALC_MR_KNM(mirror,22)) {
        u_nm_accel_get(acc_22_nr2_1, max_n, max_m, kq->qxt1_22, kq->qyt1_22, nr2);
        u_nm_accel_get(acc_22_nr2_2, max_n, max_m, kq->qxt2_22, kq->qyt2_22, nr2);
    }
    
    // K12
    if (CALC_MR_KNM(mirror,12)) {
        u_nm_accel_get(acc_12_nr1_1, max_n, max_m, kq->qxt1_12, kq->qyt1_12, nr1);
        u_nm_accel_get(acc_12_nr2_2, max_n, max_m, kq->qxt2_12, kq->qyt2_12, nr2);
    }
    
    // K21
    if (CALC_MR_KNM(mirror,21)) {
        u_nm_accel_get(acc_21_nr2_1, max_n, max_m, kq->qxt1_21, kq->qyt1_21, nr2);
        u_nm_accel_get(acc_21_nr1_2, max_n, max_m, kq->qxt2_21, kq->qyt2_21, nr1);
    }
    
    int num_coeffs = inter.num_fields * inter.num_fields;
    int current_coeff = 1;
    
    time_t starttime = time(NULL);
    
    rom_map_t *rom = mirror->map_rom;
    
    knm_workspace_t *ws11 = &mirror->map_rom->roq11.knm_ws;
    knm_workspace_t *ws22 = &mirror->map_rom->roq22.knm_ws;
    knm_workspace_t *ws12 = &mirror->map_rom->roq12.knm_ws;
    knm_workspace_t *ws21 = &mirror->map_rom->roq21.knm_ws;
    
    int n, m, n1, m1, n2, m2;
    
    set_progress_action_text("Calculating ROMHOM cache for %s", mirror->name);
    
    print_progress_and_time(num_coeffs, current_coeff, starttime);
   
    if (CALC_MR_KNM(mirror,11) && rom->roq11.enabled){
        fill_unn_cache(&(ws11->ux_cache_11), acc_11_nr1_1->acc_n, acc_11_nr1_2->acc_n, true);
        fill_unn_cache(&(ws11->uy_cache_11), acc_11_nr1_1->acc_m, acc_11_nr1_2->acc_m, true);
    }

    if (CALC_MR_KNM(mirror,12) && rom->roq12.enabled){
        fill_unn_cache(&(ws12->ux_cache_12), acc_12_nr1_1->acc_n, acc_12_nr2_2->acc_n, true);
        fill_unn_cache(&(ws12->uy_cache_12), acc_12_nr1_1->acc_m, acc_12_nr2_2->acc_m, true);
    }

    if (CALC_MR_KNM(mirror,21) && rom->roq21.enabled){
        fill_unn_cache(&(ws21->ux_cache_21), acc_21_nr2_1->acc_n, acc_21_nr1_2->acc_n, true);
        fill_unn_cache(&(ws21->uy_cache_21), acc_21_nr2_1->acc_m, acc_21_nr1_2->acc_m, true);
    }

    if (CALC_MR_KNM(mirror,22) && rom->roq22.enabled){
        fill_unn_cache(&(ws22->ux_cache_22), acc_22_nr2_1->acc_n, acc_22_nr2_2->acc_n, true);
        fill_unn_cache(&(ws22->uy_cache_22), acc_22_nr2_1->acc_m, acc_22_nr2_2->acc_m, true);
    }
    
    set_progress_action_text("Calculating ROMHOM knm for %s", mirror->name);
    
    complex_t results[4];
    
    // Iterate over each mode to calculate k_n1,m1,n2,m2
    for (n = 0; n < inter.num_fields; n++) {    
        for (m = 0; m < inter.num_fields; m++) {
         
            // If we are using the knm to calc knm then we do not need to bother
            // with doing all the integrating for any of the lower triangle.
            if (!calc_knm_transpose || (n >= m)) {

                //Transform linear mode index system into actual TEM_NM mode
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                
                // Set default values of a unity matrix
                if (n1==n2 && m1==m2){
                    results[0] = complex_1;
                    results[1] = complex_1;
                    results[2] = complex_1;
                    results[3] = complex_1;
                } else {
                    results[0] = complex_0;
                    results[1] = complex_0;
                    results[2] = complex_0;
                    results[3] = complex_0;
                }
                
                // compute some constants that are n1,n2,m1,m2 dependant
                if (CALC_MR_KNM(mirror,11) && rom->roq11.enabled){
                    complex_t znm1c1 = z_by_zc(z_by_z(acc_11_nr1_1->acc_n->prefac[n1], acc_11_nr1_1->acc_m->prefac[m1]),
                                               z_by_z(acc_11_nr1_2->acc_n->prefac[n2], acc_11_nr1_2->acc_m->prefac[m2]));

                    results[0] = z_by_z(do_romhom_real_int(&rom->roq11, ws11->d_u_xy, &ws11->ux_cache_11, &ws11->uy_cache_11, n1, m1, n2, m2), znm1c1);
                }

                // Disable transmission calculation as ROM maps are only for reflection currently
                if (CALC_MR_KNM(mirror,12) && rom->roq12.enabled) {
                    complex_t znm1c2 = z_by_zc(z_by_z(acc_12_nr1_1->acc_n->prefac[n1], acc_12_nr1_1->acc_m->prefac[m1]),
                                               z_by_z(acc_12_nr2_2->acc_n->prefac[n2], acc_12_nr2_2->acc_m->prefac[m2]));
                    
                    results[1] = z_by_z(do_romhom_real_int(&rom->roq12, ws12->d_u_xy, &ws12->ux_cache_12, &ws12->uy_cache_12, n1, m1, n2, m2), znm1c2);
                }

                if (CALC_MR_KNM(mirror,21) && rom->roq21.enabled) {
                    complex_t znm2c1 = z_by_zc(z_by_z(acc_21_nr2_1->acc_n->prefac[n1], acc_21_nr2_1->acc_m->prefac[m1]),
                                               z_by_z(acc_21_nr1_2->acc_n->prefac[n2], acc_21_nr1_2->acc_m->prefac[m2]));
                    
                    results[2] = z_by_z(do_romhom_real_int(&rom->roq21, ws21->d_u_xy, &ws21->ux_cache_21, &ws21->uy_cache_21, n1, m1, n2, m2), znm2c1);
                }

                if (CALC_MR_KNM(mirror,22) && rom->roq22.enabled) {
                    complex_t znm2c2 = z_by_zc(z_by_z(acc_22_nr2_1->acc_n->prefac[n1], acc_22_nr2_1->acc_m->prefac[m1]),
                                               z_by_z(acc_22_nr2_2->acc_n->prefac[n2], acc_22_nr2_2->acc_m->prefac[m2]));
                    
                    results[3] = z_by_z(do_romhom_real_int(&rom->roq22, ws22->d_u_xy, &ws22->ux_cache_22, &ws22->uy_cache_22, n1, m1, n2, m2), znm2c2);
                }
                
                knm->k11[n][m] = results[0];
                knm->k12[n][m] = results[1];
                knm->k21[n][m] = results[2];
                knm->k22[n][m] = results[3];
                
                // If we have a mode matched beam we are able to calculate the transposed
                // elements by applying a simple phase factor
                if (calc_knm_transpose && !(n1 == n2 && m1 == m2)) {
                    // beam must be mode matched for this so either input or output q will do
                    knm->k11[m][n] = z_by_phr(results[0], 2.0 * gouy(mirror->knm_q.qxt1_11) * (n2 - n1 + m2 - m1));
                    knm->k12[m][n] = z_by_phr(results[1], 2.0 * gouy(mirror->knm_q.qxt1_12) * (n2 - n1 + m2 - m1));
                    knm->k21[m][n] = z_by_phr(results[2], 2.0 * gouy(mirror->knm_q.qxt1_21) * (n2 - n1 + m2 - m1));
                    knm->k22[m][n] = z_by_phr(results[3], 2.0 * gouy(mirror->knm_q.qxt1_22) * (n2 - n1 + m2 - m1));
                }
            }
            
            current_coeff++;
            print_progress_and_time(num_coeffs, current_coeff, starttime);
        }
    }
    
    // store q values used for computation for later comparison
    memcpy(&mirror->prev_rom_q, kq, sizeof(mirror_knm_q_t));
    
    endTimer(timer);
}

/**
 * q values are used to compare against previous q values used to calculate
 * the knm's not for the actual calculation, these are got from mirror->knm_q
 */
void compute_mirror_knm_integral(mirror_t *mirror, double nr1, double nr2, bitflag integrating, bitflag mismatch, bitflag astigmatism) {
    int timer = startTimer("MAP");
    
    surface_merged_map_t *map = &(mirror->map_merged);
    mirror_knm_t *knm = &(mirror->knm_map);

    double abs_err = (mirror->knm_cuba_abs_err != 0) ? mirror->knm_cuba_abs_err : init.abserr;
    double rel_err = (mirror->knm_cuba_rel_err != 0) ? mirror->knm_cuba_rel_err : init.relerr;
    
    // A quick check is needed to determine if any maps have actually been applied.
    // if so and there is no other integrating needed we need not continue.
    // Otherwise we need to allocate some memory to store the results in.
    if (map->noMapPresent && !integrating) {
        endTimer(timer);
        return;
    }
    
    if (!IS_MIRROR_KNM_ALLOCD(knm))
        bug_error("mirror map knm has not been allocated");

    // We now need to determine whether we actually need to compute the integral
    // again. As this method can be called multiple times if one of the parameters
    // is varied by the x-axis.
    bool recompute = false;

    recompute = recompute | (map->_nr1 != nr1);
    recompute = recompute | (map->_nr2 != nr2);
    recompute = recompute | (map->_angle != mirror->angle);
                
    recompute = recompute | !ceq(mirror->knm_q.qxt1_11, map->mirror_knm_q.qxt1_11);
    recompute = recompute | !ceq(mirror->knm_q.qxt2_11, map->mirror_knm_q.qxt2_11);
    recompute = recompute | !ceq(mirror->knm_q.qyt1_11, map->mirror_knm_q.qyt1_11);
    recompute = recompute | !ceq(mirror->knm_q.qyt2_11, map->mirror_knm_q.qyt2_11);

    recompute = recompute | !ceq(mirror->knm_q.qxt1_12, map->mirror_knm_q.qxt1_12);
    recompute = recompute | !ceq(mirror->knm_q.qxt2_12, map->mirror_knm_q.qxt2_12);
    recompute = recompute | !ceq(mirror->knm_q.qyt1_12, map->mirror_knm_q.qyt1_12);
    recompute = recompute | !ceq(mirror->knm_q.qyt2_12, map->mirror_knm_q.qyt2_12);

    recompute = recompute | !ceq(mirror->knm_q.qxt1_21, map->mirror_knm_q.qxt1_21);
    recompute = recompute | !ceq(mirror->knm_q.qxt2_21, map->mirror_knm_q.qxt2_21);
    recompute = recompute | !ceq(mirror->knm_q.qyt1_21, map->mirror_knm_q.qyt1_21);
    recompute = recompute | !ceq(mirror->knm_q.qyt2_21, map->mirror_knm_q.qyt2_21);

    recompute = recompute | !ceq(mirror->knm_q.qxt1_22, map->mirror_knm_q.qxt1_22);
    recompute = recompute | !ceq(mirror->knm_q.qxt2_22, map->mirror_knm_q.qxt2_22);
    recompute = recompute | !ceq(mirror->knm_q.qyt1_22, map->mirror_knm_q.qyt1_22);
    recompute = recompute | !ceq(mirror->knm_q.qyt2_22, map->mirror_knm_q.qyt2_22);
    
    recompute = recompute | (abs_err != map->_abserr);
    recompute = recompute | (rel_err != map->_relerr);
    
    recompute = recompute | (init.lambda != map->_lambda);
    
    // check the current methods against the previously used methods
    recompute = recompute | (map->interpolation_method != map->_intermeth);
    recompute = recompute | (map->interpolation_size != map->_intersize);
    recompute = recompute | (map->integration_method != map->_intmeth);

    recompute = recompute | ((mirror->knm_flags & INT_APERTURE) && (mirror->r_aperture != map->_r_aperture));

    recompute = recompute | ((mirror->knm_flags & INT_BAYER_HELMS) && (map->_xbeta != mirror->beta_x));
    recompute = recompute | ((mirror->knm_flags & INT_BAYER_HELMS) && (map->_ybeta != mirror->beta_y));
    
    recompute = recompute | (mirror->x_off != map->_x_off);
    recompute = recompute | (mirror->y_off != map->_y_off);

    if (mirror->knm_force_saved){
        if (!mirror->map_merged.knm_calculated){
            gerror("* Mirror %s is being forced to use saved knm values but no knm values have been loaded\n", mirror->name);
        }
        
        recompute = false;
        
        if ((inter.debug & 128) && !options.quiet) 
            message("* Mirror %s is being forced to use saved knm values despite any parameter difference\n", mirror->name);
    }
    
    int start_tem = 0;
    
    if (!recompute) {
        // So far everything should be the same, the last comparison to make
        // is between the maximum number of calculated modes, if more are requested
        // start off where we were, if not we have the previous ones already
        if (map->_maxtem >= inter.tem) {
            if ((inter.debug & 128) && !options.quiet) 
                message("* Did not need to integrate any knm's for mirror %s\n", mirror->name);
            
            endTimer(timer);
            return;
        } else {
            if (mirror->knm_force_saved){
                warn("* Mirror %s is being forced to use saved knm values but only maxtem %i was previously calculated\n", mirror->name, map->_maxtem);
            }
            
            start_tem = map->_maxtem + 1; // start from the next maxtem

            // adf this would be useful to print always (unless quiet is set)
            if (!options.quiet)
                message(" * Found previous knm's, computing coefficients for mirror %s from HG order %i\n", mirror->name, start_tem);
        }
    }
    
    // Here we store which knm directions we should compute, as depending on the
    // map content we may not need to compute them. However the value should be
    // restored once we're finished here.
    bitflag prev_knm_calc_flags = mirror->knm_calc_flags;

    bool usingMap = !(map->noMapPresent);
    
    // Firstly if check if there are any force integrations we should do,
    // which is determined by the integrating flag.
    if(!integrating){
        if(!map->hasReflectionMaps && (CALC_MR_KNM(mirror,11) || CALC_MR_KNM(mirror,22))){
            mirror->knm_calc_flags &= ~MR11Calc;
            mirror->knm_calc_flags &= ~MR22Calc;
        }
        
        if(!map->hasTransmissionMaps && (CALC_MR_KNM(mirror,12) || CALC_MR_KNM(mirror,21))){
            mirror->knm_calc_flags &= ~MR12Calc;
            mirror->knm_calc_flags &= ~MR21Calc;
        }
    }
    
    if(mirror->knm_calc_flags == 0) {
        endTimer(timer);
        return;
    }
    
    if (inter.debug && !options.quiet) {
        message("* Effects integrated for mirror %s:\n", mirror->name);

        if (usingMap)message("    - Merged maps\n");
        if (integrating & 1)message("    - Bayer-Helms\n");
        if (integrating & 2)message("    - Aperture\n");
        if (integrating & 4)message("    - Curvature\n");

        message("* Coupling coefficients to compute for %s:", mirror->name);
        if (CALC_MR_KNM(mirror,11))message(" K11");
        if (CALC_MR_KNM(mirror,12))message(" K12");
        if (CALC_MR_KNM(mirror,21))message(" K21");
        if (CALC_MR_KNM(mirror,22))message(" K22");
        message("\n");
    }

    // boolean value that states whether we should calculate knm using knm without
    // having to do the whole integral and just by applying some phase factor
    int calc_knm_transpose = (init.calc_knm_transpose && (mismatch == 0) && (astigmatism == 0));

    if (inter.debug && calc_knm_transpose == 1 && !options.quiet && !(map->usermessages & KNM_TRANSPOSE)) {
       message("* Calculating knm matrix lower triangle using phase factor\n"
               "   and upper triangle. See kat.ini calc_knm_transpose variable.\n");
        map->usermessages |= KNM_TRANSPOSE;
    }

    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2

    mirror_knm_q_t *kq = &(mirror->knm_q);
            
    if (CALC_MR_KNM(mirror,11)) {
        u_nm_accel_get(acc_11_nr1_1, max_n, max_m, kq->qxt1_11, kq->qyt1_11, nr1);
        u_nm_accel_get(acc_11_nr1_2, max_n, max_m, kq->qxt2_11, kq->qyt2_11, nr1);
    }
    
    if (CALC_MR_KNM(mirror,22)) {
        u_nm_accel_get(acc_22_nr2_1, max_n, max_m, kq->qxt1_22, kq->qyt1_22, nr2);
        u_nm_accel_get(acc_22_nr2_2, max_n, max_m, kq->qxt2_22, kq->qyt2_22, nr2);
    }
    
    if (CALC_MR_KNM(mirror,12)) {
        u_nm_accel_get(acc_12_nr1_1, max_n, max_m, kq->qxt1_12, kq->qyt1_12, nr2);
        u_nm_accel_get(acc_12_nr2_2, max_n, max_m, kq->qxt2_12, kq->qyt2_12, nr2);
    }
    
    if (CALC_MR_KNM(mirror,21)) {
        u_nm_accel_get(acc_21_nr2_1, max_n, max_m, kq->qxt1_21, kq->qyt1_21, nr1);
        u_nm_accel_get(acc_21_nr1_2, max_n, max_m, kq->qxt2_21, kq->qyt2_21, nr1);
    }
    
    mr_knm_map_int_params_t param;
    
    param.usingMap = usingMap;
    param.merged_map = map;
    param.mirror = mirror;
    param.nr1 = nr1;
    param.nr2 = nr2;
    param.acc_11_nr1_1 = (CALC_MR_KNM(mirror,11)) ? acc_11_nr1_1 : NULL;
    param.acc_11_nr1_2 = (CALC_MR_KNM(mirror,11)) ? acc_11_nr1_2 : NULL;
    param.acc_12_nr1_1 = (CALC_MR_KNM(mirror,12)) ? acc_12_nr1_1 : NULL;
    param.acc_12_nr2_2 = (CALC_MR_KNM(mirror,12)) ? acc_12_nr2_2 : NULL;
    param.acc_21_nr2_1 = (CALC_MR_KNM(mirror,21)) ? acc_21_nr2_1 : NULL;
    param.acc_21_nr1_2 = (CALC_MR_KNM(mirror,21)) ? acc_21_nr1_2 : NULL;
    param.acc_22_nr2_1 = (CALC_MR_KNM(mirror,22)) ? acc_22_nr2_1 : NULL;
    param.acc_22_nr2_2 = (CALC_MR_KNM(mirror,22)) ? acc_22_nr2_2 : NULL;
    param.knm_q = &(mirror->knm_q);
    param.knm_calc_flags = mirror->knm_calc_flags;
    
    param.aperture_radius = mirror->r_aperture;
    param.betax = mirror->beta_x;
    param.betay = mirror->beta_y;
    param.integrating = integrating;

    complex_t results[4];

    results[0] = complex_0;
    results[1] = complex_0;
    results[2] = complex_0;
    results[3] = complex_0;

    //This sets the number of cores to use for cuba library
    if (mirror->map_merged.integration_method == CUBA_CUHRE_SERIAL 
            || mirror->map_merged.integration_method == CUBA_CUHRE_PARA) {
        char buf[ERR_MSG_LEN];
        
        if(mirror->knm_cuba_cores > 0)
            sprintf(buf, "CUBACORES=%d", mirror->knm_cuba_cores);
        else
            sprintf(buf, "CUBACORES=%d", init.cuba_numprocs);
        
        putenv(buf);
    }
    
    if (inter.debug && !options.quiet && !(map->usermessages & INT_METHODS)) {
        map->usermessages |= INT_METHODS;
        
        message("* Mirror integration - %s\n", mirror->name);
        message("   - Using ");

        switch (map->integration_method) {
            case RIEMANN_SUM_NEW:
               message("Riemann integrator\n");
                break;
            case NEWTON_COTES:
               message("Newton-Cotes order %i integrator\n", map->integration_NC_order);
                break;
#if INCLUDE_CUBA == 1
            case CUBA_CUHRE_PARA:
               message("parallel cubature integrator\n");
                break;
            case CUBA_CUHRE_SERIAL:
               message("serial cubature integrator\n");
               break;
#endif
            default:
               bug_error("unhandled option %i", map->integration_method);
        }
        
       message("   - Using %i core%s for integration\n", init.cuba_numprocs, ((init.cuba_numprocs == 1) ? "" : "s"));
        
        if(usingMap){
           message("   - Using ");

            switch (map->interpolation_method) {
                case SPLINE:message("spline");
                    break;
                case LINEAR:message("linear");
                    break;
                case NEAREST:message("nearest-neighbour");
                    break;
            }

           message(" interpolation, kernel size %i\n",mirror->map_merged.interpolation_size);
        }
    }

    int num_coeffs = inter.num_fields * inter.num_fields;
    
    if(calc_knm_transpose){
        int half = (inter.num_fields * inter.num_fields - inter.num_fields)/2;
        num_coeffs = half + inter.num_fields;
    }
        
    int current_coeff = 1;
    time_t starttime = time(NULL);
            
    int n, m, n1, m1, n2, m2;
    
    if(map->integration_method == NEWTON_COTES) {
        set_progress_action_text("Calculating Newton-Cotes cache for %s", mirror->name);
        knm_workspace_t *ws = &map->knm_ws;
        
        print_progress_and_time(num_coeffs, current_coeff, starttime);

        if (CALC_MR_KNM(mirror,11)){
            fill_unn_cache(&(ws->ux_cache_11), acc_11_nr1_1->acc_n, acc_11_nr1_2->acc_n, (mismatch & 1) != 1);
            fill_unn_cache(&(ws->uy_cache_11), acc_11_nr1_1->acc_m, acc_11_nr1_2->acc_m, (mismatch & 1) != 1);
        }

        if (CALC_MR_KNM(mirror,12)){
            fill_unn_cache(&(ws->ux_cache_12), acc_12_nr1_1->acc_n, acc_12_nr2_2->acc_n, (mismatch & 4) != 4);
            fill_unn_cache(&(ws->uy_cache_12), acc_12_nr1_1->acc_m, acc_12_nr2_2->acc_m, (mismatch & 4) != 4);
        }

        if (CALC_MR_KNM(mirror,21)){
            fill_unn_cache(&(ws->ux_cache_21), acc_21_nr2_1->acc_n, acc_21_nr1_2->acc_n, (mismatch & 8) != 8);
            fill_unn_cache(&(ws->uy_cache_21), acc_21_nr2_1->acc_m, acc_21_nr1_2->acc_m, (mismatch & 8) != 8);
        }

        if (CALC_MR_KNM(mirror,22)){
            fill_unn_cache(&(ws->ux_cache_22), acc_22_nr2_1->acc_n, acc_22_nr2_2->acc_n, (mismatch & 2) != 2);
            fill_unn_cache(&(ws->uy_cache_22), acc_22_nr2_1->acc_m, acc_22_nr2_2->acc_m, (mismatch & 2) != 2);
        }
    }
    
    set_progress_action_text("Integrating map on %s", mirror->name);
    
    print_progress_and_time(num_coeffs, current_coeff, starttime);
    
    // Iterate over each mode to calculate k_n1,m1,n2,m2
    for (n = 0; n < inter.num_fields; n++) {    
        for (m = 0; m < inter.num_fields; m++) {
         
            // If we are using the knm to calc knm then we do not need to bother
            // with doing all the integrating for any of the lower triangle.
            if (!calc_knm_transpose || (n >= m)) {

                //Transform linear mode index system into actual TEM_NM mode
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);
                
                // There is no need to calculate knm's that have been loaded
                // or calculated before
                if (n1 + m1 >= start_tem || n2 + m2 >= start_tem) {
                    param.m1 = m1;
                    param.m2 = m2;
                    param.n1 = n1;
                    param.n2 = n2;

                    // compute some constants that are n1,n2,m1,m2 dependant
                    if (CALC_MR_KNM(mirror,11))
                        param.znm1c1 = z_by_zc(z_by_z(acc_11_nr1_1->acc_n->prefac[n1], acc_11_nr1_1->acc_m->prefac[m1]),
                                               z_by_z(acc_11_nr1_2->acc_n->prefac[n2], acc_11_nr1_2->acc_m->prefac[m2]));
                    else
                        param.znm1c1 = complex_0;
                    
                    if (CALC_MR_KNM(mirror,12))
                        param.znm1c2 = z_by_zc(z_by_z(acc_12_nr1_1->acc_n->prefac[n1], acc_12_nr1_1->acc_m->prefac[m1]),
                                               z_by_z(acc_12_nr2_2->acc_n->prefac[n2], acc_12_nr2_2->acc_m->prefac[m2]));
                    else
                        param.znm1c2 = complex_0;
                    
                    if (CALC_MR_KNM(mirror,21))
                        param.znm2c1 = z_by_zc(z_by_z(acc_21_nr2_1->acc_n->prefac[n1], acc_21_nr2_1->acc_m->prefac[m1]),
                                               z_by_z(acc_21_nr1_2->acc_n->prefac[n2], acc_21_nr1_2->acc_m->prefac[m2]));
                    else
                        param.znm2c1 = complex_0;
                    
                    if (CALC_MR_KNM(mirror,22))
                        param.znm2c2 = z_by_zc(z_by_z(acc_22_nr2_1->acc_n->prefac[n1], acc_22_nr2_1->acc_m->prefac[m1]),
                                               z_by_z(acc_22_nr2_2->acc_n->prefac[n2], acc_22_nr2_2->acc_m->prefac[m2]));
                    else
                        param.znm2c2 = complex_0;

                    if (map->save_interp_file){
                        if(inter.debug && !options.quiet && !(map->usermessages & INTERP_SAVE)){
                           message("* Writing interpolated data for each knm for mirror %s, switch off for many knm!!!\n",mirror->name);
                            map->usermessages = map->usermessages | INTERP_SAVE;
                        }
                        
                        char ipfn[FILENAME_MAX];
                        sprintf(ipfn,"%s_%i%i_%i%i.interp",mirror->name,n1,m1,n2,m2);
                        open_file_to_write_ascii(ipfn, &ipfile);
                    }
                    
                    if (map->save_integration_points){
                        if (inter.debug && !options.quiet && !(map->usermessages & INTERP_SAVE)) {
                            message("* Writing integration sample points for each knm for %s, switch off for many knm!!!\n", mirror->name);
                            map->usermessages = map->usermessages | INTERP_SAVE;
                        }
                        
                        char idfn[FILENAME_MAX];
                        sprintf(idfn, "%s_%i%i_%i%i.intdata", mirror->name, n1, m1, n2, m2);
                        open_file_to_write_ascii(idfn, &idfile);
                    }
                    
                    if (n1==n2 && m1==m2){
                        results[0] = complex_1;
                        results[1] = complex_1;
                        results[2] = complex_1;
                        results[3] = complex_1;
                    } else {
                        results[0] = complex_0;
                        results[1] = complex_0;
                        results[2] = complex_0;
                        results[3] = complex_0;
                    }
                    
                    switch (map->integration_method) {
                        case RIEMANN_SUM_NEW:
                            do_riemann_sum_new_int((void*)&param, MIRROR_CMP, results);
                            break;
                        case NEWTON_COTES:
                            do_newton_cotes_int((void*)&param, MIRROR_CMP, results, mismatch);
                            break;
#if INCLUDE_CUBA == 1
                        case CUBA_CUHRE_SERIAL:
                            do_serial_cuhre_map_int(&param, MIRROR_CMP, results, abs_err, rel_err);
                            break;
                        case CUBA_CUHRE_PARA:
                            do_para_cuhre_map_int(&param, MIRROR_CMP, results, abs_err, rel_err);
                            break;
#endif
                        default:
                            bug_error("Could not handle a map integration of type %d", map->integration_method);
                    }

                    if (map->save_integration_points) {
                        fflush(idfile);
                        fclose(idfile);
                        idfile = NULL;
                    }
                    
                    if (map->save_interp_file){
                        fflush(ipfile);
                        fclose(ipfile);
                        ipfile = NULL;
                    }

                    knm->k11[n][m] = z_by_x(results[0], mirror->map_amplitude);
                    knm->k12[n][m] = z_by_x(results[1], mirror->map_amplitude);
                    knm->k21[n][m] = z_by_x(results[2], mirror->map_amplitude);
                    knm->k22[n][m] = z_by_x(results[3], mirror->map_amplitude);
                    
                    // If we have a mode matched beam we are able to calculate the transposed
                    // elements by applying a simple phase factor
                    if (calc_knm_transpose && !(n1 == n2 && m1 == m2)) {
                        // TODO what qx/qy parameter to use here the input or output one?
                        knm->k11[m][n] = z_by_phr(results[0], 2.0 * gouy(mirror->knm_q.qxt1_11) * (n2 - n1 + m2 - m1));
                        knm->k12[m][n] = z_by_phr(results[1], 2.0 * gouy(mirror->knm_q.qxt1_12) * (n2 - n1 + m2 - m1));
                        knm->k21[m][n] = z_by_phr(results[2], 2.0 * gouy(mirror->knm_q.qxt1_21) * (n2 - n1 + m2 - m1));
                        knm->k22[m][n] = z_by_phr(results[3], 2.0 * gouy(mirror->knm_q.qxt1_22) * (n2 - n1 + m2 - m1));
                    }

                    if (inter.debug & 64) {
                        message("%s: (%d %d) -> (%d %d) %s %s %s %s\n",
                                map->name, n1, m1, n2, m2,
                                complex_form(knm->k11[n][m]), complex_form(knm->k12[n][m]),
                                complex_form(knm->k21[n][m]), complex_form(knm->k22[n][m]));
                    }
                }
                
                current_coeff++;
                print_progress_and_time(num_coeffs, current_coeff, starttime);
            }
        }
    }

    endTimer(timer);
    
    // we now store the values used to calculate the current knm so later we can 
    // determine whether they need recalculating
    map->knm_calculated = true;
    map->_maxtem = inter.tem;
    map->_lambda = init.lambda;
    map->_nr1 = nr1;
    map->_nr2 = nr2;
    map->map_amplitude = mirror->map_amplitude;
    map->mirror_knm_q.qxt1_11 = mirror->knm_q.qxt1_11;
    map->mirror_knm_q.qxt2_11 = mirror->knm_q.qxt2_11;
    map->mirror_knm_q.qyt1_11 = mirror->knm_q.qyt1_11;
    map->mirror_knm_q.qyt2_11 = mirror->knm_q.qyt2_11;
    map->mirror_knm_q.qxt1_12 = mirror->knm_q.qxt1_12;
    map->mirror_knm_q.qxt2_12 = mirror->knm_q.qxt2_12;
    map->mirror_knm_q.qyt1_12 = mirror->knm_q.qyt1_12;
    map->mirror_knm_q.qyt2_12 = mirror->knm_q.qyt2_12;
    map->mirror_knm_q.qxt1_21 = mirror->knm_q.qxt1_21;
    map->mirror_knm_q.qxt2_21 = mirror->knm_q.qxt2_21;
    map->mirror_knm_q.qyt1_21 = mirror->knm_q.qyt1_21;
    map->mirror_knm_q.qyt2_21 = mirror->knm_q.qyt2_21;
    map->mirror_knm_q.qxt1_22 = mirror->knm_q.qxt1_22;
    map->mirror_knm_q.qxt2_22 = mirror->knm_q.qxt2_22;
    map->mirror_knm_q.qyt1_22 = mirror->knm_q.qyt1_22;
    map->mirror_knm_q.qyt2_22 = mirror->knm_q.qyt2_22;
    
    map->_x_off = mirror->x_off;
    map->_y_off = mirror->y_off;
    map->_angle = mirror->angle;
    map->_r_aperture = mirror->r_aperture;
    map->_xbeta = mirror->beta_x;
    map->_ybeta = mirror->beta_y;
    map->_abserr = abs_err;
    map->_relerr = rel_err;
    map->_intermeth = map->interpolation_method;
    map->_intersize = map->interpolation_size;
    map->_intmeth = map->integration_method;
        
    if (map->save_to_file)
        write_knm_file((void*) mirror, false, MIRROR_CMP);

    if ((!(mirror->map_merged.noMapPresent) || integrating) && mirror->map_merged.save_knm_matrices) {
        char buf[LINE_LEN];
        sprintf(buf, "%s_integrated", map->filename);
        write_mirror_knm_to_matrix_file(buf, &(mirror->knm_map));
    }
    
    // restore the knm flags
    mirror->knm_calc_flags = prev_knm_calc_flags;
}

void check_map_size(surface_map_t *map, complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2, double nr1, double nr2) {
    double w1, w2, r0x, r0y;
    double diam_beam, diam_map;

    // Some empirically derived typical sizes for beam integration
    w1 = w_size(qx1, nr1);
    w2 = w_size(qx2, nr2);
    r0x = max(sqrt(inter.tem + 0.5) * w1, sqrt(inter.tem + 0.5) * w2);

    w1 = w_size(qy1, nr1);
    w2 = w_size(qy2, nr2);
    r0y = max(sqrt(inter.tem + 0.5) * w1, sqrt(inter.tem + 0.5) * w2);

    diam_beam = 2 * max(r0x, r0y);
    //fprintf(stdout,"cols = %d, center =%g\n",map->cols, map->x0);
    r0x = min(fabs(map->cols - (map->x0 + 0.5)), fabs(map->x0 + 0.5)) * map->xstep;
    r0y = min(fabs(map->cols - (map->y0 + 0.5)), fabs(map->y0 + 0.5)) * map->ystep;
    diam_map = 2 * min(r0x, r0y);

    if (diam_map < 4 * diam_beam) {
        warn("map %s: map size (%g) is smaller than the recommended size (%g).\n",
                map->name, diam_map, 4 * diam_beam);
    }

    if (inter.debug & 64) {
        //      message("Estimated sizes: max beam diameter: %g m, min map length: %g m\n",diam_beam,diam_map);
        if (diam_map > 6 * diam_beam) {
            warn("map %s the map has a much larger physical size (%g) than the beam size (%g)\n",
                    map->name, diam_map, diam_beam);
        }
    }
}

void get_mr_acc_zc_values(KNM_MIRROR_NODE_DIRECTION_t knum, mr_knm_map_int_params_t *pmr, u_nm_accel_t **a1, u_nm_accel_t **a2, complex_t **zc){
    switch (knum) {
        case MR11:
            *a1 = pmr->acc_11_nr1_1;
            *a2 = pmr->acc_11_nr1_2;
            *zc = &pmr->znm1c1;
            break;
        case MR12:
            *a1 = pmr->acc_12_nr1_1;
            *a2 = pmr->acc_12_nr2_2;
            *zc = &pmr->znm1c2;
            break;
        case MR21:
            *a1 = pmr->acc_21_nr2_1;
            *a2 = pmr->acc_21_nr1_2;
            *zc = &pmr->znm2c1;
            break;
        case MR22:
            *a1 = pmr->acc_22_nr2_1;
            *a2 = pmr->acc_22_nr2_2;
            *zc = &pmr->znm2c2;
            break;
        default:
            bug_error("Unacceptable knum input %i in get_mr_acc_zc_values", pmr->knum);
    }
}

void dump_knm_map_integration_params(FILE *fp, mr_knm_map_int_params_t tmp) {
    // make sure the file pointer isn't null
    assert(fp != NULL);
   
    fprintf(fp, " *** knm_map_integration_param for mirror %s, map: %s\n",tmp.mirror->name, tmp.merged_map->name);
    fprintf(fp, "     n1: %d, m1: %d, n2: %d, m2: %d\n", tmp.n1, tmp.m1, tmp.n2, tmp.m2);

    dump_u_nm_accel(fp, *(tmp.acc_11_nr1_1));
    dump_u_nm_accel(fp, *(tmp.acc_11_nr1_2));
    dump_u_nm_accel(fp, *(tmp.acc_22_nr2_1));
    dump_u_nm_accel(fp, *(tmp.acc_22_nr2_2));
    dump_u_nm_accel(fp, *(tmp.acc_21_nr2_1));
    dump_u_nm_accel(fp, *(tmp.acc_21_nr1_2));
    dump_u_nm_accel(fp, *(tmp.acc_12_nr1_1));
    dump_u_nm_accel(fp, *(tmp.acc_12_nr2_2));
   
    fprintf(fp, "     nr1:%g, nr2:%g\n",tmp.nr1,tmp.nr2);
    fprintf(fp, "     A:%g, Cr:%g, Ct:%g, Br:%g, Bt=%g\n",tmp.A, tmp.Cr, tmp.Ct, tmp.Br, tmp.Bt);
    fprintf(fp, "     kr:%g, kt:%g, k0:%g\n",tmp.kr,tmp.kt, tmp.k0);
    fprintf(fp, "     xmon:%g, xmax:%g, ymin:%g, ymax:%g\n",tmp.xmin[0],tmp.xmax[0], tmp.xmin[1],tmp.xmax[1]);
    fprintf(fp, "     znm1c1:%s, znm1c2:%s, znm2c1:%s, znm2c2:%s\n",complex_form(tmp.znm1c1),complex_form(tmp.znm1c2),complex_form(tmp.znm2c1),complex_form(tmp.znm2c2));
    fprintf(fp, "     mirror node direction %d\n",tmp.knum);
    fprintf(fp, "     J1:%g, J2:%g\n",tmp.J1,tmp.J2);
    fprintf(fp, "     ap_radius:%g\n",tmp.aperture_radius);
    fprintf(fp, "     pol limits %d, usingMap %d, knm_calc_flags %d, integrating %d\n", tmp.polar_limits_used, tmp.usingMap, tmp.knm_calc_flags, tmp.integrating);
    fprintf(fp, "     betax:%g, betay:%g\n",tmp.betax,tmp.betay);
    dump_mirror_knm_q(fp, *(tmp.knm_q));
    //dump_maps(fp, tmp);
}

void dump_mirror_knm_q(FILE *fp, mirror_knm_q_t tmp) {
    fprintf(fp, "     11: qxt1:%s, qxt2:%s, qyt1:%s, qyt2:%s\n",complex_form(tmp.qxt1_11),complex_form(tmp.qxt2_11),complex_form(tmp.qyt1_11),complex_form(tmp.qyt2_11));
    fprintf(fp, "     12: qxt1:%s, qxt2:%s, qyt1:%s, qyt2:%s\n",complex_form(tmp.qxt1_12),complex_form(tmp.qxt2_12),complex_form(tmp.qyt1_12),complex_form(tmp.qyt2_12));
    fprintf(fp, "     21: qxt1:%s, qxt2:%s, qyt1:%s, qyt2:%s\n",complex_form(tmp.qxt1_21),complex_form(tmp.qxt2_21),complex_form(tmp.qyt1_21),complex_form(tmp.qyt2_21));
    fprintf(fp, "     22: qxt1:%s, qxt2:%s, qyt1:%s, qyt2:%s\n",complex_form(tmp.qxt1_22),complex_form(tmp.qxt2_22),complex_form(tmp.qyt1_22),complex_form(tmp.qyt2_22));
}

void dump_u_nm_accel(FILE *fp, u_nm_accel_t tmp){
    fprintf(fp, "     acc_n: pre %s n %d wz %g k %g k2q %s\n",complex_form(*(tmp.acc_n->prefac)),tmp.acc_n->n,tmp.acc_n->sqrt2_wz,tmp.acc_n->k,complex_form(tmp.acc_n->negK_2q));
}

void dump_maps(FILE *fp, mr_knm_map_int_params_t tmp){
    fprintf(fp, " *** Writing maps to file ...\n");
    write_map_to_file("dump_map_r_abs",  tmp.merged_map->cols, tmp.merged_map->rows,tmp.merged_map->r_abs);    
    write_map_to_file("dump_map_r_phs",  tmp.merged_map->cols, tmp.merged_map->rows,tmp.merged_map->r_phs);    
    write_map_to_file("dump_map_t_abs",  tmp.merged_map->cols, tmp.merged_map->rows,tmp.merged_map->t_abs);    
    write_map_to_file("dump_map_t_phs",  tmp.merged_map->cols, tmp.merged_map->rows,tmp.merged_map->t_phs);    
}

void write_map_to_file(char *filename, int cols, int rows, double **data){
    FILE *file = NULL;
  
    
    open_file_to_write_ascii(filename, &file);

    int i, j;

    for (i = 0; i < cols; i++) {
        for (j = 0; j < rows; j++) {
            fprintf(file, "%.15g ", data[i][j]);
        }
        fprintf(file, "\n");
    }

    fprintf(file, "\n");

    fclose(file);
}


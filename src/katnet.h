/*
	Header file for libMNet
  
  version:
  release date:
  
  M Hewitson
	
	$Id$
*/

#ifndef KAT_NET_H
#define KAT_NET_H


#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>       
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <dirent.h>
#include <fnmatch.h>
#include <netdb.h>
#include "mnet.h"

#define MINPORT 11000
#define MAXPORT 11010

/*----------------------------------------------*/
/*               from katnet.c                  */
/*----------------------------------------------*/
mstream * kat_connect(char *hostname, int port);
int kat_disconnect(mstream *s);



#endif /* KAT_NET_H */


/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

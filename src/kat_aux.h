// $Id$

/*!
 * \file kat_aux.h
 * \brief Header file for auxilliary routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_AUX_H
#define KAT_AUX_H

#include "kat_matrix_ccs.h"

int getTime(timespec_t *tp);
int getTimeHighRes(timespec_t *tp);

int handle_NICSLU(int ret);
int startTimer(const char* name);
void endTimer(int id);
void closeTimerFile();

void file_MD5(FILE *mapfile, unsigned char *hash);
void bs_get_nr(beamsplitter_t *bs, double *nr1, double *nr2);

bool is_tuned(double *var, double **source, void **rtn, int *type);
bool is_var_tuned(double *var);
int is_minimizer_tuned(void *var, void **rtn, int *type);

void add_carrier_frequency(const char *name, double f, frequency_t **freq);
void add_sideband_frequency(const char *name, frequency_t **freq, frequency_t *cfreq, double *fmod, int type, int order);
void add_user_frequency(const char *name, double f);

frequency_t* get_carrier_frequency(double frequency);
frequency_t* get_signal_frequency(double frequency);

void print_frequency_couplings(const char *name, int num_fs, int **does_f_couple, int **f_couple_order);
void print_used_frequency_list();
void update_frequency_coupling(int allocing);
void update_frequency_list();
void fill_frequency_linked_list();
void reduce_frequency_linked_list();

void get_path_io(const char* input, char* output, bool unixStyle);
const char* get_path_i(const char* input, bool unixStyle);
int system_call(const char* command);
char *replace_str(char *str, char *orig, char *rep);
int getNumCores();
int get_signal_type(char *s);
char *get_signal_type_str(int signal_type);
char *get_signal_unit_str(int component_type, int signal_type);
void check_for_long_line(FILE *fp, char *s);
int prepare_line(char *s, int mode);
void check_funcname(char *name, const char *s);
int find_equal(char *name, char *fstring);
void sort_set(void);
void sort_variable_names(void);
void insert_constants_values(char *s);
int replace_string(char *s, char *s1, char *s2, int s1len, int s2len);
int get_mirror_index_from_name(int mirror_index, char *mapname);
int get_field_index_from_tem(int n, int m);
int get_field_index_from_tem_LG(int p, int l);
int get_field_index_from_local_tem(int n, int m, int maxtem);
void set_all_tems(void);
double fac(int n);
void print_time();
void sprint_time(char *message_str, double secs);
double xdb(double x);
double hermite(int n, double x);
double dbesj0(double x);
double dbesj1(double x);
double bessn(int n, double x);
void check_name(const char *line, const char *s);
int find_name(const char *s);
void dupdump(void); //!< Dump duplicate string
char *duplicate_string(const char *s);
char *duplicate_to_longer_string(const char *s, int n);
int get_node_index_for(const char *node_name);
int update_node_index_for(const char *node_name, int direction);
void connect_component_to_node(int node_idx, void *comp, int type);
int get_reduced_index_for(int node_index);
int get_overall_component_index(int type, int component_index);
int get_function(double **function, const char *input_string);
int set_nr(const char *input_string);
int get_component_index_from_name(const char *s);
int get_component_type(int i);
int get_component_index(int i);
int get_component_type_decriment_index(int *i);
char *get_component_name(const int i);
char *get_component_type_name(const int i);
int get_node_index_from_name(const char *node_name);
int get_detector_or_node_index_from_name(const char *s);
char *get_detector_name(const int i);
int ctoi(char c, int *i);
int my_atoi(char *s, int *i);
bool atod(const char *s, double *d);
char *complex_form(const complex_t z);
char *complex_form15(const complex_t z);
char *freq_form(double f);
char *int_form(int i);
char *double_form(double x);
char *xdouble_form(double x);
char *long_form(double c, int space);
void si_prefix(double *d, char *s);
complex_t my_cpow(complex_t z, double x);
complex_t my_cexp(complex_t z);
complex_t my_clog(complex_t z);
complex_t pow_complex(complex_t z, int nom, int denom);
complex_t pow_complex_i(int k);
int pow_neg_1(int n);
int pow_two(int n);
double zeroin(double ax, double bx, double (*f) (double), double tol);
bool string_matches(char *input_string, char *match_string);
bool string_matches_exactly(const char *input_string, const char *match_string);
bool isCygserverRunning();
const char *get_comp_name2(int type, int list_index);
const char *get_comp_name(int type, void *comp);
bool string_starts_with(const char *a, const char *b);

int parse_motion_value(motion_out_t *mout, double M, double Ix, double Iy, int num_surf_motion);
int motion_string_to_index(const char *v, double M, double Ix, double Iy, int num_surf_motion);
int motion_string_to_mirror_tf(const char *v, mirror_t *m, transfer_func_t **tf);
int motion_string_to_bs_tf(const char *v, beamsplitter_t *m, transfer_func_t **tf);

int get_function_idx(char* name);

int strhash(char *key);

// Andreas, removed at the function bodies have been
// commneted out for a while 08.11.2019

//int get_node_q_from(node_t *node, int component_idx, complex_t *qx, complex_t *qy);
//int get_node_q_to(node_t *node, int component_idx, complex_t *qx, complex_t *qy);

#if OS == __WIN_32__ || OS == __WIN_64__
const char *strcasestr(const char *s1, const char *s2);
#endif

#endif // KAT_AUX_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

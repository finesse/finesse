/* 
 * File:   kat_matrix_ccs.h
 * Author: Daniel
 *
 * Created on 12 August 2013, 10:33
 */

#ifndef KAT_MATRIX_CCS_H
#define	KAT_MATRIX_CCS_H

#include "kat.h"
#include <stdint.h>

UF_long klu_zl_tsolve_sparse (klu_l_symbolic *, klu_l_numeric *, UF_long, UF_long,
    double *, UF_long, UF_long *, UF_long, klu_l_common * ) ;

UF_long klu_zl_solve_sparse (klu_l_symbolic *, klu_l_numeric *, UF_long, UF_long,
    double *, UF_long nblocks, UF_long *, klu_l_common *) ;



typedef struct pseudocolumn_item {
    int index;
    int type;
    void *ptr;
} pseudocolumn_item_t;

typedef struct pseudocolumn{
    struct pseudocolumn *next;
    
    UT_array *items;
    
    int type;
    void *component;
    
    int value_base_idx; // The index in the values array this column starts
    int nnz; // the number of elements this column takes in the value array
    
} pseudocolumn_t;

typedef struct matrix_ccs {
    double *values;   //!< sparse CCS matrix numerical values (length=2*inter.num_nonzero)
    
    // The following are void because depending on which solver we use
    // the data type varies.
    void *row_idx;    /** sparse COO matrix row index        (length=inter.num_nonzero)*/
    void *col_idx;    /** sparse COO matrix col index        (length=inter.num_nonzero)*/
    void *col_ptr;    /** sparse CCS matrix column pointer   (length=inter.num_eqns+1) */
    
    long *row_col_count; // stores how many rows per column are present
    
    // variables used to count the number of eqns and nonzeros in the matrix
    int num_eqns;
    int num_nonzeros;
} matrix_ccs_t;

typedef struct ifo_matrix_vars{
    matrix_ccs_t M;
    double *rhs_values;     //!< ride-hand-side for sparse matrix   (length=2*inter.num_eqns)
    
    pseudocolumn_t *pcol_head, *pcol_curr;
    int pcols_num;
    void *solver_opts; // struct that contains solver specific objects
    
    int type; // STANDARD or SIGNAL_QUANTUM
    
    // frequencies associated with this matrix
    int num_frequencies;
    frequency_t **frequencies;
    
    // modulators, mirrors and beamsplitters are the only component that can couple frequencies. Signal frequencies from
    // other components are handled by including them as sources to the signal sideband matrix
    
    int ***mod_f_couple_order;  // order of coupling if equivalent mod_does_f_couple is non-zero
    int ***mod_does_f_couple;   // 0 if no coupling, 1 if there is
    int ***mod_f_couple_allocd; // 0 if no memory is to be allocated 1 if it is
    
    int ***m_f_couple_order;  // order of coupling if equivalent m_does_f_couple is non-zero
    int ***m_does_f_couple;   // 0 if no coupling, 1 if there is
    int ***m_f_couple_allocd; // 0 if no memory is to be allocated 1 if it is
    
    int ***bs_f_couple_order;  // order of coupling if equivalent bs_does_f_couple is non-zero
    int ***bs_does_f_couple;   // 0 if no coupling, 1 if there is
    int ***bs_f_couple_allocd; // 0 if no memory is to be allocated 1 if it is
    
    // stores the index which a given node occupies in the RHS vector of this matrix
    // maps directly to inter.node_list[i]
    int *node_rhs_idx_1; // rhs index for port 1 of node 
    int *node_rhs_idx_2; // rhs index for port 2 of node 
    int *mirror_rhs_idx;
    int *bs_rhs_idx;
    int *modulator_rhs_idx;
    int *space_rhs_idx;
    int *diode_rhs_idx;
    int *dbs_rhs_idx;
    int *sagnac_rhs_idx;
    int *lens_rhs_idx;
    int *grating_rhs_idx;
    uint64_t *output_rhs_idx; /** For a given output index the RHS index of the port this output is attached to is stored. This must be 64 bit as for homodyne detectors we store 2 ints in one 64 bit type. */
    int *block_rhs_idx;
    int *feedback_rhs_idx;
    
    int input_num_blocks; /** Number of blocks of non-zero elements in the input array */
    long *input_blocks; /** Size 2*input_num_blocks, n=start index of non-zeros, n+1=end index of non-zeros */
} ifo_matrix_vars_t;


int __get_output_rhs_port(ifo_matrix_vars_t *matrix, node_t *node, node_connections_t *nc, bool is_second_beam, const char *name);

void get_ccs_matrix_sparse_input_blocks();
void dump_matrix_ccs(matrix_ccs_t *matrix, const char* fname);
void dump_matrix_ccs_labels(ifo_matrix_vars_t *ifo_matrix, const char* fname);

bool include_mode_coupling(coupling_info_t *clpng, int ni, int nj, int mi, int mj);

void ccs_zgemv(matrix_ccs_t *M, complex_t *x, complex_t *y);

int get_port_rhs_idx(int freq, int mode);
int get_rhs_idx(ifo_matrix_vars_t *M, node_t *node, int port, int freq, int mode);
int get_node_rhs_idx(int port, int freq, int mode, int num_freqs);
int get_comp_rhs_idx2(ifo_matrix_vars_t *matrix, int type, int comp_index);

void check_node_direction();
void alloc_ifo_matrix_ccs(long *bytes, ifo_matrix_vars_t *matrix_ccs);
void alloc_matrix_ccs_freq_couplings(ifo_matrix_vars_t *matrix_ccs);
void build_ccs_matrix(ifo_matrix_vars_t *matrix);
void get_ccs_matrix(ifo_matrix_vars_t *matrix);
void fill_ccs_matrix(ifo_matrix_vars_t *matrix);
void refactor_ccs_matrix(ifo_matrix_vars_t *matrix);
void solve_ccs_matrix(ifo_matrix_vars_t *matrix, double *rhs, bool transpose, bool conjugate);
void solve_ccs_matrix_sparse_input(ifo_matrix_vars_t *matrix, double *rhs, bool transpose, long nblocks, long *blocks);
void clear_ccs_matrix_rhs(ifo_matrix_vars_t *matrix);
void set_rhs_ccs_matrix(long idx, complex_t value, ifo_matrix_vars_t *matrix);
void free_ccs_matrix(ifo_matrix_vars_t *matrix);

void set_row_ptr(matrix_ccs_t *matrix, int ix, long val);
long get_col_ptr(void *col_ptr, int ix);
void set_col_ptr(void *col_ptr, int ix, long val);
#endif	/* KAT_MATRIX_CCS_H */


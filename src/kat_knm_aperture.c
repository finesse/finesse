#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_fortran.h"
#include "kat_optics.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_aa.h"
#include "kat_check.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include "kat_knm_bayer.h"
#include <stdlib.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_pow_int.h>
#include <gsl/gsl_sf_gamma.h>

extern init_variables_t init;
extern interferometer_t inter;

static u_nm_accel_t *acc_11_nr1_1, *acc_11_nr1_2;
static u_nm_accel_t *acc_22_nr2_1, *acc_22_nr2_2;
static u_nm_accel_t *acc_21_nr2_1, *acc_21_nr1_2;
static u_nm_accel_t *acc_12_nr1_1, *acc_12_nr2_2;

void alloc_knm_accel_mirror_aperture_mem(long *bytes) {

    // need to pick out the maximum number order for allocating memory
    int max_m=mem.hg_mode_order, max_n=mem.hg_mode_order;

    // make sure they are null before allocating new memory
    assert(acc_11_nr1_1 == NULL);
    assert(acc_11_nr1_2 == NULL);
    assert(acc_12_nr1_1 == NULL);
    assert(acc_12_nr2_2 == NULL);
    assert(acc_21_nr2_1 == NULL);
    assert(acc_21_nr1_2 == NULL);
    assert(acc_22_nr2_1 == NULL);
    assert(acc_22_nr2_2 == NULL);

    // allocate memory for all the accelerators
    acc_11_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_11_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_22_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_22_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_12_nr1_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_12_nr2_2 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_21_nr2_1 = u_nm_accel_alloc(max_n, max_m, bytes);
    acc_21_nr1_2 = u_nm_accel_alloc(max_n, max_m, bytes);
}

double C_n(int n, double x){
    assert(n>=0);
    
    switch(n){
        case 0:
            return 1.0;
        case 1:
            return 2 - x;
        case 2:
            return 6 - 6*x+x*x;
        case 3:
            return 24 - 36*x + 12*x*x - x*x*x;
        case 4:
            return 120 - 240*x + 120*x*x - 20*x*x*x + x*x*x*x;
        default:
            return -(x-2*n)*C_n(n-1, x) - n*(n-1)*C_n(n-2,x);
    }
}

double C_1_n(int n, double x){
    assert(n>=0);
    
    switch(n){
        case 1:
            return -x;
        case 2:
            return -3*x +x*x;
        case 3:
            return -12*x + 8*x*x - x*x*x;
        case 4:
            return -60*x * 60*x*x - 15*x*x*x + x*x*x*x;
        default:
            return -(x-2*n+1)*C_1_n(n-1, x) - n*(n-2)*C_1_n(n-2,x);
    }
}

double B_nmkl(int m, int n, int k, int l, double rhosq){
    if(n==0 && m==0 && k==0 && l==0){
        return 1 - exp(-rhosq);
    }else if(m==0 && n==0 && GSL_IS_EVEN(k) && GSL_IS_EVEN(l)){
        k = k/2;
        l = l/2;
        return pow_neg_1(k+l) * gsl_sf_fact(2*k)* gsl_sf_fact(2*l) / (gsl_sf_fact(k)*gsl_sf_fact(l)*gsl_sf_fact(k+l))
                * rhosq*exp(-rhosq) * C_n(k+l-1, rhosq);
    } else if(m==0 && n==1 && GSL_IS_EVEN(k) && GSL_IS_ODD(l)){
        k = k/2;
        l = (l+1)/2;
        return pow_neg_1(k+l) * gsl_sf_fact(2*k)* gsl_sf_fact(2*l) / (gsl_sf_fact(k)*gsl_sf_fact(l)*gsl_sf_fact(k+l))
                * rhosq*exp(-rhosq) * C_1_n(k+l-1, rhosq);
    } else if(m==1 && n==0 && GSL_IS_ODD(k) && GSL_IS_EVEN(l)){
        k = (k+1)/2;
        l = l/2;
        return pow_neg_1(k+l) * gsl_sf_fact(2*k)* gsl_sf_fact(2*l) / (gsl_sf_fact(k)*gsl_sf_fact(l)*gsl_sf_fact(k+l))
                * rhosq*exp(-rhosq) * C_1_n(k+l-1, rhosq);
    } else
        bug_error("Can't compute this aperture knm factor");
    
    return 0.0;
}

double G_matrix_element(int m, int n, int k, int l, double rsq){
    assert(m>=0);
    assert(n>=0);
    assert(k>=0);
    assert(l>=0);
    
    double c_nm = 1 / sqrt(gsl_sf_pow_int(2.0,n+m) * gsl_sf_fact(n)*gsl_sf_fact(m) );
    double c_lk = 1 / sqrt(gsl_sf_pow_int(2.0,l+k) * gsl_sf_fact(k)*gsl_sf_fact(l) );
    double Gnmkl = 0;
    
    if((n==0 && m==0) || (n==0&&m==1) || (n==1&&m==0))
        Gnmkl = c_nm * c_lk / PI * B_nmkl(n,m,k,l,rsq);
    else if(n > 1)
        Gnmkl = sqrt((l+1)/(double)n) * G_matrix_element(m,n-1,k,l+1,rsq)
                + sqrt(l/(double)n) * G_matrix_element(m,n-1,k,l-1,rsq)
                + sqrt((n-1)/(double)n) * G_matrix_element(m,n-2,k,l,rsq);
    else if(m > 1)    
        Gnmkl = sqrt((k+1)/(double)m) * G_matrix_element(m-1,n,k+1,l,rsq)
                + sqrt(k/(double)m) * G_matrix_element(m-1,n,k-1,l,rsq)
                + sqrt((m-1)/(double)m) * G_matrix_element(m-2,n,k,l,rsq);
    
    return Gnmkl;
        
}

void fill_mirror_knm_aperture_matrix_analytic(mirror_t *mirror, double nr1){
    
    mirror_knm_t *amap = &(mirror->knm_aperture);
    // as we will be calculating aperture knm we need to allocate the memory for it
    // remember that free also needs to be called later
    if (!IS_MIRROR_KNM_ALLOCD(amap))
        bug_error("mirror aperture knm has not been allocated");
    
    warn("EXPERIMENTAL analytic aperture calculation!!");
    
    mirror->knm_aperture.IsIdentities = false;
        
    double rhosq11 = mirror->r_aperture / w_size(mirror->knm_q.qxt1_11, nr1);
    
    int i,j,n,m,k,l;
    
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&m,&n,i);
     
        for(j=0; j<inter.num_fields; j++){
            get_tem_modes_from_field_index(&k,&l,j);
            
            mirror->knm_aperture.k11[i][j].re = G_matrix_element(m,n,k,l,rhosq11);
        }
    }    
}


complex_t __compute_HG_square(int n, int _n, double R, u_n_accel_t *a1, u_n_accel_t *a2) {
    assert(a1 != NULL);
    assert(a2 != NULL);
    assert(a1->sqrt2_wz == a2->sqrt2_wz);
    assert(R > 0);
    
    int p;
    
    complex_t k = z_by_x(z_by_zc(a1->prefac[n], a2->prefac[_n]), 1.0 / a1->sqrt2_wz);
    
    // rescale R for integration limits
    R *= a1->sqrt2_wz;
    
    double expR = exp(-R*R);
    double sum = 0, tmp = 0;
    
    for (p = 0; p <= min(n, _n); p++) {
        int _p = n + _n - 2*p - 1;
        
        if(_p+1 == 0) {
            tmp = SQRTPI * erf(R);
        } else if((_p+1) % 2 == 1) {
            tmp = 0;
        } else {
            tmp = 2 * hermite(_p, 0) - expR * (hermite(_p, R) - hermite(_p, -R));
        }
        
        sum += tmp * pow_two(p) * fac(p) * gsl_sf_choose(n, p) * gsl_sf_choose(_n, p);
    }
    
    return z_by_x(k, sum);
}

void fill_mirror_knm_square_aperture(mirror_t *mirror, double nr1, double nr2) {
    
    int max_m, max_n;
    get_tem_modes_from_field_index(&max_n, &max_m, inter.num_fields - 1);
    max_m = max_n; // max m is no the m is also the max_n. e.g. we get n=1 m=2 for maxtem 2

    mirror_knm_q_t *kq = &(mirror->knm_q);
            
    if (CALC_MR_KNM(mirror, 11)) {
        u_nm_accel_get(acc_11_nr1_1, max_n, max_m, kq->qxt1_11, kq->qyt1_11, nr1);
        u_nm_accel_get(acc_11_nr1_2, max_n, max_m, kq->qxt2_11, kq->qyt2_11, nr1);
    }
    
    if (CALC_MR_KNM(mirror, 22)) {
        u_nm_accel_get(acc_22_nr2_1, max_n, max_m, kq->qxt1_22, kq->qyt1_22, nr2);
        u_nm_accel_get(acc_22_nr2_2, max_n, max_m, kq->qxt2_22, kq->qyt2_22, nr2);
    }
    
    if (CALC_MR_KNM(mirror, 12)) {
        u_nm_accel_get(acc_12_nr1_1, max_n, max_m, kq->qxt1_12, kq->qyt1_12, nr2);
        u_nm_accel_get(acc_12_nr2_2, max_n, max_m, kq->qxt2_12, kq->qyt2_12, nr2);
    }
    
    if (CALC_MR_KNM(mirror, 21)) {
        u_nm_accel_get(acc_21_nr2_1, max_n, max_m, kq->qxt1_21, kq->qyt1_21, nr1);
        u_nm_accel_get(acc_21_nr1_2, max_n, max_m, kq->qxt2_21, kq->qyt2_21, nr1);
    }
    
    mirror_knm_t *amap = &(mirror->knm_aperture);
    
    // as we will be calculating aperture knm we need to allocate the memory for it
    // remember that free also needs to be called later
    if (!IS_MIRROR_KNM_ALLOCD(amap))
        bug_error("mirror aperture knm has not been allocated");
    
    mirror->knm_aperture.IsIdentities = false;
     
    int i,j,n,m,k,l;
    
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&n,&m,i);
     
        for(j=0; j<inter.num_fields; j++){
            get_tem_modes_from_field_index(&k,&l,j);
            
            if (CALC_MR_KNM(mirror, 11)) {
                mirror->knm_aperture.k11[i][j] = z_by_z(__compute_HG_square(n, k, mirror->r_aperture, acc_11_nr1_1->acc_n, acc_11_nr1_2->acc_n),
                                                    __compute_HG_square(m, l, mirror->r_aperture, acc_11_nr1_1->acc_m, acc_11_nr1_2->acc_m));
            }
            
            if (CALC_MR_KNM(mirror, 12)) {
                mirror->knm_aperture.k12[i][j] = z_by_z(__compute_HG_square(n, k, mirror->r_aperture, acc_12_nr1_1->acc_n, acc_12_nr2_2->acc_n),
                                                    __compute_HG_square(m, l, mirror->r_aperture, acc_12_nr1_1->acc_m, acc_12_nr2_2->acc_m));
            }
            
            if (CALC_MR_KNM(mirror, 21)) {
                mirror->knm_aperture.k21[i][j] = z_by_z(__compute_HG_square(n, k, mirror->r_aperture, acc_21_nr2_1->acc_n, acc_21_nr1_2->acc_n),
                                                    __compute_HG_square(m, l, mirror->r_aperture, acc_21_nr2_1->acc_m, acc_21_nr1_2->acc_m));
            }
            
            if (CALC_MR_KNM(mirror, 22)) {
                mirror->knm_aperture.k22[i][j] = z_by_z(__compute_HG_square(n, k, mirror->r_aperture, acc_22_nr2_1->acc_n, acc_22_nr2_2->acc_n),
                                                    __compute_HG_square(m, l, mirror->r_aperture, acc_22_nr2_1->acc_m, acc_22_nr2_2->acc_m));
            }
        }
    }    
}

// $Id$

/*!
 * \file kat_spa.c
 * \brief Sparse matrix library interface routines.
 *
 * These routines also set up the interferometer matrix.
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_inline.c"
#include "kat_spa.h"
#include "kat_calc.h"
#include "klu.h"
#include "kat_check.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_matrix_ccs.h"
#include "kat_optics.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include "kat_aa.h"
#include "kat_quant.h"

#include <gsl/gsl_cblas.h>

const complex_t complex_i = {0.0, 1.0}; //!<  sqrt(-1) or 0 + i
const complex_t complex_mi = {0.0, -1.0}; //!<  sqrt(-1) or 0 - i
const complex_t complex_1 = {1.0, 0.0}; //!<  1 in complex space: 1 + 0i
const complex_t complex_m1 = {-1.0, 0.0}; //!< -1 in complex space: -1 + 0i
const complex_t complex_0 = {0.0, 0.0}; //!<  0 in complex space: 0 + 0i

//extern init_variables_t in;
extern interferometer_t inter;
extern options_t options;
extern memory_t mem;
extern klu_sparse_var_t klu;

extern ifo_matrix_vars_t M_ifo_car;
extern ifo_matrix_vars_t M_ifo_sig;

extern complex_t *quant_s;

extern int *c_s; //!< Frequency value list [frequency]
extern double *f_s; //!< Frequency value list [frequency]
extern complex_t ***a_s; //!< Amplitude list [field] [output] [frequency]
extern int *t_s; //!< Type_of_Signal list [frequency]

int nijlist1; //!< One list of nij values
int nijlist2; //!< Another list of nij values

extern complex_t *car_ws; // Workspace for carrier computations for rad pressure effects


//! Solve a sparse matrix

/*!
 * \param matrix_type the type of matrix to solve (standard or quantum)
 */
void solve_matrix(int matrix_type) {
    int tid = startTimer("SOLVE_MATRIX");
    // sanity checks on input
    assert(matrix_type == STANDARD || matrix_type == SIGNAL_QCORR);

    if(matrix_type == STANDARD)
        solve_ccs_matrix(&M_ifo_car, M_ifo_car.rhs_values, false, false);
    else if(matrix_type == SIGNAL_QCORR)
        solve_ccs_matrix(&M_ifo_sig, M_ifo_sig.rhs_values, false, false);
    else
        bug_error("matrix_type not handled");
    endTimer(tid);
}


/**
 * Fills in the reflected q values for a beamsplitter for knm computations.
 * This does not fill in transmitted values as it is currently only used for
 * reflections of surface motions.
 * 
 * @param bs beamsplitter in question
 * @param knm_q struct to fill
 */
void get_refl_q_in_out_bs(beamsplitter_t *bs, bs_knm_q_t *knm_q){
    int component = get_overall_component_index(BEAMSPLITTER, bs->comp_index);
    
    node_t *node1 = &inter.node_list[bs->node1_index];
    node_t *node2 = &inter.node_list[bs->node2_index];
    node_t *node3 = &inter.node_list[bs->node3_index];
    node_t *node4 = &inter.node_list[bs->node4_index];
    
    double nr1=1, nr2=1;
    bs_get_nr(bs, &nr1, &nr2);
    
    complex_t qx1, qx2, qy1, qy2;
    complex_t qx3, qx4, qy3, qy4;
    
    if (NOT node1->gnd_node) {
        if (node1->traced_from_component_index == component) {
            // reverse q so that beam comes _to_ the bs from node1
            qx1 = cminus(cconj(node1->qx));
            qy1 = cminus(cconj(node1->qy));
        } else {
            qx1 = node1->qx;
            qy1 = node1->qy;
        }
    }else{
        qx1 = complex_0;
        qy1 = complex_0;
    }

    if (NOT node2->gnd_node) {
        if (node2->traced_from_component_index == component) {
            qx2 = node2->qx;
            qy2 = node2->qy;
        } else {
            // reverse q so that beam goes _from_ the bs to node2
            qx2 = cminus(cconj(node2->qx));
            qy2 = cminus(cconj(node2->qy));
        }
    }else{
        qx2 = complex_0;
        qy2 = complex_0;
    }

    if (NOT node3->gnd_node) {
        if (node3->traced_from_component_index == component) {
            // reverse q so that beam comes _to_ the bs from node3
            qx3 = cminus(cconj(node3->qx));
            qy3 = cminus(cconj(node3->qy));
        } else {
            qx3 = node3->qx;
            qy3 = node3->qy;
        }
    }else{
        qx3 = complex_0;
        qy3 = complex_0;
    }

    if (NOT node4->gnd_node) {
        if (node4->traced_from_component_index == component) {
            qx4 = node4->qx;
            qy4 = node4->qy;
        } else {
            // reverse q so that beam goes _from_ the bs to node4
            qx4 = cminus(cconj(node4->qx));
            qy4 = cminus(cconj(node4->qy));
        }
    }else{
        qx4 = complex_0;
        qy4 = complex_0;
    }
    
    // calculate the individual q values for each KNM and if we should even
    // bother calculating them    
    if (NOT node1->gnd_node && NOT node2->gnd_node) {
        
        calculate_bs_qt_qt2(BS12, knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                            bs->node1_index, bs->node2_index, bs->node3_index, bs->node4_index, component);
        calculate_bs_qt_qt2(BS21, knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                            bs->node1_index, bs->node2_index, bs->node3_index, bs->node4_index, component);
    } else {
        knm_q->qxt1_12 = complex_0;
        knm_q->qyt1_12 = complex_0;
        knm_q->qxt2_12 = complex_0;
        knm_q->qyt2_12 = complex_0;
        knm_q->qxt1_21 = complex_0;
        knm_q->qyt1_21 = complex_0;
        knm_q->qxt2_21 = complex_0;
        knm_q->qyt2_21 = complex_0;
    }
    
    if (NOT node3->gnd_node && NOT node4->gnd_node) {
        calculate_bs_qt_qt2(BS34, knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                            bs->node1_index, bs->node2_index, bs->node3_index, bs->node4_index, component);
        calculate_bs_qt_qt2(BS43, knm_q, qx1, qy1, qx2, qy2, qx3, qy3, qx4, qy4, nr1, nr2,
                            bs->node1_index, bs->node2_index, bs->node3_index, bs->node4_index, component);
    } else {
        knm_q->qxt1_34 = complex_0;
        knm_q->qyt1_34 = complex_0;
        knm_q->qxt2_34 = complex_0;
        knm_q->qyt2_34 = complex_0;
        knm_q->qxt1_43 = complex_0;
        knm_q->qyt1_43 = complex_0;
        knm_q->qxt2_43 = complex_0;
        knm_q->qyt2_43 = complex_0;
    }
}

void get_refl_q_in_out_mirror(mirror_t *mirror, mirror_knm_q_t *knm_q){
    
    int component_index = get_overall_component_index(MIRROR, mirror->comp_index);
    
    complex_t qx1, qx2, qy1, qy2;
    node_t *n1 = &inter.node_list[mirror->node1_index];
    node_t *n2 = &inter.node_list[mirror->node2_index];
    
    if (NOT n1->gnd_node) {        
        if (n1->traced_from_component_index == mirror->comp_index) {
            // reverse q so that beam comes _to_ the mirror from node1
            qx1 = cminus(cconj(n1->qx));
            qy1 = cminus(cconj(n1->qy));
        } else {
            qx1 = n1->qx;
            qy1 = n1->qy;
        }
    }else{
        qx1.re = 0;
        qx1.im = 0;
        qy1.re = 0;
        qy1.im = 0;
    }

    if (NOT n2->gnd_node) {        
        if (n2->traced_from_component_index == component_index) {
            qx2 = n2->qx;
            qy2 = n2->qy;
        } else {
            // reverse q so that beam goes _from_ the mirror to node2,
            // this requires to use -beta for the reflection node2 -> node2 
            qx2 = cminus(cconj(n2->qx));
            qy2 = cminus(cconj(n2->qy));
        }
    }else{
        qx2.re = 0;
        qx2.im = 0;
        qy2.re = 0;
        qy2.im = 0;
    }
    
    double nr1 = (n1->n) ? *n1->n : 0;
    double nr2 = (n2->n) ? *n2->n : 0;
    
    // calculate the individual q values for each KNM and if we should even
    // bother calculating them    
    if (NOT n1->gnd_node) {
        calculate_mirror_qt_qt2(MR11, knm_q, qx1, qy1, qx2, qy2, nr1, nr2, n1->list_index, n2->list_index, component_index);
    } else {
        knm_q->qxt1_11 = complex_0;
        knm_q->qxt2_11 = complex_0;
        knm_q->qyt1_11 = complex_0;
        knm_q->qyt2_11 = complex_0;
    }

    if (NOT n2->gnd_node) {        
        calculate_mirror_qt_qt2(MR22, knm_q, qx1, qy1, qx2, qy2, nr1, nr2, n1->list_index, n2->list_index, component_index);
    } else {
        knm_q->qxt1_22 = complex_0;
        knm_q->qxt2_22 = complex_0;
        knm_q->qyt1_22 = complex_0;
        knm_q->qyt2_22 = complex_0;
    }
}

void fill_mirror_rot_motion_to_field(frequency_t *fcar, mirror_t *mirror, complex_t factor_x_a,
                              complex_t ***x_a1, complex_t ***x_a2, complex_t ***y_a1,
                              complex_t ***y_a2, complex_t *ac_1i, complex_t *ac_2i,
                              complex_t tuning1u, complex_t tuning1l, complex_t tuning2u, complex_t tuning2l){
    int lidx, uidx;
    
    // get lower and upper sideband index
    int slidx = fcar->sig_lower->index; 
    int suidx = fcar->sig_upper->index;
    
    node_t *n1 = &inter.node_list[mirror->node1_index];
    node_t *n2 = &inter.node_list[mirror->node2_index];
        
    mirror_knm_q_t knm_q = {{0}};
    
    get_refl_q_in_out_mirror(mirror, &knm_q);
                
    double nr1 = (n1->n) ? *n1->n : 1;
    double nr2 = (n2->n) ? *n2->n : 1;
    
    // we now compute the knmx/y coupling coefficient, note the q value we use here is q1' as this
    // is the expansion parameter used when separating all the coupling matrices so that later we multiply
    // Knmx from the left not the right.
    complex_t knmx1 = (!n1->gnd_node && mirror->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_11, nr1), gouy(knm_q.qxt1_11)) : complex_0;
    complex_t knmx2 = (!n2->gnd_node && mirror->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_22, nr1), gouy(knm_q.qxt1_22)) : complex_0;
    complex_t knmy1 = (!n1->gnd_node && mirror->Iy > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qyt1_11, nr2), gouy(knm_q.qyt1_11)) : complex_0;
    // extra minus sign here makes the result agree with mirror tilting from before.
    // not sure of the exact origin though...
    complex_t knmy2 = (!n2->gnd_node && mirror->Iy > 0) ? z_by_xphr(complex_1,  -0.5 * w_size(knm_q.qyt1_22, nr2), gouy(knm_q.qyt1_22)) : complex_0;
    
    int i, j, u, v, u_, v_;
        
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&u, &v, i);
        
        if(mirror->Ix > 0){
            if(!n1->gnd_node){
                *x_a1[suidx][i] = complex_0;
                *x_a1[slidx][i] = complex_0;
            }
            if(!n2->gnd_node){
                *x_a2[suidx][i] = complex_0;
                *x_a2[slidx][i] = complex_0;
            }
        }
        
        if(mirror->Iy > 0){
            if(!n1->gnd_node){
                *y_a1[suidx][i] = complex_0;
                *y_a1[slidx][i] = complex_0;
            }
            if(!n2->gnd_node){
                *y_a2[suidx][i] = complex_0;
                *y_a2[slidx][i] = complex_0;
            }
        }
        
        for(j=0; j<inter.num_fields; j++){
            get_tem_modes_from_field_index(&u_, &v_, j);
            
            complex_t knmx_total_1 = {0,0};
            complex_t knmx_total_2 = {0,0};
            complex_t knmy_total_1 = {0,0};
            complex_t knmy_total_2 = {0,0};
            
            // Here the two elements of the 
            if(mirror->Ix > 0){
                if(v == v_){
                    
                    if(u > 0) { 
                        lidx = get_field_index_from_tem(u-1, v);
                        // conjugate times knm_no_rgouy is to match up with previous results when no moment of inertia is present.
                        // Only tested against with a tilt applied to the mirror and a signal also applied
                        if(!n1->gnd_node) z_inc_z(&knmx_total_1, z_by_zc(z_by_x(knmx1, sqrt(u)), mirror->knm_no_rgouy.k11[lidx][j]));
                        if(!n2->gnd_node) z_inc_z(&knmx_total_2, z_by_zc(z_by_x(knmx2, sqrt(u)), mirror->knm_no_rgouy.k22[lidx][j]));
                    }

                    if(get_field_index_from_tem(u+1, v) < inter.num_fields && u+v+1 <= inter.tem) {
                        uidx = get_field_index_from_tem(u+1, v);
                        // conjugate times knm_no_rgouy is to match up with previous results when no moment of inertia is present.
                        // Only tested against with a tilt applied to the mirror and a signal also applied
                        if(!n1->gnd_node) z_inc_z(&knmx_total_1, zc_by_zc(z_by_x(knmx1, sqrt(u+1)), mirror->knm_no_rgouy.k11[uidx][j]));
                        if(!n2->gnd_node) z_inc_z(&knmx_total_2, zc_by_zc(z_by_x(knmx2, sqrt(u+1)), mirror->knm_no_rgouy.k22[uidx][j]));
                    }
                    
                    if(!n1->gnd_node){
                        complex_t tmp1 = z_by_z(ac_1i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_1, u, v, u_, v_, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11)));
                        z_inc_z(x_a1[suidx][i], tmp1);
                        z_inc_z(x_a1[slidx][i], cconj(tmp1));
                    }
                    
                    if(!n2->gnd_node){
                        complex_t tmp2 = z_by_z(ac_2i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_2, u, v, u_, v_, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22)));
                        z_inc_z(x_a2[suidx][i], tmp2);
                        z_inc_z(x_a2[slidx][i], cconj(tmp2));
                    }
                }
            }
            
            if(mirror->Iy > 0){
                if(v > 0) { 
                    lidx = get_field_index_from_tem(u, v-1);
                    if(!n1->gnd_node) z_inc_z(&knmy_total_1, z_by_z(z_by_x(knmy1, sqrt(v)), mirror->knm_no_rgouy.k11[lidx][j]));
                    if(!n2->gnd_node) z_inc_z(&knmy_total_2, z_by_z(z_by_x(knmy2, sqrt(v)), mirror->knm_no_rgouy.k22[lidx][j]));
                }

                if(get_field_index_from_tem(u, v+1) < inter.num_fields && u+v+1 <= inter.tem) {
                    uidx = get_field_index_from_tem(u, v+1);
                    if(!n1->gnd_node) z_inc_z(&knmy_total_1, zc_by_z(z_by_x(knmy1, sqrt(v+1)), mirror->knm_no_rgouy.k11[uidx][j]));
                    if(!n2->gnd_node) z_inc_z(&knmy_total_2, zc_by_z(z_by_x(knmy2, sqrt(v+1)), mirror->knm_no_rgouy.k22[uidx][j]));
                }
                
                if(!n1->gnd_node){
                    complex_t tmp1 = z_by_z(ac_1i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_1, u, v, u_, v_, knm_q.qxt1_11, knm_q.qxt2_11, knm_q.qyt1_11, knm_q.qyt2_11)));
                    z_inc_z(y_a1[suidx][i], tmp1);
                    z_inc_z(y_a1[slidx][i], cconj(tmp1));
                }
                
                if(!n1->gnd_node) {
                    complex_t tmp2 = z_by_z(ac_2i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_2, u, v, u_, v_, knm_q.qxt1_22, knm_q.qxt2_22, knm_q.qyt1_22, knm_q.qyt2_22)));
                    z_inc_z(y_a2[suidx][i], tmp2);
                    z_inc_z(y_a2[slidx][i], cconj(tmp2));
                }
            }
        }
        
        // check if there is any tuning that needs to be applied
        if(mirror->phi != 0.0) {
            if(!n1->gnd_node) {
                if(mirror->Ix > 0) {
                    *x_a1[suidx][i] = z_by_z( *x_a1[suidx][i], tuning1u);
                    *x_a1[slidx][i] = z_by_zc( *x_a1[slidx][i], tuning1l);
                }
                if(mirror->Iy > 0) {
                    *y_a1[suidx][i] = z_by_z( *y_a1[suidx][i], tuning1u);
                    *y_a1[slidx][i] = z_by_zc( *y_a1[slidx][i], tuning1l);
                }
            }
            if(!n2->gnd_node) {
                if(mirror->Ix > 0) {
                    *x_a2[suidx][i] = z_by_z( *x_a2[suidx][i], tuning2u);
                    *x_a2[slidx][i] = z_by_zc( *x_a2[slidx][i], tuning2l);
                }
                if(mirror->Iy > 0) {
                    *y_a2[suidx][i] = z_by_z( *y_a2[suidx][i], tuning2u);
                    *y_a2[slidx][i] = z_by_zc( *y_a2[slidx][i], tuning2l);
                }
            }
        }
    }
}

void fill_bs_rot_motion_to_field(frequency_t *fcar, beamsplitter_t *bs, complex_t factor_x_a,
                              complex_t ***x_a1, complex_t ***x_a2, complex_t ***y_a1, complex_t ***y_a2,
                              complex_t ***x_a3, complex_t ***x_a4, complex_t ***y_a3, complex_t ***y_a4,
                              complex_t *ac_1i, complex_t *ac_2i, complex_t *ac_3i, complex_t *ac_4i,
                              complex_t tuning1u, complex_t tuning1l, complex_t tuning2u, complex_t tuning2l,
                              complex_t tuning3u, complex_t tuning3l, complex_t tuning4u, complex_t tuning4l) {
    int lidx, uidx;
    
    // get lower and upper sideband index
    int slidx = fcar->sig_lower->index; 
    int suidx = fcar->sig_upper->index;
    
    node_t *n1 = &inter.node_list[bs->node1_index];
    node_t *n2 = &inter.node_list[bs->node2_index];
    node_t *n3 = &inter.node_list[bs->node3_index];
    node_t *n4 = &inter.node_list[bs->node4_index];
        
    bs_knm_q_t knm_q = {{0}};
    
    get_refl_q_in_out_bs(bs, &knm_q);
                
    double nr1 = (n1->n) ? *n1->n : 1;
    double nr2 = (n3->n) ? *n3->n : 1;
    
    // we now compute the knmx/y coupling coefficient, note the q value we use here is q1' as this
    // is the expansion parameter used when separating all the coupling matrices so that later we multiply
    // Knmx from the left not the right.
    complex_t knmx1 = (!n1->gnd_node && bs->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_21, nr1), gouy(knm_q.qxt1_21)) : complex_0;
    complex_t knmy1 = (!n1->gnd_node && bs->Iy > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qyt1_21, nr1), gouy(knm_q.qyt1_21)) : complex_0;
    complex_t knmx2 = (!n2->gnd_node && bs->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_12, nr1), gouy(knm_q.qxt1_12)) : complex_0;
    complex_t knmy2 = (!n2->gnd_node && bs->Iy > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qyt1_12, nr1), gouy(knm_q.qyt1_12)) : complex_0;
    
    complex_t knmx3 = (!n3->gnd_node && bs->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_43, nr2), gouy(knm_q.qxt1_43)) : complex_0;
    complex_t knmx4 = (!n4->gnd_node && bs->Ix > 0) ? z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_34, nr2), gouy(knm_q.qxt1_34)) : complex_0;
    // extra minus sign here makes the result agree with mirror tilting from before.
    // not sure of the exact origin though...
    complex_t knmy3 = (!n3->gnd_node && bs->Iy > 0) ? z_by_xphr(complex_1,  -0.5 * w_size(knm_q.qyt1_43, nr2), gouy(knm_q.qyt1_43)) : complex_0;
    complex_t knmy4 = (!n4->gnd_node && bs->Iy > 0) ? z_by_xphr(complex_1,  -0.5 * w_size(knm_q.qyt1_34, nr2), gouy(knm_q.qyt1_34)) : complex_0;
    
    int i, j, u, v, u_, v_;
        
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&u, &v, i);
        
        if(bs->Ix > 0){
            if(!n1->gnd_node){
                *x_a1[suidx][i] = complex_0;
                *x_a1[slidx][i] = complex_0;
            }
            if(!n2->gnd_node){
                *x_a2[suidx][i] = complex_0;
                *x_a2[slidx][i] = complex_0;
            }
            if(!n3->gnd_node){
                *x_a3[suidx][i] = complex_0;
                *x_a3[slidx][i] = complex_0;
            }
            if(!n4->gnd_node){
                *x_a4[suidx][i] = complex_0;
                *x_a4[slidx][i] = complex_0;
            }
        }
        
        if(bs->Iy > 0){
            if(!n1->gnd_node){
                *y_a1[suidx][i] = complex_0;
                *y_a1[slidx][i] = complex_0;
            }
            if(!n2->gnd_node){
                *y_a2[suidx][i] = complex_0;
                *y_a2[slidx][i] = complex_0;
            }
            if(!n3->gnd_node){
                *y_a3[suidx][i] = complex_0;
                *y_a3[slidx][i] = complex_0;
            }
            if(!n4->gnd_node){
                *y_a4[suidx][i] = complex_0;
                *y_a4[slidx][i] = complex_0;
            }
        }
        
        for(j=0; j<inter.num_fields; j++){
            get_tem_modes_from_field_index(&u_, &v_, j);
            
            complex_t knmx_total_1 = {0,0};
            complex_t knmx_total_2 = {0,0};
            complex_t knmy_total_1 = {0,0};
            complex_t knmy_total_2 = {0,0};
            complex_t knmx_total_3 = {0,0};
            complex_t knmx_total_4 = {0,0};
            complex_t knmy_total_3 = {0,0};
            complex_t knmy_total_4 = {0,0};
            
            // Here the two elements of the 
            if(bs->Ix > 0){
                if(v == v_){
                    
                    if(u > 0) { 
                        lidx = get_field_index_from_tem(u-1, v);
                        // conjugate times knm_no_rgouy is to match up with previous results when no moment of inertia is present.
                        // Only tested against with a tilt applied to the mirror and a signal also applied
                        if(!n1->gnd_node) z_inc_z(&knmx_total_1, z_by_zc(z_by_x(knmx1, sqrt(u)), bs->knm_no_rgouy.k21[lidx][j]));
                        if(!n2->gnd_node) z_inc_z(&knmx_total_2, z_by_zc(z_by_x(knmx2, sqrt(u)), bs->knm_no_rgouy.k12[lidx][j]));
                        if(!n3->gnd_node) z_inc_z(&knmx_total_3, z_by_zc(z_by_x(knmx3, sqrt(u)), bs->knm_no_rgouy.k43[lidx][j]));
                        if(!n4->gnd_node) z_inc_z(&knmx_total_4, z_by_zc(z_by_x(knmx4, sqrt(u)), bs->knm_no_rgouy.k34[lidx][j]));
                    }

                    if(get_field_index_from_tem(u+1, v) < inter.num_fields && u+v+1 <= inter.tem) {
                        uidx = get_field_index_from_tem(u+1, v);
                        // conjugate times knm_no_rgouy is to match up with previous results when no moment of inertia is present.
                        // Only tested against with a tilt applied to the mirror and a signal also applied
                        if(!n1->gnd_node) z_inc_z(&knmx_total_1, zc_by_zc(z_by_x(knmx1, sqrt(u+1)), bs->knm_no_rgouy.k21[uidx][j]));
                        if(!n2->gnd_node) z_inc_z(&knmx_total_2, zc_by_zc(z_by_x(knmx2, sqrt(u+1)), bs->knm_no_rgouy.k12[uidx][j]));
                        if(!n3->gnd_node) z_inc_z(&knmx_total_3, zc_by_zc(z_by_x(knmx3, sqrt(u+1)), bs->knm_no_rgouy.k43[uidx][j]));
                        if(!n4->gnd_node) z_inc_z(&knmx_total_4, zc_by_zc(z_by_x(knmx4, sqrt(u+1)), bs->knm_no_rgouy.k34[uidx][j]));
                    }
                    
                    if(!n1->gnd_node && !n2->gnd_node){
                        complex_t tmp1 = z_by_z(ac_2i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_1, u, v, u_, v_, knm_q.qxt1_21, knm_q.qxt2_21, knm_q.qyt1_21, knm_q.qyt2_21)));
                        z_inc_z(x_a1[suidx][i], tmp1);
                        z_inc_z(x_a1[slidx][i], cconj(tmp1));
                        
                        complex_t tmp2 = z_by_z(ac_1i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_2, u, v, u_, v_, knm_q.qxt1_12, knm_q.qxt2_12, knm_q.qyt1_12, knm_q.qyt2_12)));
                        z_inc_z(x_a2[suidx][i], tmp2);
                        z_inc_z(x_a2[slidx][i], cconj(tmp2));
                    }
                    
                    if(!n3->gnd_node && !n4->gnd_node){
                        complex_t tmp3 = z_by_z(ac_4i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_3, u, v, u_, v_, knm_q.qxt1_43, knm_q.qxt2_43, knm_q.qyt1_43, knm_q.qyt2_43)));
                        z_inc_z(x_a3[suidx][i], tmp3);
                        z_inc_z(x_a3[slidx][i], cconj(tmp3)); 
                        
                        complex_t tmp4 = z_by_z(ac_3i[j], z_by_z(factor_x_a, rev_gouy(knmx_total_4, u, v, u_, v_, knm_q.qxt1_34, knm_q.qxt2_34, knm_q.qyt1_34, knm_q.qyt2_34)));
                        z_inc_z(x_a4[suidx][i], tmp4);
                        z_inc_z(x_a4[slidx][i], cconj(tmp4));
                    }
                }
            }
            
            if(bs->Iy > 0){
                if(v > 0) { 
                    lidx = get_field_index_from_tem(u, v-1);
                    if(!n1->gnd_node) z_inc_z(&knmy_total_1, z_by_z(z_by_x(knmy1, sqrt(v)), bs->knm_no_rgouy.k21[lidx][j]));
                    if(!n2->gnd_node) z_inc_z(&knmy_total_2, z_by_z(z_by_x(knmy2, sqrt(v)), bs->knm_no_rgouy.k12[lidx][j]));
                    if(!n3->gnd_node) z_inc_z(&knmy_total_3, z_by_z(z_by_x(knmy3, sqrt(v)), bs->knm_no_rgouy.k43[lidx][j]));
                    if(!n4->gnd_node) z_inc_z(&knmy_total_4, z_by_z(z_by_x(knmy4, sqrt(v)), bs->knm_no_rgouy.k34[lidx][j]));
                    
                }

                if(get_field_index_from_tem(u, v+1) < inter.num_fields && u+v+1 <= inter.tem) {
                    uidx = get_field_index_from_tem(u, v+1);
                    if(!n1->gnd_node) z_inc_z(&knmy_total_1, zc_by_z(z_by_x(knmy1, sqrt(v+1)), bs->knm_no_rgouy.k21[uidx][j]));
                    if(!n2->gnd_node) z_inc_z(&knmy_total_2, zc_by_z(z_by_x(knmy2, sqrt(v+1)), bs->knm_no_rgouy.k12[uidx][j]));
                    if(!n3->gnd_node) z_inc_z(&knmy_total_3, zc_by_z(z_by_x(knmy3, sqrt(v+1)), bs->knm_no_rgouy.k43[uidx][j]));
                    if(!n4->gnd_node) z_inc_z(&knmy_total_4, zc_by_z(z_by_x(knmy4, sqrt(v+1)), bs->knm_no_rgouy.k34[uidx][j]));
                }
                
                if(!n2->gnd_node && !n1->gnd_node){
                    complex_t tmp1 = z_by_z(ac_2i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_1, u, v, u_, v_, knm_q.qxt1_21, knm_q.qxt2_21, knm_q.qyt1_21, knm_q.qyt2_21)));
                    z_inc_z(y_a1[suidx][i], tmp1);
                    z_inc_z(y_a1[slidx][i], cconj(tmp1));
                    
                    complex_t tmp2 = z_by_z(ac_1i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_2, u, v, u_, v_, knm_q.qxt1_12, knm_q.qxt2_12, knm_q.qyt1_12, knm_q.qyt2_12)));
                    z_inc_z(y_a2[suidx][i], tmp2);
                    z_inc_z(y_a2[slidx][i], cconj(tmp2));
                }
                
                if(!n4->gnd_node && !n3->gnd_node) {
                    complex_t tmp3 = z_by_z(ac_4i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_3, u, v, u_, v_, knm_q.qxt1_43, knm_q.qxt2_43, knm_q.qyt1_43, knm_q.qyt2_43)));
                    z_inc_z(y_a3[suidx][i], tmp3);
                    z_inc_z(y_a3[slidx][i], cconj(tmp3)); 
                    complex_t tmp4 = z_by_z(ac_3i[j], z_by_z(factor_x_a, rev_gouy(knmy_total_4, u, v, u_, v_, knm_q.qxt1_34, knm_q.qxt2_34, knm_q.qyt1_34, knm_q.qyt2_34)));
                    z_inc_z(y_a4[suidx][i], tmp4);
                    z_inc_z(y_a4[slidx][i], cconj(tmp4));
                }
            }
        }
        
        // check if there is any tuning that needs to be applied
        if(bs->phi != 0.0) {
            if(!n1->gnd_node) {
                if(bs->Ix > 0) {
                    *x_a1[suidx][i] = z_by_z( *x_a1[suidx][i], tuning1u);
                    *x_a1[slidx][i] = z_by_zc( *x_a1[slidx][i], tuning1l);
                }
                if(bs->Iy > 0) {
                    *y_a1[suidx][i] = z_by_z( *y_a1[suidx][i], tuning1u);
                    *y_a1[slidx][i] = z_by_zc( *y_a1[slidx][i], tuning1l);
                }
            }
            if(!n2->gnd_node) {
                if(bs->Ix > 0) {
                    *x_a2[suidx][i] = z_by_z( *x_a2[suidx][i], tuning2u);
                    *x_a2[slidx][i] = z_by_zc( *x_a2[slidx][i], tuning2l);
                }
                if(bs->Iy > 0) {
                    *y_a2[suidx][i] = z_by_z( *y_a2[suidx][i], tuning2u);
                    *y_a2[slidx][i] = z_by_zc( *y_a2[slidx][i], tuning2l);
                }
            }
            if(!n3->gnd_node) {
                if(bs->Ix > 0) {
                    *x_a3[suidx][i] = z_by_z( *x_a3[suidx][i], tuning3u);
                    *x_a3[slidx][i] = z_by_zc( *x_a3[slidx][i], tuning3l);
                }
                if(bs->Iy > 0) {
                    *y_a3[suidx][i] = z_by_z( *y_a3[suidx][i], tuning3u);
                    *y_a3[slidx][i] = z_by_zc( *y_a3[slidx][i], tuning3l);
                }
            }
            if(!n4->gnd_node) {
                if(bs->Ix > 0) {
                    *x_a4[suidx][i] = z_by_z( *x_a4[suidx][i], tuning4u);
                    *x_a4[slidx][i] = z_by_zc( *x_a4[slidx][i], tuning4l);
                }
                if(bs->Iy > 0) {
                    *y_a4[suidx][i] = z_by_z( *y_a4[suidx][i], tuning4u);
                    *y_a4[slidx][i] = z_by_zc( *y_a4[slidx][i], tuning4l);
                }
            }
        }
    }
}

/**
 * Fills in the matrix elements coupling incoming and outgoing fields on either
 * side of an optic to either x or y rotation.
 */
void fill_mirror_field_to_rot_motion(motion_type_t type, mirror_t *mirror, complex_t factor_a_x, frequency_t *fcar,
                                complex_t ***a1i_x, complex_t ***a1o_x, complex_t ***a2i_x, complex_t ***a2o_x,
                                complex_t *ac_1i, complex_t *ac_1o, complex_t *ac_2i, complex_t *ac_2o){
    int i, u, v;
    
    // get lower and upper sideband index
    int slidx = fcar->sig_lower->index; 
    int suidx = fcar->sig_upper->index;
    
    node_t *n1 = &inter.node_list[mirror->node1_index];
    node_t *n2 = &inter.node_list[mirror->node2_index];
        
    mirror_knm_q_t knm_q = {{0}};
    get_refl_q_in_out_mirror(mirror, &knm_q);

    double nr1 = (n1->n) ? *n1->n : 1;
    double nr2 = (n2->n) ? *n2->n : 1;
    
    // compute knm values with reverse gouy already applied, i.e don't add it in the
    // first place...
    complex_t knmx1i = (n1->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_11, nr1), 0);
    complex_t knmx1o = (n1->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_11, nr1), 0);
    complex_t knmx2i = (n2->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_22, nr2), 0);
    complex_t knmx2o = (n2->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_22, nr2), 0);
    
    // this is the usual minus sign you get for all non-diagonal 
    // values in the final matrix
    factor_a_x = cminus(factor_a_x);
    
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&u, &v, i);
        
        complex_t gamma_i_1 = complex_0;
        complex_t gamma_o_1 = complex_0;
        complex_t gamma_i_2 = complex_0;
        complex_t gamma_o_2 = complex_0;
        
        double sqrtmax;
        
        if((u > 0 && type == ROTX) || (v > 0 && type == ROTY)) { 
            int lidx = 0;
            // the minus signs decided here we done mostly by computing the required
            // number of minus signs, and then some trial and error...
            if(type == ROTX){
                lidx = get_field_index_from_tem(u-1, v);
                sqrtmax = sqrt(u);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(z_by_zc(knmx1i, ac_1i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(z_by_zc(knmx1o, ac_1o[lidx]), -sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(z_by_zc(knmx2i, ac_2i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(z_by_zc(knmx2o, ac_2o[lidx]), -sqrtmax));
                }
                
            } else {
                lidx = get_field_index_from_tem(u, v-1);
                sqrtmax = sqrt(v);
             
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(z_by_zc(knmx1i, ac_1i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(z_by_zc(knmx1o, ac_1o[lidx]), sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(z_by_zc(knmx2i, ac_2i[lidx]), -sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(z_by_zc(knmx2o, ac_2o[lidx]), -sqrtmax));
                }
            }            
        }

        int uidx = 0;
            
        if(type == ROTX){
            uidx = get_field_index_from_tem(u+1, v);
        } else {
            uidx = get_field_index_from_tem(u, v+1);
        }
        
        if(uidx < inter.num_fields && u+v+1 <= inter.tem) {

            // the minus signs decided here we done mostly by computing the required
            // number of minus signs, and then some trial and error so an offset
            // beam amplitude modulated rotates the mirror in the right direction(phase)
            if(type == ROTX){
                sqrtmax = sqrt(u+1);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(zc_by_zc(knmx1i, ac_1i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(zc_by_zc(knmx1o, ac_1o[uidx]), -sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(zc_by_zc(knmx2i, ac_2i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(zc_by_zc(knmx2o, ac_2o[uidx]), -sqrtmax));
                }

            } else {
                // there is no msign minus factor for the y motion 
                // but the node 2 side is a negative contribution
                sqrtmax = sqrt(v+1);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(zc_by_zc(knmx1i, ac_1i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(zc_by_zc(knmx1o, ac_1o[uidx]), sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(zc_by_zc(knmx2i, ac_2i[uidx]), -sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(zc_by_zc(knmx2o, ac_2o[uidx]), -sqrtmax));
                }
            }

            
        }
        
        if(!n1->gnd_node) {
            *(a1i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_1);
            *(a1o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_1);
            *(a1i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_1);
            *(a1o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_1);
        }

        if(!n2->gnd_node) {
            *(a2i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_2);
            *(a2o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_2);
            *(a2i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_2);
            *(a2o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_2);
        }
    }
}

/**
 * Fills in the matrix elements coupling incoming and outgoing fields on either
 * side of an optic to either x or y rotation.
 */
void fill_bs_field_to_rot_motion(motion_type_t type, beamsplitter_t *bs, complex_t factor_a_x, frequency_t *fcar,
                                complex_t ***a1i_x, complex_t ***a1o_x, complex_t ***a2i_x, complex_t ***a2o_x,
                                complex_t ***a3i_x, complex_t ***a3o_x, complex_t ***a4i_x, complex_t ***a4o_x,
                                complex_t *ac_1i, complex_t *ac_1o, complex_t *ac_2i, complex_t *ac_2o,
                                complex_t *ac_3i, complex_t *ac_3o, complex_t *ac_4i, complex_t *ac_4o){
    int i, u, v;
    
    // get lower and upper sideband index
    int slidx = fcar->sig_lower->index; 
    int suidx = fcar->sig_upper->index;
    
    node_t *n1 = &inter.node_list[bs->node1_index];
    node_t *n2 = &inter.node_list[bs->node2_index];
    node_t *n3 = &inter.node_list[bs->node3_index];
    node_t *n4 = &inter.node_list[bs->node4_index];
    
    bs_knm_q_t knm_q = {{0}};
    get_refl_q_in_out_bs(bs, &knm_q);

    double nr1=1, nr2=1;
    bs_get_nr(bs, &nr1, &nr2);
    
    // compute knm values with reverse gouy already applied, i.e don't add it in the
    // first place...
    complex_t knmx1i = (n1->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_21, nr1), 0);
    complex_t knmx1o = (n1->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_21, nr1), 0);
    complex_t knmx2i = (n2->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_12, nr1), 0);
    complex_t knmx2o = (n2->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_12, nr1), 0);
    
    complex_t knmx3i = (n3->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_43, nr2), 0);
    complex_t knmx3o = (n3->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_43, nr2), 0);
    complex_t knmx4i = (n4->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt1_34, nr2), 0);
    complex_t knmx4o = (n4->gnd_node) ? complex_0 : z_by_xphr(complex_1,  0.5 * w_size(knm_q.qxt2_34, nr2), 0);
    
    // this is the usual minus sign you get for all non-diagonal 
    // values in the final matrix
    factor_a_x = cminus(factor_a_x);
    
    for(i=0; i<inter.num_fields; i++){
        get_tem_modes_from_field_index(&u, &v, i);
        
        complex_t gamma_i_1 = complex_0;
        complex_t gamma_o_1 = complex_0;
        complex_t gamma_i_2 = complex_0;
        complex_t gamma_o_2 = complex_0;
        complex_t gamma_i_3 = complex_0;
        complex_t gamma_o_3 = complex_0;
        complex_t gamma_i_4 = complex_0;
        complex_t gamma_o_4 = complex_0;
        
        double sqrtmax;
        
        if((u > 0 && type == ROTX) || (v > 0 && type == ROTY)) { 
            int lidx = 0;
            // the minus signs decided here we done mostly by computing the required
            // number of minus signs, and then some trial and error...
            if(type == ROTX){
                lidx = get_field_index_from_tem(u-1, v);
                sqrtmax = sqrt(u);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(z_by_zc(knmx1i, ac_1i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(z_by_zc(knmx1o, ac_1o[lidx]), -sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(z_by_zc(knmx2i, ac_2i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(z_by_zc(knmx2o, ac_2o[lidx]), -sqrtmax));
                }
                
                if(!n3->gnd_node) {
                    z_inc_z(&gamma_i_3, z_by_x(z_by_zc(knmx3i, ac_3i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_3, z_by_x(z_by_zc(knmx3o, ac_3o[lidx]), -sqrtmax));
                }

                if(!n4->gnd_node) {
                    z_inc_z(&gamma_i_4, z_by_x(z_by_zc(knmx4i, ac_4i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_4, z_by_x(z_by_zc(knmx4o, ac_4o[lidx]), -sqrtmax));
                }
                
            } else {
                lidx = get_field_index_from_tem(u, v-1);
                sqrtmax = sqrt(v);
             
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(z_by_zc(knmx1i, ac_1i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(z_by_zc(knmx1o, ac_1o[lidx]), sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(z_by_zc(knmx2i, ac_2i[lidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(z_by_zc(knmx2o, ac_2o[lidx]), sqrtmax));
                }

                if(!n3->gnd_node) {
                    z_inc_z(&gamma_i_3, z_by_x(z_by_zc(knmx3i, ac_3i[lidx]), -sqrtmax));
                    z_inc_z(&gamma_o_3, z_by_x(z_by_zc(knmx3o, ac_3o[lidx]), -sqrtmax));
                }
                
                if(!n4->gnd_node) {
                    z_inc_z(&gamma_i_4, z_by_x(z_by_zc(knmx4i, ac_4i[lidx]), -sqrtmax));
                    z_inc_z(&gamma_o_4, z_by_x(z_by_zc(knmx4o, ac_4o[lidx]), -sqrtmax));
                }
            }            
        }

        int uidx = 0;
            
        if(type == ROTX){
            uidx = get_field_index_from_tem(u+1, v);
        } else {
            uidx = get_field_index_from_tem(u, v+1);
        }
        
        if(uidx < inter.num_fields && u+v+1 <= inter.tem) {

            // the minus signs decided here we done mostly by computing the required
            // number of minus signs, and then some trial and error so an offset
            // beam amplitude modulated rotates the mirror in the right direction(phase)
            if(type == ROTX){
                sqrtmax = sqrt(u+1);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(zc_by_zc(knmx1i, ac_1i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(zc_by_zc(knmx1o, ac_1o[uidx]), -sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(zc_by_zc(knmx2i, ac_2i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(zc_by_zc(knmx2o, ac_2o[uidx]), -sqrtmax));
                }

                if(!n3->gnd_node) {
                    z_inc_z(&gamma_i_3, z_by_x(zc_by_zc(knmx3i, ac_3i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_3, z_by_x(zc_by_zc(knmx3o, ac_3o[uidx]), -sqrtmax));
                }

                if(!n4->gnd_node) {
                    z_inc_z(&gamma_i_4, z_by_x(zc_by_zc(knmx4i, ac_4i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_4, z_by_x(zc_by_zc(knmx4o, ac_4o[uidx]), -sqrtmax));
                }
                
            } else {
                // there is no msign minus factor for the y motion 
                // but the node 2 side is a negative contribution
                sqrtmax = sqrt(v+1);
                
                if(!n1->gnd_node) {
                    z_inc_z(&gamma_i_1, z_by_x(zc_by_zc(knmx1i, ac_1i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_1, z_by_x(zc_by_zc(knmx1o, ac_1o[uidx]), sqrtmax));
                }

                if(!n2->gnd_node) {
                    z_inc_z(&gamma_i_2, z_by_x(zc_by_zc(knmx2i, ac_2i[uidx]), sqrtmax));
                    z_inc_z(&gamma_o_2, z_by_x(zc_by_zc(knmx2o, ac_2o[uidx]), sqrtmax));
                }

                if(!n3->gnd_node) {
                    z_inc_z(&gamma_i_3, z_by_x(zc_by_zc(knmx2i, ac_3i[uidx]), -sqrtmax));
                    z_inc_z(&gamma_o_3, z_by_x(zc_by_zc(knmx2o, ac_3o[uidx]), -sqrtmax));
                }
                
                if(!n4->gnd_node) {
                    z_inc_z(&gamma_i_4, z_by_x(zc_by_zc(knmx4i, ac_4i[uidx]), -sqrtmax));
                    z_inc_z(&gamma_o_4, z_by_x(zc_by_zc(knmx4o, ac_4o[uidx]), -sqrtmax));
                }
            }
        }
        
        if(!n1->gnd_node) {
            *(a1i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_1);
            *(a1o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_1);
            *(a1i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_1);
            *(a1o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_1);
        }

        if(!n2->gnd_node) {
            *(a2i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_2);
            *(a2o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_2);
            *(a2i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_2);
            *(a2o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_2);
        }
        
        if(!n3->gnd_node) {
            *(a3i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_3);
            *(a3o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_3);
            *(a3i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_3);
            *(a3o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_3);
        }

        if(!n4->gnd_node) {
            *(a4i_x[suidx][i]) = z_by_z(factor_a_x, gamma_i_4);
            *(a4o_x[suidx][i]) = z_by_z(factor_a_x, gamma_o_4);
            *(a4i_x[slidx][i]) = z_by_zc(factor_a_x, gamma_i_4);
            *(a4o_x[slidx][i]) = z_by_zc(factor_a_x, gamma_o_4);
        }
    }
}
/**
 * @param motion_idx Motion index (can also have z, pitch, yaw, enabled)
 * @param surface_idx Index of just the surface motion
 * @param m Mirror object
 * @param factor_a_x optical to motion coupling factor
 * @param factor_x_a motion to optical coupling factor
 * @param fcar  carrier frequency object
 * @param ac_1i node 1 incoming carrier field array
 * @param ac_1o node 1 outgoing carrier field array
 * @param ac_2i node 2 incoming carrier field array
 * @param ac_2o node 2 outgoing carrier field array
 * @param tuning1u tuning factor for node 1 side upper sideband
 * @param tuning1l tuning factor for node 1 side lower sideband
 * @param tuning2u tuning factor for node 2 side upper sideband
 * @param tuning2l tuning factor for node 2 side lower sideband
 */
void fill_surf_motion_coupling(int motion_idx, int surface_idx, mirror_t *m,
        complex_t factor_a_x, complex_t factor_x_a, frequency_t *fcar,
        complex_t *__restrict__ ac_1i, complex_t *__restrict__ ac_1o,
        complex_t *__restrict__ ac_2i, complex_t *__restrict__ ac_2o,
        complex_t tuning1u, complex_t tuning1l, complex_t tuning2u, complex_t tuning2l){
    
    node_t *n1 = &inter.node_list[m->node1_index];
    node_t *n2 = &inter.node_list[m->node2_index];
    
    int i;
    
    int slidx = fcar->sig_lower->index;
    int suidx = fcar->sig_upper->index;
        
    complex_t *__restrict__ tmp = (complex_t*) malloc(inter.num_fields * sizeof(complex_t));
    
    if(!n1->gnd_node){
        complex_t *__restrict__ Knm1o = &(m->knm_surf_motion_1o[surface_idx][0][0]);
        complex_t *__restrict__ Knm1i = &(m->knm_surf_motion_1i[surface_idx][0][0]);
        complex_t **__restrict__ a1il_x = m->a1i_x[motion_idx][slidx];
        complex_t **__restrict__ a1ol_x = m->a1o_x[motion_idx][slidx];
        complex_t **__restrict__ a1iu_x = m->a1i_x[motion_idx][suidx];
        complex_t **__restrict__ a1ou_x = m->a1o_x[motion_idx][suidx];
        complex_t **__restrict__ x_a1l = m->x_a1[motion_idx][slidx];
        complex_t **__restrict__ x_a1u = m->x_a1[motion_idx][suidx];
        
        /*
         * Compute motion to field coupling
         */
        
        // factor_x_a should be for the 11 reflection
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * sqrt(m->R));

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(m->knm_surf_x_a_1[surface_idx][0][0]),    // A
                    inter.num_fields,       // lda
                    ac_1i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a1u[0],               // Y
                    1);                     // Y stride

        // lower sideband creation is simply the conjugate of the upper
        for(i=0; i<inter.num_fields; i++) {
            *x_a1l[i] = cconj(*x_a1u[i]);
        }
        
        // plus we must also propagate the new sidebands away from the mirror
        if(m->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a1u[i] = z_by_z(*x_a1u[i], tuning1u);
                *x_a1l[i] = z_by_zc(*x_a1l[i], tuning1l);
            }
        }
        
        /*
         * Compute Field to motion coupling
         */

        // Old code before minus sign fixes
        //cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
        //            &complex_m1, Knm1o, inter.num_fields, ac_1o, 1, &complex_0, tmp, 1);

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &complex_1,             // alpha
                    Knm1o,                  // A
                    inter.num_fields,       // lda
                    ac_1o,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    tmp,                    // Y
                    1);                     // Y stride

        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        // -1 as a force from side 1 force mirror in negative z direction
        complex_t a_x_1_2 = z_by_x(factor_a_x, (-1)*(-1));

        for(i=0; i<inter.num_fields; i++) {
            *a1ou_x[i] = z_by_zc(a_x_1_2, tmp[i]);
            *a1ol_x[i] = z_by_z(a_x_1_2, tmp[i]);
        }
        
        // Old code before minus sign fixes
        // cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
        //             &complex_m1, Knm1i, inter.num_fields, ac_1i, 1, &complex_0, tmp, 1);
        
        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &complex_1,             // alpha
                    Knm1i,                  // A
                    inter.num_fields,       // lda
                    ac_1i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    tmp,                    // Y
                    1);                     // Y stride

        for(i=0; i<inter.num_fields; i++) {
            *a1iu_x[i] = z_by_zc(a_x_1_2, tmp[i]);
            *a1il_x[i] = z_by_z(a_x_1_2, tmp[i]);
        }
    }
    
    if(!n2->gnd_node){
        complex_t *__restrict__ Knm2o = &(m->knm_surf_motion_2o[surface_idx][0][0]);
        complex_t *__restrict__ Knm2i = &(m->knm_surf_motion_2i[surface_idx][0][0]);
        complex_t **__restrict__ a2il_x = m->a2i_x[motion_idx][slidx];
        complex_t **__restrict__ a2ol_x = m->a2o_x[motion_idx][slidx];
        complex_t **__restrict__ a2iu_x = m->a2i_x[motion_idx][suidx];
        complex_t **__restrict__ a2ou_x = m->a2o_x[motion_idx][suidx];
        complex_t **__restrict__ x_a2l = m->x_a2[motion_idx][slidx];
        complex_t **__restrict__ x_a2u = m->x_a2[motion_idx][suidx];
    
        // Here we times the the carrier scattering matrix on reflection to the
        // incident carrier fields. These are then modulated:
        // Y = alpha * A * X

        // factor_x_a should be for the 11 reflection, so for reflection from 
        // 22 we need the extra minus sign as we see opposite motion
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * (-1) * sqrt(m->R));

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(m->knm_surf_x_a_2[surface_idx][0][0]),    // A
                    inter.num_fields,       // lda
                    ac_2i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a2u[0],               // Y
                    1);                     // Y stride

        // lower sideband is simply the conjugate of the upper
        // plus we must also propagate the new sidebands away from the mirror
        for(i=0; i<inter.num_fields; i++) {
            *x_a2l[i] = cconj(*x_a2u[i]);
        }
        
        
        if(m->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a2u[i] = z_by_z(*x_a2u[i], tuning2u);
                *x_a2l[i] = z_by_zc(*x_a2l[i], tuning2l);
            }
        }
        
        //cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
        //            &complex_1, Knm2o, inter.num_fields, ac_2o, 1, &complex_0, tmp, 1);

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &complex_1,             // alpha
                    Knm2o,                  // A
                    inter.num_fields,       // lda
                    ac_2o,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    tmp,                    // Y
                    1);                     // Y stride
        
        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        complex_t a_x_22 = z_by_x(factor_a_x, (-1));

        for(i=0; i<inter.num_fields; i++) {
            *a2ou_x[i] = z_by_zc(a_x_22,  tmp[i]);
            *a2ol_x[i] = z_by_z(a_x_22, tmp[i]);
        }

        //cblas_zgemv(CblasRowMajor, CblasNoTrans, inter.num_fields, inter.num_fields,
        //            &complex_1, Knm2i, inter.num_fields, ac_2i, 1, &complex_0, tmp, 1);

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &complex_1,             // alpha
                    Knm2i,                  // A
                    inter.num_fields,       // lda
                    ac_2i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    tmp,                    // Y
                    1);                     // Y stride

        for(i=0; i<inter.num_fields; i++) {
            *a2iu_x[i] = z_by_zc(a_x_22, tmp[i]);
            *a2il_x[i] = z_by_z(a_x_22, tmp[i]);
        }
    }
    
    free(tmp);
}

void fill_mirror_z_motion_coupling(int k, mirror_t *m, complex_t factor_a_x, complex_t factor_x_a, frequency_t *fcar,
        complex_t *__restrict__ ac_1i, complex_t *__restrict__ ac_1o, complex_t *__restrict__ ac_2i, complex_t *__restrict__ ac_2o,
        complex_t tuning1u, complex_t tuning1l, complex_t tuning2u, complex_t tuning2l){
    
    node_t *n1 = &inter.node_list[m->node1_index];
    node_t *n2 = &inter.node_list[m->node2_index];
    
    int i;
    
    int slidx = fcar->sig_lower->index;
    int suidx = fcar->sig_upper->index;

    if(!n1->gnd_node){
        complex_t **__restrict__ a1il_x = m->a1i_x[k][slidx];
        complex_t **__restrict__ a1ol_x = m->a1o_x[k][slidx];
        complex_t **__restrict__ a1iu_x = m->a1i_x[k][suidx];
        complex_t **__restrict__ a1ou_x = m->a1o_x[k][suidx];
        complex_t **__restrict__ x_a1l = m->x_a1[k][slidx];
        complex_t **__restrict__ x_a1u = m->x_a1[k][suidx];
        
        // Here we generate the sidebands from the mirror motion
        // As the motion to field elements are all in the same column we use cblas_zgemv,
        // we don't have to worry about the memory alignment as we are using CCS matrix. 

        // Here we times the the carrier scattering matrix on reflection to the
        // incident carrier fields. These are then modulated:
        // Y = alpha * A * X

        // factor_x_a should be for the 11 reflection
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * sqrt(m->R));

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(m->knm.k11[0][0]),    // A
                    inter.num_fields,       // lda
                    ac_1i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a1u[0],               // Y
                    1);                     // Y stride
        
        
        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a1l[i] = cconj(*x_a1u[i]);
        }

        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        // -1 as a force from side 1 force mirror in negative z direction
        complex_t a_x_1_2 = z_by_x(factor_a_x, (-1)*(-1));

        for(i=0; i<inter.num_fields; i++) {
            *a1ou_x[i] = z_by_zc(a_x_1_2, ac_1o[i]);
            *a1ol_x[i] = z_by_z(a_x_1_2, ac_1o[i]);
            *a1iu_x[i] = z_by_zc(a_x_1_2, ac_1i[i]);
            *a1il_x[i] = z_by_z(a_x_1_2, ac_1i[i]);
        }
        
        // plus we must also propagate the new sidebands away from the mirror
        if(m->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a1u[i] = z_by_z(*x_a1u[i], tuning1u);
                *x_a1l[i] = z_by_zc(*x_a1l[i], tuning1l);
            }
        }
    }
    
    if(!n2->gnd_node){
        complex_t **__restrict__ x_a2l = m->x_a2[k][slidx];
        complex_t **__restrict__ x_a2u = m->x_a2[k][suidx];
        complex_t **__restrict__ a2il_x = m->a2i_x[k][slidx];
        complex_t **__restrict__ a2ol_x = m->a2o_x[k][slidx];
        complex_t **__restrict__ a2iu_x = m->a2i_x[k][suidx];
        complex_t **__restrict__ a2ou_x = m->a2o_x[k][suidx];
        
        // Here we generate the sidebands from the mirror motion
        // as the motion to field elements are all in the same column
        // we don't have to worry about the memory alignment, as we are using CCS
        // matrix. 

        // Here we times the the carrier scattering matrix on reflection to the
        // incident carrier fields. These are then modulated:
        // Y = alpha * A * X

        // factor_x_a should be for the 11 reflection, so for reflection from 
        // 22 we need the extra minus sign as we see opposite motion
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * (-1) * sqrt(m->R));

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(m->knm.k22[0][0]),    // A
                    inter.num_fields,       // lda
                    ac_2i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a2u[0],               // Y
                    1);                     // Y stride
        
        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a2l[i] = cconj(*x_a2u[i]);
        }

        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        complex_t a_x_22 = z_by_x(factor_a_x, (-1));

        for(i=0; i<inter.num_fields; i++) {
            *a2ou_x[i] = z_by_zc(a_x_22, ac_2o[i]);
            *a2ol_x[i] = z_by_z (a_x_22, ac_2o[i]);

            *a2iu_x[i] = z_by_zc(a_x_22, ac_2i[i]);
            *a2il_x[i] = z_by_z (a_x_22, ac_2i[i]);
        }
        
        // plus we must also propagate the new sidebands away from the mirror
        if(m->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a2u[i] = z_by_z(*x_a2u[i], tuning2u);
                *x_a2l[i] = z_by_zc(*x_a2l[i], tuning2l);
            }
        }
    }
}

void fill_bs_z_motion_coupling(int k, beamsplitter_t *bs, complex_t factor_a_x, complex_t factor_x_a, frequency_t *fcar,
        complex_t *__restrict__ ac_1i, complex_t *__restrict__ ac_1o, complex_t *__restrict__ ac_2i, complex_t *__restrict__ ac_2o,
        complex_t *__restrict__ ac_3i, complex_t *__restrict__ ac_3o, complex_t *__restrict__ ac_4i, complex_t *__restrict__ ac_4o,
        complex_t tuning1u, complex_t tuning1l, complex_t tuning2u, complex_t tuning2l,
        complex_t tuning3u, complex_t tuning3l, complex_t tuning4u, complex_t tuning4l){
    
    node_t *n1 = &inter.node_list[bs->node1_index];
    node_t *n2 = &inter.node_list[bs->node2_index];
    node_t *n3 = &inter.node_list[bs->node3_index];
    node_t *n4 = &inter.node_list[bs->node4_index];
    
    int i;
    
    int slidx = fcar->sig_lower->index;
    int suidx = fcar->sig_upper->index;

    if(!n1->gnd_node && !n2->gnd_node){
        // output at node 1 from incoming carrier at node 2
        complex_t **__restrict__ a1il_x = bs->a1i_x[k][slidx];
        complex_t **__restrict__ a1ol_x = bs->a1o_x[k][slidx];
        complex_t **__restrict__ a1iu_x = bs->a1i_x[k][suidx];
        complex_t **__restrict__ a1ou_x = bs->a1o_x[k][suidx];
        complex_t **__restrict__ x_a1l = bs->x_a1[k][slidx];
        complex_t **__restrict__ x_a1u = bs->x_a1[k][suidx];
        
        // factor_x_a should be for the 11 reflection
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * sqrt(bs->R));
        
        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(bs->knm.k21[0][0]),   // A - knm node 2 -> node 1
                    inter.num_fields,       // lda
                    ac_2i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a1u[0],               // Y
                    1);                     // Y stride

        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a1l[i] = cconj(*x_a1u[i]);
        }

        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        // -1 as a force from side 1 force mirror in negative z direction
        // This value can be used for the node 1->2 and 2->1 coupling
        complex_t a_x_1_2 = z_by_x(factor_a_x, (-1)*(-1));
                
        // lower sideband is simply the conjugate of the upper
        // plus we must also propagate the new sidebands away from the mirror
        for(i=0; i<inter.num_fields; i++) {
            *a1ou_x[i] = z_by_zc(a_x_1_2, ac_1o[i]);
            *a1ol_x[i] = z_by_z(a_x_1_2, ac_1o[i]);
            *a1iu_x[i] = z_by_zc(a_x_1_2, ac_1i[i]);
            *a1il_x[i] = z_by_z(a_x_1_2, ac_1i[i]);
        }
        
        if(bs->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a1u[i] = z_by_z(*x_a1u[i], tuning1u);
                *x_a1l[i] = z_by_zc(*x_a1l[i], tuning1l);
            }
        }
        
        // Now to fill in the opposite, incoming node 1 carrier to node 2
        complex_t **__restrict__ a2il_x = bs->a2i_x[k][slidx];
        complex_t **__restrict__ a2ol_x = bs->a2o_x[k][slidx];
        complex_t **__restrict__ a2iu_x = bs->a2i_x[k][suidx];
        complex_t **__restrict__ a2ou_x = bs->a2o_x[k][suidx];
        complex_t **__restrict__ x_a2l = bs->x_a2[k][slidx];
        complex_t **__restrict__ x_a2u = bs->x_a2[k][suidx];
        
        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(bs->knm.k12[0][0]),   // A - knm node 1 -> node 2
                    inter.num_fields,       // lda
                    ac_1i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a2u[0],               // Y
                    1);                     // Y stride
        
        // lower sideband is simply the conjugate of the upper
        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a2l[i] = cconj(*x_a2u[i]);
        }

        for(i=0; i<inter.num_fields; i++) {
            *a2ou_x[i] = z_by_zc(a_x_1_2, ac_2o[i]);
            *a2ol_x[i] = z_by_z(a_x_1_2, ac_2o[i]);
            *a2iu_x[i] = z_by_zc(a_x_1_2, ac_2i[i]);
            *a2il_x[i] = z_by_z(a_x_1_2, ac_2i[i]);
        }
        
        // plus we must also propagate the new sidebands away from the mirror
        if(bs->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a2u[i] = z_by_z(*x_a2u[i], tuning2u);
                *x_a2l[i] = z_by_zc(*x_a2l[i], tuning2l);
            }
        }
    }
    
    if(!n3->gnd_node && !n4->gnd_node){
        complex_t **__restrict__ x_a3l = bs->x_a3[k][slidx];
        complex_t **__restrict__ x_a3u = bs->x_a3[k][suidx];
        complex_t **__restrict__ a3il_x = bs->a3i_x[k][slidx];
        complex_t **__restrict__ a3ol_x = bs->a3o_x[k][slidx];
        complex_t **__restrict__ a3iu_x = bs->a3i_x[k][suidx];
        complex_t **__restrict__ a3ou_x = bs->a3o_x[k][suidx];
        
        // factor_x_a should be for the 11 reflection, so for reflection from 
        // 22 we need the extra minus sign as we see opposite motion
        // Also need an extra minus sign as we are setting the off diagonal elements
        // in the matrix
        complex_t alpha = z_by_x(factor_x_a, (-1) * (-1) * sqrt(bs->R));

        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(bs->knm.k43[0][0]),    // A
                    inter.num_fields,       // lda
                    ac_4i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a3u[0],               // Y
                    1);                     // Y stride
        
        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a3l[i] = cconj(*x_a3u[i]);
        }

        // Set the matrix elements for the optical to motion coupling
        // -1 for being off diag elements
        complex_t a_x_3_4 = z_by_x(factor_a_x, (-1));

        for(i=0; i<inter.num_fields; i++) {
            *a3ou_x[i] = z_by_zc(a_x_3_4,  ac_3o[i]);
            *a3ol_x[i] = z_by_z(a_x_3_4, ac_3o[i]);
            *a3iu_x[i] = z_by_zc(a_x_3_4,  ac_3i[i]);
            *a3il_x[i] = z_by_z(a_x_3_4, ac_3i[i]);
        }
        
        if(bs->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a3u[i] = z_by_z(*x_a3u[i], tuning3u);
                *x_a3l[i] = z_by_zc(*x_a3l[i], tuning3l);
            }
        }
        
        complex_t **__restrict__ x_a4l = bs->x_a4[k][slidx];
        complex_t **__restrict__ x_a4u = bs->x_a4[k][suidx];
        complex_t **__restrict__ a4il_x = bs->a4i_x[k][slidx];
        complex_t **__restrict__ a4ol_x = bs->a4o_x[k][slidx];
        complex_t **__restrict__ a4iu_x = bs->a4i_x[k][suidx];
        complex_t **__restrict__ a4ou_x = bs->a4o_x[k][suidx];
        
        cblas_zgemv(CblasRowMajor,
                    CblasNoTrans,
                    inter.num_fields,       // M
                    inter.num_fields,       // N
                    &alpha,                 // alpha
                    &(bs->knm.k34[0][0]),    // A
                    inter.num_fields,       // lda
                    ac_3i,                  // X
                    1,                      // X stride
                    &complex_0,             // beta (0 so we overwrite Y)
                    x_a4u[0],               // Y
                    1);                     // Y stride
        
        for(i=0; i<inter.num_fields; i++) {
            // Need to conjugate for the lower sideband equations
            *x_a4l[i] = cconj(*x_a4u[i]);
        }

        for(i=0; i<inter.num_fields; i++) {    
            *a4ou_x[i] = z_by_zc(a_x_3_4,  ac_4o[i]);
            *a4ol_x[i] = z_by_z(a_x_3_4, ac_4o[i]);
            *a4iu_x[i] = z_by_zc(a_x_3_4,  ac_4i[i]);
            *a4il_x[i] = z_by_z(a_x_3_4, ac_4i[i]);
        }
        
        if(bs->phi != 0.0){
            for(i=0; i<inter.num_fields; i++) {
                *x_a4u[i] = z_by_z(*x_a4u[i], tuning4u);
                *x_a4l[i] = z_by_zc(*x_a4l[i], tuning4l);
            }
        }
    }
}

inline complex_t compute_rotate_factor(complex_t q, double nr, int n_comp_index, int comp_index){
    if (n_comp_index == comp_index) q = cminus(cconj(q));
    return z_by_x(cconj(q), -1.0 / w0_size(q, nr));
}

void fill_optic_motion_coupling(){
    ifo_matrix_vars_t *Mcar = &M_ifo_car;
    complex_t *crhs  = (complex_t*)Mcar->rhs_values;
    
    assert(Mcar != NULL && Mcar->type == STANDARD);
    assert(inter.num_motion_eqns > 0);
    
    int i, k;
    
    complex_t G_Omega_z = complex_0, G_Omega_rx = complex_0, G_Omega_ry = complex_0;
    
    double s_fsig = inter.fsig;
    
    for(i=0; i<inter.num_mirrors; i++){
        mirror_t *m = &inter.mirror_list[i];
        
        if(m->num_motions > 0){            
            
            node_t *n1 = &inter.node_list[m->node1_index];
            node_t *n2 = &inter.node_list[m->node2_index];
            
            // Minus sign for mech suceptibility already in the transfer function. Just scale with
            // mass/inertia
            if (m->mass > 0) G_Omega_z = z_by_x(calc_transfer_func(m->long_tf, s_fsig), 1.0/m->mass);
            if (m->Ix > 0) G_Omega_rx = z_by_x(calc_transfer_func(m->rot_x_tf, s_fsig), 1.0/m->Ix);
            if (m->Iy > 0) G_Omega_ry = z_by_x(calc_transfer_func(m->rot_y_tf, s_fsig), 1.0/m->Iy);
            
            int f;
           
            for(f=0; f<inter.num_carrier_freqs; f++){
                
                frequency_t *fcar = inter.carrier_f_list[f];
                
                assert(fcar != NULL);
                assert(fcar->sig_lower != NULL);
                assert(fcar->sig_upper != NULL);
                                
                // Due to ill-conditioning of the matrix from the carrier frequency term
                // we need to store the mirror motion in units of something other than meters
                // using the init.x_scale
                
                double k0 =  TWOPI*(inter.f0 + fcar->f)/init.clight; // carrier wavenumber

                // factor for motion to field coupling for upper sideband, lower one is just the conjugate of this
                // exp(i2kz) => i2kz/2(exp(i Omega)+exp(-i Omega))
                // Factor for side 1 of mirror
                complex_t factor_x_a = co(0, 2 * k0 * init.x_scale / 2) ;
                
                // factor from field to motion coupling
                double factor_a_x = init.epsilon_0_c / (init.clight * init.x_scale);

                // TODO add index of refraction to these later
                // phase that the carrier and sideband will pick up due to tuning
                double phi_c  = m->phi * (1.0 + fcar->f / inter.f0);
                double phi_sl = m->phi * (1.0 + (fcar->f + fcar->sig_lower->f) / inter.f0); 
                double phi_su = m->phi * (1.0 + (fcar->f + fcar->sig_upper->f) / inter.f0); 
                                
                // compute the extra tuning the upper and lower sideband
                // will get on each side, saves a bunch of cos/sin computations
                complex_t tuning1u = z_by_ph(complex_1, (phi_c + phi_su));
                complex_t tuning1l = z_by_ph(complex_1, (phi_c + phi_sl));
                complex_t tuning2u = z_by_ph(complex_1, -(phi_c + phi_su));
                complex_t tuning2l = z_by_ph(complex_1, -(phi_c + phi_sl));
                        
                complex_t *ac_1i=NULL, *ac_1o=NULL;
                complex_t *ac_2i=NULL, *ac_2o=NULL;
                
                // if there is no carrier tuning we can bypass the above
                // and just point straight to the RHS vector
                if(!n1->gnd_node){
                    ac_1i = &crhs[get_rhs_idx(Mcar, n1, 1, fcar->index, 0)];
                    ac_1o = &crhs[get_rhs_idx(Mcar, n1, 2, fcar->index, 0)];
                }

                if(!n2->gnd_node){
                    ac_2i = &crhs[get_rhs_idx(Mcar, n2, 2, fcar->index, 0)];
                    ac_2o = &crhs[get_rhs_idx(Mcar, n2, 1, fcar->index, 0)];
                }
                
                complex_t ***a1i_x=NULL, ***a1o_x=NULL, ***a2i_x=NULL, ***a2o_x=NULL;
                complex_t ***a1i_y=NULL, ***a1o_y=NULL, ***a2i_y=NULL, ***a2o_y=NULL;
                complex_t ***x_a1=NULL,  ***x_a2=NULL,  ***y_a1=NULL,  ***y_a2=NULL;
                
                bool hasRotMotion = false;
                
                for(k=0; k<m->num_motions; k++){
                    if(m->motion_type[k] == Z){
                        fill_mirror_z_motion_coupling(k, m, z_by_x(G_Omega_z, factor_a_x), factor_x_a, fcar, ac_1i, ac_1o, ac_2i, ac_2o, tuning1u, tuning1l, tuning2u, tuning2l);
                    } else if(m->motion_type[k] == ROTX){
                        hasRotMotion = true;
                        
                        if(!n1->gnd_node){
                            a1i_x = m->a1i_x[k];
                            a1o_x = m->a1o_x[k];
                            x_a1 = m->x_a1[k];
                        }
                        
                        if(!n2->gnd_node){
                            a2i_x = m->a2i_x[k];
                            a2o_x = m->a2o_x[k];
                            x_a2 = m->x_a2[k];
                        }
                        
                        fill_mirror_field_to_rot_motion(ROTX, m, z_by_x(G_Omega_rx, factor_a_x), fcar, a1i_x, a1o_x, a2i_x, a2o_x, ac_1i, ac_1o, ac_2i, ac_2o);
                        
                    } else if(m->motion_type[k] == ROTY){
                        hasRotMotion = true;
                
                        if(!n1->gnd_node){
                            a1i_y = m->a1i_x[k];
                            a1o_y = m->a1o_x[k];
                            y_a1 = m->x_a1[k];
                        }
                        
                        if(!n2->gnd_node){
                            a2i_y = m->a2i_x[k];
                            a2o_y = m->a2o_x[k];
                            y_a2 = m->x_a2[k];
                        }
                        
                        fill_mirror_field_to_rot_motion(ROTY, m, z_by_x(G_Omega_ry, -factor_a_x), fcar, a1i_y, a1o_y, a2i_y, a2o_y, ac_1i, ac_1o, ac_2i, ac_2o);
                        
                    } else if(m->motion_type[k] == SURFACE) {
                        // There are 2 indices effectively, the motion index(X, Rx, Ry, S0, S1,...) and surface motion index (S0, S1, S2,...))
                        size_t surf_idx = k - (m->num_motions - m->num_surface_motions);
                        
                        complex_t H = z_by_x(calc_transfer_func(m->surface_motions_tf[surf_idx], s_fsig), factor_a_x);
                        
                        fill_surf_motion_coupling(k, surf_idx, m, H, factor_x_a, fcar, ac_1i, ac_1o, ac_2i, ac_2o, tuning1u, tuning1l, tuning2u, tuning2l);
                    }
                }
                
                if(hasRotMotion) {
                    fill_mirror_rot_motion_to_field(fcar, m, cminus(factor_x_a), x_a1, x_a2, y_a1, y_a2, ac_1i, ac_2i, tuning1u, tuning1l, tuning2u, tuning2l);
                }
            }
            
            // lastly fill in any motion linking
            if(m->motion_links != NULL){
                
                motion_link_t *p = NULL;
                bool set_internal_coupling = false;
                
                /* Firstly we need to zero all the previous motion linking
                 * matrix elements as multiple links might contribute to the
                 * same matrix elements
                 */
                utarray_foreach(m->motion_links, p) {
                    int n, f;
                    
                    if(p->from_list_idx != p->to_list_idx) {
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++){
                                *(p->a1i_x[f][n]) = complex_0;
                                *(p->a1o_x[f][n]) = complex_0;
                                *(p->a2i_x[f][n]) = complex_0;
                                *(p->a2o_x[f][n]) = complex_0;
                            }
                        }
                    } else 
                        set_internal_coupling = true;
                }
                
                /* if there is internal coupling between motions we use a 
                 * temporary storage matrix which needs zeroing too
                 */
                if(set_internal_coupling){
                    size_t size = sizeof(complex_t) * inter.num_fields * inter.num_signal_freqs * m->num_motions;
                    memset(m->ic_a1i_x, 0, size);
                    memset(m->ic_a1o_x, 0, size);
                    memset(m->ic_a2i_x, 0, size);
                    memset(m->ic_a2o_x, 0, size);
                }
                
                utarray_foreach(m->motion_links, p) {
                    /* The transfer function we need to compute is how a force from 
                     * the 'from' component motion couples to the 'to'. These
                     * values are previously calculated but included the force-to-motion
                     * transfer function of the from_motion, therefore we need to divide
                     * that out and apply the force-to-force and the force-to-motion
                     * transfer function of the 'to' component.
                     */
                    complex_t tf = (calc_transfer_func(p->f_f, s_fsig));
                    tf = z_by_z(tf, div_complex(calc_transfer_func(p->f_x_to, s_fsig), calc_transfer_func(p->f_x_from, s_fsig)));
                    
                    int f, n;
                   
                    if(p->from_list_idx != p->to_list_idx){
                        int from = p->from_motion;
                        
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++) {
                                z_inc_z(p->a1i_x[f][n], z_by_z(tf, *(m->a1i_x[from][f][n])));
                                z_inc_z(p->a1o_x[f][n], z_by_z(tf, *(m->a1o_x[from][f][n])));
                                z_inc_z(p->a2i_x[f][n], z_by_z(tf, *(m->a2i_x[from][f][n])));
                                z_inc_z(p->a2o_x[f][n], z_by_z(tf, *(m->a2o_x[from][f][n])));
                            }
                        }
                    } else {
                        /*
                         * When the link is between the different motions of the
                         * same component the matrix elements we need to use are
                         * the same as the those for the normal motion computations.
                         * We can't simply just copy the computations as in the
                         * previous case as we will end up copying the editted 
                         * values back again.
                         */
                        int to = p->to_motion;
                        int from = p->from_motion;
                        int ix;
                        
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++){
                                ix = to*inter.num_signal_freqs*inter.num_fields + f*inter.num_fields + n;
                                z_inc_z(&(m->ic_a1i_x[ix]), z_by_z(tf, *(m->a1i_x[from][f][n])));
                                z_inc_z(&(m->ic_a1o_x[ix]), z_by_z(tf, *(m->a1o_x[from][f][n])));
                                z_inc_z(&(m->ic_a2i_x[ix]), z_by_z(tf, *(m->a2i_x[from][f][n])));
                                z_inc_z(&(m->ic_a2o_x[ix]), z_by_z(tf, *(m->a2o_x[from][f][n])));
                            }
                        }
                    }
                }
                
                if(set_internal_coupling) {
                    int to, f, n, ix;
                    
                    for(to=0; to<m->num_motions; to++){
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++) {
                                ix = to*inter.num_signal_freqs*inter.num_fields + f*inter.num_fields + n;
                                z_inc_z(m->a1i_x[to][f][n], m->ic_a1i_x[ix]);
                                z_inc_z(m->a1o_x[to][f][n], m->ic_a1o_x[ix]);
                                z_inc_z(m->a2i_x[to][f][n], m->ic_a2i_x[ix]);
                                z_inc_z(m->a2o_x[to][f][n], m->ic_a2o_x[ix]);
                            }
                        }
                    }
                }
            }
        }
    }
    
    for(i=0; i<inter.num_beamsplitters; i++){
        beamsplitter_t *bs = &inter.bs_list[i];
        
        if(bs->num_motions > 0){            
            
            node_t *n1 = &inter.node_list[bs->node1_index];
            node_t *n2 = &inter.node_list[bs->node2_index];
            node_t *n3 = &inter.node_list[bs->node3_index];
            node_t *n4 = &inter.node_list[bs->node4_index];
            
            if(bs->mass > 0) G_Omega_z = z_by_x(calc_transfer_func(bs->long_tf, s_fsig), 1.0/bs->mass);
            if(bs->Ix > 0) G_Omega_rx = z_by_x(calc_transfer_func(bs->rot_x_tf, s_fsig), 1.0/bs->Ix);
            if(bs->Iy > 0) G_Omega_ry = z_by_x(calc_transfer_func(bs->rot_y_tf, s_fsig), 1.0/bs->Iy);
            
            int f;
           
            for(f=0; f<inter.num_carrier_freqs; f++){
                
                frequency_t *fcar = inter.carrier_f_list[f];
                
                assert(fcar != NULL);
                assert(fcar->sig_lower != NULL);
                assert(fcar->sig_upper != NULL);
                                
                // Due to ill-conditioning of the matrix from the carrier frequency term
                // we need to store the mirror motion in units of something other than meters
                // using the init.x_scale
                
                double alpha = cos(bs->alpha_1*RAD);

                double k0 =  TWOPI*(inter.f0 + fcar->f)/init.clight; // carrier wavenumber

                // factor for motion to field coupling for upper sideband, lower one is just the conjugate of this
                // exp(i2kz) => i2kz/2(exp(i Omega)+exp(-i Omega))
                // Factor for side 1 of mirror
                complex_t factor_x_a = co(0, alpha * 2 * k0 * init.x_scale / 2) ;

                // factor from field to motion coupling
                // extra factor of 2 due to the fact we throw away the
                // negative frequency term of the power fluctuations
                double factor_a_x = alpha * init.epsilon_0_c / (init.clight * init.x_scale);

                // TODO add index of refraction to these later
                // phase that the carrier and sideband will pick up due to tuning
                double phi_c =  bs->phi * (1.0 + fcar->f / inter.f0) * alpha;
                double phi_sl = bs->phi * (1.0 + (fcar->f + fcar->sig_lower->f) / inter.f0) * alpha; 
                double phi_su = bs->phi * (1.0 + (fcar->f + fcar->sig_upper->f) / inter.f0) * alpha; 
                                
                // compute the extra tuning the upper and lower sideband
                // will get on each side, saves a bunch of cos/sin computations
                complex_t tuning1u = z_by_ph(complex_1, (phi_c + phi_su));
                complex_t tuning1l = z_by_ph(complex_1, (phi_c + phi_sl));
                complex_t tuning2u = z_by_ph(complex_1, (phi_c + phi_su));
                complex_t tuning2l = z_by_ph(complex_1, (phi_c + phi_sl));
                complex_t tuning3u = z_by_ph(complex_1, -(phi_c + phi_su));
                complex_t tuning3l = z_by_ph(complex_1, -(phi_c + phi_sl));
                complex_t tuning4u = z_by_ph(complex_1, -(phi_c + phi_su));
                complex_t tuning4l = z_by_ph(complex_1, -(phi_c + phi_sl));
                
                complex_t *ac_1i=NULL, *ac_1o=NULL;
                complex_t *ac_2i=NULL, *ac_2o=NULL;
                complex_t *ac_3i=NULL, *ac_3o=NULL;
                complex_t *ac_4i=NULL, *ac_4o=NULL;
                
                // if there is no carrier tuning we can bypass the above
                // and just point straight to the RHS vector
                if(!n1->gnd_node){
                    ac_1i = &crhs[get_rhs_idx(Mcar, n1, 1, fcar->index, 0)];
                    ac_1o = &crhs[get_rhs_idx(Mcar, n1, 2, fcar->index, 0)];
                }

                if(!n2->gnd_node){
                    ac_2i = &crhs[get_rhs_idx(Mcar, n2, 1, fcar->index, 0)];
                    ac_2o = &crhs[get_rhs_idx(Mcar, n2, 2, fcar->index, 0)];
                }
                
                if(!n3->gnd_node){
                    ac_3i = &crhs[get_rhs_idx(Mcar, n3, 1, fcar->index, 0)];
                    ac_3o = &crhs[get_rhs_idx(Mcar, n3, 2, fcar->index, 0)];
                }
                
                if(!n4->gnd_node){
                    ac_4i = &crhs[get_rhs_idx(Mcar, n4, 1, fcar->index, 0)];
                    ac_4o = &crhs[get_rhs_idx(Mcar, n4, 2, fcar->index, 0)];
                }
                
                complex_t ***a1i_x=NULL, ***a1o_x=NULL, ***a2i_x=NULL, ***a2o_x=NULL;
                complex_t ***a1i_y=NULL, ***a1o_y=NULL, ***a2i_y=NULL, ***a2o_y=NULL;
                complex_t ***a3i_x=NULL, ***a3o_x=NULL, ***a4i_x=NULL, ***a4o_x=NULL;
                complex_t ***a3i_y=NULL, ***a3o_y=NULL, ***a4i_y=NULL, ***a4o_y=NULL;
                complex_t ***x_a1=NULL,  ***x_a2=NULL,  ***y_a1=NULL,  ***y_a2=NULL;
                complex_t ***x_a3=NULL,  ***x_a4=NULL,  ***y_a3=NULL,  ***y_a4=NULL;
                
                bool hasRotMotion = false;
                
                for(k=0; k<bs->num_motions; k++){
                    if(bs->motion_type[k] == Z){
                        fill_bs_z_motion_coupling(k, bs, z_by_x(G_Omega_z, factor_a_x), factor_x_a, fcar,
                                ac_1i, ac_1o, ac_2i, ac_2o, ac_3i, ac_3o, ac_4i, ac_4o,
                                tuning1u, tuning1l, tuning2u, tuning2l, tuning3u, tuning3l, tuning4u, tuning4l);
                    } else if(bs->motion_type[k] == ROTX){
                        hasRotMotion = true;
                        
                        if(!n1->gnd_node){
                            a1i_x = bs->a1i_x[k];
                            a1o_x = bs->a1o_x[k];
                            x_a1 = bs->x_a1[k];
                        }
                        
                        if(!n2->gnd_node){
                            a2i_x = bs->a2i_x[k];
                            a2o_x = bs->a2o_x[k];
                            x_a2 = bs->x_a2[k];
                        }
                        
                        if(!n3->gnd_node){
                            a3i_x = bs->a3i_x[k];
                            a3o_x = bs->a3o_x[k];
                            x_a3 = bs->x_a3[k];
                        }
                        
                        if(!n4->gnd_node){
                            a4i_x = bs->a4i_x[k];
                            a4o_x = bs->a4o_x[k];
                            x_a4 = bs->x_a4[k];
                        }
                        
                        fill_bs_field_to_rot_motion(ROTX, bs, z_by_x(G_Omega_rx, factor_a_x), fcar,
                                                    a1i_x, a1o_x, a2i_x, a2o_x, 
                                                    a3i_x, a3o_x, a4i_x, a4o_x,
                                                    ac_1i, ac_1o, ac_2i, ac_2o, ac_3i, ac_3o, ac_4i, ac_4o);
                        
                    } else if(bs->motion_type[k] == ROTY){
                        
                        hasRotMotion = true;
                
                        if(!n1->gnd_node){
                            a1i_y = bs->a1i_x[k];
                            a1o_y = bs->a1o_x[k];
                            y_a1 = bs->x_a1[k];
                        }
                        
                        if(!n2->gnd_node){
                            a2i_y = bs->a2i_x[k];
                            a2o_y = bs->a2o_x[k];
                            y_a2 = bs->x_a2[k];
                        }
                        
                        if(!n3->gnd_node){
                            a3i_y = bs->a3i_x[k];
                            a3o_y = bs->a3o_x[k];
                            y_a3 = bs->x_a3[k];
                        }
                        
                        if(!n4->gnd_node){
                            a4i_y = bs->a4i_x[k];
                            a4o_y = bs->a4o_x[k];
                            y_a4 = bs->x_a4[k];
                        }
                        
                        fill_bs_field_to_rot_motion(ROTY, bs, z_by_x(G_Omega_ry, -factor_a_x), fcar,
                                                    a1i_y, a1o_y, a2i_y, a2o_y,
                                                    a3i_y, a3o_y, a4i_y, a4o_y,
                                                    ac_1i, ac_1o, ac_2i, ac_2o,
                                                    ac_3i, ac_3o, ac_4i, ac_4o);
                        
                    } else if(bs->motion_type[k] == SURFACE) {
                        bug_error("not surface motions at bs\n");
                    }
                }
                
                if(hasRotMotion)
                    fill_bs_rot_motion_to_field(fcar, bs, factor_x_a,
                                                x_a1, x_a2, y_a1, y_a2, 
                                                x_a3, x_a4, y_a3, y_a4,
                                                ac_1i, ac_2i, ac_3i, ac_4i,
                                                tuning1u, tuning1l, tuning2u, tuning2l,
                                                tuning3u, tuning3l, tuning4u, tuning4l);
            }
            
            // lastly fill in any motion linking
            if(bs->motion_links != NULL){
                
                motion_link_t *p = NULL;
                bool set_internal_coupling = false;
                
                /* Firstly we need to zero all the previous motion linking
                 * matrix elements as multiple links might contribute to the
                 * same matrix elements
                 */
                utarray_foreach(bs->motion_links, p) {
                    int n, f;
                    
                    if(p->from_list_idx != p->to_list_idx) {
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++){
                                *(p->a1i_x[f][n]) = complex_0;
                                *(p->a1o_x[f][n]) = complex_0;
                                *(p->a2i_x[f][n]) = complex_0;
                                *(p->a2o_x[f][n]) = complex_0;
                                *(p->a3i_x[f][n]) = complex_0;
                                *(p->a3o_x[f][n]) = complex_0;
                                *(p->a4i_x[f][n]) = complex_0;
                                *(p->a4o_x[f][n]) = complex_0;
                            }
                        }
                    } else 
                        set_internal_coupling = true;
                }
                
                /* if there is internal coupling between motions we use a 
                 * temporary storage matrix which needs zeroing too
                 */
                if(set_internal_coupling){
                    size_t size = sizeof(complex_t) * inter.num_fields * inter.num_signal_freqs * bs->num_motions;
                    memset(bs->ic_a1i_x, 0, size);
                    memset(bs->ic_a1o_x, 0, size);
                    memset(bs->ic_a2i_x, 0, size);
                    memset(bs->ic_a2o_x, 0, size);
                    memset(bs->ic_a3i_x, 0, size);
                    memset(bs->ic_a3o_x, 0, size);
                    memset(bs->ic_a4i_x, 0, size);
                    memset(bs->ic_a4o_x, 0, size);
                }
                
                utarray_foreach(bs->motion_links, p) {
                    /* The transfer function we need to compute is how a force from 
                     * the 'from' component motion couples to the 'to'. These
                     * values are previously calculated but included the force-to-motion
                     * transfer function of the from_motion, therefore we need to divide
                     * that out and apply the force-to-force and the force-to-motion
                     * transfer function of the 'to' component.
                     */
                    complex_t tf = (calc_transfer_func(p->f_f, s_fsig));
                    tf = z_by_z(tf, div_complex(calc_transfer_func(p->f_x_to, s_fsig), calc_transfer_func(p->f_x_from, s_fsig)));
                    
                    int f, n;
                   
                    if(p->from_list_idx != p->to_list_idx){
                        int from = p->from_motion;
                        
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++) {
                                z_inc_z(p->a1i_x[f][n], z_by_z(tf, *(bs->a1i_x[from][f][n])));
                                z_inc_z(p->a1o_x[f][n], z_by_z(tf, *(bs->a1o_x[from][f][n])));
                                z_inc_z(p->a2i_x[f][n], z_by_z(tf, *(bs->a2i_x[from][f][n])));
                                z_inc_z(p->a2o_x[f][n], z_by_z(tf, *(bs->a2o_x[from][f][n])));
                                z_inc_z(p->a3i_x[f][n], z_by_z(tf, *(bs->a3i_x[from][f][n])));
                                z_inc_z(p->a3o_x[f][n], z_by_z(tf, *(bs->a3o_x[from][f][n])));
                                z_inc_z(p->a4i_x[f][n], z_by_z(tf, *(bs->a4i_x[from][f][n])));
                                z_inc_z(p->a4o_x[f][n], z_by_z(tf, *(bs->a4o_x[from][f][n])));
                            }
                        }
                    } else {
                        /*
                         * When the link is between the different motions of the
                         * same component the matrix elements we need to use are
                         * the same as the those for the normal motion computations.
                         * We can't simply just copy the computations as in the
                         * previous case as we will end up copying the editted 
                         * values back again.
                         */
                        int to = p->to_motion;
                        int from = p->from_motion;
                        int ix;
                        
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++){
                                ix = to*inter.num_signal_freqs*inter.num_fields + f*inter.num_fields + n;
                                z_inc_z(&(bs->ic_a1i_x[ix]), z_by_z(tf, *(bs->a1i_x[from][f][n])));
                                z_inc_z(&(bs->ic_a1o_x[ix]), z_by_z(tf, *(bs->a1o_x[from][f][n])));
                                z_inc_z(&(bs->ic_a2i_x[ix]), z_by_z(tf, *(bs->a2i_x[from][f][n])));
                                z_inc_z(&(bs->ic_a2o_x[ix]), z_by_z(tf, *(bs->a2o_x[from][f][n])));
                                z_inc_z(&(bs->ic_a3i_x[ix]), z_by_z(tf, *(bs->a3i_x[from][f][n])));
                                z_inc_z(&(bs->ic_a3o_x[ix]), z_by_z(tf, *(bs->a3o_x[from][f][n])));
                                z_inc_z(&(bs->ic_a4i_x[ix]), z_by_z(tf, *(bs->a4i_x[from][f][n])));
                                z_inc_z(&(bs->ic_a4o_x[ix]), z_by_z(tf, *(bs->a4o_x[from][f][n])));
                            }
                        }
                    }
                }
                
                if(set_internal_coupling) {
                    int to, f, n, ix;
                    
                    for(to=0; to<bs->num_motions; to++){
                        for(f=0; f<inter.num_signal_freqs; f++){
                            for(n=0; n<inter.num_fields; n++) {
                                ix = to*inter.num_signal_freqs*inter.num_fields + f*inter.num_fields + n;
                                z_inc_z(bs->a1i_x[to][f][n], bs->ic_a1i_x[ix]);
                                z_inc_z(bs->a1o_x[to][f][n], bs->ic_a1o_x[ix]);
                                z_inc_z(bs->a2i_x[to][f][n], bs->ic_a2i_x[ix]);
                                z_inc_z(bs->a2o_x[to][f][n], bs->ic_a2o_x[ix]);
                                z_inc_z(bs->a3i_x[to][f][n], bs->ic_a3i_x[ix]);
                                z_inc_z(bs->a3o_x[to][f][n], bs->ic_a3o_x[ix]);
                                z_inc_z(bs->a4i_x[to][f][n], bs->ic_a4i_x[ix]);
                                z_inc_z(bs->a4o_x[to][f][n], bs->ic_a4o_x[ix]);
                            }
                        }
                    }
                }
            }
        }
    }
}

void fill_matrix_feedback_elements(ifo_matrix_vars_t *matrix) {
    assert(matrix != NULL);
    assert(matrix->type == SIGNAL_QCORR);
    
    slink_t *feedback;
    int f,i,n;
   
    for(n=0; n<inter.num_slinks; n++) {
        feedback = &(inter.slink_list[n]);

        output_data_t *out = &inter.output_data_list[feedback->output_list_idx];
        light_out_t *lout = &inter.light_out_list[out->detector_index];
        
        if(!inter.initialised_quantum_calculations){
            warn("\n No quantum noise detectors found for using slink with\n");
            for(f=0; f<inter.num_signal_freqs; f++) {
                for(i=0; i<inter.num_fields; i++) {
                    *(feedback->a_d[f][i]) = complex_0;
                }
            }

        } else {
            // zero the selection vector
            memset(quant_s, 0, sizeof(complex_t)*M_ifo_sig.M.num_eqns);

            fill_demod_f_table(lout);
            fill_carrier_qnoise_contributions(lout, false);

            fill_qnoised_selection_vector(out->detector_index, quant_s, NULL);

            for(f=0; f<inter.num_signal_freqs; f++) {
                for(i=0; i<inter.num_fields; i++) {
                    long idx = matrix->output_rhs_idx[feedback->output_list_idx] + f*inter.num_fields+i;

                    *(feedback->a_d[f][i]) = cminus(quant_s[idx]);
                }
            }

            if(feedback->tf != NULL){
                *(feedback->d_x) = z_by_x(cminus(calc_transfer_func(feedback->tf, inter.fsig)), 1 / init.x_scale);
            } else {
                feedback->d_x->re = -1;
                feedback->d_x->im = 0;
            }
        }
    }
}

/*!
 * \param f frequency of light field to fill
 */
void fill_matrix_mirror_elements(mirror_t *mirror, double fin, double fout, int K, bool conjugate) {
    double Jk;
    
    if(mirror->dither_m == 0.0 && K == 0)
        Jk = 1.0;
    else
        if (K < 0)
            Jk = pow_neg_1(K) * bessn(-K, TWOPI * mirror->dither_m / init.lambda * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0));
        else
            Jk = bessn(K, TWOPI * mirror->dither_m / init.lambda * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0));
    
    double r = Jk * sqrt(mirror->R);
    double t = sqrt(mirror->T);
    
    double tuning = mirror->phi * RAD * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0);
    
    complex_t _r = z_by_xphr(pow_complex_i(K), r, tuning + K*mirror->dither_phase * RAD);
    
    double c0 = _r.re;
    double s0 = _r.im;

    // field amplitudes
    int j, k;

    for (j = 0; j < inter.num_fields; j++) {
        for (k = 0; k < inter.num_fields; k++) {

            if (mirror->a11[j][k] != NULL) {
                (mirror->a11[j][k])->re = -c0;
                (mirror->a11[j][k])->im = -s0;

                *(mirror->a11[j][k]) = z_by_z(*(mirror->a11[j][k]), mirror->knm.k11[j][k]);
                
                if(conjugate) mirror->a11[j][k]->im *= -1;
            }

            if (mirror->a12[j][k] != NULL) {
                (mirror->a12[j][k])->re = 0.0;
                (mirror->a12[j][k])->im = -t;

                *(mirror->a12[j][k]) = z_by_z(*(mirror->a12[j][k]), mirror->knm.k12[j][k]);
                
                if(conjugate) {
                    mirror->a12[j][k]->im *= -1;
                }
            }
            
            if (mirror->a21[j][k] != NULL) {
                (mirror->a21[j][k])->re = 0.0;
                (mirror->a21[j][k])->im = -t;

                *(mirror->a21[j][k]) = z_by_z(*(mirror->a21[j][k]), mirror->knm.k21[j][k]);
                
                if(conjugate) {
                    mirror->a21[j][k]->im *= -1;
                }
            }
            
            if (mirror->a22[j][k] != NULL) {
                (mirror->a22[j][k])->re = -c0;
                (mirror->a22[j][k])->im = s0;

                *(mirror->a22[j][k]) = z_by_z(*(mirror->a22[j][k]), mirror->knm.k22[j][k]);
                if(conjugate) mirror->a22[j][k]->im *= -1;
            }
        }
    }    
}


//! Dump relevant sparse matrix elements for mirrors

/*!
 *
 */
void dump_matrix_mirror_elements(FILE *fp) {
    int mirror_index;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "dumping mirror matrices\n");

    for (mirror_index = 0; mirror_index < inter.num_mirrors; mirror_index++) {
        mirror_t mirror;
        mirror = inter.mirror_list[mirror_index];
        fprintf(fp, "mirror %d, name: %s\n", mirror_index, mirror.name);

        int j, k;

        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                int n1, m1, n2, m2;

                get_tem_modes_from_field_index(&n1, &m1, j);
                get_tem_modes_from_field_index(&n2, &m2, k);

                fprintf(fp, "(%d, %d) [%d %d -> %d %d]\n", j, k, n1, m1, n2, m2);
                if (mirror.a11[j][k] != NULL) {
                    fprintf(fp, "a11: %s, ", complex_form15(*(mirror.a11[j][k])));
                }
                if (mirror.a22[j][k] != NULL) {
                    fprintf(fp, "a22: %s, ", complex_form15(*(mirror.a22[j][k])));
                }
                if (mirror.a12[j][k] != NULL) {
                    fprintf(fp, "a12: %s, ", complex_form15(*(mirror.a12[j][k])));
                }
                if (mirror.a21[j][k] != NULL) {
                    fprintf(fp, "a21: %s\n", complex_form15(*(mirror.a21[j][k])));
                }
            }
        }
    }
}






//! Dump relevant sparse matrix elements for spaces

/*!
 *
 */
void dump_matrix_space_elements(FILE *fp) {

    int space_index;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    fprintf(fp, "dumping space matrices\n");

    for (space_index = 0; space_index < inter.num_spaces; space_index++) {
        space_t space;
        space = inter.space_list[space_index];
        fprintf(fp, "space %d, name: %s\n", space_index, space.name);

        int j, k;

        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                int n1, m1, n2, m2;

                get_tem_modes_from_field_index(&n1, &m1, j);
                get_tem_modes_from_field_index(&n2, &m2, k);

                fprintf(fp, "(%d, %d) [%d %d -> %d %d]\n", j, k, n1, m1, n2, m2);
                if (space.a12[j][k] != NULL) {
                    fprintf(fp, "a12: %s, ", complex_form15(*(space.a12[j][k])));
                }
                if (space.a21[j][k] != NULL) {
                    fprintf(fp, "a21: %s\n", complex_form15(*(space.a21[j][k])));
                }
            }
        }
    }
}



//! Fill relevant sparse matrix elements with beamsplitter component terms

/*!
 * \param f frequency of light field to fill
 */
void fill_matrix_beamsplitter_elements(beamsplitter_t *bs, frequency_t *_fin, frequency_t *_fout, int K, bool conjugate) {
    double Jk;
    
    double fin = _fin->f;
    double fout = _fout->f;
    
    double R,T;
    fRT_t *fRTL = NULL;
    
    // If we have set frequency dependent R and T
    if(bs->fRT)
        HASH_FIND_STR(bs->fRT, _fin->name, fRTL);
    
    if(fRTL) {
        R = fRTL->R;
        T = fRTL->T;
    } else {
        R = bs->R;
        T = bs->T;
    }
    
    if(bs->dither_m == 0.0 && K == 0) {
        Jk = 1.0;
    } else {
        if (K < 0)
            Jk = pow_neg_1(K) * bessn(-K, TWOPI * bs->dither_m / init.lambda * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0));
        else
            Jk = bessn(K, TWOPI * bs->dither_m / init.lambda * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0));
    }
    
    double rb = Jk*sqrt(bs->backscatter);
    double r = Jk*sqrt(R - bs->backscatter);
    double t = sqrt(T);
    double alpha = bs->alpha_1 * RAD;
    double phi = bs->phi * RAD * (1.0 + fin / inter.f0 + 1.0 + fout / inter.f0) * cos(alpha);
    complex_t _r = z_by_xphr(pow_complex_i(K), r, phi + K*bs->dither_phase * RAD);
    
    double c0 = _r.re;
    double s0 = _r.im;
    
    // term for backscatter
    complex_t _rb = z_by_xphr(pow_complex_i(K), -rb, phi + K*bs->dither_phase * RAD);
    
    // field amplitudes
    int j, k;

    for (j = 0; j < inter.num_fields; j++) { // outputs
        for (k = 0; k < inter.num_fields; k++) { // frequencies

            if (bs->a11[j][k] != NULL && bs->backscatter > 0) {
                // Backscatter
                bs->a11[j][k]->re = -_rb.re;
                bs->a11[j][k]->im = -_rb.im;
                
                // TODO !!! Coupling matrix is not consider yet for backscatter
                
                if(conjugate){
                    bs->a11[j][k]->im *= -1;
                }
            }
            
            if (bs->a12[j][k] != NULL) {
                (bs->a12[j][k])->re = -c0;
                (bs->a12[j][k])->im = -s0;
                
                *(bs->a12[j][k]) = z_by_z(*(bs->a12[j][k]), bs->knm.k12[j][k]);
                
                if(conjugate){
                    bs->a12[j][k]->im *= -1;
                }
            }
            
            if (bs->a22[j][k] != NULL && bs->backscatter > 0) {
                // Backscatter
                bs->a22[j][k]->re = -_rb.re;
                bs->a22[j][k]->im = -_rb.im;
                
                // TODO !!! Coupling matrix is not consider yet for backscatter
                
                if(conjugate){
                    bs->a22[j][k]->im *= -1;
                }
            }
            
            if (bs->a21[j][k] != NULL) {
                (bs->a21[j][k])->re = -c0;
                (bs->a21[j][k])->im = -s0;
                
                *(bs->a21[j][k]) = z_by_z(*(bs->a21[j][k]), bs->knm.k21[j][k]);
                
                if(conjugate){
                    bs->a21[j][k]->im *= -1;
                }
            }

            if (bs->a13[j][k] != NULL) {
                (bs->a13[j][k])->re = 0.0;
                (bs->a13[j][k])->im = -t;

                *(bs->a13[j][k]) = z_by_z(*(bs->a13[j][k]), bs->knm.k13[j][k]);
                
                if(conjugate){
                    bs->a13[j][k]->im *= -1;
                }
            }
            
            if (bs->a31[j][k] != NULL) {
                (bs->a31[j][k])->re = 0.0;
                (bs->a31[j][k])->im = -t;

                *(bs->a31[j][k]) = z_by_z(*(bs->a31[j][k]), bs->knm.k31[j][k]);
                
                if(conjugate){
                    bs->a31[j][k]->im *= -1;
                }
            }

            if (bs->a33[j][k] != NULL && bs->backscatter > 0) {
                // Backscatter
                bs->a33[j][k]->re = -_rb.re;
                bs->a33[j][k]->im = _rb.im;
                
                // TODO !!! Coupling matrix is not consider yet for backscatter
                
                if(conjugate){
                    bs->a33[j][k]->im *= -1;
                }
            }
            
            if (bs->a34[j][k] != NULL) {
                (bs->a34[j][k])->re = -c0;
                (bs->a34[j][k])->im = s0;

                *(bs->a34[j][k]) = z_by_z(*(bs->a34[j][k]), bs->knm.k34[j][k]);
                
                if(conjugate){
                    bs->a34[j][k]->im *= -1;
                }
            }
            
            if (bs->a44[j][k] != NULL && bs->backscatter > 0) {
                // Backscatter
                bs->a44[j][k]->re = -_rb.re;
                bs->a44[j][k]->im = _rb.im;
                
                // TODO !!! Coupling matrix is not consider yet for backscatter
                
                if(conjugate){
                    bs->a44[j][k]->im *= -1;
                }
            }
            
            if (bs->a43[j][k] != NULL) {
                (bs->a43[j][k])->re = -c0;
                (bs->a43[j][k])->im = s0;
                
                *(bs->a43[j][k]) = z_by_z(*(bs->a43[j][k]), bs->knm.k43[j][k]);
                
                if(conjugate){
                    bs->a43[j][k]->im *= -1;
                }
            }
            
            if (bs->a24[j][k] != NULL) {
                (bs->a24[j][k])->re = 0.0;
                (bs->a24[j][k])->im = -t;

                *(bs->a24[j][k]) = z_by_z(*(bs->a24[j][k]), bs->knm.k24[j][k]);
                
                if(conjugate){
                    bs->a24[j][k]->im *= -1;
                }
            }
            
            if (bs->a42[j][k] != NULL) {
                (bs->a42[j][k])->re = 0.0;
                (bs->a42[j][k])->im = -t;

                *(bs->a42[j][k]) = z_by_z(*(bs->a42[j][k]), bs->knm.k42[j][k]);
                
                if(conjugate){
                    bs->a42[j][k]->im *= -1;
                }
            }
        }
    }
}

//! Fill relevant sparse matrix elements with grating component terms

/*!
 *
 * \todo qcorr branches untested
 */
void fill_matrix_grating_elements(grating_t *grating, double f, bool conjugate) {
    double eta[4] = {0};
    (void)f;// suppress compiler warning
    int j, k;
    switch (grating->num_of_ports) {
        case 2:
            for (j = 0; j < 2; j++) {
                eta[j] = sqrt(grating->eta[j]);
            }

            // field amplitudes
            for (j = 0; j < inter.num_fields; j++) {
                for (k = 0; k < inter.num_fields; k++) {
                    if (j == k) {
                        if (grating->a12[j][k] != NULL) {
                            (grating->a12[j][k])->re = 0.0;
                            (grating->a12[j][k])->im = -eta[1];
                            (grating->a21[j][k])->re = 0.0;
                            (grating->a21[j][k])->im = -eta[1];
                        }

                        if (grating->a11[j][k] != NULL) {
                            (grating->a11[j][k])->re = -eta[0];
                            (grating->a11[j][k])->im = 0.0;
                        }

                        if (grating->a22[j][k] != NULL) {
                            (grating->a22[j][k])->re = -eta[0];
                            (grating->a22[j][k])->im = 0.0;
                        }
                    } else {
                        if (grating->a12[j][k] != NULL) {
                            *(grating->a12[j][k]) = complex_0;
                            *(grating->a21[j][k]) = complex_0;
                        }

                        if (grating->a11[j][k] != NULL) {
                            *(grating->a11[j][k]) = complex_0;
                        }

                        if (grating->a22[j][k] != NULL) {
                            *(grating->a22[j][k]) = complex_0;
                        }
                    }
                    
                    if(conjugate){
                        if(grating->a11[j][k] != NULL) grating->a11[j][k]->im *= -1;
                        if(grating->a12[j][k] != NULL) grating->a12[j][k]->im *= -1;
                        if(grating->a21[j][k] != NULL) grating->a21[j][k]->im *= -1;
                        if(grating->a22[j][k] != NULL) grating->a22[j][k]->im *= -1;
                    }
                }
            }
            
            break;
        case 3:
            for (j = 0; j < 4; j++) {
                eta[j] = sqrt(grating->eta[j]);
            }

            /* coupling matrix :
             * eta2 exp(i phi2)  eta1 exp(i phi1)  eta0 exp(i phi0) 
             * eta1 exp(i phi1)  rho0 exp(i phi0)  eta1 exp(i phi1) 
             * eta0 exp(i phi0)  eta1 exp(i phi1)  eta2 exp(i phi2) 
             */
            double s3 = sin(grating->phi[0]) * eta[3];
            double c3 = cos(grating->phi[0]) * eta[3];

            double s0 = sin(grating->phi[0]) * eta[0];
            double c0 = cos(grating->phi[0]) * eta[0];

            double s1 = sin(grating->phi[1]) * eta[1];
            double c1 = cos(grating->phi[1]) * eta[1];

            double s2 = sin(grating->phi[2]) * eta[2];
            double c2 = cos(grating->phi[2]) * eta[2];

            // field amplitudes
            for (j = 0; j < inter.num_fields; j++) {
                for (k = 0; k < inter.num_fields; k++) {
                    if (j == k) {
                        if (grating->a11[j][k] != NULL) {
                            (grating->a11[j][k])->re = -c2;
                            (grating->a11[j][k])->im = -s2;
                        }

                        if (grating->a22[j][k] != NULL) {
                            (grating->a22[j][k])->re = -c2;
                            (grating->a22[j][k])->im = -s2;
                        }

                        if (grating->a33[j][k] != NULL) {
                            (grating->a33[j][k])->re = -c3;
                            (grating->a33[j][k])->im = -s3;
                        }

                        if (grating->a12[j][k] != NULL) {
                            (grating->a12[j][k])->re = -c0;
                            (grating->a12[j][k])->im = -s0;
                            (grating->a21[j][k])->re = -c0;
                            (grating->a21[j][k])->im = -s0;
                        }

                        if (grating->a13[j][k] != NULL) {
                            (grating->a13[j][k])->re = -c1;
                            (grating->a13[j][k])->im = -s1;
                            (grating->a31[j][k])->re = -c1;
                            (grating->a31[j][k])->im = -s1;
                        }

                        if (grating->a32[j][k] != NULL) {
                            (grating->a32[j][k])->re = -c1;
                            (grating->a32[j][k])->im = -s1;
                            (grating->a23[j][k])->re = -c1;
                            (grating->a23[j][k])->im = -s1;
                        }
                    } else {
                        if (grating->a11[j][k] != NULL) {
                            *(grating->a11[j][k]) = complex_0;
                        }

                        if (grating->a22[j][k] != NULL) {
                            *(grating->a22[j][k]) = complex_0;
                        }

                        if (grating->a33[j][k] != NULL) {
                            *(grating->a33[j][k]) = complex_0;
                        }

                        if (grating->a12[j][k] != NULL) {
                            *(grating->a12[j][k]) = complex_0;
                            *(grating->a21[j][k]) = complex_0;
                        }

                        if (grating->a13[j][k] != NULL) {
                            *(grating->a13[j][k]) = complex_0;
                            *(grating->a31[j][k]) = complex_0;
                        }

                        if (grating->a23[j][k] != NULL) {
                            *(grating->a23[j][k]) = complex_0;
                            *(grating->a32[j][k]) = complex_0;
                        }
                    }
                    
                    if(conjugate){
                        if(grating->a11[j][k] != NULL) grating->a11[j][k]->im *= -1;
                        if(grating->a12[j][k] != NULL) grating->a12[j][k]->im *= -1;
                        if(grating->a13[j][k] != NULL) grating->a13[j][k]->im *= -1;
                        if(grating->a21[j][k] != NULL) grating->a21[j][k]->im *= -1;
                        if(grating->a22[j][k] != NULL) grating->a22[j][k]->im *= -1;
                        if(grating->a23[j][k] != NULL) grating->a23[j][k]->im *= -1;
                        if(grating->a31[j][k] != NULL) grating->a31[j][k]->im *= -1;
                        if(grating->a32[j][k] != NULL) grating->a32[j][k]->im *= -1;
                        if(grating->a33[j][k] != NULL) grating->a33[j][k]->im *= -1;
                    }
                }
            }
            
            break;
        case 4:
            for (j = 0; j < 2; j++) {
                eta[j] = sqrt(grating->eta[j]);
            }

            // field amplitudes
            for (j = 0; j < inter.num_fields; j++) {
                for (k = 0; k < inter.num_fields; k++) {
                    if (j == k) {
                        if (grating->a14[j][k] != NULL) {
                            (grating->a14[j][k])->re = -eta[0];
                            (grating->a14[j][k])->im = 0.0;
                            (grating->a41[j][k])->re = -eta[0];
                            (grating->a41[j][k])->im = 0.0;
                        }

                        if (grating->a13[j][k] != NULL) {
                            (grating->a13[j][k])->re = 0.0;
                            (grating->a13[j][k])->im = -eta[1];
                            (grating->a31[j][k])->re = 0.0;
                            (grating->a31[j][k])->im = -eta[1];
                        }

                        if (grating->a42[j][k] != NULL) {
                            (grating->a42[j][k])->re = 0.0;
                            (grating->a42[j][k])->im = -eta[1];
                            (grating->a24[j][k])->re = 0.0;
                            (grating->a24[j][k])->im = -eta[1];
                        }
                    } else {
                        if (grating->a14[j][k] != NULL) {
                            *(grating->a14[j][k]) = complex_0;
                            *(grating->a41[j][k]) = complex_0;
                        }

                        if (grating->a13[j][k] != NULL) {
                            *(grating->a13[j][k]) = complex_0;
                            *(grating->a31[j][k]) = complex_0;
                        }

                        if (grating->a42[j][k] != NULL) {
                            *(grating->a42[j][k]) = complex_0;
                            *(grating->a24[j][k]) = complex_0;
                        }
                    }
                    
                    if(conjugate){
                        if(grating->a11[j][k] != NULL) grating->a11[j][k]->im *= -1;
                        if(grating->a12[j][k] != NULL) grating->a12[j][k]->im *= -1;
                        if(grating->a13[j][k] != NULL) grating->a13[j][k]->im *= -1;
                        if(grating->a14[j][k] != NULL) grating->a14[j][k]->im *= -1;
                        if(grating->a21[j][k] != NULL) grating->a21[j][k]->im *= -1;
                        if(grating->a22[j][k] != NULL) grating->a22[j][k]->im *= -1;
                        if(grating->a23[j][k] != NULL) grating->a23[j][k]->im *= -1;
                        if(grating->a24[j][k] != NULL) grating->a24[j][k]->im *= -1;
                        if(grating->a31[j][k] != NULL) grating->a31[j][k]->im *= -1;
                        if(grating->a32[j][k] != NULL) grating->a32[j][k]->im *= -1;
                        if(grating->a33[j][k] != NULL) grating->a33[j][k]->im *= -1;
                        if(grating->a34[j][k] != NULL) grating->a34[j][k]->im *= -1;
                        if(grating->a41[j][k] != NULL) grating->a41[j][k]->im *= -1;
                        if(grating->a42[j][k] != NULL) grating->a42[j][k]->im *= -1;
                        if(grating->a43[j][k] != NULL) grating->a43[j][k]->im *= -1;
                        if(grating->a44[j][k] != NULL) grating->a44[j][k]->im *= -1;
                    }
                }
            }
            
            break;
        default:
            bug_error("gratings, wrong number of ports");
    }
    
}

//! Fill relevant sparse matrix elements with space component terms

/*!
 * \param f frequency of light field to fill
 *
 * \todo qcorr branch untested
 */
void fill_matrix_space_elements(space_t *space, double f, bool conjugate) {
    double omega = TWOPI * f;
    // changed phi from plus to minus, see manual eq 3.32 281005
    double phi = omega * space->n / init.clight * space->L;

    // field amplitudes
    int j, k;
    for (j = 0; j < inter.num_fields; j++) {
        for (k = 0; k < inter.num_fields; k++) {

            int tn, tm;
            get_tem_modes_from_field_index(&tn, &tm, j);

            double gouyx = 0.0;
            double gouyy = 0.0;

            if (inter.set_tem_phase_zero & 2) {
                gouyx = tn * RAD * space->gouy_x;
                gouyy = tm * RAD * space->gouy_y;
            } else {
                gouyx = (tn + 0.5) * RAD * space->gouy_x;
                gouyy = (tm + 0.5) * RAD * space->gouy_y;
            }

            double c0 = cos(-phi + gouyx + gouyy);
            double s0 = sin(-phi + gouyx + gouyy);

            if (space->a12[j][k] != NULL) {
                (space->a12[j][k])->re = -c0;
                (space->a12[j][k])->im = -s0;

                *(space->a12[j][k]) = z_by_z(*(space->a12[j][k]), space->k12[j][k]);
                if(conjugate) space->a12[j][k]->im *= -1;
            }

            if (space->a21[j][k] != NULL) {
                (space->a21[j][k])->re = -c0;
                (space->a21[j][k])->im = -s0;

                *(space->a21[j][k]) = z_by_z(*(space->a21[j][k]), space->k21[j][k]);
                if(conjugate) space->a21[j][k]->im *= -1;
            }
        }
    }
}

void fill_matrix_block_elements(ifo_matrix_vars_t *matrix) {
    // fill matrix for spaces
    int block_index;
    int f_offset = 0;
    
    assert(matrix->type == STANDARD || matrix->type == SIGNAL_QCORR);
    
    if(matrix->type == STANDARD) {
        f_offset = 0;
    } else if(matrix->type == SIGNAL_QCORR) {
        f_offset = M_ifo_car.num_frequencies;
    }
    
    for (block_index = 0; block_index < inter.num_blocks; block_index++) {
        block_t *block = &(inter.block_list[block_index]);
        int fin, j, k;

        for(fin=0; fin < matrix->num_frequencies; fin++){
            bool isblocked = eq(matrix->frequencies[fin]->f, block->f_block_hz);
            
            for (j = 0; j < inter.num_fields; j++) {
                for (k = 0; k < inter.num_fields; k++) {
                    // block frequency from 
                    if (block->a12f[f_offset+fin][j][k] != NULL) {
                        (block->a12f[f_offset+fin][j][k])->re = (j==k && !isblocked) ? 1.0 : 0.0;
                        (block->a12f[f_offset+fin][j][k])->im = 0.0;
                    }

                    if (block->a21f[f_offset+fin][j][k] != NULL) {
                        (block->a21f[f_offset+fin][j][k])->re = (j==k) ? 1.0 : 0.0;
                        (block->a21f[f_offset+fin][j][k])->im = 0.0;
                    }
                }
            }
        }
    }
}

//! Fill relevant sparse matrix elements with space component terms

/*!
 * \param f frequency of light field to fill
 *
 * \todo qcorr branch untested
 */
void fill_matrix_sagnac_elements(bool conjugate) {
    // fill matrix for spaces
    int sagnac_index;
    for (sagnac_index = 0; sagnac_index < inter.num_sagnacs; sagnac_index++) {
        sagnac_t *sagnac;
        sagnac = &(inter.sagnac_list[sagnac_index]);

        // field amplitudes
        int j, k;
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {

                int tn, tm;
                get_tem_modes_from_field_index(&tn, &tm, j);

                double c0 = cos(RAD*sagnac->dphi);
                double s0 = sin(RAD*sagnac->dphi);

                // Sagnac effect means beam travelling one way has opposite 
                // phase change of that travelling in the other direction
                if (sagnac->a12[j][k] != NULL) {
                    (sagnac->a12[j][k])->re = c0;
                    (sagnac->a12[j][k])->im = (conjugate ? -s0 : s0);
                }

                if (sagnac->a21[j][k] != NULL) {
                    (sagnac->a21[j][k])->re = c0;
                    (sagnac->a21[j][k])->im = (conjugate ? s0 : -s0);
                }
            }
        }
    }
}



//! Compute the amplitude of a modulation sideband of an EOM

/*!
 * \param component_index component index
 * \param modulation_order order of modulation
 * \param sideband_index flag indicating upper or lower sideband
 *
 * \see Test_get_modulation_amplitude()
 */
complex_t get_modulation_amplitude(int component_index,
        int modulation_order, int sideband_index) {
    // sanity checks on inputs
    assert(component_index >= 0);
    assert(component_index < inter.num_components);
    // what should modulation_order be???

    assert(sideband_index == 1 || sideband_index == -1);

    if (sideband_index != 1 && sideband_index != -1) {
        bug_error("lower mod_amp");
    }

    modulator_t *modulator;
    modulator = &(inter.modulator_list[component_index]);

    double amplification_factor, phase;
    if (modulator->type == MODTYPE_AM) {
        amplification_factor = modulator->modulation_index * 0.25;
        // 310502 Keita, changed from 0.0 to current (following line)
        phase = sideband_index * modulator->phase;
    } else {
        amplification_factor = bessn(modulation_order, modulator->modulation_index);
        phase = sideband_index * modulation_order * (90.0 + modulator->phase);
        // set minus for J_-k = (-1)^k J_k
        if (odd(modulation_order)) {
            amplification_factor = sideband_index * amplification_factor;
        }
    }

    complex_t modulation_amplitude = co(amplification_factor, 0.0);
    modulation_amplitude = z_by_ph(modulation_amplitude, phase);

    return modulation_amplitude;
}


/**
 * The modulator coupling is different for quantum noise propagation, we can't
 * add fields coherently like signal fields.
 * 
 * TODO Currently we make the assumption that the modulator only couples vacuum
 * noise, no squeezed fields which this will not work for. This just sets all
 * coupling to 0 for the modulator and it should just act like a source.
 * 
 * @param matrix
 */
void fill_matrix_modulator_elements_quantum(ifo_matrix_vars_t *matrix){
    assert(matrix->type == SIGNAL_QCORR && "Only works with signal matrix");
    
    int f_offset = M_ifo_car.num_frequencies; // frequency RHS index offset of signal 
                                              // fields from carrier fields
    
    // fill matrix for modulators
    int i, j, k, fin, fout;
    for (i = 0; i < inter.num_modulators; i++) {

        modulator_t *mod;
        mod = &(inter.modulator_list[i]);
        complex_t tmp;
        
        int nj, mj, nk, mk;
        
        // field amplitudes
        for(fin = 0; fin < matrix->num_frequencies; fin++){
            bool do_conj = (matrix->type == SIGNAL_QCORR && matrix->frequencies[fin]->order == -1) ? true : false;
            
            for(fout = 0; fout < matrix->num_frequencies; fout++){
                // only compute if we have set these frequencies to couple
                if(matrix->mod_f_couple_allocd[i][fin][fout]){
                    if(fin == fout)
                        tmp = complex_m1;
                    else
                        tmp = complex_0;
                    
                    for (j = 0; j < inter.num_fields; j++) {
                        get_tem_modes_from_field_index(&nj, &mj, j);
                            
                        for (k = 0; k < inter.num_fields; k++) {
                            get_tem_modes_from_field_index(&nk, &mk, k);
                            
                            if(include_mode_coupling(&mod->a_cplng_12, nj, nk, mj, mk)){
                                if (mod->a12f[f_offset+fin][f_offset+fout][j][k] != NULL) {
                                    *(mod->a12f[f_offset+fin][f_offset+fout][j][k]) = z_by_z(tmp, mod->k12[j][k]);
                                    if(do_conj) mod->a12f[f_offset+fin][f_offset+fout][j][k]->im *= -1;
                                } else if(matrix->mod_does_f_couple[i][fin][fout])
                                    bug_error("No elements were got for modulator %s for coupling between frequency %i and %i, direction 12", mod->name, fin, fout);

                            }

                            if(include_mode_coupling(&mod->a_cplng_21, nj, nk, mj, mk)){
                                if (mod->a21f[f_offset+fin][f_offset+fout][j][k] != NULL) {
                                    *(mod->a21f[f_offset+fin][f_offset+fout][j][k]) = z_by_z(tmp, mod->k21[j][k]);
                                    if(do_conj) mod->a12f[f_offset+fin][f_offset+fout][j][k]->im *= -1;
                                } else if(matrix->mod_does_f_couple[i][fin][fout])
                                    bug_error("No elements were got for modulator %s for coupling between frequency %i and %i, direction 21", mod->name, fin, fout);
                            }
                        }
                    }
                } 
            }
        }
    }
}

/**
 * Fills the modulator matrix elements for the new all frequency matrix
 * calculation
 */
void fill_matrix_modulator_elements_new(ifo_matrix_vars_t *matrix) {
    // f_offset is used to select the pointers to the various frequency elements
    // for either the carrier or signal frequencies
    int f_offset = 0;
    
    assert(matrix->type == STANDARD || matrix->type == SIGNAL_QCORR);
    
    if(matrix->type == STANDARD) {
        f_offset = 0;
    } else if(matrix->type == SIGNAL_QCORR) {
        f_offset = M_ifo_car.num_frequencies;
    }
    
    // fill matrix for modulators
    int i, j, k, fin, fout;
    for (i = 0; i < inter.num_modulators; i++) {

        modulator_t *mod;
        mod = &(inter.modulator_list[i]);

        complex_t tmp = complex_0;
        
        // field amplitudes
        for(fin = 0; fin < matrix->num_frequencies; fin++){
            
            bool do_conj = (matrix->type == SIGNAL_QCORR && matrix->frequencies[fin]->order == -1) ? true : false;
                    
            for(fout = 0; fout < matrix->num_frequencies; fout++){
                // only compute if we have set these frequencies to couple
                if(matrix->mod_f_couple_allocd[i][fin][fout]){
                    
                    if(matrix->mod_does_f_couple[i][fin][fout]){
                        // check if we are using conjugate of lower sidebands that we don't couple to
                        // another field which isn't a conjugate, i.e. another lower sideband
                        if(!inter.warned_mod_up_low_coupling && do_conj && matrix->frequencies[fout]->order != -1){
                            warn("Modulator %s is coupling a lower signal sideband to a non-conjugated sideband, will give wrong results.\n", mod->name);
                            inter.warned_mod_up_low_coupling = true;
                        }
                        
                        int order = matrix->mod_f_couple_order[i][fin][fout];

                        if (mod->type == MODTYPE_AM) {

                            // ensure order is between the max and min of the component
                            assert(mod->order == 1);
                            assert(order <= mod->order && order >= -mod->order);

                            if(order == 0){
                                tmp.re = 1.0 - mod->modulation_index * 0.5;
                                tmp.im = 0;
                            }else
                                tmp = get_modulation_amplitude(i, order, (order < 0) ? -1 : 1);

                        } else if(mod->type == MODTYPE_PM) {

                            // ensure order is between the max and min of the component
                            assert(order <= mod->order && order >= -mod->order);

                            if(order == 0){
                                double Jk = bessn(0, mod->modulation_index);
                                
                                // if a single sideband is created then we need to
                                // scale up the carrier amplitude by the amount that would
                                // normally go into the missing sideband
                                if (mod->single_sideband_mode == ON) {
                                    Jk = 1.0 - (1.0 - Jk) * 0.5;
                                }
                                
                                tmp = z_by_x(pow_complex_i(order), Jk);
                                
                            }else{
                                tmp = get_modulation_amplitude(i, (order < 0) ? -order : order, (order < 0) ? -1 : 1);
                            }
                        } else if(mod->type == MODTYPE_PITCH || mod->type == MODTYPE_YAW){
                            // TODO add in refractive index
                            if (fin == fout){
                                tmp = complex_1;
                            } else {
                                tmp = z_by_xph(complex_i, mod->modulation_index * TWOPI / init.lambda, mod->phase);
                            }

                        } else if(mod->type == MODTYPE_W0){
                            if (fin == fout){
                                tmp = complex_1;
                            } else {
                                tmp = z_by_xph(complex_1, mod->modulation_index, mod->phase);
                            }
                        } else if(mod->type == MODTYPE_Z){
                            if (fin == fout){
                                tmp = complex_1;
                            } else {
                                tmp = z_by_xph(complex_i, mod->modulation_index, mod->phase);
                            }
                        } else
                            bug_error("modulator type %i unhandled", mod->type);
                        
                    } else
                        tmp = complex_0; // make sure we zero any allocated but not coupling matrix elements
                    
                    int nj,mj,nk,mk;
                    
                    zmatrix k12, k21;
                    
                    // Set the coupling matrix to use depending on the modulation
                    // type. Coupling between same frequency always uses the
                    // normal k matrix
                    if(fin != fout && (mod->type == MODTYPE_YAW || mod->type == MODTYPE_PITCH || mod->type == MODTYPE_W0 || mod->type == MODTYPE_Z)) {
                        k12 = mod->ktm12;
                        k21 = mod->ktm21;
                    } else {
                        k12 = mod->k12;
                        k21 = mod->k21;
                    }
                    
                    for (j = 0; j < inter.num_fields; j++) {
                        get_tem_modes_from_field_index(&nj, &mj, j);
                        
                        for (k = 0; k < inter.num_fields; k++) {
                            get_tem_modes_from_field_index(&nk, &mk, k);
                            
                            if(include_mode_coupling(&mod->a_cplng_12, nj, nk, mj, mk)){
                                if (mod->a12f[f_offset+fin][f_offset+fout] != NULL) {
                                    *(mod->a12f[f_offset+fin][f_offset+fout][j][k]) = z_by_x(z_by_z(tmp, k12[j][k]), -1.0);
                                    if(do_conj) mod->a12f[f_offset+fin][f_offset+fout][j][k]->im *= -1;
                                    
                                    //warn("%i %i %i%i->%i%i: %s\n", fin, fout, nj, mj, nk, mk, complex_form(*(mod->a12f[f_offset+fin][f_offset+fout][j][k])));
                                    
                                } else if(matrix->mod_does_f_couple[i][fin][fout])
                                    bug_error("No elements were got for modulator %s for coupling between frequency %i and %i, direction 12", mod->name, fin, fout);
                            }
                            
                            if(include_mode_coupling(&mod->a_cplng_21, nj, nk, mj, mk)){
                                if (mod->a21f[f_offset+fin][f_offset+fout][j][k] != NULL) {
                                    *(mod->a21f[f_offset+fin][f_offset+fout][j][k]) = z_by_x(z_by_z(tmp, k21[j][k]), -1.0);
                                    if(do_conj) mod->a21f[f_offset+fin][f_offset+fout][j][k]->im *= -1;
                                } else if(matrix->mod_does_f_couple[i][fin][fout])
                                    bug_error("No elements were got for modulator %s for coupling between frequency %i and %i, direction 21", mod->name, fin, fout);
                            }
                        }
                    }
                } 
            }
        }
    }
}

//! Fill relevant sparse matrix elements with lens component terms

/*!
 *
 * \todo qcorr branch untested
 */
void fill_matrix_lens_elements(bool conjugate) {
    // fill matrix for lenses
    int lens_index;
    for (lens_index = 0; lens_index < inter.num_lenses; lens_index++) {
        lens_t *lens;
        lens = &(inter.lens_list[lens_index]);

        // field amplitudes
        int j, k;
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (lens->a12[j][k] != NULL) {
                    (lens->a12[j][k])->re = -1.0;
                    (lens->a12[j][k])->im = 0.0;

                    *(lens->a12[j][k]) = z_by_z(*(lens->a12[j][k]), lens->k12[j][k]);
                    if(conjugate) lens->a12[j][k]->im *= -1;
                }

                if (lens->a21[j][k] != NULL) {
                    (lens->a21[j][k])->re = -1.0;
                    (lens->a21[j][k])->im = 0.0;

                    *(lens->a21[j][k]) = z_by_z(*(lens->a21[j][k]), lens->k21[j][k]);
                    if(conjugate) lens->a21[j][k]->im *= -1;
                }
            }
        }
    }
}

void fill_matrix_dbs_elements(ifo_matrix_vars_t *matrix) {
    // fill matrix for diodes
    int dbs_index;
    
    for (dbs_index = 0; dbs_index < inter.num_dbss; dbs_index++) {
        dbs_t *dbs;
        dbs = &(inter.dbs_list[dbs_index]);
        
        // f_offset is used to select the pointers to the various frequency elements
        // for either the carrier or signal frequencies
        int f_offset = 0;

        assert(matrix->type == STANDARD || matrix->type == SIGNAL_QCORR);

        if(matrix->type == STANDARD) {
            f_offset = 0;
        } else if(matrix->type == SIGNAL_QCORR) {
            f_offset = M_ifo_car.num_frequencies;
        }
        
        // fill matrix for modulators
        int j, k, f, nj, mj, nk, mk;
        
        for(f = 0; f < matrix->num_frequencies; f++){
            bool do_conj = (matrix->type == SIGNAL_QCORR && matrix->frequencies[f]->order == -1) ? true : false;
            
            for (j = 0; j < inter.num_fields; j++) {
                get_tem_modes_from_field_index(&nj, &mj, j);

                for (k = 0; k < inter.num_fields; k++) {
                    get_tem_modes_from_field_index(&nk, &mk, k);

                    if (dbs->a13f[f_offset+f][j][k] != NULL) {
                        *dbs->a13f[f_offset+f][j][k] = complex_m1;

                        *(dbs->a13f[f_offset+f][j][k]) = z_by_z(*(dbs->a13f[f_offset+f][j][k]), dbs->k13[j][k]);

                        if(do_conj)
                            dbs->a13f[f_offset+f][j][k]->im *= -1;
                    }

                    if (dbs->a34f[f_offset+f][j][k] != NULL) {
                        *dbs->a34f[f_offset+f][j][k] = complex_m1;

                        *(dbs->a34f[f_offset+f][j][k]) = z_by_z(*(dbs->a34f[f_offset+f][j][k]), dbs->k34[j][k]);

                        if(do_conj)
                            dbs->a34f[f_offset+f][j][k]->im *= -1;
                    }

                    if (dbs->a21f[f_offset+f][j][k] != NULL) {
                        *dbs->a21f[f_offset+f][j][k] = complex_m1;

                        *(dbs->a21f[f_offset+f][j][k]) = z_by_z(*(dbs->a21f[f_offset+f][j][k]), dbs->k21[j][k]);

                        if(do_conj)
                            dbs->a21f[f_offset+f][j][k]->im *= -1;
                    }

                    if (dbs->a42f[f_offset+f][j][k] != NULL) {
                        *dbs->a42f[f_offset+f][j][k] = complex_m1;

                        *(dbs->a42f[f_offset+f][j][k]) = z_by_z(*(dbs->a42f[f_offset+f][j][k]), dbs->k42[j][k]);

                        if(do_conj)
                            dbs->a42f[f_offset+f][j][k]->im *= -1;
                    }
                }
            }

        }
    }
}

/*!
 * Fill relevant sparse matrix elements with diode component terms
 */
void fill_matrix_diode_elements(bool conjugate) {
    // fill matrix for diodes
    int diode_index;
    
    for (diode_index = 0; diode_index < inter.num_diodes; diode_index++) {
        diode_t *diode;
        diode = &(inter.diode_list[diode_index]);

        double loss = sqrt(1.0 - diode->loss);
        double leak = pow(10.0, -diode->S / 20.0);
        double out = sqrt(1.0 - pow(10.0, -diode->S / 10.0));
        
        // field amplitudes
        int j, k;
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                switch(diode->option) {
                    case 0:
                        if (diode->a12[j][k] != NULL) {
                            (diode->a12[j][k])->re = -1.0;
                            (diode->a12[j][k])->im = 0.0;

                            *(diode->a12[j][k]) = z_by_x(z_by_z(*(diode->a12[j][k]), diode->k12[j][k]), loss);
                            if(conjugate) diode->a12[j][k]->im *= -1;
                        }

                        if (diode->a21[j][k] != NULL) {
                            (diode->a21[j][k])->re = -leak;
                            (diode->a21[j][k])->im = 0.0;

                            *(diode->a21[j][k]) = z_by_x(z_by_z(*(diode->a21[j][k]), diode->k21[j][k]), loss);
                            if(conjugate) diode->a21[j][k]->im *= -1;
                        }

                        if (diode->a23[j][k] != NULL) {
                            // output from node 2 to node 3 is whatever is not suppressed
                            (diode->a23[j][k])->re = -out;
                            (diode->a23[j][k])->im = 0.0;

                            *(diode->a23[j][k]) = z_by_x(z_by_z(*(diode->a23[j][k]), diode->k23[j][k]), loss);
                            if(conjugate) diode->a23[j][k]->im *= -1;
                        }

                        if (diode->a32[j][k] != NULL) {
                            (diode->a32[j][k])->re = -out;
                            (diode->a32[j][k])->im = 0.0;

                            *(diode->a32[j][k]) = z_by_x(z_by_z(*(diode->a32[j][k]), diode->k32[j][k]), loss);
                            if(conjugate) diode->a32[j][k]->im *= -1;
                        }
                        break;
                        
                    case 1:
                        if (diode->a12[j][k] != NULL) {
                            (diode->a12[j][k])->re = -0.0;
                            (diode->a12[j][k])->im = 0.0;
                            
                            *(diode->a12[j][k]) = z_by_x(z_by_z(*(diode->a12[j][k]), diode->k12[j][k]), loss);
                            if(conjugate) diode->a12[j][k]->im *= -1;
                        }

                        if (diode->a21[j][k] != NULL) {
                            (diode->a21[j][k])->re = -1.0;
                            (diode->a21[j][k])->im = 0.0;

                            *(diode->a21[j][k]) = z_by_x(z_by_z(*(diode->a21[j][k]), diode->k21[j][k]), loss);
                            if(conjugate) diode->a21[j][k]->im *= -1;
                        }

                        if (diode->a23[j][k] != NULL) {
                            (diode->a23[j][k])->re = 0.0;
                            (diode->a23[j][k])->im = 0.0;
                        }

                        if (diode->a32[j][k] != NULL) {
                            (diode->a32[j][k])->re = -1;
                            (diode->a32[j][k])->im = 0.0;

                            *(diode->a32[j][k]) = z_by_x(z_by_z(*(diode->a32[j][k]), diode->k32[j][k]), loss);
                            if(conjugate) diode->a32[j][k]->im *= -1;
                        }
                        break;
                }
            }
        }
    }
}

//! Set a value of the RHS vector

/*!
 * \param vector_index The vector index (aka vector element) to set to z_value
 * \param z_value The value to set for the RHS vector element
 * \param matrix_type The type of matrix to set up (STANDARD or QCORR)
 */
void klu_rhs(int vector_index, complex_t z_value, int matrix_type) {
    // sanity checks on inputs
    assert(matrix_type == STANDARD || matrix_type == SIGNAL_QCORR);

    // assign the value to the RHS vector element
    switch (matrix_type) {
        case STANDARD:
            // make sure the element to be assigned is within bounds
            assert(vector_index > -1);
            assert(vector_index < inter.num_eqns);

            klu.rhs[2 * vector_index] = z_value.re;
            klu.rhs[2 * vector_index + 1] = z_value.im;
            break;
        case SIGNAL_QCORR:
            // make sure the element to be assigned is within bounds
            assert(vector_index > -1);
            break;
        default:
            bug_error("no such type");
    }
}

//! Set a value of the RHS vector

/*!
 * \param vector_index The vector index (aka vector element) to set to z_value
 * \param z_value The value to set for the RHS vector element
 * \param matrix_type The type of matrix to set up (STANDARD or QCORR)
 */
void set_rhs(int vector_index, complex_t z_value, int matrix_type) {
    // sanity checks on inputs
    assert(matrix_type == STANDARD || matrix_type == SIGNAL_QCORR);
    
    // NaN is the only value that returns false for these clauses
    assert(!(z_value.re != z_value.re && z_value.im != z_value.im) && "RHS is a Nan");

    if(matrix_type == STANDARD)
        set_rhs_ccs_matrix(vector_index, z_value, &M_ifo_car);
    else if(matrix_type == SIGNAL_QCORR)
        set_rhs_ccs_matrix(vector_index, z_value, &M_ifo_sig);
    else
        bug_error("matrix_type not handled");
}


//! Clear the RHS vector (reset to zero)

/*!
 * \param matrix_type the type of matrix to clear (standard or quantum)
 */
void clear_rhs(int matrix_type) {
    // sanity checks on inputs
    assert(matrix_type == STANDARD || matrix_type == SIGNAL_QCORR);

    if(matrix_type == STANDARD)
        clear_ccs_matrix_rhs(&M_ifo_car);
    else if(matrix_type == SIGNAL_QCORR)
        clear_ccs_matrix_rhs(&M_ifo_sig);
    else
        bug_error("matrix_type not handled");
}

//! Factor a sparse matrix

/*!
 * \param matrix_type the type of matrix to factor (standard or quantum)
 */
void factor_matrix(int matrix_type) {
    //int tid = startTimer("SOLVE_MATRIX");
    // sanity checks on inputs
    assert(matrix_type == STANDARD || matrix_type == SIGNAL_QCORR);

    if(matrix_type == STANDARD)
        refactor_ccs_matrix(&M_ifo_car);
    else if(matrix_type == SIGNAL_QCORR)
        refactor_ccs_matrix(&M_ifo_sig);
    else
        bug_error("matrix_type not handled");
    
    //endTimer(tid);
}

//! Check connections in interferometer
/*!
 *
 */
void check_connections(void) {
    int i;

    for (i = 0; i < inter.num_nodes; i++) {
        /*
        if ((inter.node_list[i].connect < 2) && 
            (inter.node_list[i].io == 0) && 
            (spa.sprhs[2*inter.num_fields*i + 1].re == 0) && 
            (spa.sprhs[2*inter.num_fields*i + 1].im == 0) &&
            NOT inter.node_list[i].gnd_node) {
          // removed warning for the moment as I believe this
          // doesnt really concern the user but prevents him/her
          // from seeing the important warnings
          //warn("Warning: node %s less than 2 connections.\n", node_print(i));
        }
         */

        if (inter.node_list[i].connect == 2 && inter.node_list[i].io & 2) {
            warn("Warning: input at node %s inside interferometer.\n",
                    node_print(i));
        }

        if (inter.node_list[i].connect > 2) {
            gerror(" node %s more than 2 connections.\n", node_print(i));
        }

        if ((inter.node_list[i].connect == 0) && (inter.node_list[i].io)) {
            gerror(" input/output at node %s not connected to interferometer.\n",
                    node_print(i));
        }
    }
}


//! Update element count for KLU matrix (CCS format)

/*!
 * \param node_index1 index of node1 of component
 * \param node_index1 index of node2 of component (call this function twice for bs)
 * \param colum_index1 matrix index referring to node1 entry
 * \param colum_index2 matrix index referring to node2 entry
 * \param mode which type of updating should be performed (i.e. reflection+ transmission - or - transmission only)
 *
 * mode MIRROR:  n1->n2, n2->n1, n1->n1, n2->n2
 * mode SPACE:   n1->n2, n2->n1
 * mode GRATING: n2->n2 (this just filles the last connections after update has been called with MIRROR+SPACE already)
 *
 */
void update_element_count(int node_index1, int node_index2, int row_index_1, int row_index_2, int mode) {
    node_t node1, node2;
    node1 = inter.node_list[node_index1];
    node2 = inter.node_list[node_index2];

    switch (mode) {
        case MIRROR:
            if (NOT node1.gnd_node) {
                klu.col_ptr[row_index_1]++;
            }
            if (NOT node2.gnd_node) {
                klu.col_ptr[row_index_2]++;
            }
        case SPACE:
            if ((NOT node1.gnd_node) && (NOT node2.gnd_node)) {
                klu.col_ptr[row_index_1]++;
                klu.col_ptr[row_index_2]++;
            }
            break;
        case GRATING:
            if (NOT node2.gnd_node) {
                klu.col_ptr[row_index_2]++;
            }
            break;
        default:
            bug_error("incorrect mode");
    }
}

//! Reset connect counts for all nodes

/*!
 *
 */
void reset_connect_count(void) {
    int i;
    for (i = 0; i < inter.num_nodes; i++) {
        inter.node_list[i].connect = 0;
    }
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

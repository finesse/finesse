// $Id$
/*!
         * \file kat_server.c
 * \brief routines related to the server mode of FINESSE
 *
 * The server-client functionality and the code in this file
 * have been provided by Martin Hewitson (martin.hewitson@aei.mpg.de)
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_config.h"
#include "kat_server.h"
#include "kat_read.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_calc.h"

extern init_variables_t in;
extern interferometer_t inter;
extern options_t options;
extern local_var_t vlocal;

static bbuffer *cq; //!< the main command queue
static int lastid; //!< the last id???

char largebuffer[STRBUFFLEN]; //!< string for read buffer

/*! send a string to the socket descriptor 
 *
 * \param socketfd to be documented
 * \param s to be documented
 *
 * \todo complete documentation of arguments
 *
 * \retval sent - number of bytes sent 
 */
int sendStringl(int socketfd, char *s) {
    int sent;
    long int len;
#if DEBUG
    debug_msg("sending %s\n", s);
#endif

    len = strlen(s) + 1;
    sendLong(socketfd, len);
    sent = send(socketfd, s, len, 0);

    return sent;
}

/*! read a string 
 *
 * \param socketfd to be documented
 * \param s to be documented
 *
 * \todo complete documentation of arguments
 *
 * \retval read - number of bytes read 
 */
int readStringl(int socketfd, char **s) {
    int read;
    long int strlength;

    read = readLong(socketfd, &strlength);
    
    if (strlength >= STRBUFFLEN)
        gerror("Servermode, command exceeds reading buffer");

    largebuffer[0] = '\0';
    *s = largebuffer;

    read = recv(socketfd, *s, strlength, 0);

    return read;
}

/*! send a double
 *
 * \param socketfd to be documented
 * \param d to be documented
 *
 * \todo complete documentation of arguments
 * 
 */
int sendDouble(int socketfd, double d) {
    int sent;
    char packed[8] = {0};

    pack_double(d, &packed[0], 1);

    //  sent=sendStringl(socketfd, packed);
    sent = send(socketfd, packed, 8, 0);
    return sent;
}

/*! Read a double from a file stream 
 *
 * \param socketfd to be documented
 * \param d to be documented
 *
 * \todo complete documentation of arguments
 *  
 *  \retval read - number of bytes read 
 */
int readDouble(int socketfd, double *d) {
    int received;
    char packed[8] = {0};


    // read data
    //	received = readStringl (socketfd, &packed);
    received = recv(socketfd, &packed, 8, 0);

    *d = unpack_double(&packed[0], 1);

    return received * sizeof (double);
}

/*! Receive and parse commands from socket and call the appropriate functions
 *
 * \param sd to be documented
 * \param name to be documented
 *
 * \todo complete documentation of arguments
 */
int receivecommands_sock(int sd, char *name) {
    UNUSED(name);
    char *buff;
    int got;
    long int nop;
    double x;

    nop = -1;

    // set socket to tcp-nodelay

    int flag = 1;
    int result = setsockopt(sd, /* socket affected */
            IPPROTO_TCP, /* set option at TCP level */
            TCP_NODELAY, /* name of option */
            (char *) &flag, /* the cast is historical 
                                              cruft */
            sizeof (int)); /* length of option value */
    
    if (result < 0)
        warn("Warning: cannot set socket to TCP_NOLDELAY");


    while (1) {
        got = readStringl(sd, &buff);

        if (got < 0) {
            warn("### receivecommands: couldn't read command\n");
        } else {
            if (inter.debug)
                debug_msg("*** receivecommands: received %s from %s ... checking command list\n", buff, name);

            // process the "quit" command
            if ((strncmp(buff, "QUIT", strlen("QUIT"))) == 0) {
                warn("*** receivecommands: processing quit command\n");
                return (0);
            } else if ((strncmp(buff, "INIT", strlen("INIT"))) == 0) {
                fprintf(stdout, "*** receivecommands: processing init command\n");
                got = readStringl(sd, &buff);
                fprintf(stdout, "received %s\n", buff);
                got = readLong(sd, &nop);
                fprintf(stdout, "received %ld\n", nop);
                got = readStringl(sd, &buff);
                fprintf(stdout, "received %s\n", buff);
                got = readStringl(sd, &buff);
                fprintf(stdout, "received %s\n", buff);
                got = readLong(sd, &nop);
                fprintf(stdout, "received %ld\n", nop);
                got = readLong(sd, &nop);
                fprintf(stdout, "received %ld\n", nop);
                got = readDouble(sd, &x);
                fprintf(stdout, "received %g\n", x);
                got = readStringl(sd, &buff);
                fprintf(stdout, "received %s\n", buff);
                fflush(stdout);
            } else if ((strncmp(buff, "CONFIG", strlen("CONFIG"))) == 0) {
                warn("*** receivecommands: processing CONFIG command\n");
                nop = read_tune_params_sock(sd);
                if (nop < 1) {
                    sendStringl(sd, "ERROR");
                } else {
                    sendStringl(sd, "OK");
                }
                warn("*** receivecommands: received %ld parameters\n", nop);
                inter.num_tune_params = (int) nop;
                sendLong(sd, inter.num_output_cols - 1);
            } else if ((strncmp(buff, "TUNE", strlen("TUNE"))) == 0) {
                if (inter.debug) {
                    debug_msg("*** receivecommands: processing TUNE command\n");
                    debug_msg("*** reading %d values\n", inter.num_tune_params);
                    debug_msg("*** receivecommands: received %ld parameters\n", nop);
                }
                read_tune_values_sock(sd);
                write_server_result_sock(sd);
            } else if ((strncmp(buff, "INFO", strlen("INFO"))) == 0) {
                warn("*** receivecommands: processing INFO command\n");
                if (inter.num_tune_params < 1) {
                    sendStringl(sd, "ERROR");
                } else {
                    sendStringl(sd, "OK");
                }
                sendLong(sd, inter.num_tune_params);

                write_current_tune_values_sock(sd);
            } else {
                warn("*** unknown command: %s\n", buff);
            }
        }
    }
    return 0;
}

/*! to be documented
 *
 * \param socketfd to be documented
 *
 * \todo complete documentation
 */
int read_tune_params_sock(int socketfd) {
    char *buffer;
    int i, j;
    int nread;
    long int nop;
    char component[MAX_TOKEN_LEN] = {0};
    char parameter[MAX_TOKEN_LEN] = {0};
    char unit[MAX_TOKEN_LEN] = {0};
    //  char sval[LINE_LEN] = { 0 };
    char *rest, *tscan, *tscanptr;
    tune_t *tune;
    int ptr;

    rest = (char*) malloc(BUFF_LEN);
    tscan = (char*) malloc(BUFF_LEN);

    if ((rest == NULL) || (tscan == NULL)) {
        fprintf(stderr, "### read_tune_params: error allocating buffer memory\n");
        free(rest);
        free(tscan);
        return (-1);
    }

    readLong(socketfd, &nop);

    fprintf(stdout, "Number of Parameters = %ld\n", nop);
    //fflush(stdout);

    if (nop < 1) {
        fprintf(stderr, "### read_tune_params: wrong number of parameters\n");
        free(rest);
        free(tscan);
        return (-1);
    }

    readStringl(socketfd, &buffer);
    if (strlen(buffer) >= BUFF_LEN) {
        fprintf(stderr, "### parameter list too long: %s\n", buffer);
        free(rest);
        free(tscan);
        return (-1);
    }
    fprintf(stdout, "Read parameter list: %s\n", buffer);

    // allocate memory for the parameters to be tuned
    inter.tune_list = (tune_t *) calloc(nop + 1, sizeof (tune_t));
    if (inter.tune_list == NULL) {
        fprintf(stderr, "### read_tune_params: error allocating parameter memory\n");
        free(rest);
        free(tscan);
        return (-1);
    }


    nread = 0;
    strcpy(tscan, buffer);
    tscanptr = tscan;
    for (i = 0; i < nop; i++) {
        if ((nread = sscanf(tscanptr, "%14s %14s%n %s", component, parameter,
                &ptr, rest)) < 2) {
            fprintf(stderr, "parameter string '%s':\n"
                    "expected 'component parameter'\n", buffer);
            {
                free(rest);
                free(tscan);
                return -1;
            }
        }

        tune = &inter.tune_list[i];

        j = get_component_index_from_name(component);
        if (j == NOT_FOUND) {
            fprintf(stderr, "parameter string '%s':\ncomponent name '%s' not found.\n", buffer, component);
        }

        if (get_xparam(&tune->target, j, parameter, unit, buffer, &tune->lborder,
                &tune->uborder, &tune->min, &tune->max)) {
            free(rest);
            free(tscan);
            return (-1);
        }

        // check for more than one pointer to the same value
        for (j = 0; j < i; j++) {

            if (tune->target == inter.tune_list[j].target) {
                fprintf(stderr, "same parameter (%s) used twice\n", parameter);
                {
                    free(rest);
                    free(tscan);
                    return (-1);
                }
            }
        }

        for (j = 0; j < inter.num_put_cmds; j++) {
            if (tune->target == inter.put_list[j].target) {
                fprintf(stderr, "parameter '%s' used with a 'put'\n", parameter);
                {
                    free(rest);
                    free(tscan);
                    return (-1);
                }
            }
        }

        tune->startvalue = *(tune->target);

        tscanptr = tscanptr + ptr;
    }

    if (nread > 2) {
        warn("line '%s':\ntext '%s' ignored\n", buffer, rest);
    }

    free(rest);
    free(tscan);

    return nop;
}



//! 

/*! This function reads the parameter list following the 
 *  tune command. It returns the number of parameters read.
 *  Or -1 on error.
 *
 *  \param socketfd to be documented
 *
 *  \todo to be documented completely
 */
int read_tune_values_sock(int socketfd) {
    int i;
    tune_t tune_tmp;
    double x;

    for (i = 0; i < inter.num_tune_params; i++) {
        readDouble(socketfd, &x);

        /* possible extion to have 'additional' mode, like put*
           if (puttmp.mode) {
           x = inter.put_list[i].startvalue + (*(inter.put_list[i].source));
           }
           else {
           x = *(inter.put_list[i].source);
           }
         */

        tune_tmp = inter.tune_list[i];

        if ((tune_tmp.lborder == 1 && x < tune_tmp.min)
                || (tune_tmp.uborder == 1 && x > tune_tmp.max) || (tune_tmp.lborder == 2
                && x <= tune_tmp.min)
                || (tune_tmp.uborder == 2 && x >= tune_tmp.max)) {
            server_gerror("parameter %s %s out of range\n", tune_tmp.cname, tune_tmp.pname);
            return (-1);
        }
        inter.tune_list[i].value = x;
    }

    for (i = 0; i < inter.num_tune_params; i++) {
        if (inter.debug) {
            message("tune %d : %g\n", i, inter.tune_list[i].value);
        }

        *(inter.tune_list[i].target) = inter.tune_list[i].value;
    }

    return (0);
}


//!

/*!
 * \todo undocumented
 */
int write_current_tune_values_sock(int socketfd) {
    int i;
    double dout;

    for (i = 0; i < inter.num_tune_params; i++) {
        dout = *(inter.tune_list[i].target);

        if (inter.debug)
            debug_msg("Sending: %g\n", dout);
        
#ifndef OSWIN
        sendDouble(socketfd, dout);
#endif
    }
    return (0);
}

/*! Create a bounded buffer of given length
 *
 * \param length to be documented
 *
 * \todo complete documentation
 *
 * \retval a pointer to the new bounded buffer
 */
void *create_bounded_buffer(int length) {
    bbuffer *b = 0x0; // new boundedbuffer structure 

    // allocate memory for bounded buffer
    b = (bbuffer*) malloc(sizeof (bbuffer));
    if (b == NULL) {
        fprintf(stderr, "### create_bounded_buffer: error creating bounded buffer.\n");
        return NULL;
    }
    // allocate memory to store the addresses of the objects
    // in the buffer
    b->objects = (void**) malloc(sizeof (void*) * length);

    b->maxLength = length; // set max length
    b->head = 0; // set head to start 
    b->tail = 0; // set tail to start
    b->length = 0; // set length to zero

    // create semaphore
    pthread_mutex_init(&(b->bb_lock), NULL);

    return (void*) b; // return pointer to this bounded buffer.
}

/*! Destroy a boundedbuffer
 *
 * \param b - a pointer to the bounded buffer structure
 */
void destroy_bounded_buffer(bbuffer *b) {
    pthread_mutex_destroy(&(b->bb_lock)); // destroy mutex
    free(b->objects); // free the array of object addresses
    free(b); // free the boundedbuffer structure
}

/*! Get the address at the head and move the head on.
 *
 * \param b - a pointer to the bounded buffer structure
 *
 * \retval void *val - the address at the head of the queue
 */
void *get_head(bbuffer *b) {
    void *val; // the current head address

    // move the head on one
    val = b->objects[b->head];

    b->head = (b->head + 1) % b->maxLength; // move the head element on one

    return val;
}

/*! Get the address offset from head
 *
 * \param b - a pointer to the bounded buffer structure
 * \param offset to be documented
 *
 * \todo complete documentation
 *
 * \retval void *val - the address at the head of the queue
 */
void *get_head_offset(bbuffer *b, int offset) {
    void *val; // the current head address
    int idx;

    idx = (b->head - 1 - offset);

    if (idx < 0)
        idx += b->maxLength;
    idx %= b->maxLength;

    //idx = (b->head -1 - offset)  - (b->head -1 - offset)/b->maxLength;
    // move the head on one
    
    val = b->objects[idx];

    //b->head = (b->head + 1) % b->maxLength;       // move the head element on one
    
    return val;
}

/*! Get the address at the tail and move the tail on. 
 *
 * \param b - a pointer to the bounded buffer structure 
 *
 * \retval void *val - the address at the tail of the queue    
 */
void *get_tail(bbuffer *b) {
    void *val; // the address to return

    // store the address held at the tail
    val = b->objects[b->tail];

    // move the tail on one.
    b->tail = (b->tail + 1) % b->maxLength;

    return val; // return the address
}

/*! This call will be started in a thread and only talks to clients
 * to fill the command queue and return results from the results queue.
 *
 * \param port to be documented
 *
 * \todo complete documentation
 *
 * *** We should catch a HUP signal and exit the thread properly.
 */
void *cmd_listen(void *port) {
    struct sockaddr_in sin; // local socket address
    struct sockaddr_in pin; // remote socket address
    int sd; // server socket descriptor
    int tsd; // connected socket descriptor
    unsigned int addr_size; // the size in bytes of address struct
    int sock_timeout = 1; // used 
    char buf[200]; // a string buffer
    int numconnections; // number of connections

    // init
    lastid = 0;
    numconnections = 0;

    // create a socket
    sd = socket(AF_INET, SOCK_STREAM, 0);

    // set the socket address/port to be reusable immediately after 
    // program exits
    if ((setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &sock_timeout, sizeof (int))) < 0) {
        fprintf(stderr, "### listen: error setting socket options.\n");
        exit(-1);
    }

    // set local socket address details
    bzero(&sin, sizeof (sin)); // clear address struct
    sin.sin_family = AF_INET; // set inet family
    // adf, 19.09.2013, why is this htonll?
    //sin.sin_addr.s_addr = htonll(INADDR_ANY); // set address
    //sin.sin_port = htonll((long) port); // set port
    sin.sin_addr.s_addr = htonl(INADDR_ANY); // set address
    sin.sin_port = htons((long) port); // set port

    // bind socket to address
    if ((bind(sd, (struct sockaddr *) &sin, sizeof (sin))) < 0) {
        fprintf(stderr, "### listen: error binding socket to address.\n");
        exit(-1);
    }

    // set this socket to listen -- cue up NCONN connections
    if ((listen(sd, NCONN)) < 0) {
        fprintf(stderr, "### listen: can't listen on this socket.\n");
        exit(-1);
    }

    message("*** listen: listening on port %hi\n", ntohs(sin.sin_port));

    // accept connections forever...
    while (1) {
        char connectee[200] = {0};

        // accept a connection
        addr_size = (sizeof (pin));

        if ((tsd = accept(sd, (struct sockaddr *) &pin, &addr_size)) < 0) {
            fprintf(stderr, "### listen: couldn't accept connection.\n");
        } else {
            // get connectee's name
            strcpy(connectee, inet_ntoa(pin.sin_addr));
            sprintf(buf, ":%hi", ntohs(pin.sin_port));
            strcat(connectee, buf);
            message("\n*** listen: accepted a connection from %s\n", connectee);

            numconnections++;
            message("*** listen: num connections = %d\n\n", numconnections);
            fflush(stdout);

            // receive commands
            receivecommands_sock(tsd, connectee);
            numconnections--;
            message("*** listen: num connections = %d\n\n", numconnections);

        }
    }
}

/*! This funtion starts the threads for listening for connections
 *  and for running commands 
 */
void initserver(void) {
    job jobbuff[QUEUE_LENGTH]; // An array of jobs
    int j, rc, status;

    // thread bits
    pthread_t pt;
    pthread_attr_t pt_attr; // attribute structure for process thread
    pthread_t lt;
    pthread_attr_t lt_attr; // attribute structure for listen thread

    // setup a command queue
    if ((cq = create_bounded_buffer(QUEUE_LENGTH)) == NULL) {
        fprintf(stderr, "### error creating bounded buffer for command queue.\n");
        exit(-1);
    }

    // and wrap this bounded buffer around the job array
    for (j = 0; j < QUEUE_LENGTH; j++)
        cq->objects[j] = (void*) &jobbuff[j];


    
    // start a listen thread for filling the process queue
    pthread_attr_init(&lt_attr);
    pthread_attr_setdetachstate(&lt_attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&lt, &lt_attr, cmd_listen, (void *) (long) (options.portnumber));
    
    // Start a thread to process the command queue
    pthread_attr_init(&pt_attr);
    pthread_attr_setdetachstate(&pt_attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&pt, &pt_attr, cmd_process, NULL);



    // then we block until both threads terminate, which may be never

    pthread_attr_destroy(&pt_attr);
    pthread_attr_destroy(&lt_attr);

    //!< \todo investigate type-punned warning here, and see if can fix
    rc = pthread_join(pt, (void **) &status);
    if (rc) {
        //!< \todo probably should use gerror() in this situation
        warn("*** ERROR; return code from process thread is %d\n", rc);
        exit(-1); //!< \todo shouldn't one use my_finish() here?
    }
    message("*** Completed join with process thread: status= %d\n", status);

    rc = pthread_join(lt, (void **) &status);
    if (rc) {
        warn("*** ERROR; return code from listen thread is %d\n", rc);
        exit(-1); //!< \todo shouldn't one use my_finish() here?
    }
    message("*** Completed join with listen thread status= %d\n", status);

    // clean up

    // exit main
    pthread_exit(NULL);
}

/*! This call will be started in a thread and only processes the commands
 *  in the command queue.
 *
 * \param arg to be documented
 *
 * \todo complete documentation
 *
 * *** We should catch a HUP signal and exit the thread properly.
 */
void *cmd_process(void *arg) {
    job *jb;

    while (1) {
        message("*** cmd_process: processing [%d]...", cq->length);

        // get lock on queue 
        pthread_mutex_lock(&(cq->bb_lock)); // lock the queue

        // read the tail of the queue
        if (cq->length > 0) {
            jb = (job*) get_tail(cq);
            message(" (%s:%ld) ", jb->cmd, jb->id);

            cq->length--; // shorten queue now
        }

        message("done.\n");

        pthread_mutex_unlock(&(cq->bb_lock)); // unlock the queue
        sleep(5);
    }
    // silly return to silence compiler warnings
    return arg;
}

//! To be documented

/*!
 * \param socketfd file descriptor of the socket???
 * 
 * \todo document this function
 */
void write_server_result_sock(int socketfd) {
    int num_axes;
    complex_t x, z;
    double y, dout;
    output_data_t *out;

    dout = 0.0;

    if (inter.num_diff_cmds) {
      compute_derivative(vlocal.zsolution, 0); // compute a derivative of a y value 
    } else {
      compute_result(vlocal.zsolution); // compute the y value 
    }

    y = 0.0;
    if (inter.yaxis[1]) {
        num_axes = 1;
    } else {
        num_axes = 0;
    }

    int j, k;
    for (j = 0; j < inter.num_outputs; j++) {
        out = &(inter.output_data_list[j]);
        z = vlocal.zsolution[j];

        for (k = 0; k <= num_axes; k++) {
            if (out->output_type == COMPLEX) {
                // scale output value by user defined factor
                x = z;
                x = z_by_x(x, out->user_defined_scale);

                switch (inter.yaxis[k]) {
                    case OTYPE_RE:
                        dout = x.re;
                        break;
                    case OTYPE_IM:
                        dout = x.im;
                        break;
                    case OTYPE_ABS:
                        dout = zmod(x);
                        break;
                    case OTYPE_DEG:
                        dout = zdeg(x);
                        break;
                    case OTYPE_DEGP:
                        dout = zdegp(x);
                        break;
                    case OTYPE_DEGM:
                        dout = zdegm(x);
                        break;
                    case OTYPE_DB:
                        dout = 0.5 * zdb(x);
                        break;
                    default:
                        bug_error("inter.yaxis[k] 1 ");
                        break;
                }
            } else {
                // scale output value by user defined factor
                y = z.re;
                y = y * out->user_defined_scale;

                switch (inter.yaxis[k]) {
                    case OTYPE_RE:
                    case OTYPE_ABS:
                        dout = y;
                        break;
                    case OTYPE_DB:
                        dout = xdb(y);
                        break;
                    case OTYPE_IM:
                    case OTYPE_DEG:
                    case OTYPE_DEGM:
                        dout = 0.0;
                        break;
                    default:
                        bug_error("inter.yaxis[k] 2 ");
                        break;
                }
            }
            if (inter.debug) {
                debug_msg("Sending: %g\n", dout);
            }
            sendDouble(socketfd, dout);
        }
    }
}




//! 64 bit version of htonl, from http://www.viva64.com/en/k/0018/
/*!
 * 
 * 
 * \todo document this function
 */
#define TYP_INIT 0 
#define TYP_SMLE 1 
#define TYP_BIGE 2 

#ifndef htonll
unsigned long long htonll(unsigned long long src) {
    static int typ = TYP_INIT;
    unsigned char c;

    union {
        unsigned long long ull;
        unsigned char c[8];
    } x;
    if (typ == TYP_INIT) {
        x.ull = 0x01;
        typ = (x.c[7] == 0x01ULL) ? TYP_BIGE : TYP_SMLE;
    }
    if (typ == TYP_BIGE)
        return src;
    x.ull = src;
    c = x.c[0];
    x.c[0] = x.c[7];
    x.c[7] = c;
    c = x.c[1];
    x.c[1] = x.c[6];
    x.c[6] = c;
    c = x.c[2];
    x.c[2] = x.c[5];
    x.c[5] = c;
    c = x.c[3];
    x.c[3] = x.c[4];
    x.c[4] = c;
    return x.ull;
}
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

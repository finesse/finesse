// $Id$

/*!
 * \file kat_calc.h
 * \brief Header file for general calculation routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_CALC_H
#define KAT_CALC_H

#include "kat_matrix_ccs.h"

void minimize();
void set_light_input_ports();
complex_t calc_transfer_func(transfer_func_t *mtf, double f);
complex_t knm_tilt_sig(int sig_type, complex_t X_bar, int n1, int m1, int n2, int m2);
void get_xaxis_values(axis_t *axis, double *dx, double *x_min, double *x_max, int round);
void compute_functions(void);
void compute_puts(void);
void signals_rhs(double f, double f1, int carrier, int lower2,
        int modulator_index, int modulation_order, int lower1,
        complex_t z);
void carrier_out(void); //!< to be implemented \todo implement
void x12_out(void);
void x3_out(void);
void x0_out(void);
void store_solution_old(double frequency, int frequency_index, int field_type);
void store_carrier_solutions();
void store_signal_solutions();
void store_quantum_solutions(int noise_in, int f_in);
double get_classical_power(int detector_index);
int create_mix_table(int num_demod, double fmix[MAX_MIX + 1][MAX_DEMOD + 1]);
complex_t set_demodulation_phase(
        int num_demod,
        complex_t z[MAX_MIX + 1],
        double fmix[MAX_MIX + 1][MAX_DEMOD + 1],
        light_out_t *light_output,
        int num_mixes);
complex_t get_demodulation_signal(
        int num_demod,
        int detector_index,
        light_out_t *light_output,
        complex_t z_tmp);

void compute_data(void);
void write_result(void);
void compute_derivative(complex_t *zsol, int n);
void do_function(int function_index);
void autogain(int nlock, int counter);
int is_not_locked(int nlock);
void do_locks(int nlock);
void compute_locks(int mode);
void rebuild_all(void);
void tune_parameters(void);
void create_data_array(complex_t *sol);
void compute_result(complex_t *z_tmp);
complex_t xyamp(node_t *sig_node, complex_t q);

void data_point_new(int);

void fill_detectors(void);
complex_t beam_shape(int detector_index);
complex_t beam_convolution(int detector_index);

complex_t amplitude_detector(int k);
complex_t photo_detector0(int k);
complex_t photo_detector2(int k, int num_demod);

void write_data_header(FILE *ofp);
void write_data_file(void);
complex_t get_amplitude_sum(
        int mix_index,
        int field_index_outer,
        int field_index_inner,
        int detector_index,
        double f_ref,
        double pdfactor,
        light_out_t *light_output);
complex_t get_mixed_amplitude(
        int detector_index,
        int mix_index,
        double f_ref,
        light_out_t *light_output);


complex_t get_qshot_noise_spectral_density(
        int num_demod,
        int detector_index,
        light_out_t *light_output,
        complex_t demod_amplitude);

void calc_mirror_signal_vars(signal_t *signal, mirror_t *mirror, double *factor, double *phase, double *ph1, double f, double f_c);
void calc_space_signal_vars(space_t *space, double *factor, double *ph1, double f_c, int order);
void calc_beamsplitter_signal_vars(beamsplitter_t *beamsplitter, double *factor, double *ph1, double f, double f_c);

complex_t calc_reflect_tilt_signal(signal_t *signal, complex_t **knm, int in, int out, complex_t X_bar);

void fill_light_signal_rhs(signal_t *signal);
void fill_mirror_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig);
void fill_space_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig);
void fill_bs_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig);
void fill_modulator_signal_rhs(signal_t *signal, ifo_matrix_vars_t* M_car, ifo_matrix_vars_t* M_sig);

#endif // KAT_CALC_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

// $Id$

/*!
 * \file kat_io.h
 * \brief Header file for input-output routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_IO_H
#define KAT_IO_H

#define EOF_CHECK \
if(pos > filesize) \
    gerror(" ! %s:%i :: Prematurely reached the end of the map file %s. The file" \
            " could be corrupt or the incorrect map file for the mirror. Please" \
            " delete all .map.bin, .knm.bin and .knm files for mirror %s" \
            " and regenerate them or replace with the correct files\n",__FILE__,__LINE__, filename)

typedef enum MAP_COMPONENTS{
    R_ABS=0,
    R_PHS=1,
    T_ABS=2,
    T_PHS=3
} MAP_COMPONENTS_t;

typedef enum KNM_COMPONENT{
    MIRROR_CMP=1,
    BEAMSPLITTER_CMP=2
} KNM_COMPONENT_t;

void convert_knm_file(const char *filename, const char *newfilename, KNM_COMPONENT_t knmcmp);
bool read_knm_file(void *cmp, bool converting, KNM_COMPONENT_t knmcmp);
void write_knm_file(void *cmp, bool converting, KNM_COMPONENT_t knmcmp);
void write_mirror_knm_to_matrix_file(char *filename, mirror_knm_t *knm);
void write_bs_knm_to_matrix_file(char *filename, bs_knm_t *knm);
int check_if_file_exists(char *filename);
void make_backup_of_file(char *filename);
void open_file_to_write_ascii(char *filename, FILE **handle);
void write_map_coefficient_to_file(char *filename, surface_map_t *map);
const char *node_print(int n);
void bold_on(FILE *fp);
void bold_off(FILE *fp);
void gerror(const char *error_msg, ...);
void server_gerror(const char *error_msg, ...);

#define CYGSERVER_WARNING "The Cygwin Cygserver service is not running and is required to use\n" \
                          "   the Cuba integration routines, defaulting to using Riemann sum.\n" \
                          "   Please see manual section 'Mirror surface maps' for more details\n"

//! Wrapper macro to print bug error messages
#define bug_error(error_msg, ...) \
    _bug_error(__func__, __FILE__, __LINE__, error_msg, ##__VA_ARGS__)
void _bug_error(const char *function_name, const char *source_fname,
        int line_number, const char *error_msg, ...);

//! Wrapper macro to print warning messages
#define warn(format_str, ...) _user_message(format_str, WARNING, ##__VA_ARGS__)
//! Wrapper macro to print general output messages
#define message(format_str, ...) _user_message(format_str, MESSAGE, ##__VA_ARGS__)
void _user_message(const char *message_string, int message_type, ...);

#ifdef DEBUG
//! Macro to insert debugging messages when DEBUG is true, otherwise nothing
#define debug_msg(...) _debug_msg(__func__, ##__VA_ARGS__);
//! Macro to insert fprintf statements when DEBUG is true, otherwise nothing
#define debug_msg_file(...) fprintf(__VA_ARGS__);
#else
//! Macro to insert debugging messages when DEBUG is true, otherwise nothing
#define debug_msg(...)
//! Macro to insert fprintf statements when DEBUG is true, otherwise nothing
#define debug_msg_file(...)
#endif
void _debug_msg(const char *function_name, const char *message_string, ...);

void print_version(FILE *file);
void print_help2(void);
void print_help(void);
void max_min(void);
void set_progress_action_text(const char* action, ...);
void set_progress_message_text(const char* message, ...);
void print_progress(int num_points, int current_point);
void clear_progress();
void print_progress_and_time(int num_points, int current_point, time_t starttime);

#endif  // KAT_IO_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

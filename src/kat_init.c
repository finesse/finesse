// $Id$

/*!
 * \file kat_init.c
 * \brief Various initialisation routines for the interferometer simulation
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#include "kat.h"
#include "kat_config.h"
#include "kat_inline.c"
#include "kat_optics.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_init.h"
#include "kat_dump.h"
#include "kat_calc.h"
#include "kat_aa.h"
#include "kat_knm_mirror.h"
#include "kat_knm_int.h"
#include "kat_knm_bs.h"
#include "kat_quant.h"

#ifdef OSWIN
#undef UCHAR // formulc.h defines this already and so does windows.h, so dont do it again!
#include <windows.h>
// remove definitions that cause conflicts, as generally causes problems and original WIN32 defintions aren't used
#undef small 
#undef OUT
#undef max
#undef min
#endif

unsigned int nTraces = 0; // Number of traces performed so far

extern bool bCygserverRunning;
extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern memory_t mem;
extern local_var_t vlocal;

extern bool update_fields; //!< Flag to say whether or not to update the fields

extern const complex_t complex_i;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;

extern ifo_matrix_vars_t M_ifo_car;
extern ifo_matrix_vars_t M_ifo_sig;

extern FILE *fp_log;

//! Initialisation of variables used in simulation

/*!
 *
 */
void initialise_simulation_variables(void) {
    /* some initializations */

    // variables used for temporary storage of results
    // in order to skip function calls if the 
    // input parameter of successive calls are the same
    vlocal.x_p1 = 0;
    vlocal.x_p2 = 0;
    vlocal.z_in_p1 = complex_0;
    vlocal.z_out_p1 = complex_0;
    vlocal.z_in_p2 = complex_0;
    vlocal.z_out_p2 = complex_0;

    vlocal.natLogsOfN[0] = 0;
    vlocal.natLogMax = 0;

    vlocal.zk1 = complex_0;
    vlocal.kn1 = -1;
    vlocal.kn2 = -1;
    vlocal.kqx1 = complex_0;
    vlocal.kqx2 = complex_0;
    vlocal.kgammax = 0.0;
    vlocal.knr = 0.0;

    // variables of the interferometer setup 
    inter.num_mirrors = 0;
    inter.num_beamsplitters = 0;
    inter.num_spaces = 0;
    inter.num_squeezers = 0;
    inter.num_light_inputs = 0;
    inter.num_light_outputs = 0;
    inter.num_modulators = 0;
    inter.num_lenses = 0;
    inter.num_frequencies = 0;
    inter.num_non_signal_frequencies = 0;
    inter.num_signals = 0;
    inter.num_variables = 0;
    inter.num_scale_cmds = 0;
    inter.num_diodes = 0;
    inter.num_attributes = 0;
    inter.num_gauss_cmds = 0;
    inter.num_gratings = 0;
    inter.num_outputs = 0;
    inter.num_motion_outputs = 0;
    inter.num_beam_param_outputs = 0;
    inter.num_convolution_outputs = 0;
    inter.num_mirror_phase_outputs = 0;
    inter.num_cavities = 0;
    inter.num_diff_cmds = 0;
    inter.num_put_cmds = 0;
    inter.num_tune_params = 0;
    inter.num_func_cmds = 0;
    inter.num_locks = 0;
    inter.num_motion_eqns = 0;
    inter.startlock = 0;
    inter.num_locks_and_funcs = 0;
    inter.showiterate = 0;
    inter.num_set_cmds = 0;
    inter.lock = 0;
    inter.ploty2axis = false;
    inter.startnode = 0;
    inter.startnode_set = 0;
    inter.tem = 0;
    inter.num_constants = 0;
    inter.mats = 1;
    inter.tem_is_set = false;
    inter.beam.set = false;
    inter.beam.x = 0;
    inter.beam.y = 0;
    inter.beam.swap = 0;
    inter.rebuild = 0;
    inter.num_nodes = 0;
    inter.num_dump_nodes = 0;
    inter.num_reduced_nodes = inter.num_nodes - inter.num_dump_nodes;
    inter.num_quadratures = 2;
    inter.x1.xsteps = inter.x1.xtype = NO_FREQ;
    inter.x2.xsteps = inter.x2.xtype = NO_FREQ;
    inter.x3.xsteps = inter.x3.xtype = NO_FREQ;
    inter.ytype = FLIN;
    inter.f0 = init.clight / init.lambda;
    inter.yaxis[0] = 0;
    inter.yaxis[1] = 0;
    inter.debug = 0;
    inter.fsig = 0.0;
    inter.mfsig = 0.0;
    inter.time = 0;
    inter.noscripts = 0;
    inter.noxaxis = 0;
    inter.powers = 0;
    inter.multi = false;
    inter.subplot = false;
    inter.pause = 0;
    inter.printmatrix = 0;
    inter.mismatches = 0;
    inter.mismatches_options = IGNORE_LOW_R_MIRROR_BS | IGNORE_LOW_T_MIRROR_BS | AVERAGE_Q;
    inter.mismatches_lower = 1e-6;
    inter.mismatches_R_T_limit = 1e-6;
    inter.printqnoiseinputs = false;
    inter.trace = 0;
    inter.splot = 0;
    inter.retrace = 0; // retrace is set automatically when inter.rebuild &2
    inter.retrace_manual = 0; // or it can be switched on/off explicitly by the user
    inter.shotnoise = 0;
    inter.mycolor = 0;
    inter.width = 2;
    inter.set_tem_phase_zero = 3;
    inter.video = 0;
    inter.x3_axis_is_set = false;
    inter.knm = 0;
    inter.null = 0.0; /// zero
    inter.dnull = &(inter.null); // pointer to zero
    inter.num_matlab_cmds = 0;
    inter.num_matlabplot_cmds = 0;
    inter.all_quantum_components = true;
    inter.warned_quantum_no_fsig = false;
    inter.warned_mod_up_low_coupling = false;
    inter.num_mirror_dithers = 0;
    inter.num_bs_dithers = 0;
    inter.num_slinks = 0;
    inter.num_qnoise_inputs = 0;
    inter.num_transfer_funcs = 0;
    inter.num_blocks = 0;

}

//! Setup the system ready for simulation

/*!
 *
 * \todo split up into separate routines
 */
void setup_system(void) {

    // check if 'powers' is set but no xaxis, if so, set noxaxis
    if (inter.powers && inter.x1.xtype == NO_FREQ && !options.servermode){
        inter.noxaxis =1;
    }

    // check for 'noxaxis' command and if set overwrite all xaxis commands
    if (inter.noxaxis == 1) {
        inter.video = 0;
        inter.splot = OFF;
        inter.x3_axis_is_set = 0;
        inter.x2.xsteps = NO_FREQ;
        inter.x2.xtype = NO_FREQ;
        inter.x3.xsteps = NO_FREQ;
        inter.x3.xtype = NO_FREQ;

        inter.x1.xtype = FLIN;
        inter.x1.xaxis = inter.dnull;
        inter.x1.op = 0.0;
        inter.x1.lborder = 0;
        inter.x1.uborder = 0;
        inter.x1.min = 0;
        inter.x1.max = 0;
        inter.x1.xmin = 0.0;
        inter.x1.xmax = 0.0;
        strcpy(inter.x1.xname, "");
        inter.x1.xsteps = 0;
        inter.x1.minus_xaxis = 0.0;

        inter.x2.xaxis = inter.dnull;
        inter.x2.op = 0.0;
        inter.x2.lborder = 0;
        inter.x2.uborder = 0;
        inter.x2.min = 0;
        inter.x2.max = 0;
        inter.x2.xmin = 0.0;
        inter.x2.xmax = 0.0;
        strcpy(inter.x2.xname, "");
        inter.x2.xsteps = 0;
        inter.x2.minus_xaxis = 0.0;

        inter.x3.xaxis = inter.dnull;
        inter.x3.op = 0.0;
        inter.x3.lborder = 0;
        inter.x3.uborder = 0;
        inter.x3.min = 0;
        inter.x3.max = 0;
        inter.x3.xmin = 0.0;
        inter.x3.xmax = 0.0;
        strcpy(inter.x3.xname, "");
        inter.x3.xsteps = 0;
        inter.x3.minus_xaxis = 0.0;

        inter.num_gnuterm_cmds = 0;
        inter.num_pyterm_cmds = 0;

        message("'noxaxis' has been set, ignoring all other xaxis commands\n");
    }

    // check for at least one laser
    if (inter.num_light_inputs < 1) {
        gerror("no input laser specified\n");
    }

    // clear all outputs
    clear_output_data_list();

    // clear all locks
    clear_lock_list();

    // check all gratings (and rebuild)
    check_and_rebuild_gratings();


    // check if diff xbeta/ybeta is set but deriv_h is too large
    check_derivative_stepsize();

    // check if x3 axis is set for video mode
    if (inter.video && !inter.x3_axis_is_set) {
        inter.video = 0; // switch of video mode
    }

    if (inter.x3_axis_is_set && !inter.video && !inter.splot) {
        gerror("x3axis without x2axis or video mode makes no sense.\n");
    }

    // explicitly switch off HG mode by 'maxtem off'
    // or switch on HG mode if either gauss/cav/maxtem/beam/bp was used   
    set_hermite_gauss_mode();

    set_default_terminal();

    // check if no gnu/py terms have been set previously
    // if not then disable them from running


    if (inter.num_gnuterm_cmds == 0)
        inter.num_gnuterm_cmds = NO_GNUTERM;

    if (inter.num_pyterm_cmds == 0)
        inter.num_pyterm_cmds = NO_PYTERM;

    // swich gnuterms and pyterm to no if noscripts has been set
    if (inter.noscripts) {
        inter.num_gnuterm_cmds = NO_GNUTERM;
        inter.num_pyterm_cmds = NO_PYTERM;
    }


    // not compatible with new set-put ??
    // checking axis use with beam analyser
    check_axis_for_beam_analyser();

    if (inter.x1.xtype == NO_FREQ && !options.servermode) {
        gerror("missing 'xaxis' instruction\n");
    }

    if (options.servermode) {
        update_fields = 1;
    }

    if (inter.x2.xsteps == NO_FREQ) {
        inter.x2.xsteps = 0;
    }

    if ((inter.num_gnuterm_cmds != NO_GNUTERM) && inter.splot &&
            (inter.num_outputs > 1 || inter.yaxis[1])) { // sum ?
        if (NOT inter.multi) {
            inter.ploty2axis = false;
            warn("plotting only one output (use 'multi' to plot all).\n");
        }
    }

    /*
     *     if (inter.splot && inter.yaxis[1])
     *     {
     * 
     *     warn("second y-axis not allowed for surface plot, using one.\n");
     *     inter.yaxis[1]=0;
     *     }
     *    */

    check_photodetector_demod_phases();

    // yaxis set ? If not, set default values -------------------
    set_default_yaxis_values();

    // checking for y axis settings --------------------------------
    check_yaxis_settings();

    inter.num_output_cols = 1 + inter.num_outputs;

    if (inter.yaxis[1]) {
        inter.num_output_cols += inter.num_outputs;
    }

    if (inter.splot) {
        inter.num_output_cols++;
    }

    /*
    if (inter.debug) {
      message("columns: %d\n", inter.num_output_cols);
    }
     */

    //! \todo the following code does absolutely nothing; remove???
    /*
     int j = 1;
    int modulator_index;
    for (modulator_index = 0;
            modulator_index < inter.num_modulators;
            modulator_index++) {
      if (inter.modulator_list[modulator_index].single_sideband_mode) { // added 281003
        j++;
      }
      else {
        j += 2 * inter.modulator_list[modulator_index].order;
      }
    }
     */
    // setting scaling factors for outputs -------
    set_output_scaling_factors();

    // check if there are (plottable) outputs
    check_for_plottable_outputs();

    // check index of refraction at beam splitters
    // and set it if required
    check_refractive_index_at_beamsplitters();

    // compute number of fields size of sparse matrix etc-----------------------
    
    // number of different fields (00, 01, 10, 11, 02, ...)
    inter.num_fields = (inter.tem + 1) * (inter.tem + 2) / 2;

    // calculate the number of equations from number nodes the number of fields
    // num_eqns gives the rank of the sparse matrix
    // If we are using an IFO matrix solver which solves all frequencies at once
    // then we have to use an extra factor of num_frequencies

    // these are counted later on when building the matrix
    M_ifo_car.M.num_eqns = -1;
    M_ifo_sig.M.num_eqns = -1;

    // calculate number of non-zero elements in matrix. Currently this is 
    // 2 * nodes * fields + num_of_diagonal_elements or rank+rank

    // WRONG ######################################################################
    // this is counted by KLU later. This could be removed?
    inter.num_nonzero = -1;

    // setup commands related to higher order modes only
    setup_hermite_gauss_extension();

    // check if any lasers have been set using LG modes, if so we need to convert
    // to HG
    int laser_index;

    for (laser_index = 0; laser_index < inter.num_light_inputs; laser_index++) {
        // check if laser has been set with LG modes
        if (!inter.light_in_list[laser_index].isHGModes) {
            complex_t *A_hg, *pcl;

            A_hg = (complex_t*) calloc(inter.num_fields + 1, sizeof (complex_t));
            pcl = inter.light_in_list[laser_index].power_coeff_list;

            lg_2_hg(pcl, A_hg);

            int i;

            for (i = 0; i < inter.num_fields; i++) {
                //int _n,_m,_p,_l;
                //get_tem_modes_from_field_index(&_n, &_m, i);
                //get_tem_modes_from_LG_field_index(&_p, &_l, i);

                pcl[i] = A_hg[i];
            }
            free(A_hg);
        }
    }

    // normalising input fields to overall power-------------------------------

    int field_index;

    double sum;
    for (laser_index = 0; laser_index < inter.num_light_inputs; laser_index++) {
        // No normalisation for squeezed light, same noise in all modes.
        if (!inter.light_in_list[laser_index].isSqueezed){
            sum = 0;
            for (field_index = 0; field_index < inter.num_fields; field_index++) {
                sum += zabs(inter.light_in_list[laser_index].power_coeff_list[field_index]);
            }
        
            for (field_index = 0; field_index < inter.num_fields; field_index++) {
                inter.light_in_list[laser_index].power_coeff_list[field_index] =
                        z_by_x(inter.light_in_list[laser_index].power_coeff_list[field_index], 1.0 / sqrt(sum));
            }
        }
    }

    // setting correct Gouy phase for all laser inputs
    for (laser_index = 0; laser_index < inter.num_light_inputs; laser_index++) {
        light_in_t l = inter.light_in_list[laser_index];

        complex_t qx = inter.node_list[l.node_index].qx;
        complex_t qy = inter.node_list[l.node_index].qy;

        double phase00;
        if (inter.set_tem_phase_zero & 2) {
            phase00 = (0.5 * gouy(qx) + 0.5 * gouy(qy));
        } else {
            phase00 = 0.0;
        }

        double phase;
        int n, m;
        for (field_index = 0; field_index < inter.num_fields; field_index++) {
            get_tem_modes_from_field_index(&n, &m, field_index);
            phase = ((n + 0.5) * gouy(qx) + (m + 0.5) * gouy(qy));

            l.power_coeff_list[field_index] =
                    z_by_phr(l.power_coeff_list[field_index], phase - phase00);
        }
    }


    // print debug output -----------------------------------------------------
    if (inter.debug & 1) {
        if (NOT options.quiet) {
            debug(stdout);
        }
        if (fp_log != NULL) {
            debug(fp_log);
        }
    }

    // print beam sizes for nodes with beam detetcors
    int i;
    for (i = 0; i < inter.num_outputs; i++) {
        output_data_t output_data = inter.output_data_list[i];
        if (output_data.detector_type == BEAM) {
            double nr = *inter.node_list[output_data.node_index].n;
            message("beam parameter and beam size at detector '%s': \n", output_data.name);
            message("wx0 = %sm, wy0 = %sm, ",
                    double_form(w0_size(*output_data.qx, nr)),
                    double_form(w0_size(*output_data.qy, nr)));
            message("wx  = %sm  wy  = %sm\n",
                    double_form(w_size(*output_data.qx, nr)),
                    double_form(w_size(*output_data.qy, nr)));
        }
    }

    // set parameters for progress print
    set_progress_printing_parameters();

    // the setup routine has now been called, hence set...
    inter.setup = true;

    // execute func and put once for initialisation
    compute_functions();
    compute_puts();
}


//! Checks if a warning regaring transfer function phases should be printed

/**
 *  Loops through all photodiodes, to see if demoulations have been
 *  set with phases even when yaxis deg has been explicitly set, if
 *  this is the case print a warning.
 */

void check_photodetector_demod_phases(void) {
    int pd_index;
    if (inter.yaxis[1] || inter.yaxis[0] == OTYPE_DEG || inter.yaxis[0] == OTYPE_DEGP ||
            inter.yaxis[0] == OTYPE_DEGM) { // complex data assumed?
        for (pd_index = 0; pd_index < inter.num_light_outputs; pd_index++) {
            if (inter.light_out_list[pd_index].num_demods > 0) {
                if (inter.output_data_list[inter.light_out_list[pd_index].output_idx].output_type == REAL) {
                    warn("Detector '%s' outputs real data when last demodulation phase is set.\n", inter.light_out_list[pd_index].name);
                }
            }
        }
    }
}

//! Clears output data list in preparation for simulation

void clear_output_data_list(void) {
    int output_index;
    for (output_index = 0; output_index < inter.num_outputs; output_index++) {
        inter.output_data_list[output_index].signal = complex_0;
        inter.output_data_list[output_index].re = 0.0;
        inter.output_data_list[output_index].im = 0.0;
        inter.output_data_list[output_index].abs = 0.0;
        inter.output_data_list[output_index].deg = 0.0;
    }
    //!< \todo return from here one day
}

//! Clears lock list in preparation for simulation

void clear_lock_list(void) {
    int lock_index;
    for (lock_index = 0; lock_index < inter.num_locks; lock_index++) {
        inter.lock_list[lock_index].last1 = 0.0;
        inter.lock_list[lock_index].last2 = 0.0;
    }
    //!< \todo return from here one day
}

//! Checks and rebuilds all gratings

void check_and_rebuild_gratings(void) {
    int grating_index;
    for (grating_index = 0; grating_index < inter.num_gratings; grating_index++) {
        rebuild_grating(&inter.grating_list[grating_index]);
    }
    //!< \todo return from here one day
}

//! Checks the size of the derivative step size

void check_derivative_stepsize(void) {
    int deriv_index;
    for (deriv_index = 0; deriv_index < inter.num_diff_cmds; deriv_index++) {
        derivative_t *derivative;
        derivative = &inter.deriv_list[deriv_index];
        if ((string_matches(derivative->name, "xbeta") ||
                string_matches(derivative->name, "xBeta") ||
                string_matches(derivative->name, "ybeta") ||
                string_matches(derivative->name, "yBeta"))
                &&
                (init.deriv_h > 1.e-9)) {
            warn("deriv_h might to be too large for a "
                    "differentiation of angles;\n"
                    "   it should be smaller than the typical "
                    "range of the parameter!\n");
        }
    }
    //!< \todo return from here one day
}

//! Sets the Hermite Gauss mode appropriately depending upon commands used

void set_hermite_gauss_mode(void) {
    if (inter.tem == NOT_SET) {

        if (inter.debug) {
            message("Switching off HG mode\n");
        }

        inter.tem = 0;
        inter.tem_is_set = false;
        inter.trace = 0;
        inter.retrace = 0;

        // beam analysers do not work without HG mode
        if (inter.beam.set) {
            gerror("cannot use beam analyser without Hermite-Gauss modes.\n");
        }

        // beam parameter detectors do not work without HG mode
        if (inter.num_beam_param_outputs) {
            gerror("cannot use beam parameter detectors without "
                    "Hermite-Gauss modes.\n");
        }

        // convolution detectors do not work without HG mode
        if (inter.num_convolution_outputs) {
            gerror("cannot use convolution detectors without "
                    "Hermite-Gauss modes.\n");
        }
    }// switch on HG mode if either gauss/cav/maxtem/beam/bp/conv was used   
    else {
        if ((inter.num_gauss_cmds ||
                inter.num_cavities ||
                inter.beam.set ||
                inter.trace ||
                inter.num_beam_param_outputs ||
                inter.num_convolution_outputs)
                &&
                (!inter.tem_is_set)) {
            warn("Switching on higher-order modes and setting maxtem 0\n");
            inter.tem_is_set = true;
            inter.tem = 0; // default order of TEM modes = 0
        }
    }

    // if still no HG modes but retrace=1 -> set retrace=0
    if (!inter.tem_is_set && inter.retrace) {
        //    message("Ignoring `retrace' when not in HG mode\n");
        inter.retrace = 0;
    }

    // TODO now that we might have suddenly switched on HG modes it would be good to check a few things again

    //!< \todo return from here one day
}

//! Sets the default terminal if none were previously set

void set_default_terminal(void) {

    if (inter.num_pyterm_cmds==0 && inter.num_gnuterm_cmds==0) {
        if (init.plotting == PLOT_PYTHON) {/* No python terminals explicitly given */
            /* Set default terminal */
            inter.pyterm[0] = init.screenterm;
            if (inter.debug & 1) {
                message("Setting python screen terminal\n");
            }
            inter.num_pyterm_cmds = 1;
        } else if (init.plotting == PLOT_GNUPLOT) {/* No gnuplot terminals explicitly given */
            /* Set default terminal */
            switch (OS) {
                case UNIX:
                    inter.gnuterm[0] = init.unixterm;
                    if (inter.debug & 1) {
                        message("Assuming Linux system, setting  Gnuplot x11 terminal\n");
                    }
                    break;
                case MACOS:
                    // should this be unixterm or macosterm (i.e. x11 or aqua)???
                    inter.gnuterm[0] = init.macosterm;
                    if (inter.debug & 1) {
                        message("Assuming OSX system, setting Gnuplot QT terminal\n");
                    }
                    break;
                case __WIN_32__:
                case __WIN_64__:
                    inter.gnuterm[0] = init.winterm;
                    if (inter.debug & 1) {
                        message("Assuming Windows system, setting Gnuplot windows terminal\n");
                    }
                    break;
                default:
                    // set QT as default because it is cross platform
                    inter.gnuterm[0] = init.macosterm;
                    if (inter.debug & 1) {
                        message("Cannot determine system, setting Gnuplot QT terminal\n");
                    }
            }
            inter.num_gnuterm_cmds = 1;
        }
    }
}

//! Check axis use with beam analyser
/*!
 * This sets inter.beam.x and inter.beam.y which are used in compute_data to decide
 * when the interferometer matrix needs updating.
 */
//! 

void check_axis_for_beam_analyser(void) {
    if (inter.beam.set) {
        int detector_index;
        for (detector_index = 0;
                detector_index < inter.num_light_outputs;
                detector_index++) {
            if ((inter.x1.xaxis == &inter.light_out_list[detector_index].x) ||
                    (inter.x1.xaxis == &inter.light_out_list[detector_index].y)) {
                inter.beam.x = 1;
            }
        }

        if (inter.splot) {
            int detector_index;
            for (detector_index = 0;
                    detector_index < inter.num_light_outputs;
                    detector_index++) {
                if ((inter.x2.xaxis == &inter.light_out_list[detector_index].x) ||
                        (inter.x2.xaxis == &inter.light_out_list[detector_index].y)) {
                    inter.beam.y = 1;
                }
            }
        }
    }
    //!< \todo return from here one day
}

//! Set the default values for the yaxis

void set_default_yaxis_values(void) {
    if (!inter.yaxis[0]) {

        // by default set yaxis abs
        inter.yaxis[0] = OTYPE_ABS;
        strcpy(inter.y1name, "Abs ");

        //inter.yaxis[1] = OTYPE_DEG;
        //strcpy(inter.y2name, "Phase [Deg] ");

        if (inter.splot) {
            inter.ploty2axis = false;
        } else {
            inter.ploty2axis = false;
        }

        inter.ytype = FLIN;
    }
    //!< \todo return from here one day
}

//! Checks the settings of the y axis

void check_yaxis_settings(void) {
    int j = 0;
    int output_index;
    for (output_index = 0; output_index < inter.num_outputs; output_index++) {
        if (inter.output_data_list[output_index].output_type == COMPLEX) {
            j = j | 1;
        } else if (inter.output_data_list[output_index].detector_type != PD0 &&
                inter.output_data_list[output_index].detector_type != SHOT) {
            j = j | 4;
        }
    }

    // checking if phase axis is set but not needed/useful
    if (!(j & 1)) {

        if (inter.yaxis[0] == OTYPE_DEG || inter.yaxis[0] == OTYPE_DEGP ||
                inter.yaxis[0] == OTYPE_DEGM) {
            warn("Switching to 'abs' for yaxis (no phase information available).\n");
            inter.yaxis[0] = OTYPE_ABS;
            strcpy(inter.y1name, "Abs ");
        }

        if (inter.yaxis[1] == OTYPE_DEG || inter.yaxis[1] == OTYPE_DEGP ||
                inter.yaxis[1] == OTYPE_DEGM) {
            //warn ("Ignoring second yaxis (no phase information available).\n");
            inter.yaxis[1] = 0;
        }
    }

    // checking if DB/log axis is set but signals might be negativ
    if (j & 4) {
        if (inter.yaxis[0] == OTYPE_DB || inter.ytype == FLOG) {
            warn("Warning: signal might be negative at logarithmic scale.\n");
        }
    }

    // check if subplot has been set but second yaxis is not active
    if (inter.subplot && !inter.yaxis[1]) {
        inter.subplot = false;
    }

    //!< \todo return from here one day
}

double get_scale_factor(scale_t *scale) {
    double scale_factor = 1.0;
    
    if (scale->type == SCALE_USER) {
        scale_factor = scale->factor;
    } else if (scale->type == SCALE_METER) {
        scale_factor = (2.0 * PI) / init.lambda;
        output_data_t *output_data;
        output_data = &inter.output_data_list[scale->output];

        if (output_data->detector_type == PD0 ||
                output_data->detector_type == PD1 ||
                output_data->detector_type == SHOT ||
                output_data->detector_type == QNOISE ||
                output_data->detector_type == QSHOT) {
            if (inter.light_out_list[output_data->detector_index].sensitivity == ON) {
                scale_factor = 1.0 / scale_factor;
            }
        } else if (output_data->detector_type == HOMODYNE) {
            if (inter.homodyne_list[output_data->detector_index].sensitivity == ON) {
                scale_factor = 1.0 / scale_factor;
            }
        }
    } else if (scale->type == SCALE_AMPERE) {
        /* Umrechnungsfaktor Watt nach Ampere */
        scale_factor = C_ELEC * init.qeff / H_PLANCK / C_LIGHT * init.lambda;
    } else if (scale->type == SCALE_RAD2DEG) {
        scale_factor = DEG;
    } else if (scale->type == SCALE_DEG2RAD) {
        scale_factor = RAD;
    } else if (scale->type == SCALE_QSHOT) {
        scale_factor = sqrt(H_PLANCK * inter.f0);
    } else if (scale->type == SCALE_QSHOT_METER) {
        scale_factor = (2.0 * PI) / init.lambda;
        output_data_t *output_data;
        output_data = &inter.output_data_list[scale->output];
        if (output_data->detector_type == PD0 ||
                output_data->detector_type == PD1 ||
                output_data->detector_type == SHOT ||
                output_data->detector_type == QNOISE ||
                output_data->detector_type == QSHOT) {
            if (inter.light_out_list[output_data->detector_index].sensitivity == ON) {
                scale_factor = 1.0 / scale_factor;
            }
        } else if (output_data->detector_type == HOMODYNE) {
            if (inter.homodyne_list[output_data->detector_index].sensitivity == ON) {
                scale_factor = 1.0 / scale_factor;
            }
        }
        scale_factor *= sqrt(H_PLANCK * inter.f0);
    } else if (scale->type == SCALE_PSD) {
        inter.output_data_list[scale->output].quantum_scaling = PSD;
        scale_factor = 1.0;
    } else if (scale->type == SCALE_PSD_HF) {
        inter.output_data_list[scale->output].quantum_scaling = PSD_HF;
        scale_factor = 1.0;
    } else if (scale->type == SCALE_ASD) {
        inter.output_data_list[scale->output].quantum_scaling = ASD;
        scale_factor = 1.0;
    } else if (scale->type == SCALE_ASD_HF) {
        inter.output_data_list[scale->output].quantum_scaling = ASD_HF;
        scale_factor = 1.0;
    } else {
        bug_error("new scale 1");
    }
    
    return scale_factor;
}

//! Sets the output data scaling factors

/*!
 * \todo SCALE_METER, SCALE_AMPERE, SCALE_QSHOT, SCALE_QSHOT_METER branches
 * are untested (when !(scale->output > -1))
 *
 * \todo SCALE_AMPERE, SCALE_DEG, SCALE_QSHOT, SCALE_QSHOT_METER untested
 * otherwise
 */
void set_output_scaling_factors(void) {
    int output_index;
    
    for (output_index = 0; output_index < inter.num_outputs; output_index++) {
        inter.output_data_list[output_index].user_defined_scale = 1.0;
    }

    double scale_factor = 0.0;
    int scale_cmd_index;
    
    for (scale_cmd_index = 0;
            scale_cmd_index < inter.num_scale_cmds;
            scale_cmd_index++) {
        scale_t *scale;
        scale = &inter.scale_list[scale_cmd_index];
        
        scale_factor = get_scale_factor(scale);
        
        if (scale->output > -1) {
            inter.output_data_list[scale->output].user_defined_scale *= scale_factor;
        } else {
            int output_index;
            
            for (output_index = 0; output_index < inter.num_outputs; output_index++) {
                inter.output_data_list[output_index].user_defined_scale *= scale_factor;
            }
        }
    }
    //!< \todo return from here one day
}

//! Checks for plottable outputs

void check_for_plottable_outputs(void) {
    // If we are just doing a trace then continue here
    if (options.trace_output)
        return;
    
    if (inter.num_outputs == 0) {
        gerror("no outputs have been specified in the input file\n");
    }

    bool output_is_found = false;
    int output_index = 0;
    while (NOT output_is_found && output_index < inter.num_outputs) {
        if (!inter.output_data_list[output_index].noplot) {
            output_is_found = true;
        } else {
            output_index++;
        }
    }

    if (NOT output_is_found) {
        gerror("no output (without 'noplot') has been found\n");
    }
    //!< \todo return from here one day
}

//! Checks (and sets if required) the refractive index at beamsplitters

void check_refractive_index_at_beamsplitters(void) {
    int component_index1, component_index2;
    int component_index3, component_index4;
    double *refr_index1, *refr_index2;
    bool index1_is_set, index2_is_set;

    int beamsplitter_index;
    for (beamsplitter_index = 0;
            beamsplitter_index < inter.num_beamsplitters;
            beamsplitter_index++) {
        index1_is_set = false;
        index2_is_set = false;
        refr_index1 = 0;
        refr_index2 = 0;

        beamsplitter_t *beamsplitter;
        beamsplitter = &inter.bs_list[beamsplitter_index];

        if ((NOT inter.node_list[beamsplitter->node1_index].gnd_node) &&
                (NOT inter.node_list[beamsplitter->node2_index].gnd_node)) {
            which_components(beamsplitter->node1_index,
                    &component_index1, &component_index2);
            which_components(beamsplitter->node2_index,
                    &component_index3, &component_index4);

            if (get_component_type(component_index1) == SPACE ||
                    get_component_type(component_index2) == SPACE) {
                refr_index1 = inter.node_list[beamsplitter->node1_index].n;
                index1_is_set = true;
            }

            if (get_component_type(component_index3) == SPACE ||
                    get_component_type(component_index4) == SPACE) {
                refr_index2 = inter.node_list[beamsplitter->node2_index].n;
                index2_is_set = true;
            }

            if (index2_is_set && index1_is_set) {
                if (!eq(*refr_index1, *refr_index2)) {
                    gerror("index of refraction not consistent at primary face of "
                            "beam splitter %s\n", beamsplitter->name);
                }
            } else if (index1_is_set) {
                if (inter.debug) {
                    message("Setting index of refraction at node %s, to %g\n",
                            get_node_name(beamsplitter->node2_index),
                            *inter.node_list[beamsplitter->node1_index].n);
                }
                inter.node_list[beamsplitter->node2_index].n =
                        inter.node_list[beamsplitter->node1_index].n;
            } else if (index2_is_set) {
                if (inter.debug) {
                    message("Setting index of refraction at node %s, to %g\n",
                            get_node_name(beamsplitter->node1_index),
                            *inter.node_list[beamsplitter->node2_index].n);
                }
                inter.node_list[beamsplitter->node1_index].n =
                        inter.node_list[beamsplitter->node2_index].n;
            }
        }

        index1_is_set = false;
        index2_is_set = false;
        refr_index1 = 0;
        refr_index2 = 0;

        if ((NOT inter.node_list[beamsplitter->node3_index].gnd_node) &&
                (NOT inter.node_list[beamsplitter->node4_index].gnd_node)) {
            which_components(beamsplitter->node3_index,
                    &component_index1, &component_index2);
            which_components(beamsplitter->node4_index,
                    &component_index3, &component_index4);

            if (get_component_type(component_index1) == SPACE ||
                    get_component_type(component_index2) == SPACE) {
                refr_index1 = inter.node_list[beamsplitter->node3_index].n;
                index1_is_set = true;
            }

            if (get_component_type(component_index3) == SPACE ||
                    get_component_type(component_index4) == SPACE) {
                refr_index2 = inter.node_list[beamsplitter->node4_index].n;
                index2_is_set = true;
            }

            if (index2_is_set && index1_is_set) {
                if (!eq(*refr_index1, *refr_index2)) {
                    gerror("index of refraction not consistent at secondary face of "
                            "beam splitter %s\n", beamsplitter->name);
                }
            } else if (index1_is_set) {
                if (inter.debug) {
                    message("Setting index of refraction at node %s, to %g\n",
                            get_node_name(beamsplitter->node4_index), *refr_index1);
                }
                inter.node_list[beamsplitter->node4_index].n = refr_index1;
            } else if (index2_is_set) {
                if (inter.debug) {
                    message("Setting index of refraction at node %s, to %g\n",
                            get_node_name(beamsplitter->node3_index), *refr_index2);
                }
                inter.node_list[beamsplitter->node3_index].n = refr_index2;
            }
        }
    }
    //!< \todo return from here one day
}

//! Sets up the Hermite-Gauss extension to Finesse

/*!
 * \todo Need to split this routine up into subroutines
 */
void setup_hermite_gauss_extension(void) {
    int tn, tm, i, j;

    // ###########################################################
    // Hermite Gauss extension :
    // ###########################################################

    // allow 'retrace' only if a cavity or gauss command is present
    if (inter.retrace && !(inter.num_cavities || inter.num_gauss_cmds)) {
        gerror("please give a 'cav' or 'gauss' command "
                "if you want to use 'retrace'\n");
    }

    // initialise k[][] matrices for all components
    init_ks();
   
    

    // THE FOLLOWING ONLY IF TEM_SET IS SET (i.e. multimode analysis is 
    // needed)
    // 1. trace cavities (if specified) and set cavity eigenvalues as gauss values
    // 2. set user defined gauss parameters (q)
    // 3. set base q paramters for all other nodes by tracing beam
    if (inter.tem_is_set) {

        // allocote memory for tracing algorithm
        vlocal.trace_n[0] = (int *) calloc(mem.num_nodes + 1, sizeof (int));
        vlocal.trace_n[1] = (int *) calloc(mem.num_nodes + 1, sizeof (int));
        vlocal.trace_n[2] = (int *) calloc(mem.num_nodes + 1, sizeof (int));

        if (vlocal.trace_n[0] == NULL || vlocal.trace_n[1] == NULL
                || vlocal.trace_n[2] == NULL) {
            gerror("trace_nodes: memory allocation failure(1)\n");
        }

        vlocal.trace_c1 = (int *) calloc((mem.num_nodes + 1) * 2, sizeof (int));
        vlocal.trace_c2 = (int *) calloc(mem.num_nodes + 1, sizeof (int));

        if (vlocal.trace_c1 == NULL || vlocal.trace_c2 == NULL) {
            gerror("trace_nodes: memory allocation failure(2)\n");
        }

        // compute matrix for field indices : fields -> n, m
        set_all_tems();

        if (inter.trace & 1) {
            message("--- highest order of TEM modes: %d  -> number of fields: %d\n",
                    inter.tem, inter.num_fields);
            message(" field  |  TEM_nm  (n: x direction, m: y direction)\n");

            int field_index;
            for (field_index = 0; field_index < inter.num_fields; field_index++) {
                get_tem_modes_from_field_index(&tn, &tm, field_index);
                message("   %2d   |  %2d %2d\n", field_index, tn, tm);
            }
            message("\n");
        }

        // set ABCD matrices for all components
        set_ABCDs();

        // trace cavities and set cavity q-parameter to cavity nodes
        if (inter.num_cavities) {
            compute_cavity_params();
        }

        // setting user defined q paramters
        if (inter.num_gauss_cmds) {
            int cmd_index;
            for (cmd_index = 0; cmd_index < inter.num_gauss_cmds; cmd_index++) {
                gauss_t gauss = inter.gauss_list[cmd_index];
                set_q(gauss.node_index, gauss.component_index,
                        gauss.qx, gauss.qy);
            }
        }

        trace_hermite_gauss_beam();

        if(options.trace_output){
            char fname[FNAME_LEN] = {0};
            sprintf(fname, "%s_%i.trace", inter.basename, nTraces);
            FILE *f = fopen(fname, "w");
            dump_trace(f);
            fclose(f);
        }
        
        if (inter.trace & 8) {
            if (NOT options.quiet) {
                dump_trace(stdout);
            }

            dump_trace(fp_log);
        }

        // set gouy phases for spaces
        set_gouy_phase_for_spaces();

        // check for reflectivity maps and set R=T=1 for these
        prepare_maps();

        // Set up potential workspaces and other knm calcluation memory
        for(i=0; i<inter.num_mirrors; i++) {
            if(inter.mirror_list[i].map_rom) {
                allocate_romhom_workspace(inter.mirror_list[i].map_rom, MIRROR);
            }
            
            for(j=0; j < inter.mirror_list[i].num_surface_motions; j++) {
                if(inter.mirror_list[i].surface_motions_isROM[j]) {
                    allocate_romhom_workspace(&inter.rom_maps[inter.mirror_list[i].surface_motions[j]], MIRROR);
                }
            }

            if(inter.mirror_list[i].map_merged.integration_method == NEWTON_COTES) {
                allocate_newton_cotes_workspace(&inter.mirror_list[i].map_merged.knm_ws,
                                                &inter.mirror_list[i].map_merged,
                                                MIRROR);
                
                // Create the newton cotes weight matrix
                int r = inter.mirror_list[i].map_merged.rows;
                int c = inter.mirror_list[i].map_merged.cols;
                
                double *Wx = (double*) malloc(sizeof(double) * c);
                double *Wy = (double*) malloc(sizeof(double) * r);
                
                create_newton_cotes_composite_weights(Wx, inter.mirror_list[i].map_merged.cols, inter.mirror_list[i].map_merged.integration_NC_order);
                create_newton_cotes_composite_weights(Wy, inter.mirror_list[i].map_merged.rows, inter.mirror_list[i].map_merged.integration_NC_order);
                
                int k,j;
                
                for(k=0; k<c; k++) {
                    for(j=0; j<r; j++) {
                        inter.mirror_list[i].map_merged.knm_ws.W_xy[k][j] = Wx[k] * Wy[j];
                    }
                }
                
                free(Wx);
                free(Wy);
            }
        }
        
        set_k_all_components(false);
    }
 
    if (inter.trace & 32) {
        if (NOT options.quiet) {
            dump_ks(stdout);
        }
        dump_ks(fp_log);
    }
    //###################### end of HG section ###########
    //!< \todo return from here one day
}



//! Pre process mirror maps

void alloc_merged_map_data(surface_merged_map_t *map) {
    int rows0 = map->rows;
    int cols0 = map->cols;
    int i;

    // allocate memory for total complex maps, using the first map
    // to store data for *all* maps
    map->t_abs = (double **) calloc((cols0 + 1), sizeof (double *));
    map->t_phs = (double **) calloc((cols0 + 1), sizeof (double *));
    map->r_abs = (double **) calloc((cols0 + 1), sizeof (double *));
    map->r_phs = (double **) calloc((cols0 + 1), sizeof (double *));

    if (map->t_phs == NULL || map->t_abs == NULL || map->r_phs == NULL || map->r_abs == NULL) {
        bug_error("memory allocation for surface map data failed (3)");
    }

    for (i = 0; i < cols0; i++) {
        map->t_abs[i] = (double *) calloc((rows0 + 1), sizeof (double));
        map->t_phs[i] = (double *) calloc((rows0 + 1), sizeof (double));
        map->r_abs[i] = (double *) calloc((rows0 + 1), sizeof (double));
        map->r_phs[i] = (double *) calloc((rows0 + 1), sizeof (double));

        if (map->t_phs[i] == NULL || map->t_abs[i] == NULL || map->r_phs[i] == NULL || map->r_abs[i] == NULL) {
            bug_error("memory allocation for surface map data failed (4)");
        }
    }
    
    map->x = (double*) malloc(sizeof (double) * cols0);
    map->y = (double*) malloc(sizeof (double) * rows0);

    if (map->x == NULL || map->y == NULL) {
        bug_error("Could not allocate enough memory. rows=%d cols=%d map=%s"
                , map->rows, map->cols, map->name);
    }
}

void prepare_merged_maps_comp(void* cmp, KNM_COMPONENT_t cmpid) {

    surface_merged_map_t *map0 = NULL;
    int rebuild = 0, num_maps = 0;
    char *name = NULL;
    surface_map_t **maps = NULL;

    int rows = 0, cols = 0;
    double x0 = 0, y0 = 0, xstep = 0, ystep = 0, loss = 0;

    // Here we check what kind of component we are preparing the maps for. We then
    // store the required variables, which should be similar between things that
    // use maps. The code below is generalised for these variables
    if (cmpid == MIRROR_CMP) {
        mirror_t *mirror = (mirror_t*) cmp;
        name = mirror->name;
        map0 = &(mirror->map_merged);
        rebuild = mirror->rebuild;
        num_maps = mirror->num_maps;
        maps = mirror->map;
        loss = 1 - mirror->R - mirror->T;
        x0 = (num_maps > 0) ? mirror->map[0]->x0 : 0;
        y0 = (num_maps > 0) ? mirror->map[0]->y0 : 0;
        xstep = (num_maps > 0) ? mirror->map[0]->xstep : 0;
        ystep = (num_maps > 0) ? mirror->map[0]->ystep : 0;
        rows = (num_maps > 0) ? mirror->map[0]->rows : 0;
        cols = (num_maps > 0) ? mirror->map[0]->cols : 0;

    } else if (cmpid == BEAMSPLITTER_CMP) {
        beamsplitter_t *bs = (beamsplitter_t*) cmp;
        name = bs->name;
        map0 = &(bs->map_merged);
        rebuild = bs->rebuild;
        num_maps = bs->num_maps;
        maps = bs->map;

        x0 = (num_maps > 0) ? bs->map[0]->x0 : 0;
        y0 = (num_maps > 0) ? bs->map[0]->y0 : 0;
        xstep = (num_maps > 0) ? bs->map[0]->xstep : 0;
        ystep = (num_maps > 0) ? bs->map[0]->ystep : 0;
        rows = (num_maps > 0) ? bs->map[0]->rows : 0;
        cols = (num_maps > 0) ? bs->map[0]->cols : 0;
    } else {
        bug_error("Couldn't handle component in prepare_merged_maps_comp()");
    }

    map0->hasReflectionMaps = false;
    map0->hasTransmissionMaps = false;
    
    // default merged map interpolation and integration if not set
    // by conf commands
    if (map0->interpolation_method == 0)
        map0->interpolation_method = init.mapinterpmethod;

    if (map0->interpolation_size == 0)
        map0->interpolation_size = init.mapinterpsize;

    if (map0->integration_method == 0)
        map0->integration_method = init.mapintmethod;

    if (map0->integration_NC_order == 0) 
        map0->integration_NC_order = init.mapNCOrder;
        
    sprintf(map0->name, "%s_merged_map", name);

    // set a default map filename if are not saving knm and knm hasn't been specified
    if (!map0->save_to_file && strlen(map0->filename) == 0)
        strcpy(map0->filename, name);

    // also check if we are varying some parameter that requires rebuilding
    // the knm everytime, switch off saving knm
    if (map0->save_to_file && rebuild) {
        warn("The current xaxis parameter might cause the knm data files\n"
                "   to be overwritten for each xaxis computation. Consider not\n"
                "   using knm command for component %s\n", name);
    }

    if (map0->save_knm_matrices && rebuild) {
        warn("The current xaxis parameter might cause the knm matrices\n"
                "   to be overwritten for each xaxis computation. Consider setting\n"
                "   save_knm_matrices to 0 for component %s\n", name);
    }

    if (map0->save_interp_file && rebuild) {
        warn("The current xaxis parameter might cause the interpolation data\n"
                "   to be overwritten for each xaxis computation. Consider setting\n"
                "   save_interp_file to 0 for component %s\n", name);
    }

    if (num_maps == 0) {
        // no maps have been applied so no merging is necessary, set flag
        // so later we can ignore interpolation and integrating the maps
        map0->noMapPresent = true;
        
        // although no maps have been set, we still may need to compute a
        // merged map by integrating other analytic values in.
        
        // Reading in any existing values will be done by the read_knm_file next.
        map0->rows = 0;
        map0->cols = 0;
        map0->x0 = 0;
        map0->y0 = 0;
        map0->xstep = 0;
        map0->ystep = 0;

        // try and read a previous file, if we can't don't worry about
        // loading anything else
        bool f = read_knm_file(cmp, false, cmpid);
   
        if(f){
            // Without reparsing all the maps again to generated the merged one
            // we have to assume a mix were used.
            map0->hasReflectionMaps = true;
            map0->hasTransmissionMaps = true;

            int i,j;

            // As we are reading in the merged map from the files we also need
            // to regenerate some arrays
            for (i = 0; i < map0->cols; i++) {
                map0->x[i] = (i+1 - map0->x0) * map0->xstep;

                for (j = 0; j < map0->rows; j++) {
                    map0->y[j] = (j+1 - map0->y0) * map0->ystep;
                }
            }
        }
    } else if (num_maps < 0) {
        bug_error("There appears to be a negative number of maps when calling prepare_maps");
    } else {
        // First thing we need to do is initialise the merged map where
        // all the other maps will be stored
        map0->noMapPresent = false;

        // Here we try and read any existing merged map file that has 
        // been stored previously, if not we go about initialising everything
        if (read_knm_file(cmp, false, cmpid)) {

            // if we have read in the merged map we need to check if it contains
            // a reflectivity map merged into it as we have to update the R and T
            // values

            if (map0->hasReflectivityMap) {
                if (cmpid == MIRROR_CMP) {
                    double loss = 1 - ((mirror_t*) cmp)->R - ((mirror_t*) cmp)->T;
                    ((mirror_t*) cmp)->R = 1.0 - loss;
                    ((mirror_t*) cmp)->T = 1.0 - loss;
                } else {
                    double loss = 1 - ((beamsplitter_t*) cmp)->R - ((beamsplitter_t*) cmp)->T;
                    ((beamsplitter_t*) cmp)->R = 1.0 - loss;
                    ((beamsplitter_t*) cmp)->T = 1.0 - loss;
                }
            }

        } else {
            // take the first map that was added and use that as a reference
            map0->rows = rows;
            map0->cols = cols;
            map0->x0 = x0;
            map0->y0 = y0;
            map0->xstep = xstep;
            map0->ystep = ystep;

            alloc_merged_map_data(map0);

            int i, j;
            // set temp maps and complex map to zero 
            for (i = 0; i < map0->rows; i++) {
                for (j = 0; j < map0->cols; j++) {
                    //here the merged map should start life as a map that does
                    //nothing to a beam, i.e. amplitude=1 and phase=0
                    map0->t_abs[i][j] = 1.0;
                    map0->t_phs[i][j] = 0;
                    map0->r_abs[i][j] = 1.0;
                    map0->r_phs[i][j] = 0;
                }
            }

            surface_map_t *map = NULL;

            // reflectivity maps need to overwrite R and T from the mirror command
            // TODO check this and if correct, add to the manual
            for (i = 0; i < num_maps; i++) {
                map = maps[i];
                map->Loss = loss;

                if (map->type == REFLECTIVITY_MAP) {
                    map0->hasReflectivityMap = true;
                    map->transmission = true;
                    map->reflection = true;

                    if (cmpid == MIRROR_CMP) {
                        double loss = 1.0 - ((mirror_t*) cmp)->R - ((mirror_t*) cmp)->T;
                        ((mirror_t*) cmp)->R = 1.0 - loss;
                        ((mirror_t*) cmp)->T = 1.0 - loss;
                    } else {
                        double loss = 1.0 - ((beamsplitter_t*) cmp)->R - ((beamsplitter_t*) cmp)->T;
                        ((beamsplitter_t*) cmp)->R = 1.0 - loss;
                        ((beamsplitter_t*) cmp)->T = 1.0 - loss;
                    }
                }
            }
            int k;
            bool phaseRTmapApplied = false;

            // compute ONE amplitude and ONE phase map from multiple maps
            for (k = 0; k < num_maps; k++) {
                map = maps[k];

                if (map0->rows != map->rows || map0->cols != map->cols) {
                    gerror("Multiple maps at mirror '%s' must have the same size\n", name);
                }

                if (map0->x0 != map->x0 || map0->xstep != map->xstep || map0->y0 != map->y0 || map0->ystep != map->ystep) {
                    gerror("multiple maps at mirror '%s', coordinates x0, y0, xstep, ystep don't match!\n", name);
                }

                double k0 = TWOPI / init.lambda;
                double kr = k0;
                double kt = k0;

                // prepare equation according to map type
                double A, Cr, Ct, Br, Bt;

                // silence compiler warning
                k0 = 0.0;
                A = 1.0;
                Bt = Br = 0.0;
                Ct = Cr = 0.0;

                switch (map->type) {
                    case PHASE_MAP:
                        k0 = TWOPI / init.lambda;
                        A = 1.0;
                        Bt = 0.0;
                        Ct = 0.0;
                        Br = 0.0;
                        Cr = 0.0;

                        // Below we need to check that the user is not applying
                        // a transmission phase map and a merged reflection and
                        // transmission map to the same mirror
                        if (map->transmission && map->reflection) {

                            if (map0->phaseIsOnlyTransmission)
                                gerror("You cannot apply a transmission map and a"
                                    " merged reflection and tranmission map to"
                                    " %s. Remove one or the other.\n", name);

                            phaseRTmapApplied = true;

                        } else if (map->transmission && !map->reflection) {
                            if (phaseRTmapApplied)
                                gerror("You cannot apply a transmission map and a"
                                    " merged reflection and tranmission map to"
                                    " %s. Remove one or the other.\n", name);

                            map0->phaseIsOnlyTransmission = true;
                        }


                        break;
                    case ABSORPTION_MAP:
                        k0 = 0;
                        A = 0.0;
                        Bt = -1.0;
                        Ct = 1.0;
                        Br = -1.0;
                        Cr = 1;
                        break;
                    case REFLECTIVITY_MAP:
                        k0 = 0;
                        A = 0.0;
                        Bt = -1.0;
                        Ct = 1.0;
                        Br = 1.0;
                        Cr = 0;
                        kr = 0;
                        kt = 0;
                        break;
                    default:
                        bug_error("wrong map type");
                        break;
                }

                if (!map->reflection) {
                    kr = 0.0;
                    Br = 0.0;
                    kt = -1.0 * k0;
                    map0->hasReflectionMaps = false;
                } else {
                    map0->hasReflectionMaps = true;
                }

                 if (!map->transmission) {
                    kt = 0.0;
                    Bt = 0.0;
                    map0->hasTransmissionMaps = false;
                } else {
                    map0->hasTransmissionMaps = true;
                }

                for (i = 0; i < map0->cols; i++) {
                    map0->x[i] = (i+1 - map0->x0) * map0->xstep;
                    
                    for (j = 0; j < map0->rows; j++) {
                        map0->y[j] = (j+1 - map0->y0) * map0->ystep;
                        
                        map0->r_abs[i][j] = map0->r_abs[i][j] * (A + sqrt(Cr + Br * map->data[i][j]));
                        map0->r_phs[i][j] = map0->r_phs[i][j] + 2.0 * kr * map->data[i][j];
                        map0->t_abs[i][j] = map0->t_abs[i][j] * (A + sqrt(Ct + Bt * map->data[i][j]));
                        map0->t_phs[i][j] = map0->t_phs[i][j] + kt * map->data[i][j];
                    }
                }
            }
        }
    }
}

/*!
 * Compute total maps from several user defined maps
 * per mirror.
 */
void prepare_maps() {
    //surface_map_t *map;
    int i;

    int tid = startTimer("PREPARING_MAPS");

    for (i = 0; i < inter.num_mirrors; i++) {
        mirror_t *mr = &inter.mirror_list[i];
        prepare_merged_maps_comp((void*) mr, MIRROR_CMP);
    }

    for (i = 0; i < inter.num_beamsplitters; i++) {
        beamsplitter_t *bs = &inter.bs_list[i];
        prepare_merged_maps_comp((void*) bs, BEAMSPLITTER_CMP);
    }

    endTimer(tid);
}

//! Trace the Hermite-Gauss beam throughout the interferometer

void trace_hermite_gauss_beam(void) {
    // Now start tracing beam at:
    // user defined startnode or
    // first user defined (stable) cavity or
    // if there is no cavity at first user defined gauss parameter.
    // If neither is set, then set dummy parameters for input light
    // and start trace from there.
    bool startnode_is_set = false;

    if (inter.startnode_set) {
        if (inter.node_list[inter.startnode].q_is_set) {
            trace_beam(inter.startnode);
        } else if (inter.retrace != 2) {
            gerror("no beam parameter set at user defined startnode '%s'\n", inter.node_list[inter.startnode].name);
        }

    } else {
        if (inter.num_cavities) {
            int cavity_index = 0;

            while (cavity_index < inter.num_cavities && NOT startnode_is_set) {
                if (inter.cavity_list[cavity_index].stable) {
                    trace_beam(inter.cavity_list[cavity_index].node1_index);
                    startnode_is_set = true;
                    inter.startnode = inter.cavity_list[cavity_index].node1_index;
                }
                cavity_index++;
            }
        }

        if (NOT startnode_is_set) {
            if (inter.num_gauss_cmds) {
                trace_beam(inter.gauss_list[0].node_index);
                inter.startnode = inter.gauss_list[0].node_index;
            } else {
                // no stable cavity and no gauss paramter given by user !!!
                // --->   set dummy q values for all input lasers ...
                warn("No stable cavity nor gauss parameter given, "
                        "using dummy values for all input lasers!\n");
                warn("Switching retrace off now.\n");
                inter.retrace = 0;
                int laser_index;
                for (laser_index = 0;
                        laser_index < inter.num_light_inputs;
                        laser_index++) {
                    set_q(inter.light_in_list[laser_index].node_index,
                            get_overall_component_index(LIGHT_INPUT, laser_index),
                            q_w0z(0.002, 0.0, 1.0), q_w0z(0.002, 0.0, 1.0));
                }
                // ... and start tracing at first laser
                trace_beam(inter.light_in_list[0].node_index);
                inter.startnode = inter.light_in_list[0].node_index;
            }
        }
    }
    
    nTraces++;
}

//! Set parameters for progress print

void set_progress_printing_parameters(void) {
    // for the moment pstep=1 
    init.num_points_total = inter.x1.xsteps + 1;
    if (inter.splot) {
        init.num_points_total = init.num_points_total * (inter.x2.xsteps + 1);
    }

    if (inter.x3_axis_is_set) {
        init.num_points_total = init.num_points_total * (inter.x3.xsteps + 1);
    }

    init.percentage_step = 1;
    //!< \todo return from here one day
}

//! Set gouy phases for spaces

void set_gouy_phase_for_spaces(void) {
    int node_num, node_indices[4] = {0};
    int node1_index, node2_index;
    complex_t qxt, qyt, qx1, qy1, qx2, qy2;
    double nr1, nr2;

    if (inter.trace & 16) {
        message("--- setting Gouy phase for all spaces:\n");
    }

    int space_index;
    for (space_index = 0; space_index < inter.num_spaces; space_index++) {
        int overall_component_index =
                get_overall_component_index(SPACE, space_index);
        which_nodes(overall_component_index, GND_NODE, &node_num, node_indices);
        if (inter.node_list[node_indices[0]].traced_from_component_index == overall_component_index) {
            node1_index = node_indices[1];
            node2_index = node_indices[0];
        } else {
            node1_index = node_indices[0];
            node2_index = node_indices[1];
        }

        node_t *node1;
        node1 = &inter.node_list[node1_index];
        node_t *node2;
        node2 = &inter.node_list[node2_index];

        nr1 = *node1->n;
        nr2 = *node2->n;

        if (nr1 != nr2) {
            bug_error("i-r inconsistent");
        }

        if (node1->traced_from_component_index == overall_component_index) {
            qx1 = cminus(cconj(node1->qx));
            qy1 = cminus(cconj(node1->qy));
        } else {
            qx1 = node1->qx;
            qy1 = node1->qy;
        }

        space_t *space;
        space = &inter.space_list[space_index];

        qxt = q1_q2(space->qq, qx1, nr1, nr2);
        qyt = q1_q2(space->qq, qy1, nr1, nr2);

        if (node2->traced_from_component_index == overall_component_index) {
            qx2 = node2->qx;
            qy2 = node2->qy;
        } else {
            qx2 = cminus(cconj(node2->qx));
            qy2 = cminus(cconj(node2->qy));
        }

        if (!ceq(qxt, qx2) || (!ceq(qyt, qy2))) {
            message("(old qx %s qy %s , new qx %s qy %s)\n",
                    complex_form(qx2), complex_form(qy2),
                    complex_form(qxt), complex_form(qyt));
            warn("q mismatch at space %s\n",
                    get_component_name(overall_component_index));
        }

        if ((space->attribs & GOUYX) == 0) {
            space->gouy_x = DEG * fabs(gouy(qx2) - gouy(qx1));
        }

        if ((space->attribs & GOUYY) == 0) {
            space->gouy_y = DEG * fabs(gouy(qy2) - gouy(qy1));
        }

        if (inter.trace & 16) {
            message("space %s: gouy_x=%srad=%sdeg, gouy_y=%srad=%sdeg\n",
                    get_component_name(overall_component_index),
                    xdouble_form(space->gouy_x * RAD),
                    xdouble_form(space->gouy_x),
                    xdouble_form(space->gouy_y * RAD),
                    xdouble_form(space->gouy_y));
        }
    }

    if (inter.trace & 16) {
        message("\n");
    }
    //!< \todo return from here one day
}

//! Read the initial photodetector type ???

/*!
 *
 */
void read_init_pdtype(void) {
    // read init file
    char s0[LINE_LEN], *s;
    FILE *fp;

    fp = NULL; // avoid compiler warning
    if (init.init_file_is_found == ENV_KATINI) {
      fp = fopen(init.katinifile, "r");
    } else if (init.init_file_is_found == DEFAULT_KATINI) {
      fp = fopen(INIT_FILE, "r");
    }
    
    if (fp == NULL) {
        bug_error("Could not open init file for second read");
    }

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

        if (prepare_line(s, 0)) {
            continue;
        } else if (string_matches(s, "gnuterm")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "pdtype")) {
            ri_pdtype(s, 7, fp);
        } else {
            continue;
        }

    }
    fclose(fp);
}


//! Read environmental variables

/*!
 *
 * 
 * 
 */
void read_env(void) {
    char *s;

    // reading KATINI
    init.katini_filename_set = false;
    s = getenv("KATINI");

    // if KATINI is set test for file
    if (s != NULL) {
        strcpy(init.katinifile, s);
        init.katini_filename_set = true;
    }
}

void _set_default_gnucommand() {
    switch (OS) {
        case __WIN_32__:
        case __WIN_64__:
            strcpy(init.gnucommand, "gnuplot.exe --persist $s");
            break;
        case UNIX:
            strcpy(init.gnucommand, "gnuplot -persist $s");
            break;
        case MACOS:
            strcpy(init.gnucommand, "gnuplot -persist $s");
            break;
        default:
            bug_error("wrong OS in _set_default_gnucommand");
    }
}

void _set_default_pycommand() {
    switch (OS) {
        case __WIN_32__:
        case __WIN_64__:
            strcpy(init.pycommand, "pythonw $s");
            break;
        case UNIX:
            strcpy(init.pycommand, "python $s");
            break;
        case MACOS:
            strcpy(init.pycommand, "python $s");
            break;
        default:
            bug_error("wrong OS in _set_default_python");
    }
}

//! Read the initialisation file

/*!
 *
 * \todo setting of default values when ini file not found is untested
 * \todo adding the various gnuplot terminals if not found in ini file untested
 */
void read_init(void) {

    // read init file
    char s0[LINE_LEN] = {0}, *s = NULL;
    char plot_default[LINE_LEN] = {0};
    FILE *fp = NULL;
    gnuplot_terminal_t *gnuplot_terminal = NULL;
    python_terminal_t *python_terminal = NULL;

    init_def();
    init.winterm = -1;
    init.unixterm = -1;
    init.macosterm = -1;
    init.screenterm = -1;

    //null the gnucommand string
    init.gnucommand[0] = '\0';
    init.pycommand[0] = '\0';

    fp = fopen(INIT_FILE, "r");

    if (fp == NULL && init.katini_filename_set == true) {
        fp = fopen(init.katinifile, "r");
        init.init_file_is_found = ENV_KATINI;
    } else {
        init.init_file_is_found = DEFAULT_KATINI;
    }
    
    if (fp == NULL) {
        init.init_file_is_found = 0;
        if (init.katini_filename_set) {
            warn("Could not open init file '%s, using default values.\n", init.katinifile);
        }
        else {
          warn("Could not open init file '%s', using default values.\n", INIT_FILE);
        }
        
        // setting a default python term for Linux/OSX/Windows
        python_terminal = &(init.pyterm[0]);
        python_terminal->name = duplicate_string("screen");
        python_terminal->output_is_file = false;
        python_terminal->command = duplicate_string("");
        python_terminal->suffix = duplicate_string("");

        init.screenterm = 0;

        // setting a default gnuplot term for Linux/OSX/Windows
        gnuplot_terminal = &(init.gnuterm[0]);
        gnuplot_terminal->name = duplicate_string("x11");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term x11\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.unixterm = 0;

        gnuplot_terminal = &(init.gnuterm[1]);
        gnuplot_terminal->name = duplicate_string("qt");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term aqua\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.macosterm = 1;

        gnuplot_terminal = &(init.gnuterm[2]);
        gnuplot_terminal->name = duplicate_string("windows");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term windows\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.num_gnuplot_terminals = 3;
        init.winterm = 2;

        _set_default_gnucommand();
        _set_default_pycommand();

    } else {
        while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
            s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

            if (prepare_line(s, 0)) {
                continue;
            }

            // ignore Luxor commands
            if (string_matches(s, "FINESSEDIR")) {
                continue;
            }

            if (string_matches(s, "FINESSECMD")) {
                continue;
            }

            if (string_matches(s, "datadir")) {
                continue;
            }

            if (string_matches(s, "pstep")) {
                read_init_integer(s, 5, &init.percentage_step);
            } else if (string_matches(s, "x_scale")) {
                read_init_double(s, 7, &init.x_scale);
            } else if (string_matches(s, "lambda")) {
                read_init_double(s, 6, &init.lambda);
            } else if (string_matches(s, "clight")) {
                read_init_double(s, 6, &init.clight);
            } else if (string_matches(s, "eqsmall")) {
                read_init_double(s, 7, &init.eqsmall);
            } else if (string_matches(s, "small")) {
                read_init_double(s, 5, &init.small);
            } else if (string_matches(s, "qeff")) {
                read_init_double(s, 4, &init.qeff);
            } else if (string_matches(s, "epsilon_c")) {
                warn("epsilon_c not supported in kat.ini anymore please remove it and use\n");
                warn("amplitude_scaling option instead.\n");
                //read_init_double(s, 9, &init.epsilon_0_c);
            } else if (string_matches(s, "deriv_h")) {
                read_init_double(s, 7, &init.deriv_h);
            } else if (string_matches(s, "n0")) {
                read_init_double(s, 2, &init.n0);
            } else if (string_matches(s, "quantum_scaling")) {
                // C standard says enums are equivalent to an int so we can
                // just cast the pointer to stop the warnings
                read_init_integer(s, 15, (int*) &init.default_quantum_output_scale);
            } else if (string_matches(s, "gnucommand")) {
                read_init_string(s, 10, init.gnucommand);
            } else if (string_matches(s, "amplitude_scaling")) {
                read_init_integer(s, 17, (int*) &init.amplitude_scaling);
            } else if (string_matches(s, "pythoncommand")) {
                read_init_string(s, 13, init.pycommand);
            } else if (string_matches(s, "plotting")) {
                read_init_string(s, 8, plot_default);
            } else if (string_matches(s, "gnuterm")) {
                read_init_gnuterm(s, 7, fp);
            } else if (string_matches(s, "pyterm")) {
                read_init_pyterm(s, 6, fp);
            } else if (string_matches(s, "pdtype")) {
                read_init_dummy(fp, 0);
            } else if (string_matches(s, "locksteps")) {
                read_init_integer(s, 9, &init.locksteps);
            } else if (string_matches(s, "locktest1")) {
                read_init_integer(s, 9, &init.locktest1);
            } else if (string_matches(s, "locktest2")) {
                read_init_integer(s, 9, &init.locktest2);
            } else if (string_matches(s, "lockthresholdlow")) {
                read_init_double(s, 16, &init.lockthresholdlow);
            } else if (string_matches(s, "lockthresholdhigh")) {
                read_init_double(s, 17, &init.lockthresholdhigh);
            } else if (string_matches(s, "gainfactor")) {
                read_init_double(s, 10, &init.gainfactor);
            } else if (string_matches(s, "sequential")) {
                read_init_integer(s, 10, &init.sequential);
            } else if (string_matches(s, "autogain")) {
                read_init_integer(s, 8, &init.autogain);
            } else if (string_matches(s, "autostop")) {
                read_init_integer(s, 8, &init.autostop);
            } else if (string_matches(s, "gnuversion")) {
                read_init_double(s, 10, &init.gnuversion);
            } else if (string_matches(s, "pythonversion")) {
                read_init_double(s, 13, &init.pythonversion);
            } else if (string_matches(s, "maxintop")) {
                read_init_integer(s, 8, &init.maxintop);
            } else if (string_matches(s, "maxintcuba")) {
                read_init_integer(s, 10, &init.maxintcuba);
            } else if (string_matches(s, "mapinterpsize")) {
                read_init_integer(s, 13, &init.mapinterpsize);
            } else if (string_matches(s, "mapinterpmethod")) {
                read_init_integer(s, 15, (int*) &init.mapinterpmethod);
            } else if (string_matches(s, "abserr")) {
                read_init_double(s, 6, &init.abserr);
            } else if (string_matches(s, "relerr")) {
                read_init_double(s, 6, &init.relerr);
            } else if (string_matches(s, "mapintmethod")) {
                read_init_integer(s, 12, (int*) &init.mapintmethod);
            } else if (string_matches(s, "calc_knm_transpose")) {
                read_init_integer(s, 18, &init.calc_knm_transpose);
            } else if (string_matches(s, "cuba_numprocs")) {
                read_init_integer(s, 13, &init.cuba_numprocs);
            } else if (string_matches(s, "mismatches_R_T_limit")) {
                read_init_double(s, 20, &inter.mismatches_R_T_limit);
            } else {
                warn("Init file kat.ini: input line '%s':\n missing keyword\n", s);
            }
        }
        
        fclose(fp);        
    }
    
    if (init.cuba_numprocs <= 0) {
        gerror("cuba_numprocs shouldn't be less than or equal to 0\n");
    }

    if (init.cuba_numprocs > getNumCores()) {
        warn("cuba_numprocs (%i) shouldn't be more than the number\n"
                "    of processors in this system (%i)\n", init.cuba_numprocs, getNumCores());
    }

    init.plotting = PLOT_GNUPLOT;
    if (string_matches(plot_default, "python")) {
        init.plotting = PLOT_PYTHON;
    }

    // check read values (to be extended)
    if (init.maxintop < MIN_INT_OP) {
        init.maxintop = MIN_INT_OP;
    }

    if (init.lambda <= 0) {
        gerror("kat.ini: lambda must be >0\n");
    }

    if (init.clight <= 0) {
        gerror("kat.ini: clight must be >0\n");
    }

    switch(init.amplitude_scaling){
        case ONE:
        case SQRT_2:
            init.epsilon_0_c = 1.0;
            break;
        case SQRT_2_EPS_C:
            init.epsilon_0_c = 2.65441873e-3;
            break;
    }

    if (init.epsilon_0_c <= 0) {
        gerror("kat.ini: epsilon_0_c must be >0\n");
    }

    if (init.n0 == 0) {
        gerror("kat.ini: n0 must be >0\n");
    }

    if (init.locksteps <= 0) {
        gerror("kat.ini: locksteps must be >0\n");
    }

    if (init.locktest1 <= 0) {
        gerror("kat.ini: locktest1 must be >0\n");
    }

    if (init.locktest2 <= 0) {
        gerror("kat.ini: locktest2 must be >0\n");
    }

    if (init.lockthresholdlow <= 0) {
        gerror("kat.ini: lockthresholdlow must be >0\n");
    }

    if (init.lockthresholdhigh <= 0) {
        gerror("kat.ini: lockthresholdhigh must be >0\n");
    }

    if (init.gainfactor <= 0) {
        gerror("kat.ini: gainfactor must be >0\n");
    }

    if ((init.autogain != 0) && (init.autogain != 1) && (init.autogain != 2)) {
        gerror("kat.ini: autogain must be 0/1/2\n");
    }

    if ((init.gnuversion < 3.7) || (init.gnuversion > 6.99)) {
        gerror("kat.ini: recommended versions of gnuplot are 4.x or 5.x\n");
    }

    if ((init.pythonversion < 2.7) || (init.pythonversion > 4.99)) {
        gerror("kat.ini: recommended versions of python are 2.7 or 3.x\n");
    }

    if ((init.sequential < 0) || (init.sequential > 6)) {
        gerror("kat.ini: sequential must be 0/1/2/5/6\n");
    }

    if ((init.autostop != 0) && (init.autostop != 1)) {
        gerror("kat.ini: autostop must be 0/1\n");
    }
    
    // Add x11 and windows terminal if not found in kat.ini
    if (init.unixterm < 0) {
        gnuplot_terminal = &(init.gnuterm[init.num_gnuplot_terminals]);
        gnuplot_terminal->name = duplicate_string("x11");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term x11\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.unixterm = init.num_gnuplot_terminals++;
    }

    if (init.macosterm < 0) {
        gnuplot_terminal = &(init.gnuterm[init.num_gnuplot_terminals]);
        gnuplot_terminal->name = duplicate_string("qt");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term qt\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.macosterm = init.num_gnuplot_terminals++;
    }

    if (init.winterm < 0) {
        gnuplot_terminal = &(init.gnuterm[init.num_gnuplot_terminals]);
        gnuplot_terminal->name = duplicate_string("windows");
        gnuplot_terminal->output_is_file = false;
        gnuplot_terminal->command = duplicate_string("set term windows\nset size ratio .5\n"
                "set key below\nset grid xtics ytics\n set title\n");
        init.winterm = init.num_gnuplot_terminals++;
    }

    // check if gnucommand is still nulled from before
    // reading the kat.ini file        
    if (strlen(init.gnucommand) == 0) {
        _set_default_gnucommand();
    } else {
        if (!strstr(init.gnucommand, "$s")) {
            strcat(init.gnucommand, " $s");
        }
    }
    if (strlen(init.pycommand) == 0) {
        _set_default_pycommand();
    } else {
        if (!strstr(init.pycommand, "$s")) {
            strcat(init.pycommand, " $s");
        }
    }
}

//! Read input double precision number from initialisation file

/*!
 * \param input_string input string to convert
 * \param string_offset offset in input string (to start scanning after keyword)
 * \param dp double precision number to return
 */
void read_init_double(char *input_string, int string_offset, double *dp) {
    double temp;

    if (atod(input_string + string_offset, &temp)) {
        warn("Init file 'kat.ini: couldn't read number from\n"
                "'%s'\ndefault used\n", input_string);
        return;
    } else {
        *dp = temp;
    }
}

//! Read input integer number from initialisation file

/*!
 * \param input_string input string to convert
 * \param string_offset offset in input string to start reading integer
 * \param ip integer to return
 */
void read_init_integer(char *input_string, int string_offset, int *ip) {
    int temp;

    if (sscanf(input_string + string_offset, "%d", &temp) != 1) {
        warn("Init file 'kat.ini: couldn't read number from\n"
                "'%s'\ndefault used\n", input_string);
        return;
    } else {
        *ip = temp;
    }
}

//! Read input string from initialisation file

/*!
 * \param input_string input string to convert
 * \param string_offset offset in input string (to start scanning after keyword)
 * \param return_string the string to return
 */
void read_init_string(char *input_string, int string_offset, char *return_string) {
    char *s1 = input_string + string_offset + strspn(input_string + string_offset, " \t"); /* skipped white space */
    char *s2;

    if (*s1 == '"') {
        ++s1;
        if ((s2 = strchr(s1, '"')) == NULL) {
            warn("Init file 'kat.ini': unmatched quote (\") in line\n%s\n", input_string);
            return;
        }
        assert(s2 - s1 < LINE_LEN);
        *s2 = '\0';
        strcpy(return_string, s1);
        return;
    } else if (*s1 == '\'') {
        ++s1;
        if ((s2 = strchr(s1, '\'')) == NULL) {
            warn("Init file 'kat.ini': unmatched quote (\") in line\n%s\n", input_string);
            return;
        }
        assert(s2 - s1 < LINE_LEN);
        *s2 = '\0';
        strcpy(return_string, s1);
        return;
    }

    if (*s1 == '\0') {
        warn("Init file 'kat.ini': emtpy string in line\n'%s'\n", input_string);
    }

    strcpy(return_string, s1);
}

//! Read input photodetector type

/*!
 * \param sn input string holding keyword and pdtype name
 * \param l offset in string s (to start scanning after keyword)
 * \param fp the file pointer
 */
void ri_pdtype(char *sn, int l, FILE *fp) {
    char s0[LINE_LEN], *s;
    pdtype_t *pdt;
    int ended = 0;
    int n[4] = {0};
    int m[4] = {0};
    int x[4] = {0};
    int y[4] = {0};
    int x0, y0;
    double val1;
    char sval1[LINE_LEN] = {0};
    char sm1[LINE_LEN] = {0};
    char sm2[LINE_LEN] = {0};
    char sm3[LINE_LEN] = {0};
    char sm4[LINE_LEN] = {0};
    int nread;
    char rest[82] = {0};
    char dummy1[10] = {0};
    char *dummy;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    dummy = dummy1;

    if (init.num_pdtypes >= MAX_PD_TYPE) {
        gerror("too many detector type definitions\n");
    }

    sn += l;
    while (*sn == ' ' || *sn == '\t') {
        ++sn;
    }

    if (*sn == '\0' || *sn == '#' || *sn == '%') {
        gerror("detector type name expected in '%s'\n", sn);
    }
    pdt = &(init.pdt[init.num_pdtypes]);

    if (sscanf(sn, "%s", s0) < 1) {
        bug_error("ri_pdtype1");
    }
    pdt->name = duplicate_string(s0);

    pdt->cross = (double ****) calloc(mem.hg_mode_order + 2, sizeof (double ***));

    if (pdt->cross == NULL) {
        gerror("memory allocation error (pdtype 1)\n");
    }

    int i;
    for (i = 0; i <= mem.hg_mode_order; i++) {
        pdt->cross[i] =
                (double ***) calloc(mem.hg_mode_order + 2, sizeof (double **));

        if (pdt->cross[i] == NULL) {
            gerror("memory allocation error (pdtype 1)\n");
        }

        int j;
        for (j = 0; j <= mem.hg_mode_order; j++) {
            pdt->cross[i][j] =
                    (double **) calloc(mem.hg_mode_order + 2, sizeof (double *));

            if (pdt->cross[i][j] == NULL) {
                gerror("memory allocation error (pdtype 2)\n");
            }

            int k;
            for (k = 0; k <= mem.hg_mode_order; k++) {
                pdt->cross[i][j][k] =
                        (double *) calloc(mem.hg_mode_order + 2, sizeof (double));
                if (pdt->cross[i][j][k] == NULL) {
                    message("i:%d j:%d k:%d mem.hg_mode_order:%d\n",
                            i, j, k, mem.hg_mode_order);
                    gerror("memory allocation error (pdtype 3)\n");
                }
            }
        }
    }

    for (i = 0; i <= mem.hg_mode_order; i++) {
        int j;
        for (j = 0; j <= mem.hg_mode_order; j++) {
            int k;
            for (k = 0; k <= mem.hg_mode_order; k++) {
                int p;
                for (p = 0; p <= mem.hg_mode_order; p++) {
                    pdt->cross[i][j][k][p] = 0.0;
                }
            }
        }
    }

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */
        if (prepare_line(s, 1)) {
            continue;
        }

        if (string_matches(s, "end")) {
            ended = 1;
            break;
        }

        if ((nread =
                sscanf(s, "%s %s %s %s %s %80s", sm1, sm2, sm3, sm4, sval1,
                rest)) < 5 || atod(sval1, &val1)) {
            gerror("Init file 'kat.ini', pdtype %s: \n"
                    "expected `n1 m1 n2 m2 factor', e.g. '0 0 0 1 0.1'\n", pdt->name);
        }

        x0 = y0 = 0;
        int i;
        for (i = 0; i < 4; i++) {
            x[i] = y[i] = 0;
        }

        if (sm1[0] == '*') {
            n[0] = 0;
            m[0] = mem.hg_mode_order;
        } else if (sm1[0] == 'x') {
            n[0] = 0;
            m[0] = mem.hg_mode_order;
            x[0]++;
        } else if (sm1[0] == 'y') {
            n[0] = 0;
            m[0] = mem.hg_mode_order;
            y[0]++;
        } else {
            n[0] = strtol(sm1, &dummy, 10);

            if (dummy[0] != '\0') {
                gerror("Init file 'kat.ini', pdtype %s:\n"
                        " expected `n1 m1 n2 m2 factor', e.g. '0 0 0 1 0.1'\n",
                        pdt->name);
            }

            if (n[0] < 0) {
                gerror("Init file 'kat.ini', pdtype %s: mode numbers must be >0'\n",
                        pdt->name);
            }

            if (n[0] > mem.hg_mode_order) {
                continue;
            }

            m[0] = n[0];
        }

        if (sm2[0] == '*') {
            n[1] = 0;
            m[1] = mem.hg_mode_order;
        } else if (sm2[0] == 'x') {
            x[1]++;
            n[1] = 0;
            m[1] = mem.hg_mode_order;
        } else if (sm2[0] == 'y') {
            y[1]++;
            n[1] = 0;
            m[1] = mem.hg_mode_order;
        } else {
            n[1] = strtol(sm2, &dummy, 10);

            if (dummy[0] != '\0') {
                gerror("Init file 'kat.ini', pdtype %s: "
                        "expected `n1 m1 n2 m2 factor', e.g. '0 0 0 1 0.1'\n",
                        pdt->name);
            }

            if (n[1] < 0) {
                gerror("Init file 'kat.ini', pdtype %s: mode numbers must be >0'\n",
                        pdt->name);
            }

            if (n[1] > mem.hg_mode_order) {
                continue;
            }

            m[1] = n[1];
        }
        if (sm3[0] == '*') {
            n[2] = 0;
            m[2] = mem.hg_mode_order;
        } else if (sm3[0] == 'x') {
            x[2]++;
            n[2] = 0;
            m[2] = mem.hg_mode_order;
        } else if (sm3[0] == 'y') {
            y[2]++;
            n[2] = 0;
            m[2] = mem.hg_mode_order;
        } else {
            n[2] = strtol(sm3, &dummy, 10);

            if (dummy[0] != '\0') {
                gerror("Init file 'kat.ini', pdtype %s: "
                        "expected `n1 m1 n2 m2 factor', e.g. '0 0 0 1 0.1'\n",
                        pdt->name);
            }

            if (n[2] < 0) {
                gerror("Init file 'kat.ini', pdtype %s: mode numbers must be >0'\n",
                        pdt->name);
            }

            if (n[2] > mem.hg_mode_order) {
                continue;
            }

            m[2] = n[2];
        }

        if (sm4[0] == '*') {
            n[3] = 0;
            m[3] = mem.hg_mode_order;
        } else if (sm4[0] == 'x') {
            x[3]++;
            n[3] = 0;
            m[3] = mem.hg_mode_order;
        } else if (sm4[0] == 'y') {
            y[3]++;
            n[3] = 0;
            m[3] = mem.hg_mode_order;
        } else {
            n[3] = strtol(sm4, &dummy, 10);
            
            if (dummy[0] != '\0') {
                gerror("Init file 'kat.ini', pdtype %s: "
                        "expected `n1 m1 n2 m2 factor', e.g. '0 0 0 1 0.1'\n",
                        pdt->name);
            }

            if (n[3] < 0) {
                gerror("init file 'kat.ini', pdtype %s: mode numbers must be >0'\n",
                        pdt->name);
            }

            if (n[3] > mem.hg_mode_order) {
                continue;
            }

            m[3] = n[3];
        }

        // is that OK, maybe larger values are sometimes useful?
        if (val1 < -1.0 || val1 > 1.0) {
            gerror("init file 'kat.ini', pdtype %s: "
                    "factor must be between -1 and 1\n", pdt->name);
        }

        if (nread > 5) {
            warn("init file 'kat.ini', pdtype %s: text '%s' ignored\n",
                    pdt->name, rest);
        }

        for (i = n[0]; i <= m[0]; i++) {
            if (x[0]) {
                x0 = i;

                if (x[1]) {
                    n[1] = m[1] = x0;
                }
            }

            if (y[0]) {
                y0 = i;

                if (y[1]) {
                    n[1] = m[1] = y0;
                }
            }

            int j;
            for (j = n[1]; j <= m[1]; j++) {
                if (x[1]) {
                    x0 = j;
                }

                if (x[2] && x[0] | x[1]) {
                    n[2] = m[2] = x0;
                }

                if (y[1]) {
                    y0 = j;
                }

                if (y[2] && y[0] | y[1]) {
                    n[2] = m[2] = y0;
                }

                int k;
                for (k = n[2]; k <= m[2]; k++) {
                    if (x[2]) {
                        x0 = k;
                    }

                    if (x[3] && x[0] | x[1] | x[2]) {
                        n[3] = m[3] = x0;
                    }

                    if (y[2]) {
                        y0 = k;
                    }

                    if (y[3] && y[0] | y[1] | y[2]) {
                        n[3] = m[3] = y0;
                    }

                    int p;
                    for (p = n[3]; p <= m[3]; p++) {
                        if (i > mem.hg_mode_order ||
                                j > mem.hg_mode_order ||
                                k > mem.hg_mode_order ||
                                p > mem.hg_mode_order) {
                            bug_error("pdtype, index memory error");
                        }
                        pdt->cross[i][j][k][p] = val1;
                        pdt->cross[k][p][i][j] = val1;
                    }
                }
            }
        }
    }

    if (!ended) {
        warn("init file 'kat.ini': pdtype: 'END' missing\n");
    }
    ++init.num_pdtypes;
}

//! Read input/initial gnuplot terminal

/*!
 * \param input_string input string holding keyword and gnuterm name
 * \param string_offset offset in string s (to start scanning after keyword)
 * \param fp the file pointer
 *
 * \todo file suffix setting not fully tested
 */
void read_init_gnuterm(char *input_string, int string_offset, FILE *fp) {
    assert(fp != NULL);

    gnuplot_terminal_t *gnuplot_terminal;

    int ended = 0;
    int command_length = 0;

    char *command_string;
    command_string = input_string;
    if (init.num_gnuplot_terminals >= MAX_GNUTERM) {
        gerror("too many gnuplot terminals\n");
    }

    input_string += string_offset;
    while (*input_string == ' ' || *input_string == '\t') {
        ++input_string;
    }

    if (*input_string == '\0' || *input_string == '#' || *input_string == '%') {
        gerror("gnuplot terminal name expected in '%s'\n", command_string);
    }
    gnuplot_terminal = &(init.gnuterm[init.num_gnuplot_terminals]);
    gnuplot_terminal->output_is_file = false;

    char terminal_name[LINE_LEN] = {0};
    if (sscanf(input_string, "%s", terminal_name) < 1) {
        bug_error("ri_gterm1");
    }
    gnuplot_terminal->name = duplicate_string(terminal_name);

    if (strcasecmp(terminal_name, "x11") == 0) {
        init.unixterm = init.num_gnuplot_terminals;
    } else if (strcasecmp(terminal_name, "windows") == 0) {
        init.winterm = init.num_gnuplot_terminals;
    } else if (strcasecmp(terminal_name, "qt") == 0) {
        init.macosterm = init.num_gnuplot_terminals;
    }

    while (fgets(terminal_name, LINE_LEN - 1, fp) != NULL) {
        //!< \todo gnuplot_section_string is a command within the gnuplot
        //section of a kat file.  this variable can be named better than what we
        //have here
        char *gnuplot_section_string;
        gnuplot_section_string = terminal_name + strspn(terminal_name, WHITESPACE); /* skip whitespace */

        if (prepare_line(gnuplot_section_string, 0)) {
            continue;
        }

        if (string_matches(gnuplot_section_string, "end")) {
            ended = 1;
            break;
        }

        if (string_matches(gnuplot_section_string, "suffix")) {
            gnuplot_section_string += string_offset;
            while (*gnuplot_section_string == ' ' ||
                    *gnuplot_section_string == '\t') {
                ++gnuplot_section_string;
            }

            if (*gnuplot_section_string == '\0' ||
                    *gnuplot_section_string == '#' ||
                    *gnuplot_section_string == '%' ||
                    (sscanf(gnuplot_section_string, "%s", command_string) < 1)) {
                gerror("gnuplot suffix expected in '%s'\n", gnuplot_section_string);
            }

            gnuplot_terminal->suffix = duplicate_string(command_string);
            gnuplot_terminal->output_is_file = true;
        } else {
            if (command_length == 0) {
                gnuplot_terminal->command =
                        duplicate_to_longer_string(gnuplot_section_string, 2);
                strcat(gnuplot_terminal->command, "\n");
                command_length = 1;
            } else {
                gnuplot_terminal->command =
                        duplicate_to_longer_string(gnuplot_terminal->command,
                        strlen(gnuplot_section_string) + 2);
                strcat(gnuplot_terminal->command, gnuplot_section_string);
                strcat(gnuplot_terminal->command, "\n");
            }
        }
    }

    if (!ended) {
        warn("init file 'kat.ini': gnuterm: 'END' missing\n");
    }
    ++init.num_gnuplot_terminals;

    // set file even if SUFFIX was not used for the common terminal types
    if (!gnuplot_terminal->output_is_file) {
        if (strcasecmp(gnuplot_terminal->name, "eps") == 0 ||
                strcasecmp(gnuplot_terminal->name, "ceps") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("eps");
        } else if (strcasecmp(gnuplot_terminal->name, "ps") == 0 ||
                strcasecmp(gnuplot_terminal->name, "cps") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("ps");
        } else if (strcasecmp(gnuplot_terminal->name, "gif") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("gif");
        } else if (strcasecmp(gnuplot_terminal->name, "png") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("png");
        } else if (strcasecmp(gnuplot_terminal->name, "pdf") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("pdf");
        } else if (strcasecmp(gnuplot_terminal->name, "fig") == 0) {
            gnuplot_terminal->output_is_file = true;
            gnuplot_terminal->suffix = duplicate_string("fig");
        }
    }
}



//! Read python terminal initialisation

/*!
 * \param input_string input string holding keyword and python terminal name
 * \param string_offset offset in string s (to start scanning after keyword)
 * \param fp the file pointer
 *
 * \todo file suffix setting not fully tested
 */
void read_init_pyterm(char *input_string, int string_offset, FILE *fp) {
    assert(fp != NULL);

    python_terminal_t *python_terminal;

    int ended = 0;
    int command_length = 0;

    char *command_string;
    command_string = input_string;
    if (init.num_python_terminals >= MAX_PYTERM) {
        gerror("too many python terminals\n");
    }

    input_string += string_offset;
    while (*input_string == ' ' || *input_string == '\t') {
        ++input_string;
    }

    if (*input_string == '\0' || *input_string == '#' || *input_string == '%') {
        gerror("python terminal name expected in '%s'\n", command_string);
    }
    python_terminal = &(init.pyterm[init.num_python_terminals]);
    python_terminal->output_is_file = false;

    char terminal_name[LINE_LEN] = {0};
    if (sscanf(input_string, "%s", terminal_name) < 1) {
        bug_error("ri_pterm1");
    }
    python_terminal->name = duplicate_string(terminal_name);

    if (strcasecmp(terminal_name, "screen") == 0) {
        init.screenterm = init.num_python_terminals;
    }

    while (fgets(terminal_name, LINE_LEN - 1, fp) != NULL) {
        char *python_section_string;
        python_section_string = terminal_name + strspn(terminal_name, WHITESPACE); /* skip whitespace */

        if (prepare_line(python_section_string, 0)) {
            continue;
        }

        if (string_matches(python_section_string, "end")) {
            ended = 1;
            break;
        }

        if (string_matches(python_section_string, "suffix")) {
            python_section_string += string_offset;
            while (*python_section_string == ' ' ||
                    *python_section_string == '\t') {
                ++python_section_string;
            }

            if (*python_section_string == '\0' ||
                    *python_section_string == '#' ||
                    *python_section_string == '%' ||
                    (sscanf(python_section_string, "%s", command_string) < 1)) {
                gerror("python suffix expected in '%s'\n", python_section_string);
            }

            python_terminal->suffix = duplicate_string(command_string);
            python_terminal->output_is_file = true;
        } else {
            python_terminal->suffix = duplicate_string("");
            if (command_length == 0) {
                python_terminal->command =
                        duplicate_to_longer_string(python_section_string, 2);
                strcat(python_terminal->command, "\n");
                command_length = 1;
            } else {
                python_terminal->command =
                        duplicate_to_longer_string(python_terminal->command,
                        strlen(python_section_string) + 2);
                strcat(python_terminal->command, python_section_string);
                strcat(python_terminal->command, "\n");
            }
        }
    }

    if (!ended) {
        warn("init file 'kat.ini': pyterm: 'END' missing\n");
    }
    ++init.num_python_terminals;

    // set file even if SUFFIX was not used for the common terminal types
    if (!python_terminal->output_is_file) {
        if (strcasecmp(python_terminal->name, "pdf") == 0) {
            python_terminal->output_is_file = true;
            python_terminal->suffix = duplicate_string("pdf");
        } else if (strcasecmp(python_terminal->name, "svg") == 0) {
            python_terminal->output_is_file = true;
            python_terminal->suffix = duplicate_string("svg");
        } else if (strcasecmp(python_terminal->name, "png") == 0) {
            python_terminal->output_is_file = true;
            python_terminal->suffix = duplicate_string("png");
        }
    }
}



//! Check for comment characters /* and */

/*!
 * \param s the input string to process
 */
void check_for_star(char *s) {
    char *s0;

    s0 = strstr(s, "*/");
    if (s0 != NULL) {
        gerror("line '%s':\n"
                "please use /* and */ only at the beginning of a line!\n", s);
    }
}

//! read GNUPLOT...END part of input file but ignore it

/*! 
 * (used for multiple read of the input file)
 *
 * \param fp the file pointer
 * \param mode ???
 */
void read_init_dummy(FILE *fp, int mode) {
    char s0[LINE_LEN], *s, *s1;
    int ended = 0;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, " \t"); /* skip whitespace */
        if ((s1 = strchr(s, '\n')) != NULL) {
            *s1 = '\0'; /* remove linefeed */
        }

        if (mode) {
            if (string_matches(s, "*/")) {
                ended = 1;

                if (mode == 2 && strlen(s) > 2) {
                    warn("line '%s':\ntext after '*/' ignored\n", s);
                }
                break;

            } else {
                check_for_star(s);
            }
        } else {
            if (string_matches(s, "end")) {
                ended = 1;
                break;
            }
        }

        if (s[0] == '#' || s[0] == '%' || s[0] == '"') {
            continue; /* comment line */
        }

        if (s[0] == '\n' || s[0] == '\0') {
            continue; /* empty line */
        }
    }

    if (feof(fp) && !ended) {
        if (mode) {
            gerror("reached end of file inside a /* */ comment. "
                    "Maybe '*/' not used properly?\n");
        } else {
            gerror("reached end of file inside a GNUPLOT END block\n");
        }
    }
}

//! Read extra gnuplot terminal commands

/*!
 * \param fp the file pointer
 *
 * \todo why isn't this function in kat_read?  it has nothing to do with
 * init; it reads the splot command from the .kat file
 */
void ri_gnuplot(FILE *fp) {
    char s0[LINE_LEN], *s, *s1;
    int ended = 0;
    int comlen = 0;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, " \t"); /* skip whitespace */

        // better put comments into the gnuplot file as well!
        /*
          if (s[0] == '#' || s[0] == '%' || s[0] == '"') {
          continue;              
          }
         */

        if (s[0] == '\n' || s[0] == '\0') {
            continue; /* empty line */
        }

        if ((s1 = strchr(s, '\n')) != NULL) {
            *s1 = '\0'; /* remove linefeed */
        }

        if (string_matches(s, "end")) {
            ended = 1;
            break;
        }
        if (comlen == 0) {
            inter.gnuplotcommand = duplicate_to_longer_string(s, 2);
            strcat(inter.gnuplotcommand, "\n");
            comlen = 1;
        } else {
            inter.gnuplotcommand = duplicate_to_longer_string(inter.gnuplotcommand, strlen(s) + 2);
            strcat(inter.gnuplotcommand, s);
            strcat(inter.gnuplotcommand, "\n");
        }
    }

    if (!ended) {
        warn("GNUPLOT: 'END' missing\n");
    }

    if (comlen) {
        ++inter.num_gnuplot_cmds;
    }
}

//! Read extra python terminal commands

/*!
 * \param fp the file pointer
 *
 */
void ri_python(FILE *fp) {
    char s0[LINE_LEN], *s, *s1;
    int ended = 0;
    int comlen = 0;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, " \t"); /* skip whitespace */

        if (s[0] == '\n' || s[0] == '\0') {
            continue; /* empty line */
        }

        if ((s1 = strchr(s, '\n')) != NULL) {
            *s1 = '\0'; /* remove linefeed */
        }

        if (string_matches(s, "end")) {
            ended = 1;
            break;
        }
        if (comlen == 0) {
            inter.pythoncommand = duplicate_to_longer_string(s, 2);
            strcat(inter.pythoncommand, "\n");
            comlen = 1;
        } else {
            inter.pythoncommand = duplicate_to_longer_string(inter.pythoncommand, strlen(s) + 2);
            strcat(inter.pythoncommand, s);
            strcat(inter.pythoncommand, "\n");
        }
    }

    if (!ended) {
        warn("PYTHON: 'END' missing\n");
    }

    if (comlen) {
        ++inter.num_python_cmds;
    }
}


//! Read matlab plot commands

/*!
 * \param fp the file pointer
 *
 * \todo untested
 */
void ri_matlab(FILE *fp) {
    char s0[LINE_LEN], *s, *s1;
    int ended = 0;
    int comlen = 0;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, " \t"); /* skip whitespace */

        // better put comments into the matlab file as well!
        /*
          if (s[0] == '#' || s[0] == '%' || s[0] == '"') {
          continue;              
          }
         */

        if (s[0] == '\n' || s[0] == '\0') {
            continue; /* empty line */
        }

        if ((s1 = strchr(s, '\n')) != NULL) {
            *s1 = '\0'; /* remove linefeed */
        }

        if (string_matches(s, "end")) {
            ended = 1;
            break;
        }
        if (comlen == 0) {
            inter.matlabcommand = duplicate_to_longer_string(s, 2);
            strcat(inter.matlabcommand, "\n");
            comlen = 1;
        } else {
            inter.matlabcommand = duplicate_to_longer_string(inter.matlabcommand, strlen(s) + 2);
            strcat(inter.matlabcommand, s);
            strcat(inter.matlabcommand, "\n");
        }
    }

    if (!ended) {
        warn("MATLAB: 'END' missing\n");
    }

    if (comlen) {
        ++inter.num_matlab_cmds;
    }
}

//! Read matlab plot commands

/*!
 * \param fp the file pointer
 *
 * \todo untested
 */
void ri_matlabplot(FILE *fp) {
    char s0[LINE_LEN], *s, *s1;
    int ended = 0;
    int comlen = 0;

    // make sure the file pointer isn't null
    assert(fp != NULL);

    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, " \t"); /* skip whitespace */

        // better put comments into the matlab file as well!
        /*
          if (s[0] == '#' || s[0] == '%' || s[0] == '"') {
          continue;              
          }
         */

        if (s[0] == '\n' || s[0] == '\0') {
            continue; /* empty line */
        }

        if ((s1 = strchr(s, '\n')) != NULL) {
            *s1 = '\0'; /* remove linefeed */
        }

        if (string_matches(s, "end")) {
            ended = 1;
            break;
        }
        if (comlen == 0) {
            inter.matlabplotcommand = duplicate_to_longer_string(s, 2);
            strcat(inter.matlabplotcommand, "\n");
            comlen = 1;
        } else {
            inter.matlabplotcommand = duplicate_to_longer_string(inter.matlabplotcommand, strlen(s) + 2);
            strcat(inter.matlabplotcommand, s);
            strcat(inter.matlabplotcommand, "\n");
        }
    }

    if (!ended) {
        warn("MATLABPLOT: 'END' missing\n");
    }

    if (comlen) {
        ++inter.num_matlabplot_cmds;
    }
}


//! Initialise the default values of various quantities

/*!
 *
 */
void init_def(void) {
    init.percentage_step = 5;
    init.x_scale = 1e-9;
    init.lambda = LAMBDA_YAG;
    init.clight = C_LIGHT;
    init.eqsmall = 1e-13;
    init.small = 1e-9;
    init.qeff = 1.0;
    init.amplitude_scaling = ONE;
    init.deriv_h = 1e-3;
    init.n0 = 1.0;
    init.num_gnuplot_terminals = 0;
    init.locksteps = 10000;
    init.locktest1 = 5;
    init.locktest2 = 40;
    init.lockthresholdlow = 0.01;
    init.lockthresholdhigh = 1.5;
    init.gainfactor = 3.0;
    init.autogain = 2;
    init.sequential = 5;
    init.autostop = 1;
    init.gnuversion = 4.2;
    init.pythonversion = 3.5;
    init.maxintop = 400000;
    init.abserr = 1e-6;
    init.relerr = 1e-6;
    init.maxintcuba = 1e6;
    init.default_quantum_output_scale = ASD;

#if __CYGWIN__
    // if cygserver isnt running then we need to default to using
    // a riemann sum as the cuba method won't work
    if (bCygserverRunning) {
        init.mapintmethod = CUBA_CUHRE_PARA;
    } else {
        init.mapintmethod = RIEMANN_SUM_NEW;
    }
#elif INCLUDE_CUBA == 1
    init.mapintmethod = CUBA_CUHRE_PARA;
#else 
    init.mapintmethod = RIEMANN_SUM_NEW;
#endif

    init.mapNCOrder = 8;
    init.mapinterpmethod = LINEAR;
    init.mapinterpsize = 3;
    init.calc_knm_transpose = 1;
    init.cuba_numprocs = getNumCores();
}

//! Dump to terminal of relevant initialisation variables

/*!
 *
 * \todo untested
 * \todo why isn't this routine in kat_dump.c???
 */
void init_dump(void) {
    message("init.pstep=%d\n", init.percentage_step);
    message("init.lambda=%g\n", init.lambda);
    message("init.eqsmall=%g\n", init.eqsmall);
    message("init.small=%g\n", init.small);
    message("init.qeff=%g\n", init.qeff);
    message("init.deriv_h=%g\n", init.deriv_h);
    message("init.maxintop=%d\n", init.maxintop);
    message("init.abserr=%g\n", init.abserr);
    message("init.relerr=%g\n", init.relerr);
    message("init.maxintcuba=%d\n", init.maxintcuba);
    message("init.mapintmethod=%s\n", init.mapintmethod);
    message("init.gnucommand='%s'\n", init.gnucommand);
    message("%d gnuterms:\n", init.num_gnuplot_terminals);

    int gnuterm_index;
    for (gnuterm_index = 0; gnuterm_index < init.num_gnuplot_terminals; gnuterm_index++) {
        message("#%d: name=%s file=%d suffix='%s' commands:\n%s",
                gnuterm_index, init.gnuterm[gnuterm_index].name, init.gnuterm[gnuterm_index].output_is_file, init.gnuterm[gnuterm_index].suffix,
                init.gnuterm[gnuterm_index].command);
    }

    message("%d pyterms:\n", init.num_python_terminals);

    int pyterm_index;
    for (pyterm_index = 0; pyterm_index < init.num_python_terminals; pyterm_index++) {
        message("#%d: name=%s file=%d suffix='%s' commands:\n%s",
                pyterm_index, init.pyterm[pyterm_index].name, init.pyterm[pyterm_index].output_is_file, init.pyterm[pyterm_index].suffix,
                init.pyterm[pyterm_index].command);
    }
}

//! Initialise k-matrix

/*!
 *
 */
void init_ks(void) {
    int i, j, k;
    for (i = 0; i < inter.num_mirrors; i++) {
        mirror_knm_matrix_ident(&(inter.mirror_list[i].knm));
    }

    for (i = 0; i < inter.num_beamsplitters; i++) {
        bs_knm_matrix_ident(&(inter.bs_list[i].knm));
    }

    for (i = 0; i < inter.num_spaces; i++) {
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (j == k) {
                    inter.space_list[i].k12[j][k] = complex_1;
                    inter.space_list[i].k21[j][k] = complex_1;
                } else {
                    inter.space_list[i].k12[j][k] = complex_0;
                    inter.space_list[i].k21[j][k] = complex_0;
                }
            }
        }
    }

    for (i = 0; i < inter.num_lenses; i++) {
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (j == k) {
                    inter.lens_list[i].k12[j][k] = complex_1;
                    inter.lens_list[i].k21[j][k] = complex_1;
                } else {
                    inter.lens_list[i].k12[j][k] = complex_0;
                    inter.lens_list[i].k21[j][k] = complex_0;
                }
            }
        }
    }

    for (i = 0; i < inter.num_modulators; i++) {
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (j == k) {
                    inter.modulator_list[i].k12[j][k] = complex_1;
                    inter.modulator_list[i].k21[j][k] = complex_1;
                } else {
                    inter.modulator_list[i].k12[j][k] = complex_0;
                    inter.modulator_list[i].k21[j][k] = complex_0;
                }
            }
        }
    }

    for (i = 0; i < inter.num_diodes; i++) {
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (j == k) {
                    inter.diode_list[i].k12[j][k] = complex_1;
                    inter.diode_list[i].k21[j][k] = complex_1;
                    inter.diode_list[i].k23[j][k] = complex_1;
                    inter.diode_list[i].k32[j][k] = complex_1;
                } else {
                    inter.diode_list[i].k12[j][k] = complex_0;
                    inter.diode_list[i].k21[j][k] = complex_0;
                    inter.diode_list[i].k23[j][k] = complex_0;
                    inter.diode_list[i].k32[j][k] = complex_0;
                }
            }
        }
    }
    
    for (i = 0; i < inter.num_dbss; i++) {
        for (j = 0; j < inter.num_fields; j++) {
            for (k = 0; k < inter.num_fields; k++) {
                if (j == k) {
                    inter.dbs_list[i].k13[j][k] = complex_1;
                    inter.dbs_list[i].k21[j][k] = complex_1;
                    inter.dbs_list[i].k34[j][k] = complex_1;
                    inter.dbs_list[i].k42[j][k] = complex_1;
                } else {
                    inter.dbs_list[i].k13[j][k] = complex_0;
                    inter.dbs_list[i].k21[j][k] = complex_0;
                    inter.dbs_list[i].k34[j][k] = complex_0;
                    inter.dbs_list[i].k42[j][k] = complex_0;
                }
            }
        }
    }
}


//! Retrace the system

/*!
 *
 */
void retrace(void) {
    int component;
    double nr1, nr2;
    int node_num, node_indices[4] = {0};
    int node1_index, node2_index;
    complex_t qxt, qyt, qx1, qy1, qx2, qy2;

    //080205
    //set_ABCDs();

    //unset all nodes
    int i;
    for (i = 0; i < inter.num_nodes; i++) {
        inter.node_list[i].q_is_set = false;
    }

    // trace cavities and set cavity q-parameter to cavity nodes
    if (inter.num_cavities) {
        compute_cavity_params();
    }

    // setting user defined q paramters
    if (inter.num_gauss_cmds) {
        for (i = 0; i < inter.num_gauss_cmds; i++) {
            gauss_t gauss = inter.gauss_list[i];
            set_q(gauss.node_index, gauss.component_index,
                    gauss.qx, gauss.qy);
        }
    }


    // now start tracing beam at previously used starting node 
    // just checking that a q has been set for it

    if (inter.node_list[inter.startnode].q_is_set) {
        trace_beam(inter.startnode);
    } else {
        if (inter.retrace != 2)
            gerror("Retracing: no beam parameter set at user defined startnode '%s'\n", inter.node_list[inter.startnode].name);
    }

    // set gouy phases for spaces
    for (i = 0; i < inter.num_spaces; i++) {
        component = get_overall_component_index(SPACE, i);
        which_nodes(component, GND_NODE, &node_num, node_indices);

        if (inter.node_list[node_indices[0]].traced_from_component_index == component) {
            node1_index = node_indices[1];
            node2_index = node_indices[0];
        } else {
            node1_index = node_indices[0];
            node2_index = node_indices[1];
        }

        nr1 = *inter.node_list[node1_index].n;
        nr2 = *inter.node_list[node2_index].n;

        if (inter.node_list[node1_index].traced_from_component_index == component) {
            qx1 = cminus(cconj(inter.node_list[node1_index].qx));
            qy1 = cminus(cconj(inter.node_list[node1_index].qy));
        } else {
            qx1 = inter.node_list[node1_index].qx;
            qy1 = inter.node_list[node1_index].qy;
        }

        qxt = q1_q2(inter.space_list[i].qq, qx1, nr1, nr2);
        qyt = q1_q2(inter.space_list[i].qq, qy1, nr1, nr2);

        if (inter.node_list[node2_index].traced_from_component_index == component) {
            qx2 = inter.node_list[node2_index].qx;
            qy2 = inter.node_list[node2_index].qy;
        } else {
            qx2 = cminus(cconj(inter.node_list[node2_index].qx));
            qy2 = cminus(cconj(inter.node_list[node2_index].qy));
        }

        if (!ceq(qxt, qx2) || (!ceq(qyt, qy2))) {
            warn("q mismatch at space %s\n", get_component_name(component));
        }

        if ((inter.space_list[i].attribs & GOUYX) == 0) {
            inter.space_list[i].gouy_x = DEG * fabs(gouy(qx2) - gouy(qx1));
        }

        if ((inter.space_list[i].attribs & GOUYY) == 0) {
            inter.space_list[i].gouy_y = DEG * fabs(gouy(qy2) - gouy(qy1));
        }
    }
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

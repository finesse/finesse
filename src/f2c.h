/* f2c.h  --  Standard Fortran to C header file */

/**  barf  [ba:rf]  2.  "He suggested using FORTRAN, and everybody barfed."

	- From The Shogakukan DICTIONARY OF NEW ENGLISH (Second edition) */

#ifndef F2C_INCLUDE
#define F2C_INCLUDE

typedef long int integer;
typedef unsigned long int uinteger;
typedef char *address;
typedef short int shortint;
typedef float real;
typedef double doublereal;
typedef struct { real r;  //!< \todo needs to be documented
                 real i;  //!< \todo needs to be documented
} complex;
typedef struct { doublereal r;  //!< \todo needs to be documented
                 doublereal i;  //!< \todo needs to be documented
} doublecomplex;
typedef long int logical;
typedef short int shortlogical;
typedef char logical1;
typedef char integer1;
#ifdef INTEGER_STAR_8	/* Adjust for integer*8. */
typedef long long longint;		/* system-dependent */
typedef unsigned long long ulongint;	/* system-dependent */
#define qbit_clear(a,b)	((a) & ~((ulongint)1 << (b)))
#define qbit_set(a,b)	((a) |  ((ulongint)1 << (b)))
#endif

#define TRUE_ (1)
#define FALSE_ (0)

/* Extern is for use with -E */
#ifndef Extern
#define Extern extern
#endif

/* I/O stuff */

#ifdef f2c_i2
/* for -i2 */
typedef short flag;
typedef short ftnlen;
typedef short ftnint;
#else
typedef long int flag;
typedef long int ftnlen;
typedef long int ftnint;
#endif

/*external read, write*/
typedef struct
{	flag cierr;  //!< \todo needs to be documented
	ftnint ciunit;  //!< \todo needs to be documented
	flag ciend;  //!< \todo needs to be documented
	char *cifmt;  //!< \todo needs to be documented
	ftnint cirec;  //!< \todo needs to be documented
} cilist;

/*internal read, write*/
typedef struct
{	flag icierr;  //!< \todo needs to be documented
	char *iciunit;  //!< \todo needs to be documented
	flag iciend;  //!< \todo needs to be documented
	char *icifmt;  //!< \todo needs to be documented
	ftnint icirlen;  //!< \todo needs to be documented
	ftnint icirnum;  //!< \todo needs to be documented
} icilist;

/*open*/
typedef struct
{	flag oerr;  //!< \todo needs to be documented
	ftnint ounit;  //!< \todo needs to be documented
	char *ofnm;  //!< \todo needs to be documented
	ftnlen ofnmlen;  //!< \todo needs to be documented
	char *osta;  //!< \todo needs to be documented
	char *oacc;  //!< \todo needs to be documented
	char *ofm;  //!< \todo needs to be documented
	ftnint orl;  //!< \todo needs to be documented
	char *oblnk;  //!< \todo needs to be documented
} olist;

/*close*/
typedef struct
{	flag cerr;  //!< \todo needs to be documented
	ftnint cunit;  //!< \todo needs to be documented
	char *csta;  //!< \todo needs to be documented
} cllist;

/*rewind, backspace, endfile*/
typedef struct
{	flag aerr;  //!< \todo needs to be documented
	ftnint aunit;  //!< \todo needs to be documented
} alist;

/* inquire */
typedef struct
{	flag inerr;  //!< \todo needs to be documented
	ftnint inunit;  //!< \todo needs to be documented
	char *infile;  //!< \todo needs to be documented
	ftnlen infilen;  //!< \todo needs to be documented
	ftnint	*inex;	/*!< parameters in standard's order*/
	ftnint	*inopen;  //!< \todo needs to be documented
	ftnint	*innum;  //!< \todo needs to be documented
	ftnint	*innamed;  //!< \todo needs to be documented
	char	*inname;  //!< \todo needs to be documented
	ftnlen	innamlen;  //!< \todo needs to be documented
	char	*inacc;  //!< \todo needs to be documented
	ftnlen	inacclen;  //!< \todo needs to be documented
	char	*inseq;  //!< \todo needs to be documented
	ftnlen	inseqlen;  //!< \todo needs to be documented
	char 	*indir;  //!< \todo needs to be documented
	ftnlen	indirlen;  //!< \todo needs to be documented
	char	*infmt;  //!< \todo needs to be documented
	ftnlen	infmtlen;  //!< \todo needs to be documented
	char	*inform;  //!< \todo needs to be documented
	ftnint	informlen;  //!< \todo needs to be documented
	char	*inunf;  //!< \todo needs to be documented
	ftnlen	inunflen;  //!< \todo needs to be documented
	ftnint	*inrecl;  //!< \todo needs to be documented
	ftnint	*innrec;  //!< \todo needs to be documented
	char	*inblank;  //!< \todo needs to be documented
	ftnlen	inblanklen;  //!< \todo needs to be documented
} inlist;

#define VOID void

union Multitype {	/* for multiple entry points */
	integer1 g;  //!< \todo needs to be documented
	shortint h;  //!< \todo needs to be documented
	integer i;  //!< \todo needs to be documented
	/* longint j; */
	real r;  //!< \todo needs to be documented
	doublereal d;  //!< \todo needs to be documented
	complex c;  //!< \todo needs to be documented
	doublecomplex z;  //!< \todo needs to be documented
	};

typedef union Multitype Multitype;

/*typedef long int Long;*/	/* No longer used; formerly in Namelist */

struct Vardesc {	/* for Namelist */
	char *name;  //!< \todo needs to be documented
	char *addr;  //!< \todo needs to be documented
	ftnlen *dims;  //!< \todo needs to be documented
	int  type;  //!< \todo needs to be documented
	};
typedef struct Vardesc Vardesc;

struct Namelist {
	char *name;  //!< \todo needs to be documented
	Vardesc **vars;  //!< \todo needs to be documented
	int nvars;  //!< \todo needs to be documented
	};
typedef struct Namelist Namelist;

#define abs(x) ((x) >= 0 ? (x) : -(x))
#define dabs(x) (doublereal)abs(x)
#define min(a,b) ((a) <= (b) ? (a) : (b))
#define max(a,b) ((a) >= (b) ? (a) : (b))
#define dmin(a,b) (doublereal)min(a,b)
#define dmax(a,b) (doublereal)max(a,b)
#define bit_test(a,b)	((a) >> (b) & 1)
#define bit_clear(a,b)	((a) & ~((uinteger)1 << (b)))
#define bit_set(a,b)	((a) |  ((uinteger)1 << (b)))

/* procedure parameter types for -A and -C++ */

#define F2C_proc_par_types 1
#ifdef __cplusplus
typedef int /* Unknown procedure type */ (*U_fp)(...);
typedef shortint (*J_fp)(...);
typedef integer (*I_fp)(...);
typedef real (*R_fp)(...);
typedef doublereal (*D_fp)(...), (*E_fp)(...);
typedef /* Complex */ VOID (*C_fp)(...);
typedef /* Double Complex */ VOID (*Z_fp)(...);
typedef logical (*L_fp)(...);
typedef shortlogical (*K_fp)(...);
typedef /* Character */ VOID (*H_fp)(...);
typedef /* Subroutine */ int (*S_fp)(...);
#else
typedef int /* Unknown procedure type */ (*U_fp)();
typedef shortint (*J_fp)();
typedef integer (*I_fp)();
typedef real (*R_fp)();
typedef doublereal (*D_fp)(), (*E_fp)();
typedef /* Complex */ VOID (*C_fp)();
typedef /* Double Complex */ VOID (*Z_fp)();
typedef logical (*L_fp)();
typedef shortlogical (*K_fp)();
typedef /* Character */ VOID (*H_fp)();
typedef /* Subroutine */ int (*S_fp)();
#endif
/* E_fp is for real functions when -R is not specified */
typedef VOID C_f;	/* complex function */
typedef VOID H_f;	/* character function */
typedef VOID Z_f;	/* double complex function */
typedef doublereal E_f;	/* real function with -R not specified */

/* undef any lower-case symbols that your C compiler predefines, e.g.: */

#ifndef Skip_f2c_Undefs
#undef cray
#undef gcos
#undef mc68010
#undef mc68020
#undef mips
#undef pdp11
#undef sgi
#undef sparc
#undef sun
#undef sun2
#undef sun3
#undef sun4
#undef u370
#undef u3b
#undef u3b2
#undef u3b5
#undef unix
#undef vax
#endif
#endif

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

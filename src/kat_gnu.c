// $Id$

/*!
 * \file kat_gnu.c
 * \brief Routines for organising and writing the output gnuplot file
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include <libgen.h>
#include "kat.h"
#include "kat_gnu.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_aux.h"

// test
extern interferometer_t inter;
extern init_variables_t init;
extern time_t now;
extern char *mydate;

//! my favourite colour schemes for gnuplot
/*!
 *
 */
static const int lines[3][10] = {
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
    {0, 3, 7, 4, 1, 2, 5, 6, 8, 10},
    {0, 7, 3, 8, 4, 1, 2, 5, 9, 10}
};

//! Convert an output type number into its respective name

/*!
 * \param i type reference
 *
 * \return the output type as a string
 */
char *otypename(int i) {
    switch (i) {
        case OTYPE_RE:
            return "Re";
            break;
        case OTYPE_IM:
            return "Im";
            break;
        case OTYPE_ABS:
            return "Abs";
            break;
        case OTYPE_DEG:
        case OTYPE_DEGP:
        case OTYPE_DEGM:
            return "Phase";
            break;
        case OTYPE_DB:
            return "dB";
            break;
        default:
            bug_error("incorrect output type");
            break;
    }
    return ("error");
}

//! Sets up and writes the gnuplot file

/*!
 * \param step ???
 * \param fp the file pointer
 * \param fname the file name
 */
void gnufile(int step, FILE *fp, char *fname, bool test_script) {
    char xy1, xy2;
    char mytime[30] = {0};
    int col;
    int found;
    char plotname[PLOTFILE_LEN] = {0};
    char beamsize[PLOTFILE_LEN] = {0};

    //  char ylist[PLOTFILE_LEN] = { 0 };
    //  ctemp[10] = { 0 };
    int lt;
    char title[PLOTFILE_LEN] = {0};
    int first;

    gnuplot_terminal_t *gp;

    // make sure file pointer isn't null
    assert(fp != NULL);

    // make sure step is greater than or equal to zero
    //!< \todo verify that step should be greater than or equal to zero
    assert(step >= 0);

    int i;
    double y1max=-1e40, y1min=1e40; 
    double y2max=-1e40, y2min=1e40; 
    for (i = 0; i < inter.num_outputs; i++) {
      if (!inter.output_data_list[i].noplot) {
        output_data_t current_output = inter.output_data_list[i];
        if (y1max<current_output.Omax[0])
          y1max=current_output.Omax[0];
        if (y2max<current_output.Omax[1])
          y2max=current_output.Omax[1];
        if (y1min>current_output.Omin[0])
          y1min=current_output.Omin[0];
        if (y2min>current_output.Omin[1])
          y2min=current_output.Omin[1];
      }
    }

    if (inter.beam.set && inter.beam.swap) {
        xy1 = 'y';
        xy2 = 'x';
    } else {
        xy1 = 'x';
        xy2 = 'y';
    }

    fputs("reset\n", fp);
    
    if (inter.x1.xtype == FLOG) {
        fprintf(fp, "set %.1srange[%g:%g]\n", &xy1, inter.x1.xmin / inter.x1.op,
                inter.x1.xmax / inter.x1.op);
    } else {
        fprintf(fp, "set %.1srange[%g:%g]\n", &xy1, inter.x1.xmin - inter.x1.op,
                inter.x1.xmax - inter.x1.op);
    }

    if (inter.x1.xtype == FLOG && inter.x1.xmax / inter.x1.xmin >= 10.0) {
        fprintf(fp, "set logscale %.1s\n", &xy1);
    }

    fprintf(fp, "set %.1slabel \"%s", &xy1, inter.x1.xname);
    fprintf(fp, "\"\n");

    if (inter.splot) {
        if (inter.x2.xtype == FLOG) {
            fprintf(fp, "set %.1srange[%g:%g]\n", &xy2,
                    inter.x2.xmin / inter.x2.op, inter.x2.xmax / inter.x2.op);
        } else {
            fprintf(fp, "set %.1srange[%g:%g]\n", &xy2,
                    inter.x2.xmin - inter.x2.op, inter.x2.xmax - inter.x2.op);
        }

        if (inter.x2.xtype == FLOG && inter.x2.xmax / inter.x2.xmin >= 10.0) {
            fprintf(fp, "set logscale %.1s\n", &xy2);
        }

        fprintf(fp, "set %.1slabel \"%s", &xy2, inter.x2.xname);
        fprintf(fp, "\"\n");

        fprintf(fp, "set zlabel \"%s\"\n", inter.y1name);
        fputs("set mztics 10\n", fp);
    } else {
        fprintf(fp, "set %.1slabel \"%s\"\n", &xy2, inter.y1name);
    }

    // does this work with axis switching ??
    if (inter.yaxis[1]) {
        fprintf(fp, "set y2label \"%s\"\n", inter.y2name);
        fputs("set y2tics nomirror\n", fp);
        fputs("set my2tics 3\n", fp);
        fputs("set ytics nomirror\n", fp);
    }

    fputs("set mxtics 2\n", fp);
    fputs("set mytics 2\n", fp);
    fputs("set zero 0.0\n", fp);
    //fputs("set offset graph 0, graph 0, graph 0.05, graph 0.05\n", fp);

    strcpy(mytime, asctime(localtime(&now)));
    mytime[strlen(mytime) - 1] = '\0';

    if (init.gnuversion > 4.0) {
        fprintf(fp, "set title \"%s                %s\" offset 0, 2\n",
                inter.basename, mytime);
    } else {
        fprintf(fp, "set title \"%s                %s\", 2\n", inter.basename,
                mytime);
    }

    if (inter.x3.xtype != NO_FREQ) {
        if (inter.x3.xtype == FLOG) {
            fprintf(fp, "set label \"%s = %s\" at screen .1, 0.9 \n",
                    inter.x3.xname, double_form(*inter.x3.xaxis / inter.x3.op));
        } else {
            fprintf(fp, "set label \"%s = %s\" at screen .1, 0.9 \n",
                    inter.x3.xname, double_form(*inter.x3.xaxis - inter.x3.op));
        }

        fprintf(fp, "set label \"(%d/%d)\" at screen .8, 0.9 \n", step,
                inter.x3.xsteps);
    }


    // print the beam size for the first beam detector
    /* put into label and kat_init
    if (inter.beam.set) {
      detno = -1;
      int i = 0;

      while (i < inter.num_outputs) {
        if (inter.output_data_list[i].detector_type == BEAM) {
          detno = inter.output_data_list[i].detector_index;
          i = inter.num_outputs;
        }
        i++;
      }
      if (detno == -1) {
        bug_error("detno -1");
      }
      nr = *inter.node_list[inter.output_data_list[detno].node_index].n;
      fprintf(fp, "set label \"wx0 = %sm  wy0 = %sm\" at screen .2, 0.86 \n",
              double_form(w0_size(*inter.output_data_list[detno].qx, nr)),
              double_form(w0_size(*inter.output_data_list[detno].qy, nr)));
      fprintf(fp, "set label \"wx  = %sm  wy  = %sm\" at screen .5, 0.86 \n",
              double_form(w_size(*inter.output_data_list[detno].qx, nr)),
              double_form(w_size(*inter.output_data_list[detno].qy, nr)));
    }
     */


    if (inter.ytype == FLOG) {
        if (inter.splot) {
            fputs("set log z\n", fp);
            fputs("set format z \"%g\"\n",fp);
        } else {
            fputs("set log y\n", fp);
            fputs("set format y \"%g\"\n",fp);

        }

        if (inter.yaxis[1] == OTYPE_IM) {
            fputs("set log y2\n", fp);
            fputs("set format y2 \"%g\"\n",fp);
        }
    } else {
        if (inter.splot) {
            fputs("set nolog z\n", fp);
            
        } else {
            fputs("set nolog y\n", fp);
            if (fabs(y1max-y1min)<1e-2){
              fputs("set format y \"%g\"\n",fp);
            }
        }

        if (inter.yaxis[1]) {
            fputs("set nolog y2\n", fp);
            if (fabs(y2max-y2min)<1e-2){
              fputs("set format y2 \"%g\"\n",fp);
            }
        }
    }

    /* set first term */
    //assert(inter.num_gnuterm_cmds > 0);
    gp = &(init.gnuterm[inter.gnuterm[0]]);
    fputs(gp->command, fp);
    if (gp->output_is_file) {
        fprintf(fp, "set output \"%s\"\n", inter.gnutermfn[0]);
    }

    if (strcasecmp(gp->name, "dumb") == 0) {
        fputs("pause -1 \"Hit return to show graph\"\n", fp);
    }

    // 3D plots
    if (inter.splot) {
        
        fprintf(fp,"set pm3d\n");
        fprintf(fp,"set pm3d map\n");
        fprintf(fp,"set pm3d interpolate 2,2\n");
        fprintf(fp,"set hidden3d\n");
        
        if (NOT inter.multi) {
            // fprintf(fp, "set zrange[%g:%g]\n", 99/100.0*inter.total_min, 101/100.0*inter.total_max);
            int i = 0;
            found = 0;

            while (found == 0 && i < inter.num_outputs) {
                if (!inter.output_data_list[i].noplot) {
                    found = 1;
                } else {
                    i++;
                }
            }

            if (found == 0) {
                gerror("no output (without 'noplot') has been found\n");
            }

            strcpy(plotname, inter.output_data_list[i].name);
            if (inter.output_data_list[i].is_second_beam) {
                strcat(plotname, "*");
            }

            output_data_t current_output = inter.output_data_list[i];
            if (current_output.detector_type == BEAM) {
                double nr = *inter.node_list[current_output.node_index].n;
                sprintf(beamsize, " (wx0=%s, wy0=%s)",
                        double_form(w0_size(*current_output.qx, nr)),
                        double_form(w0_size(*current_output.qy, nr)));
                strcat(plotname, beamsize);
            }

            if(init.gnuversion >= 5.0) {
                fputs("set colors classic\n", fp);
                fputs("set termoption noenhanced\n", fp);
            }
            
            if (inter.num_gnuplot_cmds) {
                fputs(inter.gnuplotcommand, fp);
            }
    
            strcat(plotname, " : ");
            strcpy(title, plotname);
            if(test_script) fputs("set term unknown\n", fp);
            fputs("splot\\\n", fp);

            if (inter.beam.set && inter.beam.swap) {
                fprintf(fp, "\'%s\' using ($2):($1):($%d) title \"%s \"  w l\n",
                        fname, 3 + i, title);
            } else {
                fprintf(fp, "\'%s\' using ($1):($2):($%d) title \"%s \"  w l\n",
                        fname, 3 + i, title);
            }
        }            // ... but newer gnuplot versions allow to plot multiple surfaces at once
        else {
            if(test_script) fputs("set term unknown\n", fp);
            
            if(init.gnuversion >= 5.0) {
                fputs("set colors classic\n", fp);
                fputs("set termoption noenhanced\n", fp);
            }
            
            if (inter.num_gnuplot_cmds) {
                fputs(inter.gnuplotcommand, fp);
            }
    
            fputs("splot", fp);
            first = 1;
            col = 3;

            int i;
            for (i = 0; i < inter.num_outputs; i++) {
                strcpy(plotname, "");

                if (inter.num_outputs > 1) {
                    strcat(plotname, inter.output_data_list[i].name);
                }

                if (inter.output_data_list[i].detector_type != FEEDBACK
                        && inter.output_data_list[i].detector_type != UFUNCTION) {
                    strcat(plotname, " ");

                    // check node indices before passing to node_print
                    assert(inter.output_data_list[i].node_index >= 0);
                    assert(inter.output_data_list[i].node_index < inter.num_nodes);

                    strcat(plotname, node_print(inter.output_data_list[i].node_index));

                    if (inter.output_data_list[i].is_second_beam) {
                        strcat(plotname, "*");
                    }
                }

                output_data_t current_output = inter.output_data_list[i];
                if (current_output.detector_type == BEAM) {
                    double nr = *inter.node_list[current_output.node_index].n;
                    sprintf(beamsize, " (wx0=%s, wy0=%s)",
                            double_form(w0_size(*current_output.qx, nr)),
                            double_form(w0_size(*current_output.qy, nr)));
                    strcat(plotname, beamsize);
                }

                strcat(plotname, " : ");
                strcpy(title, plotname);

                if (!inter.output_data_list[i].noplot) {
                    if (!first) {
                        fprintf(fp, ", ");
                    } else {
                        first = 0;
                    }

                    if (inter.beam.set && inter.beam.swap) {
                        fprintf(fp,
                                "\\\n\'%s\' using ($2):($1):($%d) title \"%s \"  w l ",
                                fname, col++, title);
                    } else {
                        fprintf(fp,
                                "\\\n\'%s\' using ($1):($2):($%d) title \"%s \"  w l",
                                fname, col++, title);
                    }

                    if (inter.yaxis[1]) {

                        if (inter.ploty2axis) {
                            strcpy(title, plotname);
                            strcat(title, inter.y2name);
                            fprintf(fp, ", ");

                            if (inter.beam.set && inter.beam.swap) {
                                fprintf(fp,
                                        "\\\n\'%s\' using ($2):($1):($%d) title \"%s \"  w l ",
                                        fname, col++, title);
                            } else {
                                fprintf(fp,
                                        "\\\n\'%s\' using ($1):($2):($%d) title \"%s \"  w l",
                                        fname, col++, title);
                            }
                        } else {
                            col++;
                        }
                    }
                } else {
                    col++;
                }
            }
            fprintf(fp, "\n");
        }
    } else {
        // ordinary 2D plots
        if(test_script) fputs("set term unknown\n", fp);
        
        if(init.gnuversion >= 5.0) {
            fputs("set colors classic\n", fp);
            fputs("set termoption noenhanced\n", fp);
        }
        
        if (inter.num_gnuplot_cmds) {
            fputs(inter.gnuplotcommand, fp);
        }
        
        fputs("plot", fp);
        first = 1;
        col = 2;

        lt = 1;

        int i;
        for (i = 0; i < inter.num_outputs; i++) {
            output_data_t current_output = inter.output_data_list[i];
            strcpy(plotname, "");

            if (inter.num_outputs > 1) {
                strcat(plotname, current_output.name);
            }

            if (current_output.detector_type != FEEDBACK &&
                    current_output.detector_type != UFUNCTION) {
                strcat(plotname, " ");

                // if the output beam param is not beam_param_not_set, 
                // append to the plotname
                if (NOT current_output.beam_param_not_set && current_output.node_index > 0) {
                    // check node indices before passing to node_print()
                    assert(current_output.node_index >= 0);
                    assert(current_output.node_index < inter.num_nodes);

                    strcat(plotname, node_print(current_output.node_index));
                }

                // mark the beam as the "other beam" if necessary
                if (current_output.is_second_beam) {
                    strcat(plotname, "*");
                }
            }

            if (current_output.detector_type == BEAM) {
                double nr = *inter.node_list[current_output.node_index].n;
                sprintf(beamsize, " (wx0=%s, wy0=%s)",
                        double_form(w0_size(*current_output.qx, nr)),
                        double_form(w0_size(*current_output.qy, nr)));
                strcat(plotname, beamsize);
            }

            strcat(plotname, " : ");
            strcpy(title, plotname);

            if (inter.yaxis[1]) {
                strcat(title, inter.y1name);
            }

            if (!inter.output_data_list[i].noplot) {
                if (!first) {
                    fprintf(fp, ", ");
                } else {
                    first = 0;
                }

                fprintf(fp, "\\\n\'%s\' using ($1):($%d) axes x1y%d "
                        "title \"%s \" w l lt %d lw %d",
                        fname, col++, 1, title,
                        (lt++ > 10 ? lt - 1 : lines[inter.mycolor][lt - 1]),
                        inter.width);
            } else {
                col++;
            }

            if (inter.yaxis[1] && inter.ploty2axis) {

                if (!inter.output_data_list[i].noplot) {
                    strcpy(title, plotname);
                    strcat(title, inter.y2name);
                    fprintf(fp, ", \\\n\'%s\' using ($1):($%d) axes x1y%d "
                            "title \"%s \" w l lt %d lw %d",
                            fname, col++, 2, title,
                            (lt++ > 10 ? lt - 1 : lines[inter.mycolor][lt - 1]),
                            inter.width);
                } else {
                    col++;
                }
            }
        }
        fprintf(fp, "\n");
    }
    
    if ((inter.pause == 1)
            && !gp->output_is_file) {
        fputs("pause -1 \"Hit return to continue\"\n", fp);
    }

    /* set more terminals */
    int k;
    for (k = 1; k < inter.num_gnuterm_cmds; k++) {
        gp = &(init.gnuterm[inter.gnuterm[k]]);
        fputs(gp->command, fp);
        if (gp->output_is_file) {
            fprintf(fp, "set output \"%s\"\n", inter.gnutermfn[k]);
        }
        if(test_script) fputs("set term unknown\n", fp);
        fputs("replot\n", fp);

        if (inter.debug & 256) {
            message("GNU: gnuc:%s\n", init.gnucommand);
            message("GNU: term:%s\n", gp->name);
        }

        if ((inter.pause == 1)
                && !gp->output_is_file) {
            fputs("pause -1 \"Hit return to continue\"\n", fp);
        }
    }
}


//! Sets up and writes the python file for plotting with matplotlib
/*!
 * 
 * \param fp the file pointer
 * \param fname the file name
 */
void pythonfile(FILE *fp, char *fname) {
   char xy1[3], xy2[3], XY1[3], XY2[3];
    char mytime[30] = {0};

    //  char ylist[PLOTFILE_LEN] = { 0 };
    //  ctemp[10] = { 0 };
 
    // make sure file pointer isn't null
  assert(fp != NULL);

    // make sure step is greater than or equal to zero
    //!< \todo verify that step should be greater than or equal to zero
    //assert(step >= 0);


    // swap axes if this is set
  if (inter.beam.set && inter.beam.swap) {
      strcpy(XY1, "Y");
      strcpy(XY2, "X");
      strcpy(xy1, "y");
      strcpy(xy2, "x");
  } else {
      strcpy(XY1, "X");
      strcpy(XY2, "Y");
      strcpy(xy1, "x");
      strcpy(xy2, "y");
  }

  strcpy(mytime, asctime(localtime(&now)));
  mytime[strlen(mytime) - 1] = '\0';
 
  
  // start writing content
  fputs("#!/usr/bin/env python\n", fp);
  
  // write comment block
  print_python_usage(fp,inter.basename);
  
  fprintf(fp,"\n__author__ = \"Finesse, http://www.gwoptics.org/finesse\"\n\n");
  
  fprintf(fp,"import numpy as np\n");
  fprintf(fp,"import matplotlib\n");
  fprintf(fp,"BACKEND = 'Qt4Agg'\n");
  fprintf(fp,"matplotlib.use(BACKEND)\n");
  fprintf(fp,"from matplotlib import rc\n");
  
  if (inter.splot) {
      // 3D plot
      fprintf(fp,"import matplotlib.backends.backend_pdf\n");
      fprintf(fp,"import matplotlib.pyplot as plt\n");
      fprintf(fp,"from mpl_toolkits.mplot3d import Axes3D\n");
      fprintf(fp,"from matplotlib import cm\n");
      fprintf(fp,"from matplotlib.colors import LogNorm\n");

      fprintf(fp,"\n");
      print_python_3Dconvert(fp);
  }
  else{
      // 2D plot
      fprintf(fp,"import matplotlib.pyplot as plt\n");      
  }
  //fprintf(fp,"formatter = matplotlib.ticker.EngFormatter(unit='', places=0)\n");
  fprintf(fp,"formatter = matplotlib.ticker.EngFormatter(unit='')\n");
  fprintf(fp,"formatter.ENG_PREFIXES[-6] = 'u'\n"); // overwrite greek letter mu
		
  bool pdf_output=false;
  int k;
  for (k = 0; k < inter.num_pyterm_cmds; k++) {
      if (init.pyterm[inter.pyterm[k]].output_is_file) {
          if (strcasecmp(init.pyterm[inter.pyterm[k]].suffix,"pdf") == 0){
              pdf_output=true;
          }
      }
  }
  if (pdf_output){
      print_python_pdf(fp,inter.basename);
  }
  
  fprintf(fp,"def run(noplot=None):\n");
  fprintf(fp,"\tdata = np.loadtxt('%s',comments='%%')\n", fname);
  fprintf(fp,"\trows,cols=data.shape\n"); 
  
  if (inter.splot) {
      fprintf(fp,"\t%.2s=data[:,0]\n",XY1);
      fprintf(fp,"\t%.2s=data[:,1]\n",XY2);
      fprintf(fp,"\tZ=data[:,2:cols]\n");
      fprintf(fp,"\tx,y,z=convert3D(X,Y,Z)\n");
  }
  else{
      fprintf(fp,"\tx=data[:,0]\n");
      fprintf(fp,"\ty=data[:,1:cols]\n");
  }
  
  fprintf(fp, "\tmytitle=\'%s                %s\'\n", inter.basename,mytime);

  // decide whether to plot second yaxis in extra plot or not
  /*
  int subplot;
  subplot = 0;
  if (inter.yaxis[1] && inter.ploty2axis) {
      fprintf(fp, "\nh1=subplot(2,1,1);\n");
      subplot = 1;
  }
  */
  
  fprintf(fp,"\tif (noplot==None):\n");
    // print user defined python plot commands
  if (inter.num_python_cmds) {
    fprintf(fp, "\t\t### --- User commands\n");
    char *token;
    token = strtok (inter.pythoncommand,"\n");
    while (token != NULL){
      fprintf(fp,"\t\t%s\n",token);
      token = strtok (NULL,"\n");
    }
    fprintf(fp, "\t\t### --- \n");
  }
  fprintf(fp,"\t\t# setting default font sizes\n");
  fprintf(fp,"\t\trc('font',**pp.font)\n");
  fprintf(fp,"\t\trc('xtick',labelsize=pp.TICK_SIZE)\n"); 
  fprintf(fp,"\t\trc('ytick',labelsize=pp.TICK_SIZE)\n"); 
  fprintf(fp,"\t\trc('text', usetex=pp.USETEX)\n");
  fprintf(fp,"\t\trc('axes', labelsize = pp.LABEL_SIZE)\n");
  fprintf(fp,"\t\tfig=plt.figure()\n");
  fprintf(fp,"\t\tfig.set_size_inches(pp.fig_size)\n");
  //fprintf(fp,"\t\tfig.set_dpi(pp.FIG_DPI)\n"); // adf 170519 removed as it does not produce conistent results
  
  // plot the various data traces or surfaces
  if (inter.splot) {
     plot3Dpython(fp,xy1, xy2);
  }
  else
  {
     plot2Dpython(fp,xy1, xy2);
  }
  
    
  if (inter.ytype == FLOG) {
      if (inter.splot) {
        //warn("Python's matplotlib cannot handle 3D plots with log axes!\n");
        //fprintf(fp, "\t\tax3.set_zscale('log',nonposz='clip')\n"); // test adf 061116
      } else {
          fprintf(fp, "\t\tax1.set_yscale('log',nonposy='clip')\n");
      }
      
  } 
  
  
  fprintf(fp,"\t\tif pp.PRINT_TITLE:\n");
  fprintf(fp,"\t\t\tplt.title(mytitle)\n");
  fprintf(fp,"\t\tif pp.SCREEN_TITLE:\n");
  fprintf(fp,"\t\t\tfig.canvas.manager.set_window_title(mytitle)\n");
  fprintf(fp,"\t\telse:\n");
  fprintf(fp,"\t\t\tfig.canvas.manager.set_window_title('')\n");  
  
  // check for other terminals with file output
  python_file_outputs(fp);
  
  // screen output requires show()
  bool all_file_output=true;
  for (k = 0; k < inter.num_pyterm_cmds; k++) {
      all_file_output &= init.pyterm[inter.pyterm[k]].output_is_file;
  }
  if (!all_file_output){
      fprintf(fp,"\t\tplt.show()\n");
  }
  
  fprintf(fp,"\treturn (x,y)\n");
  
  print_python_global_settings(fp);
  fprintf(fp,"\nif __name__==\"__main__\":\n");
  fprintf(fp,"\trun()\n");   
}


void python_file_outputs(FILE *fp){
    int k;
    for (k = 0; k < inter.num_pyterm_cmds; k++) {
        python_terminal_t *pt = &(init.pyterm[inter.pyterm[k]]);
        if (pt->output_is_file) {
            if (strcasecmp(pt->suffix,"pdf") == 0){
                fprintf(fp,"\t\t### printing PDF\n");            
                fprintf(fp,"\t\tprintPDF(fig)\n");
            }
            else {
                fprintf(fp,"\t\t### printing %s\n",pt->suffix);            
                fprintf(fp,"\t\tfilename = '%s.%s'\n",inter.basename, pt->suffix);
                fprintf(fp,"\t\tplt.savefig(filename,**pp.print_options)\n");
            }
        }
    } 
}


void plot3Dpython(FILE *fp, char *xy1, char *xy2){
    int startcol =0;
    char plotname[PLOTFILE_LEN] = {0};
    char legendstring[PLOTFILE_LEN] = {0};
    char title[PLOTFILE_LEN] = {0};
    char beamsize[PLOTFILE_LEN] = {0};
    
    int i;
    int found = 0;
    int first = 1;
    int col_step = 1;
    if (inter.yaxis[1]) {
        col_step = 2;
    }

    bool surface=false; // TODO make this a switchable option

    if (inter.yaxis[1] && inter.ploty2axis && inter.subplot) {
      if (surface){          
        fprintf(fp,"\t\tax3 = fig.add_subplot(121,  projection='3d')\n");
        fprintf(fp,"\t\tax4 = fig.add_subplot(122,  projection='3d')\n");
      }
      else{
        fprintf(fp,"\t\tax3 = fig.add_subplot(121)\n");
        fprintf(fp,"\t\tax4 = fig.add_subplot(122)\n");
      }          
    }
    else {
      if (surface){
        fprintf(fp,"\t\tax3 = fig.add_subplot(111, projection='3d')\n");
      }
      else{
        fprintf(fp,"\t\tax3 = fig.add_subplot(111)\n");
      }
    }

    fprintf(fp,"\t\txm,ym=np.meshgrid(x,y)\n");
    
    for (i = 0; i < inter.num_outputs; i++) {
        strcpy(plotname, inter.output_data_list[i].name);
        
        if (inter.output_data_list[i].detector_type != FEEDBACK
                && inter.output_data_list[i].detector_type != UFUNCTION) {
            strcat(plotname, " ");
            
            // check node indices before passing to node_print
            if(inter.output_data_list[i].node_index >= 0){
                assert(inter.output_data_list[i].node_index < inter.num_nodes);

                strcat(plotname, node_print(inter.output_data_list[i].node_index));

                if (inter.output_data_list[i].is_second_beam) {
                    strcat(plotname, "*");
                }
            }
        }

        output_data_t current_output = inter.output_data_list[i];
        if (current_output.detector_type == BEAM) {
            double nr = *inter.node_list[current_output.node_index].n;
            sprintf(beamsize, " (wx0=%s, wy0=%s)",
                    double_form(w0_size(*current_output.qx, nr)),
                    double_form(w0_size(*current_output.qy, nr)));
            strcat(plotname, beamsize);
        }

        strcpy(title, plotname);

        if (!inter.output_data_list[i].noplot) {
            found = 1;
            if (!first) {
                strcat(legendstring, ", ");
            } else {
                first = 0;
            }

            strcat(legendstring, "\'");
            strcat(legendstring, title);
            strcat(legendstring, "\'");

            if (surface){
              fprintf(fp,"\t\tsurf=ax3.plot_surface(");
            }
            else{
              fprintf(fp,"\t\tsurf=ax3.pcolor(");
            }
            
            if (inter.beam.set && inter.beam.swap) {
                fprintf(fp, "ym,xm,z[:,:,%d]", i * col_step + startcol);
            } else {
                fprintf(fp, "xm,ym,z[:,:,%d]", i * col_step + startcol);
            }
            if (surface){
              fprintf(fp,",rstride=4, cstride=4");
            }
            else{
              if (inter.ytype == FLOG){
                //int tmp_i =  i * col_step + startcol;
                //fprintf(fp,",norm=LogNorm(vmin=z[:,:,%d].min(), vmax=z[:,:,%d].max())",tmp_i,tmp_i);
                fprintf(fp,",norm=LogNorm()");
              }
            }
            fprintf(fp,",cmap=cm.jet,linewidth=0, label = '%s')\n",title);
            if (inter.subplot) {
              if (surface){
                fprintf(fp,"\t\tsurf=ax4.plot_surface(");
              }
              else{
                fprintf(fp,"\t\tsurf=ax4.pcolor(");
              }
              

              if (inter.beam.set && inter.beam.swap) {
                fprintf(fp, "ym,xm,z[:,:,%d]", i * col_step + startcol+1);
              } else {
                fprintf(fp, "xm,ym,z[:,:,%d]", i * col_step + startcol+1);
              }
              if (surface){
                fprintf(fp,",rstride=4, cstride=4");
              }
              else{
                if (inter.ytype == FLOG){
                  fprintf(fp,",norm=LogNorm()");
                }
              }
              fprintf(fp,",cmap=cm.jet,linewidth=0, label = '%s : Phase [Deg]')\n",title);
            }

            
            // only plot one if multi is not set
            if (!inter.multi) {
                i = inter.num_outputs;
            }
            // end of if (!noplot)
        }
    }
    
    //fprintf(fp,"\t\tsurf%d.set_rasterized(True) # set this for mixed-mode rendering\n",i+1);
    fprintf(fp,"\t\tsurf.set_rasterized(True) # set this for mixed-mode rendering\n");
    if (inter.subplot) {
      fprintf(fp,"\t\tsurfp.set_rasterized(True) # set this for mixed-mode rendering\n");
      //fprintf(fp,"\t\tsurfp%d.set_rasterized(True) # set this for mixed-mode rendering\n",i+1);
    }
    
    if (found == 0) {
        gerror("no output (without 'noplot') has been found\n");
    }
    
    double mmin;
    double mmax;
    //double swap;
    if (inter.x2.xtype == FLOG) {
        mmin = inter.x2.xmin / inter.x2.op;
        mmax = inter.x2.xmax / inter.x2.op;
    } else {
        mmin = inter.x2.xmin - inter.x2.op;
        mmax = inter.x2.xmax - inter.x2.op;
    }
    
    fprintf(fp,"\t\tax3.set_%.2slim(%g,%g)\n",xy2,mmin,mmax);
    if (inter.subplot) {
      fprintf(fp,"\t\tax4.set_%.2slim(%g,%g)\n",xy2,mmin,mmax);
    }
    if (inter.x2.xtype == FLOG && inter.x2.xmax / inter.x2.xmin >= 10.0) {
      //warn("Python's matplotlib cannot handle 3D plots with log axes!\n"); // test adf 061115
      fprintf(fp, "\t\tax3.set_%.2sscale('log',nonpos%.2s='clip')\n",xy2,xy2);
    }
    
    fprintf(fp,"\t\tax3.set_%.2slabel('%s')\n",xy2, inter.x2.xname);
    if (surface){
      fprintf(fp,"\t\tax3.set_zlabel('%s')\n",inter.y1name);
      fprintf(fp, "\t\tax%d.zaxis.set_major_formatter(formatter)\n",3);
    }
    fprintf(fp, "\t\tax%d.xaxis.set_major_formatter(formatter)\n",3);
    fprintf(fp, "\t\tax%d.yaxis.set_major_formatter(formatter)\n",3);

    if (inter.subplot) {
      fprintf(fp,"\t\tax4.set_%.2slabel('%s')\n",xy2, inter.x2.xname);
      if (surface){
        fprintf(fp,"\t\tax4.set_zlabel('%s')\n",inter.y2name);
        fprintf(fp, "\t\tax%d.zaxis.set_major_formatter(formatter)\n",4);
      }
      fprintf(fp, "\t\tax%d.xaxis.set_major_formatter(formatter)\n",4);
      fprintf(fp, "\t\tax%d.yaxis.set_major_formatter(formatter)\n",4);
    }

    setpythonaxisprops(fp,3,xy1);
    //Grid is on by default in mplot3d

    if (!inter.subplot) {
      if (surface){
        fprintf(fp,"\t\tfig.colorbar(surf,shrink=0.9) # adjust colorbar length?\n");
      }
      else{
        fprintf(fp,"\t\tfig.colorbar(surf)\n");
      }
    }
    
}

void plot2Dpython(FILE *fp, char *xy1, char*xy2){
    char title[PLOTFILE_LEN] = {0};
    char plotname[PLOTFILE_LEN] = {0};
    char beamsize[PLOTFILE_LEN] = {0};

    fprintf(fp,"\t\tfrom itertools import cycle\n");
    fprintf(fp,"\t\tif 'axes.color_cycle' in matplotlib.rcParams.keys():\n");
    fprintf(fp,"\t\t\tclist = matplotlib.rcParams['axes.color_cycle']\n");
    fprintf(fp,"\t\t\tcolorcycler= cycle(clist)\n");
    fprintf(fp,"\t\telse:\n");
    fprintf(fp,"\t\t\tplist = matplotlib.rcParams['axes.prop_cycle']\n");
    fprintf(fp,"\t\t\tcolorcycler= cycle(plist.by_key()['color'])\n");

    if (inter.yaxis[1] && inter.ploty2axis && inter.subplot) {
      fprintf(fp,"\t\tax1 = fig.add_subplot(211)\n");
      fprintf(fp,"\t\tax2 = fig.add_subplot(212, sharex=ax1)\n");
    }
    else {
      fprintf(fp,"\t\tax1 = fig.add_subplot(111)\n");
    }
    

    setpythonaxisprops(fp,1,xy1);

    fprintf(fp,"\t\tax%d.set_%.2slabel('%s')\n",1, xy2, inter.y1name);
    fprintf(fp, "\t\tax%d.yaxis.set_major_formatter(formatter)\n",1);

    if (inter.yaxis[1] && inter.ploty2axis) {
        if (!inter.subplot) {
          fprintf(fp,"\t\tax2 = ax1.twinx()\n");
        }
        else
          {
            //fprintf(fp,"\t\tax%d.set_%.2slabel('%s')\n",2, xy2, inter.y1name);
            //fprintf(fp, "\t\tax%d.yaxis.set_major_formatter(formatter)\n",2);
            setpythonaxisprops(fp,2,xy1);
          }
    }
    //       setpythonaxisprops(fp,1,xy1); for second axis ???


       
    int tracenumber = 1;
    int col = 0;

    int i;
    for (i = 0; i < inter.num_outputs; i++) {
        output_data_t current_output = inter.output_data_list[i];
        strcpy(plotname, "");
        
        if (inter.num_outputs > 1) {
            strcat(plotname, current_output.name);
        }
        
        if (current_output.detector_type != FEEDBACK &&
                current_output.detector_type != UFUNCTION) {
            strcat(plotname, " ");
            
            // if the output beam param is not beam_param_not_set, 
            // append to the plotname
            if (NOT current_output.beam_param_not_set && current_output.node_index > 0) {
                // check node indices before passing to node_print()
                assert(current_output.node_index >= 0);
                assert(current_output.node_index < inter.num_nodes);
                
                strcat(plotname, node_print(current_output.node_index));
            }
            
            // mark the beam as the "other beam" if necessary
            if (current_output.is_second_beam) {
                strcat(plotname, "*");
            }
        }
        
        if (current_output.detector_type == BEAM) {
            double nr = *inter.node_list[current_output.node_index].n;
            sprintf(beamsize, " (wx0=%s, wy0=%s)",
                    double_form(w0_size(*current_output.qx, nr)),
                    double_form(w0_size(*current_output.qy, nr)));
            strcat(plotname, beamsize);
        }
        
        strcat(plotname, " : ");
        strcpy(title, plotname);

        if (inter.yaxis[1]) {
          strcat(title, inter.y1name);
        }
                
        if (!inter.output_data_list[i].noplot) {
            
            //fprintf(fp,"\t\ttrace1=ax1.plot(x, y[:,0], '-', label = '%s')\n",inter.y1name);
            //fprintf(fp,"\t\ttrace2 = ax2.plot(x, y[:,1], '--r', label = '%s')\n",inter.y2name);
            
            fprintf(fp,"\t\ttrace%d=ax%d.plot(x, y[:,%d], '-', linewidth=pp.LINEWIDTH, color = next(colorcycler), label = '%s')\n",tracenumber++, 1, col++, title);
        } else {
            col++;
        }

        if (inter.yaxis[1] && inter.ploty2axis) {
            
            if (!inter.output_data_list[i].noplot) {
                strcpy(title, plotname);
                strcat(title, inter.y2name);
                if (inter.subplot) {
                  fprintf(fp,"\t\ttrace%d=ax%d.plot(x, y[:,%d], '-', linewidth=pp.LINEWIDTH, color = next(colorcycler), label = '%s')\n", tracenumber++, 2, col++, title);
                }
                else {
                  fprintf(fp,"\t\ttrace%d=ax%d.plot(x, y[:,%d], '-', dashes=[8, 4, 2, 4, 2, 4], linewidth=pp.LINEWIDTH, color = next(colorcycler), label = '%s')\n", tracenumber++, 2, col++, title);
                }
            } else {
              col++;
            }
        }
    }

    if (inter.yaxis[1]) {
      fprintf(fp,"\t\tax%d.set_%.2slabel('%s')\n",2, "y", inter.y2name);
      fprintf(fp, "\t\tax%d.yaxis.set_major_formatter(formatter)\n",2);
    }

      
    fprintf(fp,"\t\ttraces = ");
    for (i=1; i<tracenumber-1; i++){
      fprintf(fp,"trace%d + ",i);
    }
    fprintf(fp,"trace%d\n",tracenumber-1);

    if (inter.yaxis[1] && inter.ploty2axis) 
      {
        fprintf(fp,"\t\ttraces_a = ");
        for (i=1; i<tracenumber-3; i=i+2){
          fprintf(fp,"trace%d + ",i);
        }
        fprintf(fp,"trace%d\n",tracenumber-2);

        fprintf(fp,"\t\ttraces_p = ");
        for (i=2; i<tracenumber-1; i=i+2){
          fprintf(fp,"trace%d + ",i);
        }
        fprintf(fp,"trace%d\n",tracenumber-1);
      }
      else {
        fprintf(fp,"\t\ttraces_a = traces\n");
      }
      
    if (inter.yaxis[1] && inter.ploty2axis) {
      if (inter.subplot) {
        fprintf(fp,"\t\tlegends_a = [t.get_label() for t in traces_a]\n");
        fprintf(fp,"\t\tlegends_p = [t.get_label() for t in traces_p]\n");
        fprintf(fp,"\t\tax1.legend(traces_a, legends_a, loc=0, shadow=pp.SHADOW,prop={'size':pp.LEGEND_SIZE})\n");        
        fprintf(fp,"\t\tax2.legend(traces_p, legends_p, loc=0, shadow=pp.SHADOW,prop={'size':pp.LEGEND_SIZE})\n");
        fprintf(fp,"\t\tax2.grid(pp.GRID)\n");
      }
      else {
        fprintf(fp,"\t\tlegends = [t.get_label() for t in traces]\n");
        fprintf(fp,"\t\tax2.legend(traces, legends, loc=0, shadow=pp.SHADOW,prop={'size':pp.LEGEND_SIZE})\n");
      }
    }
    else {
      fprintf(fp,"\t\tlegends = [t.get_label() for t in traces]\n");
      fprintf(fp,"\t\tax1.legend(traces, legends, loc=0, shadow=pp.SHADOW,prop={'size':pp.LEGEND_SIZE})\n");        
    }
    fprintf(fp,"\t\tax1.grid(pp.GRID)\n");
}

void setpythonaxisprops(FILE *fp, int ax_number, char *xy1) 
{
    double mmin;
    double mmax;
    if (inter.x1.xtype == FLOG) {
        mmin = inter.x1.xmin / inter.x1.op;
        mmax = inter.x1.xmax / inter.x1.op;
    } else {
        mmin = inter.x1.xmin - inter.x1.op;
        mmax = inter.x1.xmax - inter.x1.op;
    }
    
    if (inter.x1.xtype != FLOG){
        if(fabs(mmax)<1 || fabs(mmax)>1000){    
            fprintf(fp, "\t\tax%d.xaxis.set_major_formatter(formatter)\n",ax_number);
        }
    }

    fprintf(fp,"\t\tax%d.set_%.2slim(%g,%g)\n",ax_number, xy1,mmin,mmax);


    if (inter.x1.xtype == FLOG && inter.x1.xmax / inter.x1.xmin >= 10.0) {
        if (inter.splot) {
          //warn("Python's matplotlib cannot handle 3D plots with log axes!\n");
          fprintf(fp, "\t\tax%d.set_%.2sscale('log',nonpos%.2s='clip')\n",ax_number, xy1,xy1); // test adf 061115
        }
        else{
          fprintf(fp, "\t\tax%d.set_%.2sscale('log',nonpos%.2s='clip')\n",ax_number,xy1,xy1);
        }
    }
    
    fprintf(fp,"\t\tax%d.set_%.2slabel('%s')\n",ax_number, xy1, inter.x1.xname);
}



void print_python_usage(FILE *fp, char *python_file_str)
{
  fprintf(fp,"\"\"\"-----------------------------------------------------------------\n");   
  fprintf(fp,"  Python file for plotting Finesse ouput %s.out\n",inter.basename);
  fprintf(fp,"  created automatically %s\n",asctime(localtime(&now)));
  fprintf(fp,"  Run from command line as: python %s.py\n",python_file_str);
  fprintf(fp,"  Load from python script as: import %s\n",python_file_str);
  fprintf(fp,"  And then use:\n");
  fprintf(fp,"  %s.run() for plotting only\n",python_file_str);
  fprintf(fp,"  x,y=%s.run() for plotting and loading the data\n",python_file_str);
  fprintf(fp,"  x,y=%s.run(1) for only loading the data\n",python_file_str);  
  fprintf(fp,"-----------------------------------------------------------------\"\"\"\n");   
}

void print_python_pdf(FILE *fp, char *python_file_str)
{
    fprintf(fp,"import matplotlib.backends.backend_pdf\n");
    fprintf(fp,"def printPDF(self):\n"); 
    fprintf(fp,"\tfilename = '%s.pdf'\n",python_file_str); 
    fprintf(fp,"\tpdfp = matplotlib.backends.backend_pdf.PdfPages(filename)\n"); 
    fprintf(fp,"\tpdfp.savefig(self,dpi=pp.DPI,bbox_inches='tight')\n"); 
    fprintf(fp,"\tpdfp.close()\n\n"); 
}

void print_python_3Dconvert(FILE *fp)
{    
  fprintf(fp,"def convert3D(X,Y,Z):\n");
  fprintf(fp,"\trow,col=Z.shape\n");
  fprintf(fp,"\tnxp=len(np.nonzero(np.equal(Y,Y[0]))[0])\n");
  fprintf(fp,"\tnyp=row//nxp\n");
  fprintf(fp,"\ty=Y[0:nyp]\n");
  fprintf(fp,"\tx=X[0:row:nyp]#note the order\n");
  fprintf(fp,"\tM=np.zeros((nyp,nxp,col))\n");
  fprintf(fp,"\tfor i in range(col):\n");
  fprintf(fp,"\t\tM[:,:,i]=np.reshape(Z[:,i],(nyp,nxp),order='F')\n");
  fprintf(fp,"\treturn (x,y,M)\n\n");
}

void print_python_global_settings(FILE *fp)
{    
    fprintf(fp,"class pp():\n");
    fprintf(fp,"\t# set some gobal settings first\n");
    fprintf(fp,"\tBACKEND = 'Qt4Agg' # matplotlib backend\n");
    fprintf(fp,"\tFIG_DPI=90 # DPI of on sceen plot\n");
    fprintf(fp,"\t# Some help in calculating good figure size for Latex\n");
    fprintf(fp,"\t# documents. Starting with plot size in pt,\n");
    fprintf(fp,"\t# get this from LaTeX using \\showthe\\columnwidth\n"); 
    fprintf(fp,"\tfig_width_pt = 484.0\n");  
    fprintf(fp,"\tinches_per_pt = 1.0/72.27  # Convert TeX pt to inches\n");
    fprintf(fp,"\tgolden_mean = (np.sqrt(5)-1.0)/2.0   # Aesthetic ratio\n");
    fprintf(fp,"\tfig_width = fig_width_pt*inches_per_pt  # width in inches\n");
    fprintf(fp,"\tfig_height = fig_width*golden_mean      # height in inches\n");
    fprintf(fp,"\tfig_size = [fig_width,fig_height]\n");
    fprintf(fp,"\t# some plot options:\n");
    fprintf(fp,"\tLINEWIDTH = 1 # linewidths of traces in plot\n");
    fprintf(fp,"\tAA = True # antialiasing of traces\n");
    fprintf(fp,"\tUSETEX = False # use Latex encoding in text\n");
    fprintf(fp,"\tSHADOW = False # shadow of legend box\n");
    fprintf(fp,"\tGRID = True # grid on or off\n");
    fprintf(fp,"\t# font sizes for normal text, tick labels and legend\n");
    fprintf(fp,"\tFONT_SIZE = 10 # size of normal text\n");
    fprintf(fp,"\tTICK_SIZE = 10 # size of tick labels\n");
    fprintf(fp,"\tLABEL_SIZE = 10 # size of axes labels\n");
    fprintf(fp,"\tLEGEND_SIZE = 10 # size of legend\n");
    fprintf(fp,"\t# font family and type\n");
    fprintf(fp,"\tfont = {'family':'sans-serif','size':FONT_SIZE}\n");
    fprintf(fp,"\tDPI=300 # DPI for saving via savefig\n");
    fprintf(fp,"\t# print options given to savefig command:\n");
    fprintf(fp,"\tprint_options = {'dpi':DPI, 'transparent':True, 'bbox_inches':'tight', 'pad_inches':0.1}\n");
    fprintf(fp,"\t# for Palatino and other serif fonts use:\n");
    fprintf(fp,"\t#font = {'family':'serif','serif':['Palatino']}\n");
    fprintf(fp,"\tSCREEN_TITLE = True # show title on screen?\n");
    fprintf(fp,"\tPRINT_TITLE = False # show title in saved file?\n");
}


//! Sets up and writes the matlab file, so far this is a quick hack, needs fixing!

/*!
 * \param fp the file pointer
 * \param fname the file name
 */
void matlabfile(FILE *fp, char *fname) {
    char xy1[3], xy2[3], XY1[3], XY2[3];
    char mytime[30] = {0};

    // make sure file pointer isn't null
    assert(fp != NULL);

    // replace - by _ in function and file names so that matlab can see
    // the functions, then replace _ by \\_ in the print strings so that
    // matlab prints this correctly
    char matlab_print_str[PLOTFILE_LEN];
    char matlab_func_str[PLOTFILE_LEN];
        
    strcpy(matlab_func_str, basename(inter.basename));
    int replace_error = replace_string(matlab_func_str, "-", "_", 1, 1);
    if (replace_error == -1) {
        bug_error("could not replace '-' in matlab string");
    }

    strcpy(matlab_print_str, matlab_func_str);
    replace_error = replace_string(matlab_print_str, "_", "\\_", 1, 2);

    if (replace_error == -1) {
        bug_error("could not replace '_' in matlab string");
    }

    if (inter.beam.set && inter.beam.swap) {
        strcpy(XY1, "Y");
        strcpy(XY2, "X");
        strcpy(xy1, "y");
        strcpy(xy2, "x");
    } else {
        strcpy(XY1, "X");
        strcpy(XY2, "Y");
        strcpy(xy1, "x");
        strcpy(xy2, "y");
    }

    write_matlab_usage(fp, matlab_func_str);

    if (inter.splot) {
        fprintf(fp, "function [x,y,z] = %s(noplot)\n", matlab_func_str);
    } else {
        fprintf(fp, "function [x,y] = %s(noplot)\n", matlab_func_str);
    }
   
    fprintf(fp, "\ndata = load(\'%s\');\n", basename(fname));
    fprintf(fp, "[rows,cols]=size(data);\n");

    if (inter.splot) {
        fprintf(fp, "%.2s=data(:,1);\n", XY1);
        fprintf(fp, "%.2s=data(:,2);\n", XY2);
        fprintf(fp, "Z=data(:,3:cols);\n");
        fputs("[x,y,z]=convert3D(X,Y,Z);\n", fp);
    } else {
        fprintf(fp, "x=data(:,1);\n");
        fprintf(fp, "y=data(:,2:cols);\n");
    }


    // print user defined matlab commands
    if (inter.num_matlab_cmds) {
        fprintf(fp, "%%%%%% --- User commands\n");
        fputs(inter.matlabcommand, fp);
        fprintf(fp, "%%%%%% --- \n");
    }

    // prepare plottitle
    strcpy(mytime, asctime(localtime(&now)));
    mytime[strlen(mytime) - 1] = '\0';
    fprintf(fp, "mytitle=\'%s                %s\';\n", matlab_print_str,
            mytime);

    fprintf(fp, "if (nargin==0)\n");
    fprintf(fp, "\nfigure(\'name\','%s');\n", matlab_func_str);

    int subplot;
    subplot = 0;
    if (inter.yaxis[1] && inter.ploty2axis) {
        fprintf(fp, "\nh1=subplot(2,1,1);\n");
        subplot = 1;
    }


    // first plot -----------------------------------------------------------------

    // 3D plots
    if (inter.splot) {

        if (inter.multi) {
            fprintf(fp, "hold on;\n");
        }
        plot3Dmatlab(fp, 0);
        if (inter.multi) {
            fprintf(fp, "hold off;\n");
        }
    } else {
        // ordinary 2D plots
        plot2Dmatlab(fp, 0);
    }


    // set axes to linear or logarithmic
    if (inter.ytype == FLOG) {
        if (inter.splot) {
            fprintf(fp, "set(gca, \'ZScale\', \'log\');\n");
        } else {
            fprintf(fp, "set(gca, \'YScale\', \'log\');\n");
        }

    } else {
        if (inter.splot) {
            fprintf(fp, "set(gca, \'ZScale\', \'lin\');\n");
        } else {
            fprintf(fp, "set(gca, \'YScale\', \'lin\');\n");
        }
    }

    // char for replacing _ in label strings
    char matlab_label[PLOTFILE_LEN];

    if (inter.splot) {

        double mmin;
        double mmax;
        double swap;
        if (inter.x2.xtype == FLOG) {
            mmin = inter.x2.xmin / inter.x2.op;
            mmax = inter.x2.xmax / inter.x2.op;
        } else {
            mmin = inter.x2.xmin - inter.x2.op;
            mmax = inter.x2.xmax - inter.x2.op;
        }

        // matlab does not accept XLim with min>max, need to use the 'reverse' keyword
        if (mmin > mmax) {
            SWAP(mmin, mmax);
            fprintf(fp, "set(gca, \'%.2sDir\', \'reverse\');\n", XY2);
        }


        fprintf(fp, "set(gca, \'%.2sLim\', [%g %g]);\n", XY2, mmin, mmax);


        if (inter.x2.xtype == FLOG && inter.x2.xmax / inter.x2.xmin >= 10.0) {
            fprintf(fp, "set(gca, \'%.2sScale\', \'log\');\n", XY2);

        }


        strcpy(matlab_label, inter.x2.xname);
        replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
        if (replace_error == -1) {
            bug_error("could not replace '_' in matlab string");
        }

        fprintf(fp, "%.2slabel(\'%s\');\n", xy2, matlab_label);

        strcpy(matlab_label, inter.y1name);
        replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
        if (replace_error == -1) {
            bug_error("could not replace '_' in matlab string");
        }

        fprintf(fp, "zlabel(\'%s\');\n", matlab_label);
        //    fputs("set mztics 10\n", fp);
    } else {

        strcpy(matlab_label, inter.y1name);
        replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
        if (replace_error == -1) {
            bug_error("could not replace '_' in matlab string");
        }

        fprintf(fp, "%.2slabel(\'%s\');\n", xy2, matlab_label);
    }

    setmatlabaxisprops(fp, XY1, xy1);

    // print user defined matlab commands
    if (inter.num_matlabplot_cmds) {
        fprintf(fp, "%%%%%% --- User plot commands\n");
        fputs(inter.matlabplotcommand, fp);
        fprintf(fp, "%%%%%% --- \n");
    }

    // second plot -----------------------------------------------------------------


    if (subplot) {
        fprintf(fp, "\nh2=subplot(2,1,2);\n");


        // 3D plots
        if (inter.splot) {
            if (inter.multi) {
                fprintf(fp, "hold on;\n");
            }
            plot3Dmatlab(fp, 1);
            if (inter.multi) {
                fprintf(fp, "hold off;\n");
            }
        }
        else {
            // ordinary 2D plots
            plot2Dmatlab(fp, 1);
        }

        strcpy(matlab_label, inter.y2name);
        replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
        if (replace_error == -1) {
            bug_error("could not replace '_' in matlab string");
        }

        fprintf(fp, "ylabel(\'%s\');\n", matlab_label);

        if (inter.ytype == FLOG) {
            if (inter.yaxis[1] == OTYPE_IM) {
                fprintf(fp, "set(gca, \'YScale\', \'log\');\n");
            }
        } else {
            fprintf(fp, "set(gca, \'YScale\', \'lin\');\n");
        }

        setmatlabaxisprops(fp, XY1, xy1);

        fprintf(fp, "\n");
        // print user defined matlab commands
        if (inter.num_matlabplot_cmds) {
            fprintf(fp, "%%%%%% --- User plot commands\n");
            fputs(inter.matlabplotcommand, fp);
            fprintf(fp, "%%%%%% --- \n");
        }

        // now switch handle to first subplot
        fprintf(fp, "subplot(2,1,1);\n");
    }


    /*
    if (inter.beam.set) {
      detno = -1;
      int i = 0;

      while (i < inter.num_outputs) {
        if (inter.output_data_list[i].detector_type == BEAM) {
          detno = inter.output_data_list[i].detector_index;
          i = inter.num_outputs;
        }
        i++;
      }

      if (detno == -1) {
        bug_error("detno -1");
      }

      nr = *inter.node_list[inter.output_data_list[detno].node_index].n;
		
          // following needs proper x,y coordinates
      fprintf(fp, "mytitle=char(mytitle,\'wx0 = %sm, wy0 = %sm, wx  = %sm, wy  = %sm\');\n",
              double_form(w0_size(*inter.output_data_list[detno].qx, nr)),
              double_form(w0_size(*inter.output_data_list[detno].qy, nr)),
              double_form(w_size(*inter.output_data_list[detno].qx, nr)),
              double_form(w_size(*inter.output_data_list[detno].qy, nr)));
    }
     */


    fprintf(fp, "title(mytitle);\n");
    fprintf(fp, "end\n\n");

    write_nargout_check(fp, inter.splot);

    if (inter.splot) {
        write3Dfunction(fp);
    }

}

//! Writes Matlab help code into .m file

/*!
 * \param fp the file pointer
 */

void write_matlab_usage(FILE *fp, char *matlab_func_str) {
    fprintf(fp, "%%----------------------------------------------------------------\n");
    if (inter.splot) {
        fprintf(fp, "%% function [x,y,z] = %s(noplot)\n", matlab_func_str);
    } else {
        fprintf(fp, "%% function [x,y] = %s(noplot)\n", matlab_func_str);
    }

    fprintf(fp, "%% Matlab function to plot Finesse output data\n");
    fprintf(fp, "%% Usage: \n");
    if (inter.splot) {
        fprintf(fp, "%% [x,y,z] = %s   ", matlab_func_str);
    } else {
        fprintf(fp, "%%   [x,y] = %s   ", matlab_func_str);
    }
    fprintf(fp, " : plots and returns the data\n");
    if (inter.splot) {
        fprintf(fp, "%% [x,y,z] = %s(1)", matlab_func_str);
    } else {
        fprintf(fp, "%%   [x,y] = %s(1)", matlab_func_str);
    }
    fprintf(fp, " : just returns the data\n");
    fprintf(fp, "%%           %s    : just plots the data\n", matlab_func_str);
    fprintf(fp, "%% Created automatically %s", asctime(localtime(&now)));
    fprintf(fp, "%% by Finesse %s (%s), %s\n", VERSION, GIT_REVISION, MYTIME);
    fprintf(fp, "%%----------------------------------------------------------------\n");
}


//! Writes Matlab code into .m file for returning output values

/*!
 * \param fp the file pointer
 * \param is_splot 0 for 2d plot and 1 for 3d plot
 */

void write_nargout_check(FILE *fp, int is_splot) {
    if (is_splot) {
        fprintf(fp, "switch nargout\n");
        fprintf(fp, " case {0}\n");
        fprintf(fp, "  clear x y z;\n");
        fprintf(fp, " case {3}\n");
        fprintf(fp, " otherwise\n");
        fprintf(fp, "  error(\'wrong number of outputs\');\n");
        fprintf(fp, "end\n");
    } else {
        fprintf(fp, "switch nargout\n");
        fprintf(fp, " case {0}\n");
        fprintf(fp, "  clear x y;\n");
        fprintf(fp, " case {2}\n");
        fprintf(fp, " otherwise\n");
        fprintf(fp, "  error(\'wrong number of outputs\');\n");
        fprintf(fp, "end\n");
    }
}

//! Writes Matlab code into .m file for converting 3D data from file into Matlab format for surf

/*!
 * \param fp the file pointer
 */

void write3Dfunction(FILE *fp) {
    fprintf(fp, "\n%%---------------------------------------------------------------\n");
    fprintf(fp, "%% Utility function to convert Finesse 3D data into Matlab format\n");
    fprintf(fp, "function [x,y,M]=convert3D(X,Y,Z)\n");
    fprintf(fp, "[row,col]=size(Z);\n");
    fprintf(fp, "nxp=length(find (Y(:)==Y(1)));\n");
    fprintf(fp, "nyp=row/nxp;\n");
    fprintf(fp, "y=Y(1:nyp);\n");
    fprintf(fp, "x=X(1:nyp:row);\n");
    fprintf(fp, "M=zeros(nyp,nxp,col);\n");
    fprintf(fp, "for i=1:col\n");
    fprintf(fp, "M(:,:,i)=reshape(Z(:,i),nyp,nxp);\n");
    fprintf(fp, "end\n");
}


/*

  // does this work with axis switching ??
  if (inter.yaxis[1]) {
    fprintf(fp, "set y2label \"%s\"\n", inter.y2name);
    fputs("set y2tics nomirror\n", fp);
    fputs("set my2tics 3\n", fp);
    fputs("set ytics nomirror\n", fp);
  }

    if (inter.yaxis[1] == OTYPE_IM) {
      fputs("set log y2\n", fp);
    }

    if (inter.yaxis[1]) {
      fputs("set nolog y2\n", fp);
        }




          if (inter.yaxis[1]) {

            if (inter.ploty2axis) {
              strcpy(title, plotname);
              strcat(title, inter.y2name);
              fprintf(fp, ", ");

              if (inter.beam.set && inter.beam.swap) {
                fprintf(fp, 
                        "\\\n\'%s\' using ($2):($1):($%d) title \"%s \"  w l ", 
                        fname, col++, title);
              }
              else {
                fprintf(fp, 
                        "\\\n\'%s\' using ($1):($2):($%d) title \"%s \"  w l", 
                        fname, col++, title);
              }
            }
            else {
              col++;
            }
          }

      if (inter.yaxis[1] && inter.ploty2axis) {

        if (!inter.output_data_list[i].noplot) {
          strcpy(title, plotname);
          strcat(title, inter.y2name);
          fprintf(fp, ", \\\n\'%s\' using ($1):($%d) axes x1y%d "
                  "title \"%s \" w l lt %d lw %d",
                  fname, col++, 2, title,
                  (lt++ > 10 ? lt - 1 : lines[inter.mycolor][lt - 1]),
                  inter.width);
        }
        else {
          col++;
        }
      }

 */




/*  // Will do this later !!!!
if (inter.x3.xtype != NO_FREQ) {
if (inter.x3.xtype == FLOG) {
  fprintf(fp, "set label \"%s = %s\" at screen .1, 0.9 \n",
          inter.x3.xname, double_form(*inter.x3.xaxis / inter.x3.op));
}
else {
  fprintf(fp, "set label \"%s = %s\" at screen .1, 0.9 \n",
          inter.x3.xname, double_form(*inter.x3.xaxis - inter.x3.op));
}

fprintf(fp, "set label \"(%d/%d)\" at screen .8, 0.9 \n", step,
        inter.x3.xsteps);
}
 */


//!

/*!
 * \todo undocumented
 */
void setmatlabaxisprops(FILE *fp, char *XY1, char *xy1) {

    // char for replacing _ in label strings
    char matlab_label[PLOTFILE_LEN];

    double mmin;
    double mmax;
    double swap;
    if (inter.x1.xtype == FLOG) {
        mmin = inter.x1.xmin / inter.x1.op;
        mmax = inter.x1.xmax / inter.x1.op;
    } else {
        mmin = inter.x1.xmin - inter.x1.op;
        mmax = inter.x1.xmax - inter.x1.op;
    }

    // matlab does not accept XLim with min>max, need to use the 'reverse' keyword
    if (mmin > mmax) {
        SWAP(mmin, mmax);
        fprintf(fp, "set(gca, \'%.2sDir\', \'reverse\');\n", XY1);
    }

    fprintf(fp, "set(gca, \'%.2sLim\', [%g %g]);\n", XY1, mmin, mmax);

    if (inter.x1.xtype == FLOG && inter.x1.xmax / inter.x1.xmin >= 10.0) {
        fprintf(fp, "set(gca, \'%.2sScale\', \'log\');\n", XY1);

    }

    strcpy(matlab_label, inter.x1.xname);
    int replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
    if (replace_error == -1) {
        bug_error("could not replace '_' in matlab string");
    }

    fprintf(fp, "%.2slabel(\'%s\');\n", xy1, matlab_label);

    // some useful things to set:
    fprintf(fp, "grid on;\n");
    if (inter.splot) {
        fprintf(fp, "colorbar;\n");
    }

}


//!

/*!
 * \todo undocumented
 */
void plot2Dmatlab(FILE *fp, int startcol) {
    int first;
    int col;
    char plotname[PLOTFILE_LEN] = {0};
    char legendstring[PLOTFILE_LEN] = {0};
    char title[PLOTFILE_LEN] = {0};
    char beamsize[PLOTFILE_LEN] = {0};

    fputs("plot(", fp);
    first = 1;
    //col = 2;
    col = 1;

    strcpy(legendstring, "");

    int i;
    for (i = 0; i < inter.num_outputs; i++) {
        output_data_t current_output = inter.output_data_list[i];
        strcpy(plotname, "");

        if (inter.num_outputs > 1) {
            strcat(plotname, current_output.name);
        }

        if (current_output.detector_type != FEEDBACK &&
                current_output.detector_type != UFUNCTION) {
            strcat(plotname, " ");

            // if the output beam param is not beam_param_not_set, 
            // append to the plotname
            if (NOT current_output.beam_param_not_set && current_output.node_index > 0) {
                // check node indices before passing to node_print()
                assert(current_output.node_index >= 0);
                assert(current_output.node_index < inter.num_nodes);

            
                strcat(plotname, node_print(current_output.node_index));
            }

            // mark the beam as the "other beam" if necessary
            if (current_output.is_second_beam) {
                strcat(plotname, "*");
            }
        }

        if (current_output.detector_type == BEAM) {
            double nr = *inter.node_list[current_output.node_index].n;
            sprintf(beamsize, " (wx0=%s, wy0=%s)",
                    double_form(w0_size(*current_output.qx, nr)),
                    double_form(w0_size(*current_output.qy, nr)));
            strcat(plotname, beamsize);
        }

        strcpy(title, plotname);

        //      if (inter.yaxis[1]) {
        //  strcat(title, inter.y1name);
        //}

        if (!inter.output_data_list[i].noplot) {
            if (!first) {
                fprintf(fp, ", ");
                strcat(legendstring, ", ");
            } else {
                first = 0;
            }

            //fprintf(fp, "data(:,1),data(:,%d)",startcol+col++);
            fprintf(fp, "x, y(:,%d)", startcol + col++);
            strcat(legendstring, "\'");
            strcat(legendstring, title);
            strcat(legendstring, "\'");

        } else {
            col++;

        }
        if (inter.yaxis[1]) {
            col++;
        }

    }

    int replace_error = replace_string(legendstring, "_", "\\_", 1, 2);
    
    if (replace_error == -1) {
        bug_error("could not replace '_' in matlab string");
    }


    fprintf(fp, ");\n");
    fprintf(fp, "legend(%s);\n", legendstring);
}


//! Writes Matlab code to plots 3D data, kind of works but is not optimised yet

/*!
 * 
 */
void plot3Dmatlab(FILE *fp, int startcol) {
    int found;
    int first;
    char plotname[PLOTFILE_LEN] = {0};
    char legendstring[PLOTFILE_LEN] = {0};
    char title[PLOTFILE_LEN] = {0};
    char beamsize[PLOTFILE_LEN] = {0};

    int i = 0;
    found = 0;
    first = 1;

    int col_step = 1;
    if (inter.yaxis[1]) {
        col_step = 2;
    }

    for (i = 0; i < inter.num_outputs; i++) {
        strcpy(plotname, inter.output_data_list[i].name);

        if (inter.output_data_list[i].detector_type != FEEDBACK
                && inter.output_data_list[i].detector_type != UFUNCTION) {
            strcat(plotname, " ");

            // check node indices before passing to node_print
            if(inter.output_data_list[i].node_index >= 0){
                assert(inter.output_data_list[i].node_index < inter.num_nodes);

                strcat(plotname, node_print(inter.output_data_list[i].node_index));

                if (inter.output_data_list[i].is_second_beam) {
                    strcat(plotname, "*");
                }
            }
        }

        output_data_t current_output = inter.output_data_list[i];
        if (current_output.detector_type == BEAM) {
            double nr = *inter.node_list[current_output.node_index].n;
            sprintf(beamsize, " (wx0=%s, wy0=%s)",
                    double_form(w0_size(*current_output.qx, nr)),
                    double_form(w0_size(*current_output.qy, nr)));
            strcat(plotname, beamsize);
        }

        strcpy(title, plotname);

        if (!inter.output_data_list[i].noplot) {
            found = 1;
            if (!first) {
                strcat(legendstring, ", ");
            } else {
                first = 0;
            }

            strcat(legendstring, "\'");
            strcat(legendstring, title);
            strcat(legendstring, "\'");

            fputs("surfc(", fp);

            if (inter.beam.set && inter.beam.swap) {
                fprintf(fp, "y,x,z(:,:,%d),\'EdgeColor\',\'none\');\n", 1 + i * col_step + startcol);
            } else {
                fprintf(fp, "x,y,z(:,:,%d),\'EdgeColor\',\'none\');\n", 1 + i * col_step + startcol);
            }

            // only plot one if multi is not set
            if (!inter.multi) {
                i = inter.num_outputs;
            }

            // end of if (!noplot)
        }
    }

    if (found == 0) {
        gerror("no output (without 'noplot') has been found\n");
    }

    // char for replacing _ in label strings
    char matlab_label[PLOTFILE_LEN];
    strcpy(matlab_label, legendstring);
    int replace_error = replace_string(matlab_label, "_", "\\_", 1, 2);
    if (replace_error == -1) {
        bug_error("could not replace '_' in matlab string");
    }


    // in 3D plot sthe legend makes not much sense, comment it out unless 'multi' is set
    //  if (inter.multi) {
    if (!startcol && inter.multi) {
        fprintf(fp, "legend(%s);\n", matlab_label);
    } else {
        fprintf(fp, "%%legend(%s);\n", matlab_label);
    }
}


/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

// $Id$

/*!
 * \file kat_server.h
 * \brief Main header file for kat_server.c as part of Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef KAT_SERVER_H
#define KAT_SERVER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>       
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <dirent.h>
#include <fnmatch.h>
#include <netdb.h>
#include "mnet.h"
#include "katnet.h"
#include <pthread.h>
#include <unistd.h>
#include <netinet/tcp.h>

#define NCONN                1    //!< Number of allowed client connections
#define MAX_COMMAND_LENGTH 100    //!< Maximum length of a command string
#define QUEUE_LENGTH         5    //!< Length of command queue
#define STRBUFFLEN        8192    //!< Length of read-string buffer

/*! 
 *  A structure to store job objects.
 */
typedef struct job {
    long int id; //!< Job id
    char cmd[MAX_COMMAND_LENGTH]; //!< Command string

} job;

/*!
        A bounded buffer structure. Access to the
        length member is controlled by a semaphore lock.
 */
typedef struct boundedbuffer {
    pthread_mutex_t bb_lock; //!< a semaphore on the length variable
    int maxLength; //!< max length of the buffer
    int head; //!< head element
    int tail; //!< tail element
    int length; //!< current length of the buffer
    void **objects; //!< the data addresses

} bbuffer;

/* from kat_server.c */

void * create_bounded_buffer(int length);
void destroy_bounded_buffer(bbuffer *b);
void *get_head(bbuffer *b);
void *get_head_offset(bbuffer *b, int offset);
void * get_tail(bbuffer *b);
// int receivecommands(int sd, char *name); // not used
int receivecommands_sock(int sd, char *name);
// int read_tune_values(mstream *s); // not used
int read_tune_values_sock(int socketfd);
// int read_tune_params(mstream *s); // not used
int read_tune_params_sock(int socketfd);
// int queue_job(char *buff); // not used
void * cmd_listen(void *port);
void * cmd_process(void *arg);
void initserver(void);
int sendDouble(int socketfd, double d);
int readDouble(int socketfd, double *d);
int write_current_tune_values_sock(int socketfd);

#ifndef htonll
unsigned long long htonll(unsigned long long src);
#endif

/* from kat_calc.c */
void write_server_result_sock(int socketfd);

#endif // KAT_SERVER_H



/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

// $Id$

/*!
 * \file kat_aa.h
 * \brief Header file for automatic alignment routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef KAT_AA_H
#define KAT_AA_H

#define DELTA_FUNC(n1,n2,m1,m2) ((n1 == n2) && (m1 == m2) ? ((n1 == m1) ? complex_1 : complex_0) : complex_0)

void set_coupling_info();
void knm_matrix_mult(zmatrix A, zmatrix B, zmatrix C);
void output_mismatch();
bool __print_mismatch(const char *dir, double mx, double my, char *output);

inline complex_t rev_gouy(complex_t k, int n1, int m1, int n2, int m2,
       complex_t qx1, complex_t qx2, complex_t qy1, complex_t qy2);
int set_k_mirror(int i);
int set_k_mirror_phasemap(int i);
int set_k_mirror_phasefunc(int i);
int set_k_beamsplitter(int i);
int set_k_space(int i);
int set_k_lens(int i);
int set_k_modulator(int i);
int set_k_diode(int i);
void set_k_all_components(bool rebuilding);
void mode_match(complex_t q1, complex_t q2, complex_t *k, double *km);
complex_t k_nmnm(int n1, int n2, int m1, int m2,
        complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2,
        double gamma_x, double gamma_y, double nrm, const unsigned int knm_flags);
complex_t k_nmnm_new(int n1, int n2, int m1, int m2,
        complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2,
        double gamma_x, double gamma_y, double nr, const unsigned int knm_flags);
complex_t S_u(complex_t X, complex_t XS, complex_t F, complex_t FS, int m1, int m2);
complex_t S_g(complex_t X, complex_t XS, complex_t F, complex_t FS, int m1, int m2);
complex_t k_mm(int m1, int m2, complex_t q1, complex_t q2, double gamma, double nr);
int num_eqns_for(int modes);
void compute_cavity_params(void);
int trace_cavity(ABCD_t *Mlocal_x, ABCD_t *Mlocal_y, int component1_index, int node1_index, int component2_index, int node2_index, int *index, int *traced);
void set_ABCD_mirror(int i);
void set_ABCD_beamsplitter(int i);
void set_ABCD_space(int i);
void set_ABCD_lens(int i);
void set_ABCD_grating(int i);
void set_ABCDs(void);
void set_qbase(int component);
void trace_beam(int startnode);
void tracing(int startnode, int start_comp);
int component_matrix(ABCD_t *matrix, int component_index, int node1_index, int node2_index, int tan_sag);
void get_component(int *component_type, int *component_index, int node1_index, int node2_index);
void which_nodes(int i, int current_node, int *num_nodes, int *node_indices);
void which_components(int node_index, int *component1_index, int *component2_index);
char *get_node_name(int node_index);
void set_q(int node_index, int component_index, complex_t qx, complex_t qy);
void check_map_size(surface_map_t *map, complex_t qx1, complex_t qy1, complex_t qx2, complex_t qy2, double nr1, double nr2);


#endif // KAT_AA_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

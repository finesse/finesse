/* -bad */
--blank-lines-after-declarations

/* -nbbb */
--no-blank-lines-before-block-comments

/* -br */
--braces-on-if-line

/* -nce */
--dont-cuddle-else

/* -cli1 */
--case-indentation0

/* -d0 */
--line-comments-indentation0

/* -di1 */
--declaration-indentation1

/* -nfc1 */
--dont-format-first-column-comments

/* -i2 */
--indent-level2

/* -nip */
--no-parameter-indentation

/* -l80 */
--line-length80

/* -lp */
--continue-at-parentheses

/* -npsl */
--dont-break-procedure-type

/* -nsc */
--dont-star-comments

/* -npcs */
--no-space-after-function-call-names

/* -nut */
--no-tabs

/* -cs */
--space-after-cast

/* -bbo */
--break-before-boolean-operator

/* -hnl */
--honour-newlines

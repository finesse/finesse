
#include "kat.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_fortran.h"
#include "kat_optics.h"
#include "kat_aux.h"
#include "kat_dump.h"
#include "kat_aa.h"
#include "kat_check.h"
#include "kat_knm_mirror.h"
#include "kat_knm_bs.h"
#include "kat_knm_bayer.h"
#include <stdlib.h>
//#include <omp.h>

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;
extern local_var_t vlocal;

extern FILE *fp_log;

extern const complex_t complex_i; //!< sqrt(-1) or 0 + i
extern const complex_t complex_1; //!< 1 but in complex space: 1 + 0i
extern const complex_t complex_m1; //!< -1 but in complex space: -1 + 0i
extern const complex_t complex_0; //!< 0 but in complex space: 0 + 0i

void _run_bayer_helms(complex_t qxt, complex_t qyt, complex_t qxt2
        , complex_t qyt2, complex_t **knm, double gamma_x, double gamma_y
        , double nr_out, const unsigned int knm_flags){
    
    int n, m, n1, n2, m1, m2;
    int tid = startTimer("BAYER_HELMS");
    
    // need to set lock so that the threads don't clash when accessing
    // the vlocal variable in k_nmnm_new
    //omp_init_lock(&knmLock);
    
    /*#pragma omp parallel for default(none) \
                private(n,m,n1,m1,n2,m2) \
                shared(qxt,qyt,qxt2,qyt2,knm,gamma_x,gamma_y,nr_out, inter) \
                copyin(vlocal)
     */
    for (n = 0; n < inter.num_fields; n++) {
        for (m = 0; m < inter.num_fields; m++) {
            get_tem_modes_from_field_index(&n1, &m1, n);
            get_tem_modes_from_field_index(&n2, &m2, m);
            
            if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                bug_error("notfound 1");
            }

            // TODO note the 'new' in the function call below. k_mnMN and k_mnmn_new should merge again later
            knm[n][m] = k_nmnm_new(n1, m1, n2, m2, qxt, qyt, qxt2, qyt2, gamma_x, gamma_y, nr_out, knm_flags);
        }
    }
    
    //omp_destroy_lock(&knmLock);
    endTimer(tid);
    
    // printing output should not be done in parallel section as it will be a mess
    if (inter.debug & 32) {
        for (n = 0; n < inter.num_fields; n++) {
            for (m = 0; m < inter.num_fields; m++) {
                get_tem_modes_from_field_index(&n1, &m1, n);
                get_tem_modes_from_field_index(&n2, &m2, m);

                if (n1 == NOT_FOUND || m1 == NOT_FOUND || n2 == NOT_FOUND || m2 == NOT_FOUND) {
                    bug_error("notfound 1");
                }

                if (inter.debug & 32 && !ceq(complex_0, knm[n][m])) {
                    message(" [%d, %d]=%s, ", n, m, complex_form(knm[n][m]));
                }
            }
            if (inter.debug & 32) {
                message("\n   ");
            }
        }
        
        message("\n");
    }
}

/**
 * This function is for internal use only by compute_bayer_helms_knm. It is just
 * to reuse similar debug code and loops.
 * 
 * @param qxt
 * @param qyt
 * @param qxt2
 * @param qyt2
 * @param knm	Pointer to the complex_t** to store the matrix
 * @param gamma_x misalignment about x
 * @param gamma_y misalignment about y
 * @param nr_out The outgoing mediums refractive index
 * @param k_num Sets which k to calculate: k11=1, k22=2, k12=3, k21=4
 */
void _mirror_compute_bayer_helms_sub_call(complex_t qxt, complex_t qyt, complex_t qxt2
        , complex_t qyt2, complex_t **knm, double gamma_x, double gamma_y
        , double nr_out, KNM_MIRROR_NODE_DIRECTION_t k_num, const unsigned int knm_flags) {
    
    complex_t kx, ky;
    double kmx, kmy;
    char num[3];
    
    if ((inter.trace & 64) || (inter.debug & 32)) {
        switch (k_num) {
            case MR11:
                strcpy(num, "11");
                break;
            case MR22:
                strcpy(num, "22");
                break;
            case MR12:
                strcpy(num, "12");
                break;
            case MR21:
                strcpy(num, "21");
                break;
            default:
                bug_error("Incorrect k_num, %i, provided for _compute_bayer_helms_sub_call", k_num);
        }
    }

    if (inter.trace & 64) {
        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            mode_match(qxt, qxt2, &kx, &kmx);
            mode_match(qyt, qyt2, &ky, &kmy);
            message(" r%s: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                    num, complex_form(kx), complex_form(ky), kmx, kmy);
        }
    }

    if (inter.debug & 32) {
        message("k%s", num);
    }

    _run_bayer_helms(qxt, qyt, qxt2, qyt2, knm, gamma_x, gamma_y, nr_out, knm_flags);
}

/**
 * This function takes a mirror object and a set or parameters and computes the 
 * misalignment and mismatched coupling coefficient matrix for the mirror. The 
 * results are places in the mirror object in its knm_bayer_helms member. Returns
 * an integer which is flagged depending on which components are mismatched.
 * 
 * @param params Parameters used to calculate the knm
 * @param mirror Mirror to compute knm for
 */
void compute_mirror_bayer_helms_knm(mirror_t *mirror, double nr1, double nr2) {

    // Allocate some memory for computing bayer-helms knm matrix
    if (!IS_MIRROR_KNM_ALLOCD(&(mirror->knm_bayer_helms)))
        bug_error("mirror bayer-helms knm has not been allocated");
    
    mirror->knm_bayer_helms.IsIdentities = false;
    
    if (inter.debug & 128 && !options.quiet)
       message("* Calculating Bayer-Helms Knm analytically for mirror %s\n", mirror->name);

    mirror_knm_t *knm_BH = &(mirror->knm_bayer_helms);
    
    if (CALC_MR_KNM(mirror,11)) {
        _mirror_compute_bayer_helms_sub_call(mirror->knm_q.qxt1_11
                , mirror->knm_q.qyt1_11
                , mirror->knm_q.qxt2_11
                , mirror->knm_q.qyt2_11
                , knm_BH->k11
                , BETA_SCALE * mirror->beta_x
                , BETA_SCALE * mirror->beta_y
                , nr1
                , MR11
                , mirror->knm_flags);
    }
	
    if (CALC_MR_KNM(mirror,22)) {
        // here we use -beta!
        _mirror_compute_bayer_helms_sub_call(mirror->knm_q.qxt1_22
                , mirror->knm_q.qyt1_22
                , mirror->knm_q.qxt2_22
                , mirror->knm_q.qyt2_22
                , knm_BH->k22
                , BETA_SCALE * mirror->beta_x
                , -BETA_SCALE * mirror->beta_y
                , nr2
                , MR22
                , mirror->knm_flags);
    }
	
    if (CALC_MR_KNM(mirror, 12) && CALC_MR_KNM(mirror, 21)) {
        _mirror_compute_bayer_helms_sub_call(mirror->knm_q.qxt1_12
                , mirror->knm_q.qyt1_12
                , mirror->knm_q.qxt2_12
                , mirror->knm_q.qyt2_12
                , knm_BH->k12
                , -(1 - nr1/nr2) * mirror->beta_x
                , -(1 - nr1/nr2) * mirror->beta_y
                , nr2
                , MR12
                , mirror->knm_flags);
				
        _mirror_compute_bayer_helms_sub_call(mirror->knm_q.qxt1_21
                , mirror->knm_q.qyt1_21
                , mirror->knm_q.qxt2_21
                , mirror->knm_q.qyt2_21
                , knm_BH->k21
                , -(1 - nr2/nr1) * mirror->beta_x
                , (1 - nr2/nr1) * mirror->beta_y
                , nr1
                , MR21
                , mirror->knm_flags);
    }
}

void _bs_node_direction_to_string(KNM_BS_NODE_DIRECTION_t knum, char* num){
    switch (knum) {
        case BS12:
            strcpy(num, "12");
            break;
        case BS21:
            strcpy(num, "21");
            break;
        case BS34:
            strcpy(num, "34");
            break;
        case BS43: 
            strcpy(num, "43");
            break;
        case BS13:
            strcpy(num, "13");
            break;
        case BS31:
            strcpy(num, "31");
            break;
        case BS24:
            strcpy(num, "24");
            break;
        case BS42: 
            strcpy(num, "42");
            break;
        default:
            bug_error("Incorrect k_num, %i, provided for _bs_node_direction_to_string", knum);
    }
}

/**
 * This function is for internal use only by compute_bayer_helms_knm. It is just
 * to reuse similar debug code and loops.
 * 
 * @param qxt
 * @param qyt
 * @param qxt2
 * @param qyt2
 * @param knm	Pointer to the complex_t** to store the matrix
 * @param gamma_x misalignment about x
 * @param gamma_y misalignment about y
 * @param nr_out The outgoing mediums refractive index
 * @param k_num Sets which k to calculate: k11=1, k22=2, k12=3, k21=4
 */
void _bs_compute_bayer_helms_sub_call(complex_t qxt, complex_t qyt, complex_t qxt2
        , complex_t qyt2, complex_t **knm, double gamma_x, double gamma_y
        , double nr_out, KNM_BS_NODE_DIRECTION_t k_num, const unsigned int knm_flags) {
    
    complex_t kx, ky;
    double kmx, kmy;
    char num[10];
    
    if ((inter.trace & 64) || (inter.debug & 32)) {
        _bs_node_direction_to_string(k_num, num);
    }

    if (inter.trace & 64) {
        if (!ceq(qxt, qxt2) || !ceq(qyt, qyt2)) {
            mode_match(qxt, qxt2, &kx, &kmx);
            mode_match(qyt, qyt2, &ky, &kmy);
            message(" r%s: Kx=%s \n      Ky=%s \n      Kmx %9.3g Kmy %9.3g\n",
                    num, complex_form(kx), complex_form(ky), kmx, kmy);
            message("      qxt1=%s\n      qxt2=%s\n      qyt1=%s\n      qyt2=%s\n",
                    complex_form(qxt),complex_form(qxt2),complex_form(qyt),complex_form(qyt2));
        }
    }

    if (inter.debug & 32) {
        message("k%s", num);
    }
    
    _run_bayer_helms(qxt, qyt, qxt2, qyt2, knm, gamma_x, gamma_y, nr_out, knm_flags);
}

/**
 * This function takes a mirror object and a set or parameters and computes the 
 * misalignment and mismatched coupling coefficient matrix for the mirror. The 
 * results are places in the mirror object in its knm_bayer_helms member. Returns
 * an integer which is flagged depending on which components are mismatched.
 * 
 * @param params Parameters used to calculate the knm
 * @param mirror Mirror to compute knm for
 * 
 * @return Returns true if the Bayer-Helms knm should be calculated in the integral
 */
bool compute_bs_bayer_helms_knm(beamsplitter_t *bs, double nr1, double nr2) {
    // check if we should integrate the bayer_helms knm's. If we do there is no
    // need to calculate the rest of this function
    if (bs->knm_flags & INT_BAYER_HELMS) {
        gerror("Integrating the Bayer-Helms coefficients using the Cuba or Riemann integrators is not implemented yet\n");
        return true;
    } else {
        bs->knm_bayer_helms.IsIdentities = false;
    }

    // Allocate some memory for computing bayer-helms knm matrix
    if (!IS_BS_KNM_ALLOCD(&(bs->knm_bayer_helms)))
        bug_error("beamsplitter bayer-helms knm has not been allocated");

    if (inter.debug & 128 && !options.quiet)
        message("* Calculating Bayer-Helms Knm analytically for beamsplitter %s\n", bs->name);

    complex_t qx1=complex_0, qx2=complex_0, qy1=complex_0, qy2=complex_0;
    complex_t **knm = NULL;
    double gamma_x=0.0, gamma_y=0.0, nr=1.0;
    int k; // k here represents our BS12, BS21 ... to BS42

    // angle coefficients that scale depending on the beam rotation
    double co1, co2;
    
    co1 = cos(bs->alpha_1 * RAD);
    co2 = cos(asin(sin(bs->alpha_1 * RAD) * (nr1 / nr2)));
    
    // now loop through each component, check whether we should calculate
    // the knm, then interpolate etc.
    for (k = BS12; k <= BS42; k++) {
        
        // the calc flags are powers of 2 and stored in a flag
        // use bit shift here to get the powers of 2 in the loop
        unsigned int kk = ((unsigned int) (1 << (k - 1)));
        
        // if we should be calculating this k continue
        if ((bs->knm_calc_flags & kk) == kk) {
            
            // Now we want to set the various values the bayer helms computation
            // will need
            switch (k) {
                case BS12:
                    qx1 = bs->knm_q.qxt1_12;
                    qx2 = bs->knm_q.qxt2_12;
                    qy1 = bs->knm_q.qyt1_12;
                    qy2 = bs->knm_q.qyt2_12;
                    knm = bs->knm_bayer_helms.k12;
                    gamma_x = BETA_SCALE * bs->beta_x;
                    gamma_y = BETA_SCALE * co1 * bs->beta_y;
                    nr = nr1;
                    break;
                case BS21:
                    qx1 = bs->knm_q.qxt1_21;
                    qx2 = bs->knm_q.qxt2_21;
                    qy1 = bs->knm_q.qyt1_21;
                    qy2 = bs->knm_q.qyt2_21;
                    knm = bs->knm_bayer_helms.k21;
                    gamma_x = BETA_SCALE * bs->beta_x;
                    gamma_y = BETA_SCALE * co1 * bs->beta_y;
                    nr = nr1;
                    break;
                case BS34:
                    qx1 = bs->knm_q.qxt1_34;
                    qx2 = bs->knm_q.qxt2_34;
                    qy1 = bs->knm_q.qyt1_34;
                    qy2 = bs->knm_q.qyt2_34;
                    knm = bs->knm_bayer_helms.k34;
                    gamma_x = BETA_SCALE * bs->beta_x;
                    gamma_y = - BETA_SCALE * co2 * bs->beta_y;
                    nr = nr2;
                    break;
                case BS43:
                    qx1 = bs->knm_q.qxt1_43;
                    qx2 = bs->knm_q.qxt2_43;
                    qy1 = bs->knm_q.qyt1_43;
                    qy2 = bs->knm_q.qyt2_43;
                    knm = bs->knm_bayer_helms.k43;
                    gamma_x = BETA_SCALE * bs->beta_x;
                    gamma_y = - BETA_SCALE * co2 * bs->beta_y;
                    nr = nr2;
                    break;
                case BS13:
                    qx1 = bs->knm_q.qxt1_13;
                    qx2 = bs->knm_q.qxt2_13;
                    qy1 = bs->knm_q.qyt1_13;
                    qy2 = bs->knm_q.qyt2_13;
                    knm = bs->knm_bayer_helms.k13;
                    gamma_x = -(1 - nr1/nr2) * bs->beta_x;
                    gamma_y = -(1 - nr1/nr2) * bs->beta_y;
                    nr = nr2;
                    break;
                case BS31:
                    qx1 = bs->knm_q.qxt1_31;
                    qx2 = bs->knm_q.qxt2_31;
                    qy1 = bs->knm_q.qyt1_31;
                    qy2 = bs->knm_q.qyt2_31;
                    knm = bs->knm_bayer_helms.k31;
                    gamma_x = -(1 - nr2/nr1) * bs->beta_x;
                    gamma_y = (1 - nr2/nr1) * bs->beta_y;
                    nr = nr1;
                    break;
                case BS24:
                    qx1 = bs->knm_q.qxt1_24;
                    qx2 = bs->knm_q.qxt2_24;
                    qy1 = bs->knm_q.qyt1_24;
                    qy2 = bs->knm_q.qyt2_24;
                    knm = bs->knm_bayer_helms.k24;
                    gamma_x = -(1 - nr1/nr2) * bs->beta_x;
                    gamma_y = -(1 - nr1/nr2) * bs->beta_y;
                    nr = nr2;
                    break;
                case BS42:
                    qx1 = bs->knm_q.qxt1_42;
                    qx2 = bs->knm_q.qxt2_42;
                    qy1 = bs->knm_q.qyt1_42;
                    qy2 = bs->knm_q.qyt2_42;
                    knm = bs->knm_bayer_helms.k42;
                    gamma_x = -(1 - nr2/nr1) * bs->beta_x;
                    gamma_y = (1 - nr2/nr1) * bs->beta_y;
                    nr = nr1;
                    break;
            }

            _bs_compute_bayer_helms_sub_call(qx1,qy1
                    , qx2
                    , qy2
                    , knm
                    , gamma_x
                    , gamma_y
                    , nr
                    , (KNM_BS_NODE_DIRECTION_t)k
                    , bs->knm_flags);
        }
    }

    return false;
}

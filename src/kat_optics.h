// $Id$

/*!
 * \file kat_optics.h
 * \brief Header file for optics-related routines in Finesse
 *
 * \todo should the check_* routines here be in kat_check.h?
 * \todo do all of the routines here belong in kat_optics.c/h???
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */	

#ifndef KAT_OPTICS_H
#define KAT_OPTICS_H

#define TWOPI_QRT 0.893243841738002308794364125788   // (2/pi)^1/4

double schottky_shotnoise(output_data_t *out, double P);
double finesse(double loss);
double gouy(complex_t q);
void rebuild_grating(grating_t *grating);
complex_t q_cavity(ABCD_t s, double nr);
complex_t u_nm(int n, int m, complex_t qx, complex_t qy, double x, double y, double nr);
complex_t u(int n, complex_t q, double x, double nr);
complex_t u_fast_prefactor(int n, complex_t q, double nr);
complex_t u_fast_x(int n, double x, u_n_accel_t *acc);
complex_t u_nm_fast_x(double x, double y, int n, int m, u_nm_accel_t * acc);
complex_t u_fast(complex_t prefactor, complex_t x);
complex_t q1_q2(ABCD_t s, complex_t q1, double n1, double n2);
ABCD_t multiply_ABCD(ABCD_t s1, ABCD_t s2);
ABCD_t inv_ABCD(ABCD_t s1);
ABCD_t reverse_ABCD(int type, ABCD_t s1, double n);
ABCD_t make_ABCD(double a, double b, double c, double d);

u_nm_accel_t* u_nm_accel_alloc(int n, int m,long *bytes);
u_n_accel_t* u_n_accel_alloc(int n,long *bytes);
void u_n_accel_get(u_n_accel_t* acc, int n, complex_t q, double nr);
void u_nm_accel_get(u_nm_accel_t* acc, int n, int m, complex_t qx, complex_t qy,  double nr);
void u_n_accel_free(u_n_accel_t* ptr);
void u_nm_accel_free(u_nm_accel_t* ptr);

void hg_2_lg(complex_t *A_hg, complex_t *A_lg);
void lg_2_hg(complex_t *A_lg, complex_t *A_hg);
void generate_bnmk();

#endif // KAT_OPTICS_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

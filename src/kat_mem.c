// $Id$

/*!
 * \file kat_mem.c
 * \brief Memory allocation routines for components used in simulation
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_mem.h"
#include "kat_read.h"
#include "kat_inline.c"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_init.h"
#include "kat_dump.h"
#include "kat_aa.h"
#include "kat_knm_mirror.h"
#include "kat_knm_aperture.h"
#include "kat_knm_bs.h"
#include "kat_quant.h"

extern options_t options;
extern init_variables_t init;
extern interferometer_t inter;
extern memory_t mem;
extern local_var_t vlocal;
extern char functokens[];
extern int endcomp; //!< Flag for the end of the computation???
extern int lastparam; //!< Flag for the last parameter???
extern int *t_s; //!< Type_of_Signal list [frequency]
extern int *transfer;
extern double *f_s; //!< Frequency value list [frequency]
extern complex_t ***a_s; //!< Amplitude list [field] [output] [frequency]

extern bs_knm_t bstmp;
extern bs_knm_t bstmap;
extern mirror_knm_t mrtmap;

extern zmatrix tmp_knm;

complex_t *car_ws = NULL;

/** The idea of this function is to allocate memory dynamically for a complex_t
 * matrix where the memory is aligned in one continuous block. This is required
 * to use these matrices in BLAS functions. However throughout the code we wish
 * to use A[n][m] rather than A[n*L +m], so we need to correctly set the 
 * pointers of A to point to the correct continuous lump of memory.
 */
void allocate_zmatrix(complex_t ***A, size_t N, long int *bytes){
    *A = (complex_t**) malloc(sizeof(complex_t*)*N);
    complex_t *A_vals = (complex_t*) malloc(sizeof(complex_t)*N*N);
    
    if(bytes) *bytes += sizeof(complex_t*)*N + sizeof(complex_t)*N*N;
    
    int i;
    // Now need to set the pointers to each row of the matrix.
    for(i=0; i<(int)N; i++){
        (*A)[i] = &A_vals[i*N];
    }
}

/** The idea of this function is to allocate memory dynamically for a complex_t
 * matrix where the memory is aligned in one continuous block. This is required
 * to use these matrices in BLAS functions. However throughout the code we wish
 * to use A[n][m] rather than A[n*L +m], so we need to correctly set the 
 * pointers of A to point to the correct continuous lump of memory.
 */
void allocate_zmatrix2(complex_t ***A, size_t Nx, size_t Ny, long int *bytes){
    *A = (complex_t**) malloc(sizeof(complex_t*)*Nx);
    complex_t *A_vals = (complex_t*) malloc(sizeof(complex_t)*Nx*Ny);
    
    if(bytes) *bytes += sizeof(complex_t*)*Nx + sizeof(complex_t)*Nx*Ny;
    
    int i;
    // Now need to set the pointers to each row of the matrix.
    for(i=0; i<(int)Nx; i++){
        (*A)[i] = &A_vals[i*Ny];
    }
}

void free_zmatrix(zmatrix A){
    free(A[0]);
    free(A);
}

/** The idea of this function is to allocate memory dynamically for a complex_t
 * matrix where the memory is aligned in one continuous block. This is required
 * to use these matrices in BLAS functions. However throughout the code we wish
 * to use A[n][m] rather than A[n*L +m], so we need to correctly set the 
 * pointers of A to point to the correct continuous lump of memory.
 */
void allocate_dmatrix(dmatrix *A, size_t N, long int *bytes){
    *A = (double**) malloc(sizeof(double*)*N);
    double *A_vals = (double*) malloc(sizeof(double)*N*N);
    
    if(bytes) *bytes += sizeof(double*)*N + sizeof(double)*N*N;
    
    int i;
    // Now need to set the pointers to each row of the matrix.
    for(i=0; i<(int)N; i++){
        (*A)[i] = &A_vals[i*N];
    }
}

/** The idea of this function is to allocate memory dynamically for a complex_t
 * matrix where the memory is aligned in one continuous block. This is required
 * to use these matrices in BLAS functions. However throughout the code we wish
 * to use A[n][m] rather than A[n*L + m], so we need to correctly set the 
 * pointers of A to point to the correct continuous lump of memory.
 */
void allocate_dmatrix2(dmatrix *A, size_t Nx, size_t Ny, long int *bytes){
    *A = (double**) malloc(sizeof(double*)*Nx);
    double *A_vals = (double*) malloc(sizeof(double)*Nx*Ny);
    
    if(bytes) *bytes += sizeof(double*)*Nx + sizeof(double)*Nx*Ny;
    
    int i;
    // Now need to set the pointers to each row of the matrix.
    for(i=0; i<(int)Nx; i++){
        (*A)[i] = &A_vals[i*Ny];
    }
}

void free_dmatrix(dmatrix A){
    free(A[0]);
    free(A);
}

void allocate_complex_pointer_matrix(complex_t ****A, size_t N, long int *bytes){
    *A = (complex_t***) malloc(sizeof(complex_t**)*N);
    complex_t **A_vals = (complex_t**) malloc(sizeof(complex_t*)*N*N);
    
    bytes += sizeof(complex_t**)*N + sizeof(complex_t*)*N*N;
    
    int i;
    // Now need to set the pointers to each row of the matrix.
    for(i=0; i<(int)N; i++){
        (*A)[i] = &A_vals[i*N];
    }
}

/** Pre scan of input file
 * Scan the input file first and allocate memory for the various components
 * and the interferometer matrix
 *
 * \param fp the file pointer
 *
 * \todo refactor for readability
 *
 * \todo break up this routine into smaller pieces!!!
 */
void pre_scan(FILE *fp) {
    char s0[LINE_LEN], *s;
    double kbytes;

    // check that the file pointer is non-null
    assert(fp != NULL);

    endcomp = 0;
    lastparam = -1;

    long int bytes = 0;
    long int bytes1 = 0;
    long int bytes2 = 0;

    mem.num_quad_outputs = 0;
    mem.num_quantum_components = 0;
    mem.num_modulators = 0;
    mem.num_mirrors = 0;
    mem.num_beamsplitters = 0;
    mem.num_spaces = 0;
    mem.num_sagnacs = 0;
    mem.num_blocks = 0;
    mem.num_lenses = 0;
    mem.num_gratings = 0;
    mem.num_squeezers = 0;
    mem.num_light_inputs = 0;
    mem.num_diodes = 0;
    mem.num_signal_inputs = 0;
    mem.num_gauss_cmds = 0;
    mem.num_light_outputs = 0;
    mem.num_outputs = 0;
    mem.num_beam_param_outputs = 0;
    mem.num_attributes = 0;
    mem.num_cavities = 0;
    mem.num_gnuterm_cmds = 0;
    mem.num_pyterm_cmds = 0;
    mem.num_diff_cmds = 0;
    mem.num_constants = 0;
    mem.num_scale_cmds = 0;
    mem.hg_mode_order = 0;
    mem.num_node_names = 0;
    mem.num_nodes = 1; // why is this set to 1 ???
    mem.num_dump_nodes = 0;
    mem.num_components = 0;
    mem.num_func_cmds = 0;
    mem.num_put_cmds = 0;
    mem.num_tmp_put_cmds = 0;
    mem.num_locks = 0;
    mem.num_set_cmds = 0;
    mem.num_transfer_funcs = 0;
    mem.num_surface_motion_maps = 0;
    mem.num_openloopgains = 0;
    mem.num_force_out = 0;
    mem.num_homodynes = 0;
    mem.num_mirror_dithers = 0;
    mem.num_bs_dithers = 0;
    mem.num_dof = 0;
    mem.num_minimize = 0;
    mem.num_minimizeN = 0;
    
    rewind(fp);
    // Check for replacement of init variables
    // TODO: move deriv_h here from kat_read and maybe add more params
    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

        if (prepare_line(s, 1)) {
            continue;
        }

        if (strncasecmp(s, "lambda", 5) == 0) {
            read_lambda(s);
        }
    }

    rewind(fp);

    // Count number of variables first
    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

        if (prepare_line(s, 1)) {
            continue;
        }

        if (string_matches(s, "gnuplot")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "python")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "/*")) {
            check_for_star(s);
            read_init_dummy(fp, 2);
        } else if (string_matches(s, "const")) {
            mem.num_constants++;
        } else if(strstr(s, "$fs") != NULL || strstr(s, "$mfs") != NULL) {
            // check if there are any inline variables that will later result in
            // an extra put command
            mem.num_tmp_put_cmds++;
            mem.num_put_cmds++;
        }
    }
    
    // Allocate memory for temporary put command string storage
    if(mem.num_tmp_put_cmds){
        bytes += (mem.num_tmp_put_cmds + 1) * sizeof (char) * LINE_LEN;
        
        inter.tmp_put_cmds = (char**) calloc(mem.num_tmp_put_cmds, sizeof(char*));
        
        int i;
        for(i=0; i<mem.num_tmp_put_cmds; i++){
            inter.tmp_put_cmds[i] = (char*) calloc(LINE_LEN, sizeof(char));
        }
    }

    // Allocate memory for variables
    inter.constant_list = (constant_t *) calloc(mem.num_constants + 1, sizeof (constant_t));
    if (inter.constant_list == NULL) {
        gerror("Memeory allocation error no. 0\n");
    }
    
    bytes += (mem.num_constants + 1) * sizeof (constant_t);

    rewind(fp);

    // Read in variables
    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

        if (prepare_line(s, 1)) {
            continue;
        }

        if (string_matches(s, "gnuplot")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "python")) {
          read_init_dummy(fp, 0);
        } else if (string_matches(s, "/*")) {
            read_init_dummy(fp, 1);
        } else if (string_matches(s, "const")) {
            read_const(s);
        } else if (string_matches(s, "xparam")
                || string_matches(s, "x2param")
                || string_matches(s, "x3param")) {
            gerror("line '%s':\nthe 'xparam' command has been replaced "
                    "in version 0.99, see the manual\n"
                    "for the new commands 'set', 'func' and 'put'\n", s);
        }
    }

    // Sort variables so that the longest names are first
    sort_variable_names();

    // then check for debug level
    rewind(fp);
    
    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */
        
        if (prepare_line(s, 1)) {
            continue;
        }

        insert_constants_values(s);

        if (string_matches(s, "gnuplot")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "python")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "/*")) {
            read_init_dummy(fp, 1);
        } else if (string_matches(s, "debug")) {
            read_debug(s);
        }
    }

    if (options.quiet) {
        inter.debug = 0;
    } else if (inter.debug & 1) {
        message("\n\n-------debug output--------\n");
        dump_constants(stdout);
    }

    if (inter.debug & 512) {
        message("Scanning input file:\n");
        fflush(stdout);
    }

    // count number of components

    rewind(fp);

    int modulation = 0;
    int sq_carrier_pairs = 0;
    
    while (fgets(s0, LINE_LEN - 1, fp) != NULL) {
        s = s0 + strspn(s0, WHITESPACE); /* skip whitespace */

        if (prepare_line(s, 1)) {
            continue;
        }

        insert_constants_values(s);

        if (string_matches(s, "gnuplot")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "python")) {
            read_init_dummy(fp, 0);
        } else if (string_matches(s, "/*")) {
            read_init_dummy(fp, 1);
        } else if (string_matches(s, "mod")) {
            mem.num_modulators++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
            modulation += get_modulation_order(s);
        } else if (string_matches(s, "block ")){
            mem.num_nodes += 2;
            mem.num_blocks++;
            mem.num_node_names++;
        } else if (string_matches(s, "m* ") ||
                string_matches(s, "m1 ") ||
                string_matches(s, "m2 ") ||
                string_matches(s, "m ") ||
                string_matches(s, "mirror ") ||
                string_matches(s, "mirror* ") ||
                string_matches(s, "mirror1 ") ||
                string_matches(s, "mirror2 ")) {
            mem.num_mirrors++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
        } else if (string_matches(s, "tf ") || string_matches(s, "tf2 ")){
            mem.num_transfer_funcs++;
        } else if (string_matches(s, "bs* ") ||
                string_matches(s, "bs2 ") ||
                string_matches(s, "bs1 ") ||
                string_matches(s, "bs ") ||
                string_matches(s, "beamsplitter* ") ||
                string_matches(s, "beamsplitter2 ") ||
                string_matches(s, "beamsplitter1 ") ||
                string_matches(s, "beamsplitter ")) {
            mem.num_beamsplitters++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 4;
        } else if (string_matches(s, "gr") &&
                NOT string_matches(s, "grating")) {
            int num = s[2];

            if (num != 9 && num != 32) {
                if (num < 50 || num > 54) {
                    num = 2;
                } else {
                    num -= 48;
                }
            } else {
                num = 2;
            }
            mem.num_gratings++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + num;
        } else if (string_matches(s, "sq ") ||
                   string_matches(s, "squeezer ")) {
            mem.num_light_inputs++;
            mem.num_node_names++;
            mem.num_nodes++;
        } else if(string_matches(s, "sq* ")) {
            mem.num_light_inputs++;
            mem.num_node_names++;
            mem.num_nodes++;
            sq_carrier_pairs += 1;
        } else if (string_matches(s, "sagnac ")) {
            mem.num_sagnacs++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
        } else if (string_matches(s, "s ") ||
                string_matches(s, "space ")) {
            mem.num_spaces++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
        } else if (string_matches(s, "lens ") || string_matches(s, "lens* ")
                    || string_matches(s, "lens** ") || string_matches(s, "lens*** ")) {
            mem.num_lenses++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
        } else if (string_matches(s, "l ") ||
                string_matches(s, "laser ") ||
                string_matches(s, "light ")) {
            mem.num_light_inputs++;
            mem.num_node_names++;
            mem.num_nodes++;
        }            // if we have a dump, this is now counted as a "real" node
            // \todo this is a waste of time, only the first token is read, hence
            // dump is never read
        else if (string_matches(s, "dump")) {
            mem.num_dump_nodes++;
            mem.num_node_names++;
            mem.num_nodes++;
        } else if (string_matches(s, "isol ") ||
                string_matches(s, "diode ")) {
            mem.num_diodes++;
            mem.num_node_names++;
            mem.num_nodes = mem.num_nodes + 2;
            
        } else if (string_matches(s, "dbs ")) {
            mem.num_dbss++;
            mem.num_node_names++;
            mem.num_nodes += 4;
            
        } else if (string_matches(s, "fsig ")) {
            mem.num_signal_inputs++;
            mem.num_node_names++;
        } else if (string_matches(s, "dof ")) {
            mem.num_dof++;
        } else if (string_matches(s, "variable ") ||
                   string_matches(s, "var ")) {
            mem.num_variables++;
        } else if (string_matches(s, "gauss ") ||
                string_matches(s, "gauss* ") ||
                string_matches(s, "gauss** ")) {
            mem.num_gauss_cmds++;
            mem.num_node_names++;
        } else if (string_matches(s, "qd ")){
            mem.num_light_outputs++;
            mem.num_node_names++;
            mem.num_outputs++;
        } else if (string_matches(s, "xd ")){
            mem.num_node_names++;
            mem.num_motion_outputs++;
            mem.num_outputs++;
        } else if (string_matches(s, "hd ") || string_matches(s, "qhd ")  || string_matches(s, "qhdN ")  || string_matches(s, "qhdS ")){
            mem.num_light_outputs += 2;
            mem.num_node_names += 2;
            mem.num_outputs++;
            mem.num_homodynes++;
        } else if (string_matches(s, "shot ") ||
                string_matches(s, "qshot ") ||
                string_matches(s, "qshotS ") ||
                string_matches(s, "qshotN ") ||
                string_matches(s, "pdS") ||
                string_matches(s, "pdN") ||
                string_matches(s, "pd") ||
                string_matches(s, "ad ") ||
                string_matches(s, "sd ") ||
                string_matches(s, "sd* ") ||
                string_matches(s, "qnoised ") ||
                string_matches(s, "qnoisedS ") ||
                string_matches(s, "qnoisedN ") ||
                string_matches(s, "beam ")) {
            mem.num_node_names++;
            mem.num_light_outputs++;
            mem.num_outputs++;

            // if we have a 'shot' detector, we must set inter.shotnoise
            // appropriately
            if (string_matches(s, "shot ")) {
                inter.shotnoise = 4;
            }
        } else if (string_matches(s, "gouy ") ||
                string_matches(s, "bp ")) {
            mem.num_node_names++;
            mem.num_beam_param_outputs++;
            mem.num_outputs++;
        } else if (string_matches(s, "cp ")) {
            mem.num_node_names++;
            mem.num_cavity_param_outputs++;
            mem.num_outputs++;
        } else if (string_matches(s, "conv ")) {
            mem.num_node_names++;
            mem.num_convolution_outputs++;
            mem.num_outputs++;
        } else if (string_matches(s, "mp ")) {
            mem.num_node_names++;
            mem.num_mirror_phase_outputs++;
            mem.num_outputs++;
        } else if (string_matches(s, "attr ")) {
            mem.num_attributes++;
            mem.num_node_names++;
        } else if (string_matches(s, "cav ") ||
                string_matches(s, "cavity ")) {
            mem.num_cavities++;
            mem.num_node_names++;
        } else if (string_matches(s, "gnuterm")) {
            mem.num_gnuterm_cmds++;
        } else if (string_matches(s, "pyterm")) {
            mem.num_pyterm_cmds++;
        } else if (string_matches(s, "diff")) {
            mem.num_diff_cmds++;
        } else if (string_matches(s, "scale")) {
            mem.num_scale_cmds++;
        } else if (string_matches(s, "minimize ") || string_matches(s, "maximize ")) {
            mem.num_minimize++;
            mem.num_outputs++;
        } else if (string_matches(s, "minimizeN ") || string_matches(s, "maximizeN ")) {
            mem.num_minimizeN++;
            mem.num_outputs += 10;
        } else if (string_matches(s, "lock")) {
            mem.num_node_names++;
            mem.num_locks++;
            mem.num_outputs++;
        } else if (string_matches(s, "put")) {
            mem.num_node_names++;
            mem.num_put_cmds++;
        } else if (string_matches(s, "func")) {
            mem.num_node_names++;
            mem.num_func_cmds++;
            mem.num_outputs++;
        } else if (string_matches(s, "map")) {
            mem.num_maps++;
        } else if (string_matches(s, "smotion")) {
            mem.num_surface_motion_maps++;
        } else if (string_matches(s, "vacuum")) {
            read_vacuum(s, true);
        } else if (string_matches(s, "set")) {
            mem.num_set_cmds++;
            mem.num_node_names++;
        } else if (string_matches(s, "slink")) {
            mem.num_feedbacks++;
        } else if (string_matches(s, "dither")) {
            mem.num_mirror_dithers++;
            mem.num_bs_dithers++;
            modulation += get_dither_order(s);
        } else if (string_matches(s, "fd")) {
            mem.num_force_out++;
            mem.num_outputs++;
        } else if (string_matches(s, "pgaind")) {
            mem.num_openloopgains++;
            mem.num_outputs++;
        }            // read maxtem, order of TEM modes
        else if (string_matches(s, "maxtem ")) {
            read_maxtem(s);
        }            /*
    // read specifier for coupling coefficient computation
    else if (string_matches(s, "knm ")) {
      read_knm(s);
    }
*/
            // if noise is used, read in and set the relevant inter.shotnoise value
        else if (string_matches(s, "noise")) {
            read_noise(s);
        }
        else if (string_matches(s, "mf") || string_matches(s, "fadd")){
            read_manual_frequency(s, 1);
        } else {
            int i = check_mhomodyne(s);
            
            if (i > 0) {
                mem.num_light_outputs += i*2;
                mem.num_node_names += i*2;
                mem.num_outputs++;
                mem.num_mhomodynes++;
            }
        }
    }

    if (inter.debug & 512) {
        message("  using TEM up to n+m=%d\n", mem.hg_mode_order);
        fflush(stdout);
    }

    // if init file has been found, read in pdtypes now
    // (now the maximum TEM order is known!)
    if (init.init_file_is_found) {
        read_init_pdtype();
    }

    if (inter.debug & 1024) {
        message("pdtypes:\n");
        dump_pdtype(stdout);
        fflush(stdout);
    }

    // compute number of derived parameters
    // ------------------------------------------------------------
    // number of names = number of names so far plus all node names
    mem.num_node_names += mem.num_nodes;
    // number of electric fields per node per frequency 
    // (a function of the order of HG modes)
    mem.num_fields = (int) (mem.hg_mode_order + 1) * (mem.hg_mode_order + 2) / 2;
       
    // the number of quadratures
    mem.num_quadratures = 2;

    // number of components, used for allocating memory for component numbers
    mem.num_components = mem.num_mirrors + mem.num_spaces + mem.num_beamsplitters
            + mem.num_modulators + mem.num_light_inputs
            + mem.num_diodes + mem.num_lenses + mem.num_dbss +
            + mem.num_gratings + mem.num_squeezers + mem.num_sagnacs + mem.num_blocks;

    debug_msg("kat_mem(): found %d components in interferometer\n", mem.num_components);

    
    // Every carrier might get modulated by further RF sidebands so include that in number.
    // Also need to include extra carrier frequency included in dual engtangled squeezer inputs
    mem.num_frequencies += (mem.num_light_inputs + sq_carrier_pairs) * (1 + modulation);
    
    // if we have signal frequencies then we have 2 extra frequencies per
    // counted carrier fields so far
    if(mem.num_signal_inputs > 0) {
        mem.num_frequencies *= 3;
    } 
    
    // number of matrix pointers (=2*no.of.nodes*no.of.fields)^2
    mem.num_pointers = 2 * mem.num_nodes * mem.num_fields * 2 * mem.num_nodes * mem.num_fields;

    // calculate the number of reduced nodes
    mem.num_reduced_nodes = mem.num_nodes - mem.num_dump_nodes;

    if (inter.debug & 512) {
        message("  %d mirrors, ", mem.num_mirrors);
        message("%d beamsplitters, ", mem.num_beamsplitters);
        message("%d spaces, ", mem.num_spaces);
        message("%d modulators\n", mem.num_modulators);
        message("%d dbs, ", mem.num_dbss);
        message("%d diode, ", mem.num_diodes);
        message("%d lenses, ", mem.num_lenses);
        message("%d gratings, ", mem.num_gratings);
        message("%d squeezers, ", mem.num_squeezers);
        message("%d lasers, ", mem.num_light_inputs);
        message("%d detectors\n", mem.num_outputs);
        message("  %d attributes, ", mem.num_attributes);
        message("%d gauss, ", mem.num_gauss_cmds);
        message("%d cavities, ", mem.num_cavities);
        message("%d constants\n", mem.num_constants);
        message("%d variables\n", mem.num_variables);
        message("  %d scales, ", mem.num_scale_cmds);
        message("  %d puts, ", mem.num_put_cmds);
        message("  %d locks, ", mem.num_locks);
        message("  %d functions, ", mem.num_func_cmds);
        message("  %d sets, ", mem.num_set_cmds);
        message("%d differentiations, ", mem.num_diff_cmds);
        message("%d gnuterms\n", mem.num_gnuterm_cmds);
        message("%d pyterms\n", mem.num_pyterm_cmds);
        message("  derived paramters:\n");
        message("  %d fields, ", mem.num_fields);
        message("  %d frequencies, ", mem.num_frequencies);
        message("  %d nodes, ", mem.num_nodes);
        message("  %d dump nodes\n", mem.num_dump_nodes);
        message("  %d reduced nodes\n", mem.num_reduced_nodes);
        message("  %d names, ", mem.num_node_names);
        message("  %d pointers\n", mem.num_pointers);
        fflush(stdout);
    }

    // alloctate memeory for reading input file, print 
    // possible allocation failure or size of allocated memory

    int err = allocate_memory(&bytes, &bytes1, &bytes2);
    if (err != 0) {
        warn("Memeory allocation error no. %d, after succesful\n", err);
        warn("allocation of \n");
        bytes += bytes1 + bytes2;

        if (bytes > 1048576) {
            kbytes = ((double) bytes) / 1048576.;
            warn("(%.3g Mbytes).\n", kbytes);
        } else {
            kbytes = ((double) bytes) / 1024.;
            warn("(%.3g Kbytes).\n", kbytes);
        }
        my_finish(1);
    }
}

//! Allocate memeory for components and variables needed for reading input file

/*!
 * \param bytes ???
 * \param bytes1 ???
 * \param bytes2 ???
 *
 * \todo separate memory allocation subsections into separate subroutines
 *
 * \todo options.check branch is untested
 */
int allocate_memory(long int *bytes, long int *bytes1, long int *bytes2) {
    double kbytes, kbytes1;
    char *s, *s1;
    char munit[7] = "Mbytes";
    char kunit[7] = "Kbytes";

    // allocate memory for gnuterm commands
    int error_code = allocate_memory_for_gnuterms(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for mechanical transfer functions
    error_code = allocate_memory_for_mech_transfer_funcs(bytes);
    if (error_code != 0) {
        return error_code;
    }
    
    // allocate memory for pyterm commands
    error_code = allocate_memory_for_pyterms(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for carrier frequencies
    error_code = allocate_memory_for_frequency_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for the amplitude list
    error_code = allocate_memory_for_amplitude_list(bytes1);
    if (error_code != 0) {
        return error_code;
    }
    
    // allocate memory for mirrors
    error_code = allocate_memory_for_mirror_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for beam splitters
    error_code = allocate_memory_for_beamsplitter_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for gratings
    error_code = allocate_memory_for_grating_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for squeezers
    error_code = allocate_memory_for_squeezer_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for spaces
    error_code = allocate_memory_for_space_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
    
    // allocate memory for sagnacs
    error_code = allocate_memory_for_sagnac_list(bytes);
    if (error_code != 0)
        return error_code;

    error_code = allocate_memory_for_block_list(bytes);
    if (error_code != 0)
        return error_code;
    
    // allocate memory for input light fields
    error_code = allocate_memory_for_light_input_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    error_code = allocate_memory_for_motion_detector_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
   
    
    // allocate memory for output light fields
    error_code = allocate_memory_for_detector_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for outputs
    error_code = allocate_memory_for_output_data_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for output beam parameter
    error_code = allocate_memory_for_beam_param_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
    // allocate memory for output cavity parameter
    error_code = allocate_memory_for_cavity_param_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for output convolution
    error_code = allocate_memory_for_convolution_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for output mirror phase
    error_code = allocate_memory_for_mirror_phase_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for modulators
    error_code = allocate_memory_for_modulator_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for frequency signals
    error_code = allocate_memory_for_signal_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for a derivative
    error_code = allocate_memory_for_derivative_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for scale commands
    error_code = allocate_memory_for_scale_cmd_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for diodes
    error_code = allocate_memory_for_diode_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for diodes
    error_code = allocate_memory_for_dbs_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
    
    // allocate memory for variables
    error_code = allocate_memory_for_variable_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
    // allocate memory for lenses
    error_code = allocate_memory_for_lens_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    error_code = allocate_memory_for_feedback_list(bytes);
    if (error_code != 0) {
        return error_code;
    }
    
    inter.minimizer = NULL;
    inter.minimizerN = NULL;

    if(mem.num_minimize) {
        inter.minimizer = (minimizer_t*) calloc(1, sizeof(minimizer_t));
    }

    if(mem.num_minimizeN) {
        inter.minimizerN = (minimizerN_t*) calloc(1, sizeof(minimizerN_t));
    }
    
    if (mem.num_minimizeN || mem.num_minimize){
        inter.do_minimize = true;
    } else {
        inter.do_minimize = false;
    }

    if(mem.num_maps || mem.num_surface_motion_maps) {
        inter.rom_maps = (rom_map_t*) calloc(mem.num_maps + mem.num_surface_motion_maps, sizeof(rom_map_t));
    }
    
    if (mem.num_dof){
        inter.dof_list = (dof_t*) calloc(mem.num_dof+1, sizeof(dof_t));
        
        if (inter.dof_list == NULL) {
            return (289);
        }
        
        *bytes += (mem.num_dof + 1) * sizeof (dof_t);
    }
    
    if (mem.num_mirror_dithers){
        inter.mirror_dither_list = (mirror_t**) calloc(mem.num_mirror_dithers+1, sizeof(mirror_t*));
        
        if (inter.mirror_dither_list == NULL) {
            return (290);
        }
        
        *bytes += (mem.num_mirror_dithers + 1) * sizeof (mirror_t*);
    }
    
    if (mem.num_bs_dithers){
        inter.bs_dither_list = (beamsplitter_t**) calloc(mem.num_bs_dithers+1, sizeof(beamsplitter_t*));
        
        if (inter.bs_dither_list == NULL) {
            return (290);
        }
        
        *bytes += (mem.num_bs_dithers + 1) * sizeof (beamsplitter_t*);
    }
    
    if(mem.num_homodynes){
        inter.homodyne_list = (homodyne_t*) calloc(mem.num_homodynes+1, sizeof(homodyne_t));
        
        if (inter.homodyne_list == NULL) {
            return (291);
        }
        
        *bytes += (mem.num_homodynes + 1) * sizeof (homodyne_t);
    }
    
    if(mem.num_mhomodynes){
        inter.mhomodyne_list = (mhomodyne_t*) calloc(mem.num_mhomodynes+1, sizeof(mhomodyne_t));
        
        if (inter.mhomodyne_list == NULL) {
            return (291);
        }
        
        *bytes += (mem.num_mhomodynes + 1) * sizeof (mhomodyne_t);
    }
    
    if (mem.num_surface_motion_maps){
        inter.surface_motion_map_list = (surface_map_t*) calloc(mem.num_surface_motion_maps +1, sizeof(surface_map_t));
        
        if (inter.surface_motion_map_list == NULL) {
            return (291);
        }
        
        *bytes += (mem.num_surface_motion_maps + 1) * sizeof (surface_map_t);
    }
    
    if (mem.num_frequencies){
        inter.tmp_f_list = (frequency_t*) calloc(mem.num_frequencies + 1, sizeof(frequency_t));
        
        if (inter.tmp_f_list == NULL) {
            return (29);
        }
        
        *bytes += (mem.num_frequencies + 1) * sizeof (frequency_t);
    }
    
    if(mem.num_force_out){
        inter.force_out_list = (force_out_t*) calloc(mem.num_force_out, sizeof(force_out_t));
        
        if (inter.force_out_list == NULL) {
            return (29);
        }
        *bytes += (mem.num_force_out) * sizeof (force_out_t);
    }
    
    if(mem.num_openloopgains){
        inter.openloopgain_list = (openloopgain_out_t*) calloc(mem.num_openloopgains, sizeof(openloopgain_out_t));
        
        if (inter.openloopgain_list == NULL) {
            return (29);
        }
        *bytes += (mem.num_openloopgains) * sizeof (openloopgain_out_t);
    }
    
    // allocate memory for attributes
    if (mem.num_attributes) {
        inter.attr_list = (attr_t *) calloc(mem.num_attributes + 1, sizeof (attr_t));
        if (inter.attr_list == NULL) {
            return (29);
        }
        *bytes += (mem.num_attributes + 1) * sizeof (attr_t);
    }

    // allocate memory for a gauss command
    if (mem.num_gauss_cmds) {
        inter.gauss_list =
                (gauss_t *) calloc(mem.num_gauss_cmds + 1, sizeof (gauss_t));
        if (inter.gauss_list == NULL) {
            return (30);
        }
        *bytes += (mem.num_gauss_cmds + 1) * sizeof (gauss_t);
    }

    // allocate memory for a cavity
    if (mem.num_cavities) {
        inter.cavity_list = (cavity_t *)
                calloc(mem.num_cavities + 1, sizeof (cavity_t));
        if (inter.cavity_list == NULL) {
            return (31);
        }
        *bytes += (mem.num_cavities + 1) * sizeof (cavity_t);
    }

    // allocate memory for a node
    if (mem.num_nodes) {
        inter.node_list = (node_t *) calloc(mem.num_nodes + 1, sizeof (node_t));
        if (inter.node_list == NULL) {
            return (32);
        }
        
        inter.node_conn_list = (node_connections_t*) calloc(mem.num_nodes +1, sizeof(node_connections_t));
        if (inter.node_list == NULL) {
            return (32);
        }
                        
        *bytes += (mem.num_nodes + 1) * sizeof (node_t);
        *bytes += (mem.num_nodes + 1) * sizeof (node_connections_t);
    }

    // allocate memory for the node names
    if (mem.num_node_names) {
        mem.names = (char **) calloc(mem.num_node_names + 1, sizeof (char *));
        if (mem.names == NULL) {
            return (33);
        }
        *bytes += (mem.num_node_names + 1) * sizeof (char *);
    }

    if (mem.num_components) {
        vlocal.component_trace_list = (int *) calloc(mem.num_components + 1, sizeof (int));
        if (vlocal.component_trace_list == NULL) {
            return (34);
        }
        *bytes += (mem.num_components + 1) * sizeof (int);
    }

    if (mem.num_nodes) {
        vlocal.trace_c1 = (int *) calloc((mem.num_nodes + 1)*2, sizeof (int));
        if (vlocal.trace_c1 == NULL) {
            return (98);
        }
        *bytes += (mem.num_nodes + 1) * 2 * sizeof (int);

        vlocal.trace_c2 = (int *) calloc(mem.num_nodes + 1, sizeof (int));
        if (vlocal.trace_c2 == NULL) {
            return (97);
        }
        *bytes += (mem.num_nodes + 1) * sizeof (int);
    }

    int i;
    for (i = 0; i < mem.num_outputs; i++) {
        inter.output_data_list[i].Omax[0] = -1e20;
        inter.output_data_list[i].Omax[1] = -1e20;
        inter.output_data_list[i].Omin[0] = 1e20;
        inter.output_data_list[i].Omin[1] = 1e20;
    }

    if (mem.num_light_outputs) {
        transfer = (int *) calloc(mem.num_light_outputs + 1, sizeof (int));
        if (transfer == NULL) {
            return (35);
        }
        *bytes += (mem.num_light_outputs + 1) * sizeof (int);
    }

    // allocate memory for puts
    if (mem.num_put_cmds) {
        inter.put_list = (put_command_t *) calloc(mem.num_put_cmds + 1, sizeof (put_command_t));
        if (inter.put_list == NULL) {
            return (89);
        }
        *bytes += (mem.num_put_cmds + 1) * sizeof (put_command_t);
    }

    // allocate memory for locks
    error_code = allocate_memory_for_locks(bytes);
    if (error_code != 0) {
        return error_code;
    }

    // allocate memory for functions
    error_code = allocate_memory_for_function_list(bytes);
    if (error_code != 0) {
        return error_code;
    }

    if (mem.num_func_cmds || mem.num_locks) {
        inter.is_locked =
                (bool *) calloc(mem.num_func_cmds + mem.num_locks + 2, sizeof (bool));
        if (inter.is_locked == NULL) {
            return (87);
        }
    }

    // allocate memory for sets
    if (mem.num_set_cmds) {
        inter.set_list = (set_command_t *) calloc(mem.num_set_cmds + 1, sizeof (set_command_t));
        if (inter.set_list == NULL) {
            return (85);
        }
        *bytes += (mem.num_set_cmds + 1) * sizeof (set_command_t);
    }

    //s_s = (signal_t**) calloc(mem.num_frequencies + 1, sizeof (signal_t*));
    t_s = (int *) calloc(mem.num_frequencies + 1, sizeof (int));
    f_s = (double *) calloc(mem.num_frequencies + 1, sizeof (double));

    if (f_s == NULL || t_s == NULL) {
        return (36);
    }
    
    *bytes1 += (mem.num_frequencies + 1) * sizeof (int);
    *bytes1 += (mem.num_frequencies + 1) * sizeof (double);

    // ddb - this allocation is done once the total number of vacuum inputs has
    // been calculated now in kat_quant.c so not to waste so much memory.
    // allocate memory for the quantum noise list
    //error_code = allocate_memory_for_quantum_amplitude_list(bytes1);
    //if (error_code != 0) {
    //    return error_code;
    //}

    // TODO this memory is for the old Fortan numerical integration and 
    // can be removed once we fully moved to the new integration routine
    if (inter.tem_is_set && inter.knm > 1) {
        mem.worklen = floor((init.maxintop - 65) / 65.0 * 8.0 + 43);
        mem.work = (double *) calloc(mem.worklen + 1, sizeof (double));

        if (mem.work == NULL) {
            return (96);
        }

        *bytes1 += (mem.worklen + 1) * sizeof (double);
    } else {
        mem.worklen = 0;
    }

    // allocate memory for some temporary complex variales in kat_calc
    vlocal.zsolution =
            (complex_t *) calloc(mem.num_outputs + 1, sizeof (complex_t));

    if (vlocal.zsolution == NULL) {
        return (41);
    }
    *bytes1 += (mem.num_outputs + 1) * sizeof (complex_t);

    vlocal.zderiv1 =
            (complex_t *) calloc(mem.num_outputs + 1, sizeof (complex_t));

    if (vlocal.zderiv1 == NULL) {
        return (42);
    }
    *bytes1 += (mem.num_outputs + 1) * sizeof (complex_t);

    vlocal.zderiv2 =
            (complex_t *) calloc(mem.num_outputs + 1, sizeof (complex_t));

    if (vlocal.zderiv2 == NULL) {
        return (43);
    }
    *bytes1 += (mem.num_outputs + 1) * sizeof (complex_t);

    if(mem.num_fields > 0 && inter.tem_is_set){ 
        // temp memory for knm matrix operations
        allocate_zmatrix(&tmp_knm, mem.num_fields, bytes);
    }
    
    // allocate memory for quantum node list
    if(mem.num_quantum_components || inter.all_quantum_components) {
        if(inter.all_quantum_components) {
            // if we are setting all components potentially as quantum noise
            // inputs we need to prepare for all of them to possibly be used
            assert(mem.num_quantum_components == 0);
            mem.num_quantum_components = mem.num_components;
        }
        
        inter.quantum_components = (int *) calloc(mem.num_quantum_components + 1, sizeof (int));
        if (inter.quantum_components == NULL) {
            return (44);
        }
        *bytes += (mem.num_quantum_components + 1) * sizeof (int);
    }
    
    /*
    // allocate memory for sparse algorithm quantum noise stuff
    if (inter.qcorr) {
        // quantum version of sparse matrix diagonal
        debug_msg("Allocating memory for quantum noise matrices\n");
        klu.Qdiag1 =
                (complex_t **) calloc(mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1, sizeof (complex_t *));

        if (klu.Qdiag1 == NULL) {
            return (45);
        }
        *bytes2 += (mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1) * sizeof (complex_t *);
        klu.Qdiag = klu.Qdiag1 + 1;

        // quantum verison of sparse matrix RHS
        klu.Qsprhs1 =
                (complex_t *) calloc(mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1, sizeof (complex_t));

        if (klu.Qsprhs1 == NULL) {
            return (46);
        }
        *bytes2 += (mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1) * sizeof (complex_t);
        klu.Qsprhs = klu.Qsprhs1 + 1;

        // quantum version of sparse matrix solution
        klu.Qspsol1 =
                (complex_t *) calloc(mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1, sizeof (complex_t));

        if (klu.Qspsol1 == NULL) {
            return (47);
        }
        *bytes2 += (mem.num_nodes
                * (mem.hg_mode_order + 1)
                * (mem.hg_mode_order + 2) + 1) * sizeof (complex_t);
        klu.Qspsol = klu.Qspsol1 + 1;
    }
    */
    // check options
    if (options.check) {
        mem.ilist1 = (int *) calloc(mem.num_pointers + 1, sizeof (int));

        if (mem.ilist1 == NULL) {
            return (48);
        }
        *bytes2 += (mem.num_pointers + 1) * sizeof (int);

        mem.ilist2 = (int *) calloc(mem.num_pointers + 1, sizeof (int));

        if (mem.ilist2 == NULL) {
            return (49);
        }
        *bytes2 += (mem.num_pointers + 1) * sizeof (int);

        mem.jlist1 = (int *) calloc(mem.num_pointers + 1, sizeof (int));

        if (mem.jlist1 == NULL) {
            return (50);
        }
        *bytes2 += (mem.num_pointers + 1) * sizeof (int);

        mem.jlist2 = (int *) calloc(mem.num_pointers + 1, sizeof (int));

        if (mem.jlist2 == NULL) {
            return (51);
        }
        *bytes2 += (mem.num_pointers + 1) * sizeof (int);
    }

    *bytes1 += *bytes2;

    if (inter.debug & 512) {
        message("Initial memory allocated:\n");

        if (*bytes > 1048576) {
            kbytes = ((double) *bytes) / 1048576.;
            s = munit;
        } else {
            kbytes = ((double) *bytes) / 1024.;
            s = kunit;
        }

        if (*bytes1 > 1048576) {
            kbytes1 = ((double) *bytes1) / 1048576.;
            s1 = munit;
        } else {
            kbytes1 = ((double) *bytes1) / 1024.;
            s1 = kunit;
        }

        message("  optical setup=%.4g %s, internal variables=%.4g %s.\n", kbytes,
                s, kbytes1, s1);
        fflush(stdout);
    }

    return (0);
}

int allocate_memory_for_mech_transfer_funcs(long int *bytes){
    if(mem.num_transfer_funcs > 0){
        inter.tf_list = (transfer_func_t*) calloc(mem.num_transfer_funcs, sizeof(transfer_func_t));

        if(inter.tf_list == NULL)
            return 1;
    }
    *bytes += (mem.num_transfer_funcs +1) * sizeof(transfer_func_t);
    return 0;
}

//! Allocate memory for gnuplot commands

/*!
 * \todo gnuterms -> gnuterm_commands???
 *
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_gnuterms(long int *bytes) {
    inter.gnuterm = (int *) calloc(mem.num_gnuterm_cmds + 1, sizeof (int));
    inter.gnutermfn =
            (char **) calloc(mem.num_gnuterm_cmds + 1, sizeof (char *));

    if (inter.gnuterm == NULL || inter.gnutermfn == NULL) {
        return (1);
    }
    *bytes += (mem.num_gnuterm_cmds + 1) * sizeof (int)
            + (mem.num_gnuterm_cmds + 1) * sizeof (char *);

    return 0;
}

//! Allocate memory for pyterm commands

/*!
 * 
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_pyterms(long int *bytes) {
    inter.pyterm = (int *) calloc(mem.num_pyterm_cmds + 1, sizeof (int));
    inter.pytermfn =
            (char **) calloc(mem.num_pyterm_cmds + 1, sizeof (char *));

    if (inter.pyterm == NULL || inter.pytermfn == NULL) {
        return (1);
    }
    *bytes += (mem.num_pyterm_cmds + 1) * sizeof (int)
            + (mem.num_pyterm_cmds + 1) * sizeof (char *);

    return 0;
}


//! Allocate memory for carrier frequency list

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_frequency_list(long int *bytes) {
    if (mem.num_frequencies) {
        inter.f = (double *) calloc(mem.num_frequencies + 1, sizeof (double));
        inter.t = (int *) calloc(mem.num_frequencies + 1, sizeof (int));

        if (inter.f == NULL || inter.t == NULL) {
            return (2);
        }
        *bytes += (mem.num_frequencies + 1) * sizeof (double)
                + (mem.num_frequencies + 1) * sizeof (int);
        inter.f[0] = NO_FREQ;
    }
    return 0;
}

void free_memory_for_mirror_list() {
    int i;
    mirror_t *mirror;

    // this is a temporary map that was used to 
    // calculate knm's
    if (IS_MIRROR_KNM_ALLOCD(&mrtmap))
        mirror_knm_free(&mrtmap);

    for (i = 0; i < mem.num_mirrors; i++) {
        mirror = &inter.mirror_list[i];
        // here we check to see if any of the knm matrices were allocated memory
        // during the knm calculation process
        if (IS_MIRROR_KNM_ALLOCD(&mirror->knm_aperture)) mirror_knm_free(&(mirror->knm_aperture));
        if (IS_MIRROR_KNM_ALLOCD(&mirror->knm_map)) mirror_knm_free(&(mirror->knm_map));
        if (IS_MIRROR_KNM_ALLOCD(&mirror->knm_bayer_helms)) mirror_knm_free(&(mirror->knm_bayer_helms));
    }
}


int alloc_freq_coupling_matrix(int ***does_f_couple, int ***f_couple_order, int ***f_couple_allocd, long int *bytes, int err){
    // Allocate memory for frequency coupling matrix
    *does_f_couple = (int**) calloc(mem.num_frequencies, sizeof(int*));
    *f_couple_order = (int**) calloc(mem.num_frequencies, sizeof(int*));
    *f_couple_allocd = (int**) calloc(mem.num_frequencies, sizeof(int*));
    
    if (*does_f_couple == NULL || *f_couple_order == NULL || *f_couple_allocd == NULL)
        return err;

    *bytes += 3 * (mem.num_frequencies + 1) * sizeof(int*);
    
    int k;
    
    for(k=0; k < mem.num_frequencies; k++){
        (*does_f_couple)[k] =  (int*) calloc(mem.num_frequencies, sizeof(int));
        (*f_couple_order)[k] =  (int*) calloc(mem.num_frequencies, sizeof(int));
        (*f_couple_allocd)[k] =  (int*) calloc(mem.num_frequencies, sizeof(int));

        if ((*does_f_couple)[k] == NULL || (*f_couple_order)[k] == NULL || (*f_couple_allocd)[k] == NULL)
            return err;
        
        *bytes += 3*(mem.num_frequencies + 1) * sizeof(int*);
    }
    
    return 0;
}

int alloc_field_freq_ptrs_mod(complex_t ******f, long int *bytes, int errf, int errn, int errm){
    int i,j,k;
    
    (*f) = (complex_t*****) calloc(mem.num_frequencies + 1, sizeof(complex_t****));
    
    if (*f == NULL)
        return errf;
    
    *bytes += (mem.num_frequencies + 1) * sizeof(complex_t****);
    
    for(k=0; k < mem.num_frequencies; k++){
        (*f)[k] =  (complex_t****) calloc(mem.num_frequencies + 1, sizeof(complex_t***));
    
        if ((*f)[k] == NULL)
            return errf;

        *bytes += (mem.num_frequencies + 1) * sizeof(complex_t***);
    
        for(i=0; i<mem.num_frequencies; i++){
            (*f)[k][i] = (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));

            if((*f)[k][i]==NULL)
                return errn;

            *bytes = (mem.num_fields + 1) * sizeof (complex_t **);

            for(j=0; j<mem.num_fields; j++){
                (*f)[k][i][j] = (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));

                if((*f)[k][i][j]==NULL)
                    return errm;

                *bytes = (mem.num_fields + 1) * sizeof (complex_t **);
            }
        }
    }
    return 0;
}

int alloc_field_freq_ptrs(complex_t *****f, long int *bytes, int errf, int errn, int errm){
    int i,j;
    
    (*f) = (complex_t****) calloc(mem.num_frequencies + 1, sizeof(complex_t***));
    
    if (*f == NULL)
        return errf;
    
    *bytes += (mem.num_frequencies + 1) * sizeof(complex_t***);
    
    for(i=0; i<mem.num_frequencies; i++){
        (*f)[i] = (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));
        
        if((*f)[i]==NULL)
            return errn;
        
        *bytes = (mem.num_fields + 1) * sizeof (complex_t **);
                
        for(j=0; j<mem.num_fields; j++){
            (*f)[i][j] = (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));
        
            if((*f)[i][j]==NULL)
                return errm;
            
            *bytes = (mem.num_fields + 1) * sizeof (complex_t **);
        }
    }
    
    return 0;
}

//! Allocate memory for mirrors

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_mirror_list(long int *bytes) {
    if (mem.num_mirrors) {
        inter.mirror_list =
                (mirror_t *) calloc(mem.num_mirrors + 1, sizeof (mirror_t));

        if (inter.mirror_list == NULL) {
            return (3);
        }
        
        *bytes += (mem.num_mirrors + 1) * sizeof (mirror_t);
        
        alloc_knm_accel_mirror_aperture_mem(bytes);
        alloc_knm_accel_mirror_mem(bytes);
        mirror_knm_alloc(&mrtmap, bytes);
        
        int i;
        for (i = 0; i < mem.num_mirrors; i++) {
            inter.mirror_list[i].comp_index = i;
            
            mirror_knm_alloc(&inter.mirror_list[i].knm, bytes);
            mirror_knm_alloc(&inter.mirror_list[i].knm_no_rgouy, bytes);
            mirror_knm_alloc(&inter.mirror_list[i].knm_bayer_helms, bytes);
            mirror_knm_alloc(&inter.mirror_list[i].knm_romhom, bytes);
            mirror_knm_alloc(&inter.mirror_list[i].knm_aperture, bytes);
            mirror_knm_alloc(&inter.mirror_list[i].knm_map, bytes);
            
            mirror_t *m = &inter.mirror_list[i];
            int err;
            
            m->k11_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            m->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            m->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            m->k22_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));

            err = alloc_field_freq_ptrs_mod(&m->a11f, bytes, 4, 4, 5);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&m->a12f, bytes, 4, 4, 5);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&m->a21f, bytes, 4, 4, 5);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&m->a22f, bytes, 4, 4, 5);
            if(err) return err;
        } 
    }

    return 0;
}

/**
 * This allocates the memory for the mirror motions. It is not called in the usual
 * memory allocation but later after the reading of various parameters, this is 
 * so that memory is not wasted on lots of components that might not have radiation
 * pressure effects.
 * 
 * @param m mirror object
 * @param num_sig_freqs number of signal frequencies 
 * @return 
 */
int alloc_mirror_motions(mirror_t *m, int num_sig_freqs){
    assert(m != NULL);
    if(num_sig_freqs == 0) return 0;
    
    int f, i, j;
    long int bytes;
    
    if(m->mass > 0.0) m->num_motions++;
    if(m->Ix > 0.0)   m->num_motions++;
    if(m->Iy > 0.0)   m->num_motions++;
    
    m->num_motions += m->num_surface_motions;
    
    m->motion_type = (motion_type_t*) malloc(m->num_motions * sizeof(motion_type_t));
            
    // fills motion type array
    i=0;
    
    if(m->mass > 0.0) m->motion_type[i++] = Z;
    if(m->Ix > 0.0)   m->motion_type[i++] = ROTX;
    if(m->Iy > 0.0)   m->motion_type[i++] = ROTY;
    
    for(j=0; j < m->num_motions; j++){
        m->motion_type[i++] = SURFACE;
    }
    
    // now allocate memory for storing various coupling matrices for surface motions
    if(m->num_surface_motions){
        m->knm_surf_motion_1o = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        m->knm_surf_motion_1i = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        m->knm_surf_motion_2o = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        m->knm_surf_motion_2i = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        m->knm_surf_x_a_1 = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        m->knm_surf_x_a_2 = (complex_t***) calloc(m->num_surface_motions, sizeof(complex_t**));
        
        for(i=0; i < m->num_surface_motions; i++){
            // only need to allocate reflection matrices
            allocate_zmatrix(&(m->knm_surf_motion_1o[i]), mem.num_fields, &bytes);
            allocate_zmatrix(&(m->knm_surf_motion_1i[i]), mem.num_fields, &bytes);
            allocate_zmatrix(&(m->knm_surf_motion_2o[i]), mem.num_fields, &bytes);
            allocate_zmatrix(&(m->knm_surf_motion_2i[i]), mem.num_fields, &bytes);
            allocate_zmatrix(&(m->knm_surf_x_a_1[i]), mem.num_fields, &bytes);
            allocate_zmatrix(&(m->knm_surf_x_a_2[i]), mem.num_fields, &bytes);
        }
    }

    if(m->num_motions){
        // allocate workspace for at least 8 nodes of a beamsplitter.
        if(car_ws == NULL) car_ws = (complex_t*) calloc(mem.num_fields * 8, sizeof(complex_t)+1);
        
        m->x_x  =  (complex_t***)  malloc(m->num_motions * sizeof(complex_t**));
        m->x_a1 =  (complex_t****) malloc(m->num_motions * sizeof(complex_t***));
        m->x_a2 =  (complex_t****) malloc(m->num_motions * sizeof(complex_t***));
        m->a1i_x = (complex_t****) malloc(m->num_motions * sizeof(complex_t***));
        m->a2i_x = (complex_t****) malloc(m->num_motions * sizeof(complex_t***));
        m->a1o_x = (complex_t****) malloc(m->num_motions * sizeof(complex_t***));
        m->a2o_x = (complex_t****) malloc(m->num_motions * sizeof(complex_t***));

        bytes += sizeof(complex_t***) * 6 * m->num_motions;

        for(i=0; i < m->num_motions; i++){
            m->x_x[i]  =  (complex_t**)  malloc(m->num_motions * sizeof(complex_t*));
            
            m->x_a1[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            m->x_a2[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            m->a1i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            m->a2i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            m->a1o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            m->a2o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));

            bytes += sizeof(complex_t**) * 6 * num_sig_freqs + sizeof(complex_t*) * m->num_motions;

            for(f=0; f < num_sig_freqs; f++){
                m->x_a1[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                m->x_a2[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                m->a1i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                m->a2i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                m->a1o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                m->a2o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));

                bytes += sizeof(complex_t*) * 6 * mem.num_fields;
            }
        }
        
        if(m->motion_links != NULL){
            motion_link_t *link = NULL;
            bool set_internal_coupling = false;
            
            utarray_foreach(m->motion_links, link) {
                if(link->to_list_idx != link->from_list_idx){
                    link->a1i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a2i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a1o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a2o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));

                    bytes += sizeof(complex_t**) * 4 * num_sig_freqs;

                    for(f=0; f < num_sig_freqs; f++){
                        link->a1i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a2i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a1o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a2o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));

                        bytes += sizeof(complex_t*) * 4 * mem.num_fields;
                    }
                } else
                    set_internal_coupling = true;
            }
            
            if(set_internal_coupling){
                size_t size = mem.num_fields * num_sig_freqs * m->num_motions;
                
                m->ic_a1i_x = calloc(size, sizeof(complex_t));
                m->ic_a1o_x = calloc(size, sizeof(complex_t));
                m->ic_a2i_x = calloc(size, sizeof(complex_t));
                m->ic_a2o_x = calloc(size, sizeof(complex_t));
            }
        }
    }
    
    return m->num_motions;
}

/**
 * This allocates the memory for the beamaplitter motions. It is not called in the usual
 * memory allocation but later after the reading of various parameters, this is 
 * so that memory is not wasted on lots of components that might not have radiation
 * pressure effects.
 * 
 * @param m mirror object
 * @param num_sig_freqs number of signal frequencies 
 * @return 
 */
int alloc_bs_motions(beamsplitter_t *bs, int num_sig_freqs){
    assert(bs != NULL);
    if(num_sig_freqs == 0) return 0;
    
    int f, i;
    long int bytes = 0;
    
    if(bs->mass > 0.0) bs->num_motions++;
    if(bs->Ix > 0.0)   bs->num_motions++;
    if(bs->Iy > 0.0)   bs->num_motions++;
    
    bs->motion_type = (motion_type_t*) malloc(bs->num_motions * sizeof(motion_type_t));
            
    // fills motion type array
    i=0;
    
    if(bs->mass > 0.0) bs->motion_type[i++] = Z;
    if(bs->Ix > 0.0)   bs->motion_type[i++] = ROTX;
    if(bs->Iy > 0.0)   bs->motion_type[i++] = ROTY;
    
    if(bs->num_motions){
        // allocate workspace for at least 8 nodes of a beamsplitter.
        if(car_ws == NULL) car_ws = (complex_t*) calloc(mem.num_fields * 8, sizeof(complex_t)+1);
        
        bs->x_x  =  (complex_t***)  malloc(bs->num_motions * sizeof(complex_t**));
        bs->x_a1 =  (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->x_a2 =  (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->x_a3 =  (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->x_a4 =  (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a1i_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a2i_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a3i_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a4i_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a1o_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a2o_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a3o_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));
        bs->a4o_x = (complex_t****) malloc(bs->num_motions * sizeof(complex_t***));

        bytes += sizeof(complex_t***) * 12 * bs->num_motions;

        for(i=0; i < bs->num_motions; i++){
            bs->x_x[i]  =  (complex_t**)  calloc(bs->num_motions, sizeof(complex_t*));
            
            bs->x_a1[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->x_a2[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->x_a3[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->x_a4[i] =  (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a1i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a2i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a3i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a4i_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a1o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a2o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a3o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
            bs->a4o_x[i] = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));

            bytes += sizeof(complex_t**) * 12 * num_sig_freqs + sizeof(complex_t*) * bs->num_motions;

            for(f=0; f < num_sig_freqs; f++){
                bs->x_a1[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->x_a2[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->x_a3[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->x_a4[i][f] =  (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a1i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a2i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a3i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a4i_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a1o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a2o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a3o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                bs->a4o_x[i][f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));

                bytes += sizeof(complex_t*) * 6 * mem.num_fields;
            }
        }
        
        if(bs->motion_links != NULL){
            motion_link_t *link = NULL;
            bool set_internal_coupling = false;
            
            utarray_foreach(bs->motion_links, link) {
                if(link->to_list_idx != link->from_list_idx){
                    link->a1i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a2i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a3i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a4i_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a1o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a2o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a3o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));
                    link->a4o_x = (complex_t***) malloc(num_sig_freqs * sizeof(complex_t**));

                    bytes += sizeof(complex_t**) * 8 * num_sig_freqs;

                    for(f=0; f < num_sig_freqs; f++){
                        link->a1i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a2i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a3i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a4i_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a1o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a2o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a3o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                        link->a4o_x[f] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));

                        bytes += sizeof(complex_t*) * 8 * mem.num_fields;
                    }
                } else {
                    set_internal_coupling = true;
                }
            }
            
            if(set_internal_coupling){
                size_t size = mem.num_fields * num_sig_freqs * bs->num_motions;
                
                bs->ic_a1i_x = calloc(size, sizeof(complex_t));
                bs->ic_a1o_x = calloc(size, sizeof(complex_t));
                bs->ic_a2i_x = calloc(size, sizeof(complex_t));
                bs->ic_a2o_x = calloc(size, sizeof(complex_t));
                bs->ic_a3i_x = calloc(size, sizeof(complex_t));
                bs->ic_a3o_x = calloc(size, sizeof(complex_t));
                bs->ic_a4i_x = calloc(size, sizeof(complex_t));
                bs->ic_a4o_x = calloc(size, sizeof(complex_t));
            }
        }
    }
    
    return bs->num_motions;
}




//! Allocate memory for beam splitters

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_beamsplitter_list(long int *bytes) {
    if (mem.num_beamsplitters) {
        inter.bs_list = (beamsplitter_t *) calloc(mem.num_beamsplitters + 1, sizeof (beamsplitter_t));

        if (inter.bs_list == NULL) {
            return (6);
        }
        *bytes += (mem.num_beamsplitters + 1) * sizeof (beamsplitter_t);
        
        alloc_knm_accel_bs_mem(bytes);
        bs_knm_alloc(&bstmp, bytes);
        bs_knm_alloc(&bstmap, bytes);
        
        
        int i;
        for (i = 0; i < mem.num_beamsplitters; i++) {
            inter.bs_list[i].comp_index = i;
            
            inter.bs_list[i].fRT = NULL;
            
            bs_knm_alloc(&inter.bs_list[i].knm, bytes);
            bs_knm_alloc(&inter.bs_list[i].knm_no_rgouy, bytes);
            bs_knm_alloc(&inter.bs_list[i].knm_bayer_helms, bytes);
            bs_knm_alloc(&inter.bs_list[i].knm_map, bytes);
            
            beamsplitter_t *bs = &inter.bs_list[i];
            int err;

            bs->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k34_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k43_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k13_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k31_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k24_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            bs->k42_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs_mod(&bs->a11f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a22f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a33f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a44f, bytes, 7, 7, 8);
            if(err) return err;


            err = alloc_field_freq_ptrs_mod(&bs->a12f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a21f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a34f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a43f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a13f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a31f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a24f, bytes, 7, 7, 8);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&bs->a42f, bytes, 7, 7, 8);
            if(err) return err;
        }
    }

    return 0;
}

//! Allocate memory for gratings

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_grating_list(long int *bytes) {
    
    int j;
                
    if (mem.num_gratings) {
        inter.grating_list =
                (grating_t *) calloc(mem.num_gratings + 1, sizeof (grating_t));
        if (inter.grating_list == NULL) {
            return (106);
        }
        *bytes += (mem.num_gratings + 1) * sizeof (grating_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_gratings; i++) {
            inter.grating_list[i].comp_index = i;
            
            grating_t *gr = &inter.grating_list[i];
            int err;

            err = alloc_field_freq_ptrs(&gr->a11f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a12f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a13f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a14f, bytes, 107, 107, 108);
            if(err) return err;

            err = alloc_field_freq_ptrs(&gr->a21f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a22f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a23f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a24f, bytes, 107, 107, 108);
            if(err) return err;

            err = alloc_field_freq_ptrs(&gr->a31f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a32f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a33f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a34f, bytes, 107, 107, 108);
            if(err) return err;

            err = alloc_field_freq_ptrs(&gr->a41f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a42f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a43f, bytes, 107, 107, 108);
            if(err) return err;
            err = alloc_field_freq_ptrs(&gr->a44f, bytes, 107, 107, 108);
            if(err) return err;
            
            // ABCD matrices (4 by 4 matrix to store matrixes from 
            // each node to each node for up to 4 ports)
            inter.grating_list[i].qqt = (ABCD_t **) calloc(4, sizeof (ABCD_t *));
            if (inter.grating_list[i].qqt == NULL)
                return (108);
            
            for (j = 0; j < 4; j++) {
                inter.grating_list[i].qqt[j] = (ABCD_t *) calloc(4, sizeof (ABCD_t));
                if (inter.grating_list[i].qqt[j] == NULL)
                    return (108);
            }
            
            inter.grating_list[i].qqs = (ABCD_t **) calloc(4, sizeof (ABCD_t *));
            
            if (inter.grating_list[i].qqs == NULL)
                return (108);
            
            for (j = 0; j < 4; j++) {
                inter.grating_list[i].qqs[j] = (ABCD_t *) calloc(4, sizeof (ABCD_t));
                if (inter.grating_list[i].qqs[j] == NULL)
                    return (108);
            }
        }
    }

    return 0;
}

//! Allocate memory for squeezers

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo untested
 */
int allocate_memory_for_squeezer_list(long int *bytes) {
    if (mem.num_squeezers) {
        inter.squeezer_list =
                (squeezer_t *) calloc(mem.num_squeezers + 1, sizeof (squeezer_t));

        if (inter.squeezer_list == NULL) {
            return (3);
        }
        *bytes += (mem.num_squeezers + 1) * sizeof (squeezer_t);

        int i;
        for (i = 0; i < mem.num_squeezers; i++) {
            // field amplitudes
            inter.squeezer_list[i].a11 =
                    (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));
            inter.squeezer_list[i].a12 =
                    (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));
            inter.squeezer_list[i].a21 =
                    (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));
            inter.squeezer_list[i].a22 =
                    (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));

            if (inter.squeezer_list[i].a11 == NULL ||
                    inter.squeezer_list[i].a12 == NULL ||
                    inter.squeezer_list[i].a21 == NULL ||
                    inter.squeezer_list[i].a22 == NULL) {
                return (4);
            }
            *bytes += 4 * (mem.num_fields + 1) * sizeof (complex_t **);

            int j;
            for (j = 0; j < mem.num_fields; j++) {
                // field amplitudes
                inter.squeezer_list[i].a11[j] =
                        (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));
                inter.squeezer_list[i].a12[j] =
                        (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));
                inter.squeezer_list[i].a21[j] =
                        (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));
                inter.squeezer_list[i].a22[j] =
                        (complex_t **) calloc(mem.num_fields + 1, sizeof (complex_t *));

                if (inter.squeezer_list[i].a11[j] == NULL ||
                        inter.squeezer_list[i].a12[j] == NULL ||
                        inter.squeezer_list[i].a21[j] == NULL ||
                        inter.squeezer_list[i].a22[j] == NULL) {
                    return (5);
                }
                *bytes += 4 * (mem.num_fields + 1) * sizeof (complex_t *);
            }
        }
    }

    return 0;
}

int allocate_memory_for_block_list(long int *bytes) {
    if (mem.num_blocks) {
        inter.block_list = (block_t *) calloc(mem.num_blocks + 1, sizeof (block_t));
        if (inter.block_list == NULL) {
            return (9);
        }
        *bytes += (mem.num_blocks + 1) * sizeof (block_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_blocks; i++) {
            inter.block_list[i].comp_index = i;
            
            block_t *b = &inter.block_list[i];
            int err;

            err = alloc_field_freq_ptrs(&b->a12f, bytes, 10, 10, 11);
            if(err) return err;

            err = alloc_field_freq_ptrs(&b->a21f, bytes, 10, 10, 11);
            if(err) return err;
        }
    }
    return 0;
}

//! Allocate memory for sagnacs

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_sagnac_list(long int *bytes) {
    if (mem.num_sagnacs) {
        inter.sagnac_list = (sagnac_t *) calloc(mem.num_sagnacs + 1, sizeof (sagnac_t));
        if (inter.sagnac_list == NULL) {
            return (9);
        }
        *bytes += (mem.num_sagnacs + 1) * sizeof (sagnac_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_sagnacs; i++) {
            inter.sagnac_list[i].comp_index = i;
            
            sagnac_t *s = &inter.sagnac_list[i];
            int err;

            err = alloc_field_freq_ptrs(&s->a12f, bytes, 10, 10, 11);
            if(err) return err;

            err = alloc_field_freq_ptrs(&s->a21f, bytes, 10, 10, 11);
            if(err) return err;
        }
    }
    return 0;
}

//! Allocate memory for spaces

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_space_list(long int *bytes) {
    if (mem.num_spaces) {
        inter.space_list = (space_t *) calloc(mem.num_spaces + 1, sizeof (space_t));
        if (inter.space_list == NULL) {
            return (9);
        }
        *bytes += (mem.num_spaces + 1) * sizeof (space_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_spaces; i++) {
            inter.space_list[i].list_index = i;
            
            allocate_zmatrix(&inter.space_list[i].k12, mem.num_fields, bytes);
            allocate_zmatrix(&inter.space_list[i].k21, mem.num_fields, bytes);
            
            space_t *s = &inter.space_list[i];
            int err;
            
            s->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            s->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs(&s->a12f, bytes, 10, 10, 11);
            if(err) return err;

            err = alloc_field_freq_ptrs(&s->a21f, bytes, 10, 10, 11);
            if(err) return err;
        }
    }
    return 0;
}

//! Allocate memory for light inputs

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_light_input_list(long int *bytes) {
    if (mem.num_light_inputs) {
        inter.light_in_list = (light_in_t *) calloc(mem.num_light_inputs + 1, sizeof (light_in_t));

        if (inter.light_in_list == NULL) {
            return (12);
        }
        *bytes += (mem.num_light_inputs + 1) * sizeof (light_in_t);
        
        int i;
        
        for (i = 0; i < mem.num_light_inputs; i++) {
            inter.light_in_list[i].comp_index = i;
            inter.light_in_list[i].z_fsig_self_coupling = true;
            inter.light_in_list[i].power_coeff_list = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));

            if (inter.light_in_list[i].power_coeff_list == NULL) {
                return (13);
            }
            *bytes += (mem.num_fields + 1) * sizeof (complex_t);
        }        
    }
    return 0;
}

int allocate_memory_for_motion_detector_list(long int *bytes) {
    if (mem.num_motion_outputs) {
        inter.motion_out_list =
                (motion_out_t *) calloc(mem.num_motion_outputs + 1, sizeof (motion_out_t));
        if (inter.motion_out_list == NULL) {
            return (14);
        }
        *bytes += (mem.num_motion_outputs + 1) * sizeof (motion_out_t);
    }
    
    return 0;
}

int allocate_memory_for_feedback_list(long int *bytes) {
    if (mem.num_feedbacks) {
        inter.slink_list = (slink_t *) calloc(mem.num_feedbacks + 1, sizeof (slink_t));
        
        if (inter.slink_list == NULL) {
            return (14);
        }
        
        *bytes += (mem.num_feedbacks + 1) * sizeof (slink_t);
        
        int i;
        
        for(i=0; i<mem.num_feedbacks; i++){
            inter.slink_list[i].a_d = (complex_t***) malloc(mem.num_frequencies * sizeof(complex_t**));
            *bytes += (mem.num_frequencies) * sizeof (complex_t**);
            
            int j;
            
            for(j=0; j<mem.num_frequencies; j++){
                inter.slink_list[i].a_d[j] = (complex_t**) calloc(mem.num_fields, sizeof(complex_t*));
                *bytes += (mem.num_fields) * sizeof (complex_t*);
            }
        }
    }
    
    return 0;
}

//! Allocate memory for detectors
/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_detector_list(long int *bytes) {
    if (mem.num_light_outputs) {
        inter.light_out_list =
                (light_out_t *) calloc(mem.num_light_outputs + 1, sizeof (light_out_t));
        if (inter.light_out_list == NULL) {
            return (14);
        }
        *bytes += (mem.num_light_outputs + 1) * sizeof (light_out_t);

        int i;
        for (i = 0; i < mem.num_light_outputs; i++) {
            inter.light_out_list[i].list_index = i;
            
            inter.light_out_list[i].masking_factor =
                    (double *) calloc(mem.num_fields + 1, sizeof (double));

            if (inter.light_out_list[i].masking_factor == NULL) {
                return (15);
            }
            *bytes += (mem.num_fields + 1) * sizeof (double);
        }
    }
    return 0;
}

//! Allocate memory for output data

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_output_data_list(long int *bytes) {
    if (mem.num_outputs) {
        inter.output_data_list =
                (output_data_t *) calloc(mem.num_outputs + 1, sizeof (output_data_t));
        if (inter.output_data_list == NULL) {
            return (80);
        }
        *bytes += (mem.num_outputs + 1) * sizeof (output_data_t);

        /*
          for (i=0;i<mem.num_outputs;i++)
          {
          inter.output_data_list[i].a0=(complex*)malloc((size_t)(mem.num_fields+1)*sizeof(complex_t));
          if (inter.output_data_list[i].a0==NULL)
          return (81);
         *bytes+=(mem.num_fields+1)*sizeof(complex_t);
          }
         */
    }
    return 0;
}

//! Allocate memory for beam parameter outputs

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_beam_param_list(long int *bytes) {
    if (mem.num_beam_param_outputs) {
        inter.beampar_out_list = (beampar_out_t *)
                calloc(mem.num_beam_param_outputs + 1, sizeof (beampar_out_t));
        if (inter.beampar_out_list == NULL) {
            return (82);
        }
        *bytes += (mem.num_beam_param_outputs + 1) * sizeof (beampar_out_t);
    }
    return 0;
}
//! Allocate memory for cavity parameter outputs

/*!
 */
int allocate_memory_for_cavity_param_list(long int *bytes) {
    if (mem.num_cavity_param_outputs) {
        inter.cavity_par_out_list = (cavity_par_out_t *)
                calloc(mem.num_cavity_param_outputs + 1, sizeof (cavity_par_out_t));
        if (inter.cavity_par_out_list == NULL) {
            return (182);
        }
        *bytes += (mem.num_cavity_param_outputs + 1) * sizeof (cavity_par_out_t);
    }
    return 0;
}

//! Allocate memory for convolution outputs

/*!
 */
int allocate_memory_for_convolution_list(long int *bytes) {
    if (mem.num_convolution_outputs) {
        inter.convolution_out_list = (convolution_out_t *)
                calloc(mem.num_convolution_outputs + 1, sizeof (convolution_out_t));
        if (inter.convolution_out_list == NULL) {
            return (982);
        }
        *bytes += (mem.num_convolution_outputs + 1) * sizeof (convolution_out_t);
    }
    return 0;
}

//! Allocate memory for mirror_phase outputs

/*!
 */
int allocate_memory_for_mirror_phase_list(long int *bytes) {
    if (mem.num_mirror_phase_outputs) {
        inter.mirror_phase_out_list = (mirror_phase_out_t *)
                calloc(mem.num_mirror_phase_outputs + 1, sizeof (mirror_phase_out_t));
        if (inter.mirror_phase_out_list == NULL) {
            return (983);
        }
        *bytes += (mem.num_mirror_phase_outputs + 1) * sizeof (mirror_phase_out_t);
    }
    return 0;
}

//! Allocate memory for modulators

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_modulator_list(long int *bytes) {
    if (mem.num_modulators) {
        inter.modulator_list = (modulator_t *) calloc(mem.num_modulators + 1, sizeof (modulator_t));
        
        if (inter.modulator_list == NULL) {
            return (16);
        }
        
        *bytes += (mem.num_modulators + 1) * sizeof (modulator_t);

        // field amplitudes
        int i;
        
        for (i = 0; i < mem.num_modulators; i++) {
            inter.modulator_list[i].comp_index = i;
                  
            allocate_zmatrix(&inter.modulator_list[i].k12, mem.num_fields, bytes);
            allocate_zmatrix(&inter.modulator_list[i].k21, mem.num_fields, bytes);
            
            // could probably be efficient and parse the modulation type to check
            // if actually using tilt modulation here.
            allocate_zmatrix(&inter.modulator_list[i].ktm12, mem.num_fields, bytes);
            allocate_zmatrix(&inter.modulator_list[i].ktm21, mem.num_fields, bytes);
            
            modulator_t *m = &inter.modulator_list[i];
            int err;
            
            m->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            m->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs_mod(&m->a12f, bytes, 17, 17, 18);
            if(err) return err;

            err = alloc_field_freq_ptrs_mod(&m->a21f, bytes, 17, 17, 18);
            if(err) return err;
        }
    }
    return 0;
}

//! Allocate memory for signals

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_signal_list(long int *bytes) {
    if (mem.num_signal_inputs) {
        inter.signal_list = (signal_t *) calloc(mem.num_signal_inputs + 1, sizeof (signal_t));
        if (inter.signal_list == NULL) {
            return (19);
        }
        *bytes += (mem.num_signal_inputs + 1) * sizeof (signal_t);

        int i;
        for (i = 0; i < mem.num_signal_inputs; i++) {
            inter.signal_list[i].at1 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].at2 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].at3 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].at4 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));

            inter.signal_list[i].ar1 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].ar2 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].ar3 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));
            inter.signal_list[i].ar4 = (complex_t *) calloc(mem.num_fields + 1, sizeof (complex_t));

            if (inter.signal_list[i].at1 == NULL ||
                    inter.signal_list[i].at2 == NULL ||
                    inter.signal_list[i].at3 == NULL ||
                    inter.signal_list[i].at4 == NULL ||

                    inter.signal_list[i].ar1 == NULL ||
                    inter.signal_list[i].ar2 == NULL ||
                    inter.signal_list[i].ar3 == NULL ||
                    inter.signal_list[i].ar4 == NULL) {
                return (20);
            }
            
            *bytes += 8 * (mem.num_fields + 1) * sizeof (complex_t);
        }
    }
    return 0;
}

//! Allocate memory for derivatives

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_derivative_list(long int *bytes) {
    if (mem.num_diff_cmds) {
        inter.deriv_list = (derivative_t *) calloc(mem.num_diff_cmds + 1, sizeof (derivative_t));
        if (inter.deriv_list == NULL) {
            return (21);
        }
        *bytes += (mem.num_diff_cmds + 1) * sizeof (derivative_t);
    }
    return 0;
}

//! Allocate memory for scale commands

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_scale_cmd_list(long int *bytes) {
    if (mem.num_scale_cmds) {
        inter.scale_list =
                (scale_t *) calloc(mem.num_scale_cmds + 1, sizeof (scale_t));
        if (inter.scale_list == NULL) {
            return (22);
        }
        *bytes += (mem.num_scale_cmds + 1) * sizeof (scale_t);
    }
    return 0;
}

//! Allocate memory for variable commands

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_variable_list(long int *bytes) {
    if (mem.num_variables) {
        inter.variable_list =
                (variable_t *) calloc(mem.num_variables + 1, sizeof (variable_t));
        if (inter.variable_list == NULL) {
            return (22); // TODO fix double number
        }
        *bytes += (mem.num_variables + 1) * sizeof (variable_t);
    }
    return 0;
}


//! Allocate memory for diodes

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_diode_list(long int *bytes) {
    if (mem.num_diodes) {
        inter.diode_list = (diode_t *) calloc(mem.num_diodes + 1, sizeof (diode_t));
        if (inter.diode_list == NULL) {
            return (23);
        }

        *bytes += (mem.num_diodes + 1) * sizeof (diode_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_diodes; i++) {
            inter.diode_list[i].comp_index = i;
            
            allocate_zmatrix(&inter.diode_list[i].k12, mem.num_fields, bytes);
            allocate_zmatrix(&inter.diode_list[i].k21, mem.num_fields, bytes);
            allocate_zmatrix(&inter.diode_list[i].k23, mem.num_fields, bytes);
            allocate_zmatrix(&inter.diode_list[i].k32, mem.num_fields, bytes);
            
            diode_t *d = &inter.diode_list[i];
            int err;
            
            d->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k23_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k32_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs(&d->a12f, bytes, 24, 24, 25);
            if(err) return err;

            err = alloc_field_freq_ptrs(&d->a21f, bytes, 24, 24, 25);
            if(err) return err;

            err = alloc_field_freq_ptrs(&d->a23f, bytes, 24, 24, 25);
            if(err) return err;                

            err = alloc_field_freq_ptrs(&d->a32f, bytes, 24, 24, 25);
            if(err) return err;                
        }
    }
    return 0;
}



int allocate_memory_for_dbs_list(long int *bytes) {
    if (mem.num_dbss) {
        inter.dbs_list = (dbs_t *) calloc(mem.num_dbss + 1, sizeof (dbs_t));
        if (inter.dbs_list == NULL) {
            return (23);
        }

        *bytes += (mem.num_dbss + 1) * sizeof (dbs_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_dbss; i++) {
            inter.dbs_list[i].comp_index = i;
            
            allocate_zmatrix(&inter.dbs_list[i].k13, mem.num_fields, bytes);
            allocate_zmatrix(&inter.dbs_list[i].k34, mem.num_fields, bytes);
            allocate_zmatrix(&inter.dbs_list[i].k21, mem.num_fields, bytes);
            allocate_zmatrix(&inter.dbs_list[i].k42, mem.num_fields, bytes);
            
            dbs_t *d = &inter.dbs_list[i];
            int err;
            
            d->k13_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k34_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            d->k42_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs(&d->a13f, bytes, 24, 24, 25);
            if(err) return err;

            err = alloc_field_freq_ptrs(&d->a34f, bytes, 24, 24, 25);
            if(err) return err;

            err = alloc_field_freq_ptrs(&d->a21f, bytes, 24, 24, 25);
            if(err) return err;                
              
            err = alloc_field_freq_ptrs(&d->a42f, bytes, 24, 24, 25);
            if(err) return err;          
        }
    }
    return 0;
}

//! Allocate memory for lenses

/*!
 * \param bytes number of bytes so far allocated???
 *
 * \todo qcorr branch is untested
 */
int allocate_memory_for_lens_list(long int *bytes) {
    if (mem.num_lenses) {
        inter.lens_list = (lens_t *) calloc(mem.num_lenses + 1, sizeof (lens_t));
        
        if (inter.lens_list == NULL) {
            return (26);
        }
        *bytes += (mem.num_lenses + 1) * sizeof (lens_t);

        // field amplitudes
        int i;
        for (i = 0; i < mem.num_lenses; i++) {
            inter.lens_list[i].comp_index = i;
            
            allocate_zmatrix(&inter.lens_list[i].k12, mem.num_fields, bytes);
            allocate_zmatrix(&inter.lens_list[i].k21, mem.num_fields, bytes);
            
            lens_t *l = &inter.lens_list[i];
            int err;

            l->k12_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            l->k21_sqrd_sum = (double*) malloc(mem.num_fields * sizeof(double));
            
            err = alloc_field_freq_ptrs(&l->a12f, bytes, 27, 27, 28);
            if(err) return err;

            err = alloc_field_freq_ptrs(&l->a21f, bytes, 27, 27, 28);
            if(err) return err;
        }
    }
    return 0;
}

//! Allocate memory for locks

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_locks(long int *bytes) {
    if (mem.num_locks) {
        inter.lock_list = (lock_command_t *) calloc(mem.num_locks + 1, sizeof (lock_command_t));
        if (inter.lock_list == NULL) {
            return (88);
        }

        *bytes += (mem.num_locks + 1) * sizeof (lock_command_t);

        inter.lock_order = (int *) calloc(mem.num_locks + 1, sizeof (int));
        if (inter.lock_order == NULL) {
            return (88);
        }

        inter.breakcount = (int *) calloc(mem.num_locks + 1, sizeof (int));
        if (inter.breakcount == NULL) {
            return (86);
        }
    }
    return 0;
}

//! Allocate memory for functions

/*!
 * \param bytes number of bytes so far allocated???
 */
int allocate_memory_for_function_list(long int *bytes) {
    if (mem.num_func_cmds) {
        inter.function_list = (func_command_t *)
                calloc(mem.num_func_cmds + 1, sizeof (func_command_t));

        if (inter.function_list == NULL) {
            return (87);
        }
        *bytes += (mem.num_func_cmds + 1) * sizeof (func_command_t);

        inter.func_order = (int *) calloc(mem.num_func_cmds + 1, sizeof (int));
        if (inter.func_order == NULL) {
            return (87);
        }
    }
    return 0;
}

//! Allocate memory for signal amplitudes

/*!
 * \param bytes1 number of bytes so far allocated???
 */
int allocate_memory_for_amplitude_list(long int *bytes1) {
    if (mem.num_fields) {
        a_s = (complex_t ***) calloc(mem.num_fields + 1, sizeof (complex_t **));

        if (a_s == NULL) {
            return (37);
        }
        *bytes1 += (mem.num_fields + 1) * sizeof (complex_t **);

        int i;
        int outputs = mem.num_light_outputs + mem.num_quad_outputs;
        
        for (i = 0; i < mem.num_fields; i++) {
            a_s[i] = (complex_t **)
                    calloc(outputs + 1, sizeof (complex_t *));

            if (a_s[i] == NULL) {
                return (38);
            }
            
            *bytes1 += (outputs + 1) * sizeof (complex_t *);

            int j;
            for (j = 0; j < outputs; j++) {
                a_s[i][j] = (complex_t *) calloc(mem.num_frequencies + 1, sizeof (complex_t));
                
                if (a_s[i][j] == NULL) {
                    return (39);
                }
                
                *bytes1 += (mem.num_frequencies + 1) * sizeof (complex_t);
            }
        }

        mem.all_tem_HG = (int *) calloc((mem.num_fields + 1)*2, sizeof (int));
        if (mem.all_tem_HG == NULL) {
            return (99);
        }
        *bytes1 += (mem.num_fields + 1) * 2 * sizeof (int);
        
        mem.all_tem_LG = (int *) calloc((mem.num_fields + 1)*2, sizeof (int));
        if (mem.all_tem_LG == NULL) {
            return (99);
        }
        *bytes1 += (mem.num_fields + 1) * 2 * sizeof (int);
    }
    return 0;
}

int allocate_memory_for_quantum_input_list() {
    
    if(inter.num_qnoise_inputs > 0){
        
        inter.qnoise_in_list = (qnoise_input_t*)calloc(inter.num_qnoise_inputs, sizeof(qnoise_input_t));
        
        if(inter.qnoise_in_list == NULL){
            return 99;
            gerror("Not enough memory for quantum noise input list");
        }
    }
    
    return 0;
}

//! Make the memory for solution data

/*!
 *
 */
void make_space(void) {
    if (mem.restab != NULL) {
        bug_error("memory allocation failure (1)");
    }

    if ((mem.restab = calloc(inter.num_output_cols * (inter.x2.xsteps + 1) *
            (inter.x1.xsteps + 1), sizeof (double))) == NULL) {
        gerror("make_space: memory allocation failure (2)\n");
    }
}

void read_lambda(const char *command_string) {
    char command_name[MAX_TOKEN_LEN] = {0};
    char value_string[MAX_TOKEN_LEN] = {0};
    double value = 0.0;
    char rest_string[REST_STRING_LEN] = {0};


    int num_vars_read = sscanf(command_string, "%s %s %80s",
            command_name, value_string, rest_string);
    int num_vars_expected = 2;
    if ((num_vars_read < num_vars_expected) || atod(value_string, &value)) {
        gerror("line '%s':\nexpected 'lambda value'\n", command_string);
    }
    warn("overwriting default wavelength with lambda=%sm\n", double_form(value));
    
    if(value > 1) {
        warn("A wavelength of %sm is quite large compared to default 1064E-9m, did you forget to specify nanometers?\n", double_form(value));
    }
    
    init.lambda = value;
    inter.f0 = init.clight / init.lambda;
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

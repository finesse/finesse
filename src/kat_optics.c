// $Id$

/*!
 * \file kat_optics.c
 * \brief Routines pertaining to various optics calculations.
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#include "kat.h"
#include "kat_inline.c"
#include "kat_optics.h"
#include "kat_io.h"
#include "kat_aux.h"
#include "kat_aa.h"
#include "kat_check.h"
#include <gsl/gsl_sf.h>

extern const complex_t complex_i;
extern const complex_t complex_mi;
extern const complex_t complex_1;
extern const complex_t complex_m1;
extern const complex_t complex_0;

extern init_variables_t init;
extern interferometer_t inter;
extern options_t options;

//! Calculate the Schottky shotnoise level

/*!
 * \param P power of the light field
 */
double schottky_shotnoise(output_data_t *out, double P) {
    // the power shouldn't be less than zero
    assert(NOT(P < 0.0));

    // Scale the result in either factors of hf or not, or PSD or ASD
    switch(out->quantum_scaling){
        case PSD_HF:
            return 2.0 * P;
        case PSD:
            return 2.0 * H_PLANCK * inter.f0 * P;
        case ASD_HF:
            return sqrt(2.0 * P);
        case ASD:
            return sqrt(2.0 * H_PLANCK * inter.f0 * P);
        case NOTSET:
            bug_error("Not set scaling");
    }
    bug_error("Unknown scale for shotnoise computation");
    return 0; //silence compiler warning.
}

//! Calculate the finesse of the cavity

/*!
 * \param cavity_loss the amount of loss in the cavity
 *
 * \return the cavity finesse
 */
double finesse(double cavity_loss) {
    double l;

    // sanity check the input
    assert(NOT(cavity_loss < 0.0));

    if (cavity_loss == 0.0) {
        return (BIG);
    } else if (cavity_loss == 1.0) {
        return (0.0);
    } else {
        l = sqrt(1.0 - cavity_loss);
        return (PI / (2.0 * asin((1.0 - l) / (2.0 * sqrt(l)))));
    }
}

//! Calculate the Gouy phase shift

/*!
 * \param q the Gaussian beam parameter
 */
double gouy(complex_t q) {
    return (atan2(q.re, q.im));
}

//! Computes q value for cavity (from ABCD matrix) S.820

/*!
 * \param s ABCD matrix for one cavity round-trip
 * \param nr index of refraction at first cavity node
 */
complex_t q_cavity(ABCD_t s, double nr) {
    int i;
    double m;
    complex_t q;

    i = check_ABCD(s, &m);

    if (i) {
        bug_error("cavity unstable");
    }

    if (s.B == 0) {
        bug_error("div by zero (B)");
    }
    q.re = (s.D - s.A) / (2.0 * s.B);
    q.im = -1.0 * fabs(sqrt(1.0 - m * m) / s.B);

    return (z_by_x(inv_complex(q), nr));
}

//! Check if ABCD of a cavity is good (0), critical (1) or unstable (2)

/*!
 * \param s ABCD matrix
 */
int check_ABCD(ABCD_t s, double *stability) {
    *stability = (s.A + s.D) / 2.0;
    
    if ((*stability) < -1 || (*stability) > 1) {
        if (inter.debug & 256) {
            message("check_ABCD m=%f\n", *stability);
        }
        return (2); // unstable
    } else if (*stability == -1 || *stability == 1) {
        return (1); // critical
    } else {
        return (0); // stable
    }
}

//! Check a cavity

/*!
 * \param cavity the cavity to check
 */
void check_cavity(cavity_t *cavity) {
    int test, no, nodes[5] = {0};

    which_nodes(cavity->component1_index, GND_NODE, &no, nodes);

    test = 0;
    int i;
    for (i = 0; i < no; i++) {
        if (nodes[i] == cavity->node1_index) {
            test = 1;
        }
    }

    if (!test) {
        gerror("cavity %s:  node1 is not at component1\n", cavity->name);
    }

    which_nodes(cavity->component2_index, GND_NODE, &no, nodes);

    test = 0;
    for (i = 0; i < no; i++) {
        if (nodes[i] == cavity->node2_index) {
            test = 1;
        }
    }

    if (!test) {
        gerror("cavity %s:  node2 is not at component2\n", cavity->name);
    }

    if ((get_component_type(cavity->component1_index) != MIRROR) &&
            (get_component_type(cavity->component1_index) != BEAMSPLITTER) &&
            (get_component_type(cavity->component1_index) != GRATING)) {
        gerror("cavity %s:  cavity end components must be mirrors, "
                "gratings or a beamsplitter\n", cavity->name);
    }
    if ((get_component_type(cavity->component2_index) != MIRROR) &&
            (get_component_type(cavity->component2_index) != BEAMSPLITTER) &&
            (get_component_type(cavity->component2_index) != GRATING)) {
        gerror("cavity %s:  cavity end components must be mirrors, "
                "gratings or a beamsplitter\n", cavity->name);
    }

    if (get_component_type(cavity->component1_index) == BEAMSPLITTER) {
        if (cavity->component1_index != cavity->component2_index) {
            gerror("cavity %s:  cavity end components must be two mirrors "
                    "or one beamsplitter\n", cavity->name);
        }

        if ((cavity->node1_index == inter.bs_list[get_component_index(cavity->component1_index)].node1_index &&
                cavity->node2_index == inter.bs_list[get_component_index(cavity->component1_index)].node4_index)
                ||
                (cavity->node1_index == inter.bs_list[get_component_index(cavity->component1_index)].node2_index &&
                cavity->node2_index == inter.bs_list[get_component_index(cavity->component1_index)].node3_index)) {
            gerror("cavity %s:  beams are not connected at these nodes\n",
                    cavity->name);
        }
        if (cavity->node1_index == cavity->node2_index) {
            gerror("cavity %s: you must use two different nodes at a beamsplitter\n",
                    cavity->name);
        }
    }
}

//! Check a user provided Gaussian beam parameter 

/*!
 * \param gauss the Gaussian beam structure to check
 */
void check_gauss(gauss_t *gauss) {
    int test, no, nodes[5] = {0};
    node_t node = inter.node_list[gauss->node_index];

    if (gauss->wx0 <= 0 || gauss->wy0 <= 0) {
        gerror("%s:  w0 must be greater than 0\n", gauss->name);
    }

    if (node.gnd_node) {
        gerror("%s:  `dump' is not a proper node\n", gauss->name);
    }

    if (gauss->component_index == NOT_FOUND) {
        gerror("%s:  component does not exist\n", gauss->name);
    }

    which_nodes(gauss->component_index, GND_NODE, &no, nodes);
    test = 0;
    int i;
    for (i = 0; i < no; i++) {
        if (nodes[i] == gauss->node_index) {
            test = 1;
        }
    }

    if (!test) {
        gerror("%s:  node is not at given component\n", gauss->name);
    }
}

//! Check the lens parameters

/*!
 * \param lens the lens to check
 */
void check_lens(lens_t *lens) {
    // assert that the node indices are in the correct range:
    assert(lens->node1_index >= 0);
    assert(lens->node2_index >= 0);
    assert(lens->node1_index < inter.num_nodes);
    assert(lens->node2_index < inter.num_nodes);
    assert(lens->node1_index != lens->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(lens->node1_reduced_index >= 0);
    assert(lens->node2_reduced_index >= 0);
    assert(lens->node1_reduced_index < inter.num_reduced_nodes);
    assert(lens->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[lens->node1_index].gnd_node &&
            NOT inter.node_list[lens->node2_index].gnd_node) {
        assert(lens->node1_reduced_index != lens->node2_reduced_index);
    }
#endif

    // make sure the focal length isn't zero
    if ((lens->fx_of_Dx == 0 && lens->is_focal_length)
            || (!lens->is_focal_length && 1.0/lens->fx_of_Dx == 0)) {
        gerror("%s  focal length must not be 0\n", lens->name);
    }

    if(lens->is_astigmatic) {
        // make sure the focal length isn't zero
        if ((lens->fy_of_Dy == 0 && lens->is_focal_length)
                || (!lens->is_focal_length && 1.0/lens->fy_of_Dy == 0)) {
            gerror("%s  focal length y must not be 0\n", lens->name);
        }
    }
}

//! Rebuild the grating (check parameter consistency and re-compute coupling phases from efficiencies) 

/*!
 * \param grating the grating to rebuild
 */
void rebuild_grating(grating_t *grating) {
    double sum;
    double etamin, etamax;
    double alphamin, alphamax;

    alphamin = alphamax = 0.0;

    if (grating->d <= 0) {
        gerror("%s:  grating period must be >0\n", grating->name);
    }

    // check coupling efficiencies
    sum = 0.0;
    int i;
    switch (grating->num_of_ports) {
        case 2:
            for (i = 0; i < 2; i++) {
                if ((grating->eta[i] < 0) || (grating->eta[i] > 1)) {
                    gerror("%s:  efficiency eta must be 0<= eta <= 1\n", grating->name);
                }
                sum += grating->eta[i];
            }

            if (sum - 1 > SMALL) {
                gerror("%s:  efficiencies inconsistent\n", grating->name);
            }
            break;
        case 3:
            for (i = 0; i < 4; i++) {
                if ((grating->eta[i] < 0) || (grating->eta[i] > 1)) {
                    gerror("%s:  efficiency eta must be 0<= eta/rho <= 1\n", grating->name);
                }
            }

            etamin = sqr(0.5 * (1 - sqrt(grating->eta[3])));
            etamax = sqr(0.5 * (1 + sqrt(grating->eta[3])));

            if (grating->eta[0] < etamin || grating->eta[0] > etamax) {
                gerror("%s:  eta0 (=%s) not in valid range (%s < eta0 < %s)\n",
                        grating->name, double_form(grating->eta[0]),
                        double_form(etamin), double_form(etamax));
            }

            if (grating->eta[2] < etamin || grating->eta[2] > etamax) {
                gerror("%s:  eta2 (=%s) not in valid range (%s < eta2 < %s)\n",
                        grating->name, double_form(grating->eta[2]),
                        double_form(etamin), double_form(etamax));
            }

            sum = 0.0;
            for (i = 0; i < 3; i++) {
                sum += grating->eta[i];
            }

            if (fabs(sum - 1.0) > SMALL) {
                gerror("%s:  eta0 + eta1 + eta2 must be = 1\n"
                        "\tsum = %g + %g + %g = %g\n",
                        grating->name, grating->eta[0], grating->eta[1],
                        grating->eta[2], sum);
            }

            sum = 2.0 * grating->eta[1] + grating->eta[3];
            if (fabs(sum - 1.0) > SMALL) {
                gerror("%s:  rho0 + 2*eta1 must be = 1\n", grating->name);
            }

            // don't remember where this comes from, probabaly wrong
            //    if (grating->eta[1] <= 2*grating->eta[0] - 1.0) {
            //  gerror("%s:  eta1 must be > 2 eta0 -1\n", grating->name);
            //}
            break;
        case 4:
            for (i = 0; i < 2; i++) {
                if ((grating->eta[i] < 0) || (grating->eta[i] > 1)) {
                    gerror("%s:  efficiency eta must be 0<= eta <= 1\n", grating->name);
                }
                sum += grating->eta[i];
            }

            if (sum - 1 > SMALL) {
                gerror("%s:  efficiencies inconsistent\n", grating->name);
            }
            break;
        default:
            bug_error("wrong number of ports; %d ports given", grating->num_of_ports);
    }
        
    // check grating periods
    switch (grating->num_of_ports) {
        case 2:
            if (grating->d < grating->dmin * init.lambda ||
                    grating->d > grating->dmax * init.lambda) {
                gerror("%s:  grating period out of range for 1st order Litrow\n",
                        grating->name);
            }

            grating->alpha = DEG * asin(init.lambda / (2 * grating->d));
            break;
        case 3:
            if (grating->d < grating->dmin * init.lambda ||
                    grating->d > grating->dmax * init.lambda) {
                gerror("%s:  grating period out of range for 2nd order Litrow\n",
                        grating->name);
            }

            grating->alpha = DEG * asin(init.lambda / (grating->d));
            break;
        case 4:
            if (grating->d < grating->dmin * init.lambda ||
                    grating->d > grating->dmax * init.lambda) {
                gerror("%s:  grating period out of range for the existence of "
                        "exactly 2 orders\n", grating->name);
            }

            if (grating->d > init.lambda) {
                alphamin = DEG * asin(1.0 - init.lambda / grating->d);
                alphamax = DEG * asin(2.0 * init.lambda / grating->d - 1.0);
            } else {
                alphamin = DEG * asin(init.lambda / grating->d - 1.0);
                alphamax = 90.0;
            }

            if (grating->alpha < alphamin || grating->alpha > alphamax) {
                gerror("%s:  angle of incidence out of range (%s--%s deg) for the \n"
                        "     existence of exactly 2 orders.  Current value: %s deg\n",
                        grating->name,
                        double_form(alphamin), double_form(alphamax),
                        double_form(grating->alpha));
            }
            break;
        default:
            bug_error("wrong number of ports");
    }

    // compute phases
    switch (grating->num_of_ports) {
        case 2:
            break;
        case 3:
            grating->phi[0] = 0.0;
            grating->phi[1] = -0.5 * acos((grating->eta[1] - 2.0 * grating->eta[0]) /
                    (2.0 * sqrt(grating->eta[3]) * sqrt(grating->eta[0])));
            grating->phi[2] =
                    acos(-1.0 * grating->eta[1] /
                    (2.0 * sqrt(grating->eta[2]) * sqrt(grating->eta[0])));
        
            break;
        case 4:
            break;
        default:
            bug_error("wrong number of ports");
    }
}

//! Check mirror parameters

/*!
 * \param mirror the mirror to check
 */
void check_mirror(mirror_t *mirror) {
    node_t node1, node2;

    // assert that the node indices are in the correct range, namely:
    assert(mirror->node1_index >= 0);
    assert(mirror->node2_index >= 0);
    assert(mirror->node1_index < inter.num_nodes);
    assert(mirror->node2_index < inter.num_nodes);
    assert(mirror->node1_index != mirror->node2_index);

    assert(mirror->r_aperture >= 0);

    // assert that the reduced node indices are in the correct range:
    assert(mirror->node1_reduced_index >= 0);
    assert(mirror->node2_reduced_index >= 0);
    assert(mirror->node1_reduced_index < inter.num_reduced_nodes);
    assert(mirror->node2_reduced_index < inter.num_reduced_nodes);
    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[mirror->node1_index].gnd_node &&
            NOT inter.node_list[mirror->node2_index].gnd_node) {
        assert(mirror->node1_reduced_index != mirror->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[mirror->node1_index];
    node2 = inter.node_list[mirror->node2_index];

    if (mirror->R > 1 || mirror->R < 0) {
        gerror("%s  reflectivity must be 0 < 1\n", mirror->name);
    } else if (mirror->T > 1 || mirror->T < 0) {
        gerror("%s  transmittivity must be 0 < 1\n", mirror->name);
    } else if (node1.gnd_node &&
            node2.gnd_node) {
        gerror("%s not connected\n", mirror->name);
    } else if (mirror->T + mirror->R - 1 > SMALL) {
        gerror("%s  mirror parameters r, t  inconsitent\n", mirror->name);
    }
    /*
      else if (1-mirror->T-mirror->R > SMALL)
      warn("Warning: %s  t and r don't add to unity\n", mirror->name);
     */
    
    if(mirror->mass < 0.0)
        gerror("%s mass is less than 0.0\n", mirror->name);
    
    if(mirror->mass == 0.0 && mirror->long_tf != NULL)
        gerror("%s has 0 mass but a mechanical transfer function\n", mirror->name);
    
    if(mirror->mass > 0.0 && mirror->long_tf == NULL)
        gerror("%s has a mass but a no mechanical transfer function\n", mirror->name);
}

//! Check the grating parameters

/*!
 * \param grating the grating to check
 */
void check_grating(grating_t *grating) {
    // assert that the node indices are in the correct range, namely:
    assert(grating->node1_index >= 0);
    assert(grating->node2_index >= 0);
    assert(grating->node1_index < inter.num_nodes);
    assert(grating->node2_index < inter.num_nodes);
    assert(grating->node1_index != grating->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(grating->node1_reduced_index >= 0);
    assert(grating->node2_reduced_index >= 0);
    assert(grating->node1_reduced_index < inter.num_reduced_nodes);
    assert(grating->node2_reduced_index < inter.num_reduced_nodes);
    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[grating->node1_index].gnd_node &&
            NOT inter.node_list[grating->node2_index].gnd_node) {
        assert(grating->node1_reduced_index != grating->node2_reduced_index);
    }
#endif

    node_t node1, node2, node3, node4;

    node1 = inter.node_list[grating->node1_index];
    node2 = inter.node_list[grating->node2_index];

    if (grating->d <= 0) {
        gerror("%s  period must be >0\n", grating->name);
    }

    switch (grating->num_of_ports) {
        case 2:
            // make sure that the unused nodes are "not found"
            assert(grating->node3_reduced_index == NOT_FOUND);
            assert(grating->node4_reduced_index == NOT_FOUND);
            if (node1.gnd_node &&
                    node2.gnd_node) {
                gerror("%s not connected\n", grating->name);
            }
            break;
        case 3:
            // check bounds on node3_index
            assert(grating->node3_index >= 0);
            assert(grating->node3_index < inter.num_nodes);
            assert(grating->node3_index != grating->node1_index &&
                    grating->node3_index != grating->node2_index);
            // check node3_reduced_index
            assert(grating->node3_reduced_index >= 0);
            assert(grating->node3_reduced_index < inter.num_reduced_nodes);
            assert(grating->node4_reduced_index == NOT_FOUND);

            node3 = inter.node_list[grating->node3_index];
            if (node1.gnd_node &&
                    node2.gnd_node &&
                    node3.gnd_node) {
                gerror("%s not connected\n", grating->name);
            }
            break;
        case 4:
            // check bounds on node3_index and node4_index
            assert(grating->node3_index >= 0);
            assert(grating->node4_index >= 0);
            assert(grating->node3_index < inter.num_nodes);
            assert(grating->node3_index < inter.num_nodes);
            assert(grating->node3_index != grating->node1_index &&
                    grating->node3_index != grating->node2_index);
            assert(grating->node4_index != grating->node1_index &&
                    grating->node4_index != grating->node2_index &&
                    grating->node4_index != grating->node3_index);
            // check node3_reduced_index
            assert(grating->node3_reduced_index >= 0);
            assert(grating->node4_reduced_index >= 0);
            assert(grating->node3_reduced_index < inter.num_reduced_nodes);
            assert(grating->node4_reduced_index < inter.num_reduced_nodes);

            node3 = inter.node_list[grating->node3_index];
            node4 = inter.node_list[grating->node4_index];
            if (node1.gnd_node &&
                    node2.gnd_node &&
                    node3.gnd_node &&
                    node4.gnd_node) {
                gerror("%s not connected\n", grating->name);
            }
            break;
        default:
            bug_error("wrong number of ports, 1");
    }
    // compute and set (and check) derived parameters 
    //rebuild_grating(grating); This CAN NOT be done here, since the setting for eta are set later via 'attr'
}

//! Check the diode parameters

/*!
 * \param diode the diode to check
 */
void check_diode(diode_t *diode) {
    node_t node1, node2;

    // assert that the node indices are in the correct range:
    assert(diode->node1_index >= 0);
    assert(diode->node2_index >= 0);
    assert(diode->node1_index < inter.num_nodes);
    assert(diode->node2_index < inter.num_nodes);
    assert(diode->node1_index != diode->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(diode->node1_reduced_index >= 0);
    assert(diode->node2_reduced_index >= 0);
    assert(diode->node1_reduced_index < inter.num_reduced_nodes);
    assert(diode->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[diode->node1_index].gnd_node &&
            NOT inter.node_list[diode->node2_index].gnd_node) {
        assert(diode->node1_reduced_index != diode->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[diode->node1_index];
    node2 = inter.node_list[diode->node2_index];

    if (node1.gnd_node ||
            node2.gnd_node) {
        gerror("%s connected to a dump\n", diode->name);
    } else if (diode->node1_index == diode->node2_index) {
        gerror("%s connected to same node twice\n", diode->name);
    }
}



void check_dbs(dbs_t *diode) {
    node_t node1, node2, node3, node4;

    // assert that the node indices are in the correct range:
    assert(diode->node1_index >= 0);
    assert(diode->node2_index >= 0);
    assert(diode->node3_index >= 0);
    assert(diode->node4_index >= 0);
    
    assert(diode->node1_index < inter.num_nodes);
    assert(diode->node2_index < inter.num_nodes);
    assert(diode->node3_index < inter.num_nodes);
    assert(diode->node4_index < inter.num_nodes);
    
    
    assert(diode->node1_index != diode->node2_index);
    assert(diode->node1_index != diode->node3_index);
    assert(diode->node1_index != diode->node4_index);
    
    assert(diode->node2_index != diode->node3_index);
    assert(diode->node2_index != diode->node4_index);
    
    assert(diode->node3_index != diode->node4_index);

    // assert that the reduced node indices are in the correct range:
    assert(diode->node1_reduced_index >= 0);
    assert(diode->node2_reduced_index >= 0);
    
    assert(diode->node1_reduced_index < inter.num_reduced_nodes);
    assert(diode->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[diode->node1_index].gnd_node &&
            NOT inter.node_list[diode->node2_index].gnd_node) {
        assert(diode->node1_reduced_index != diode->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[diode->node1_index];
    node2 = inter.node_list[diode->node2_index];
    node3 = inter.node_list[diode->node3_index];
    node4 = inter.node_list[diode->node4_index];

    if (diode->node1_index == diode->node2_index ||
            diode->node1_index == diode->node3_index ||
            diode->node1_index == diode->node4_index ||
            diode->node2_index == diode->node3_index ||
            diode->node2_index == diode->node4_index ||
            diode->node3_index == diode->node4_index) {
        gerror("%s connected to same node twice\n", diode->name);
    }
}



//! Check the beamsplitter parameters

/*!
 * \param bs the beam splitter to check
 */
void check_beamsplitter(beamsplitter_t *bs) {
    node_t node1, node2, node3, node4;

    // assert that the node indices are in the correct range:
    assert(bs->node1_index >= 0);
    assert(bs->node2_index >= 0);
    assert(bs->node3_index >= 0);
    assert(bs->node4_index >= 0);
    assert(bs->node1_index < inter.num_nodes);
    assert(bs->node2_index < inter.num_nodes);
    assert(bs->node3_index < inter.num_nodes);
    assert(bs->node4_index < inter.num_nodes);
    assert(bs->node1_index != bs->node2_index &&
            bs->node1_index != bs->node3_index &&
            bs->node1_index != bs->node4_index &&
            bs->node2_index != bs->node3_index &&
            bs->node2_index != bs->node4_index &&
            bs->node3_index != bs->node4_index);

    // assert that the reduced node indices are in the correct range:
    assert(bs->node1_reduced_index >= 0);
    assert(bs->node2_reduced_index >= 0);
    assert(bs->node3_reduced_index >= 0);
    assert(bs->node4_reduced_index >= 0);
    assert(bs->node1_reduced_index < inter.num_reduced_nodes);
    assert(bs->node2_reduced_index < inter.num_reduced_nodes);
    assert(bs->node3_reduced_index < inter.num_reduced_nodes);
    assert(bs->node4_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[bs->node1_index].gnd_node &&
            NOT inter.node_list[bs->node2_index].gnd_node &&
            NOT inter.node_list[bs->node3_index].gnd_node &&
            NOT inter.node_list[bs->node4_index].gnd_node) {
        assert(bs->node1_reduced_index != bs->node2_reduced_index &&
                bs->node1_reduced_index != bs->node3_reduced_index &&
                bs->node1_reduced_index != bs->node4_reduced_index &&
                bs->node2_reduced_index != bs->node3_reduced_index &&
                bs->node2_reduced_index != bs->node4_reduced_index &&
                bs->node3_reduced_index != bs->node4_reduced_index);
    }
#endif

    node1 = inter.node_list[bs->node1_index];
    node2 = inter.node_list[bs->node2_index];
    node3 = inter.node_list[bs->node3_index];
    node4 = inter.node_list[bs->node4_index];

    if (bs->R > 1 || bs->R < 0) {
        gerror("%s  reflectivity must be 0 <= r <= 1\n", bs->name);
    }

    if (bs->alpha_1 >= 90 || bs->alpha_1 <= -90) {
        gerror("%s  alpha must be: -90 <= alpha <= 90\n", bs->name);
    } else if (bs->T > 1 || bs->T < 0) {
        gerror("%s  transmittivity must be: 0 <= t <= 1\n", bs->name);
    } else if ((node2.gnd_node && node3.gnd_node) ||
            (node1.gnd_node && node4.gnd_node)) {
        gerror("%s not connected\n", bs->name);
    } else if (bs->R + bs->T - 1 > SMALL) {
        gerror("%s parameters r, t inconsistent\n", bs->name);
    }
    
    if(bs->mass < 0.0)
        gerror("%s mass is less than 0.0\n", bs->name);
    
    if(bs->mass == 0.0 && bs->mech_tf != NULL)
        gerror("%s has 0 mass but a mechanical transfer function\n", bs->name);
    
    if(bs->mass > 0.0 && bs->mech_tf == NULL)
        gerror("%s has a mass but a no mechanical transfer function\n", bs->name);
    
    /*
      else if (1-bs->R-bs->T > SMALL)
      warn("Warning: %s  t and r don't add to unity\n", bs->name);
     */
}

//! Check the free space parameters

/*!
 * \param sagnac the free space to check
 */
void check_sagnac(sagnac_t *sagnac) {

    node_t node1, node2;

    // assert that the node indices are in the correct range:
    assert(sagnac->node1_index >= 0);
    assert(sagnac->node2_index >= 0);
    assert(sagnac->node1_index < inter.num_nodes);
    assert(sagnac->node2_index < inter.num_nodes);
    assert(sagnac->node1_index != sagnac->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(sagnac->node1_reduced_index >= 0);
    assert(sagnac->node2_reduced_index >= 0);
    assert(sagnac->node1_reduced_index < inter.num_reduced_nodes);
    assert(sagnac->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[sagnac->node1_index].gnd_node &&
            NOT inter.node_list[sagnac->node2_index].gnd_node) {
        assert(sagnac->node1_reduced_index != sagnac->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[sagnac->node1_index];
    node2 = inter.node_list[sagnac->node2_index];

    if (node1.gnd_node &&
            node2.gnd_node) {
        gerror("%s not connected\n", sagnac->name);
    } else if (node1.gnd_node ||
            node2.gnd_node) {
        gerror("sagnac %s connected to beam dump\n", sagnac->name);
    }
}

//! Check the free space parameters

/*!
 * \param space the free space to check
 */
void check_space(space_t *space) {
    int component1_index, component2_index;

    node_t node1, node2;

    // assert that the node indices are in the correct range:
    assert(space->node1_index >= 0);
    assert(space->node2_index >= 0);
    assert(space->node1_index < inter.num_nodes);
    assert(space->node2_index < inter.num_nodes);
    assert(space->node1_index != space->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(space->node1_reduced_index >= 0);
    assert(space->node2_reduced_index >= 0);
    assert(space->node1_reduced_index < inter.num_reduced_nodes);
    assert(space->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[space->node1_index].gnd_node &&
            NOT inter.node_list[space->node2_index].gnd_node) {
        assert(space->node1_reduced_index != space->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[space->node1_index];
    node2 = inter.node_list[space->node2_index];

    if (space->L < 0) {
        gerror("%s  length must be > 0 \n", space->name);
    } else if (node1.gnd_node &&
            node2.gnd_node) {
        gerror("%s not connected\n", space->name);
    } else if (node1.gnd_node ||
            node2.gnd_node) {
        gerror("space %s connected to beam dump\n", space->name);
    } else if (space->n <= 0) {
        gerror("%s  refractive index must be > 0 \n", space->name);
    }

    if (space->n < 1) {
        warn("%s  refractive index is < 1 \n", space->name);
    }

    if (space->n > 4) {
        warn("%s  refractive index is > 4 \n", space->name);
    }

    // check whether two spaces are connected and 
    // if not set index of refraction to connected nodes

    // TODO experimetnal: changed error to warn TO BE TESTED 241012 adf
    if (NOT node1.gnd_node) {
        which_components(space->node1_index, &component1_index, &component2_index);

        if (get_component_type(component1_index) == SPACE &&
                get_component_type(component2_index) == SPACE) {
            warn("space %s connected to another space.\n"
                    " Separate with another component, e.g. a mirror!\n",
                    space->name, get_node_name(space->node1_index));
        }
        else {
            // set index of refraction to node!
            inter.node_list[space->node1_index].n = &(space->n);
        }
    }

    if (NOT node2.gnd_node) {
        which_components(space->node2_index, &component1_index, &component2_index);

        if (get_component_type(component1_index) == SPACE &&
                get_component_type(component2_index) == SPACE) {
            warn("space %s connected to another space at node %s.\n"
                    " Separate with another component, e.g. a mirror!\n",
                    space->name, get_node_name(space->node2_index));
        }
        else {
            // set index of refraction to node!
            inter.node_list[space->node2_index].n = &(space->n);
        }
    }
}

//! Check the modulator parameters

/*!
 * \param modulator the modulator to check
 */
void check_modulator(modulator_t *modulator) {
    node_t node1, node2;

    // assert that the node indices are in the correct range:
    assert(modulator->node1_index >= 0);
    assert(modulator->node2_index >= 0);
    assert(modulator->node1_index < inter.num_nodes);
    assert(modulator->node2_index < inter.num_nodes);
    assert(modulator->node1_index != modulator->node2_index);

    // assert that the reduced node indices are in the correct range:
    assert(modulator->node1_reduced_index >= 0);
    assert(modulator->node2_reduced_index >= 0);
    assert(modulator->node1_reduced_index < inter.num_reduced_nodes);
    assert(modulator->node2_reduced_index < inter.num_reduced_nodes);

    // reduced indices can be equal, but only in the case where one of
    // the nodes is a dump node.
#ifndef NDEBUG
    if (NOT inter.node_list[modulator->node1_index].gnd_node &&
            NOT inter.node_list[modulator->node2_index].gnd_node) {
        assert(modulator->node1_reduced_index != modulator->node2_reduced_index);
    }
#endif

    node1 = inter.node_list[modulator->node1_index];
    node2 = inter.node_list[modulator->node2_index];

    if (node1.gnd_node &&
            node2.gnd_node) {
        gerror("%s not connected\n", modulator->name);
    } else if (node1.gnd_node ||
            node2.gnd_node) {
        gerror("eom %s connected to beam dump\n", modulator->name);
    }
}

//! Compute the xy Hermite-Gauss function 

/*!
 *  Normalised Hermite-Gauss function as defined in Siegman's 'Lasers'
 *
 * \param n mode number
 * \param m mode number
 * \param qx Gaussian beam parameter in x-plane
 * \param qy Gaussian beam parameter in y-plane
 * \param x position on x-axis
 * \param y position on y-axis
 * \param nr refractive index
 */
complex_t u_nm(int n, int m, complex_t qx, complex_t qy, double x, double y,
        double nr) {
    complex_t zx, zy, z;

    // sanity checks on inputs
    assert(m >= 0);
    assert(n >= 0);

    assert(nr > 0.0);

    zx = u(n, qx, x, nr);
    zy = u(m, qy, y, nr);

    z = z_by_z(zx, zy);
    return (z);
}

u_nm_accel_t* u_nm_accel_alloc(int n, int m,long *bytes) {
    u_nm_accel_t *ptr = (u_nm_accel_t*) malloc(sizeof (u_nm_accel_t));
    if (ptr == NULL) gerror("Could not allocate enough memory for u_n_accel");

    ptr->acc_n = u_n_accel_alloc(n, bytes);
    ptr->acc_m = u_n_accel_alloc(m, bytes);
    
    return ptr;
}

u_n_accel_t* u_n_accel_alloc(int n, long *bytes) {
    u_n_accel_t *ptr = (u_n_accel_t*) malloc(sizeof (u_n_accel_t));
    if (ptr == NULL) gerror("Could not allocate enough memory for u_n_accel");

    ptr->prefac = (complex_t*) malloc(sizeof (complex_t)*(n + 1));
    if (ptr->prefac == NULL) gerror("Could not allocate enough memory for prefactor array");
    
    *bytes += sizeof (u_n_accel_t);
    *bytes += sizeof (complex_t)*(n + 1);
    
    return ptr;
}

void u_nm_accel_get(u_nm_accel_t* acc, int n, int m, complex_t qx, complex_t qy, double nr) {
    u_n_accel_get(acc->acc_n, n, qx, nr);
    u_n_accel_get(acc->acc_m, m, qy, nr);
}

void u_n_accel_get(u_n_accel_t* acc, int n, complex_t q, double nr) {
    
    assert(n >= 0);
    assert(nr >= 1.0);

    if (acc == NULL)
        gerror("Null u_n_accell_t pointer passed to function u_n_get_accelerator()");

    double w0 = w0_size(q, nr);
    
    complex_t q0, q0c, qc, z1, z2;
    int i;

    q0 = q_0(q);
    q0c = cconj(q0);
    qc = cconj(q);

    acc->n = n;
    acc->sqrt2_wz = SQRTTWO / w_size(q, nr);
    acc->k = 2.0 * PI * nr / init.lambda;

    z1.re = 0;
    z1.im = -1.0 * PI * nr / init.lambda;
    acc->negK_2q = div_complex(z1, q);

    for (i = 0; i <= n; i++) {
        z1 = z_by_x(zsqrt(div_complex(q0, z_by_x(q, pow(2.0, i) * fac(i) * w0))), pow(2.0 / PI, 0.25));

        z2 = pow_complex(z_by_z(div_complex(qc, q), div_complex(q0, q0c)), i, 2.0);

        acc->prefac[i] = z_by_z(z1, z2);
    }
}

void u_n_accel_free(u_n_accel_t* ptr) {
    free(ptr->prefac);
    ptr->prefac = NULL;
}

void u_nm_accel_free(u_nm_accel_t* ptr) {
    u_n_accel_free(ptr->acc_m);
    free(ptr->acc_m);

    u_n_accel_free(ptr->acc_n);
    free(ptr->acc_n);

    ptr->acc_n = NULL;
    ptr->acc_m = NULL;
}

//! Alternative method to compute the Hermite-Gauss function u_n(x,z) 

/*!
 * This function is part of u_fast, u_fast_prefactor, u_fast_x
 * that split the functionality of u_n into different parts so that
 * repeating calls with same q,n,nr can be handled faster
 *
 * \param n mode number
 * \param q Gaussian beam parameter
 * \param nr refractive index
 */

complex_t u_fast_prefactor(int n, complex_t q, double nr) {
    double factor;
    complex_t phase, z1, z2, z3, z4;

    factor = TWOPI_QRT / sqrt(npow(2, n) * fac(n) * w0_size(q, nr));

    z1 = zsqrt(div_complex(q_0(q), q));
    z2 = zsqrt(div_complex(z_by_z(q_0(q), cconj(q)), z_by_z(cconj(q_0(q)), q)));
    z3 = z_by_z(z1, pow_complex(z2, n, 1));

    phase = inv_complex(q);
    factor *= exp(-1.0 * phase.im);
    z4 = z_by_xphr(z3, factor, phase.re);

    return (z4);
}

//! Alternative method to compute the Hermite-Gauss function u_n(x,z) 

/*!
 * This function is part of u_fast, u_fast_prefactor, u_fast_x
 * that split the functionality of u_n into different parts so that
 * repeating calls with same q,n,nr can be handled faster
 *
 * \param n mode number
 * \param x position on x-axis
 */
complex_t u_fast_x(int n, double x, u_n_accel_t *acc) {
    complex_t phase, z;
    phase = z_by_x(acc->negK_2q, x * x);
    double a = exp(1.0 * phase.re) * hermite(n, x * acc->sqrt2_wz);
    z.re = a * cos(phase.im);
    z.im = a * sin(phase.im);
    return z;
}

complex_t u_nm_fast_x(double x, double y, int n, int m, u_nm_accel_t * acc) {
    return z_by_z(u_fast_x(n, x, acc->acc_n), u_fast_x(m, y, acc->acc_m));
}

//! Alternative method to compute the Hermite-Gauss function u_n(x,z) 

/*!
 * This function is part of u_fast, u_fast_prefactor, u_fast_x
 * that split the functionality of u_n into different parts so that
 * repeating calls with same q,n,nr can be handled faster.
 * 
 * This function combines the results from u_fast_x and u_fast_prefactor
 *
 * \param prefactor, previsouly computed value by u_fast_prefactor
 * \param x, previsouly computed value by u_fast_x
 */
complex_t u_fast(complex_t prefactor, complex_t x) {
    return (z_by_z(x, prefactor));
}


//! Compute the Hermite-Gauss function u_n(x,z) (Siegmann S.686)

/*!
 * Need to add more info here as to what this function is.  See Siegman ???
 *
 * \param n mode number
 * \param q Gaussian beam parameter
 * \param x position on x-axis
 * \param nr refractive index
 */
complex_t u(int n, complex_t q, double x, double nr) {
    double factor;
    complex_t phase, z1, z2, z3, z4;
    const double k = TWOPI / init.lambda * nr;

    // sanity checks on inputs
    assert(n >= 0);
    assert(nr > 0.0);

    factor = TWOPI_QRT / sqrt(npow(2, n) * fac(n) * w0_size(q, nr))
            * hermite(n, SQRTTWO * x / w_size(q, nr));

    z1 = zsqrt(div_complex(q_0(q), q));
    z2 = zsqrt(div_complex(z_by_z(q_0(q), cconj(q)), z_by_z(cconj(q_0(q)), q)));
    z3 = z_by_z(z1, pow_complex(z2, n, 1));


    phase = z_by_x(inv_complex(q), -1.0 * k * x * x / 2.0);

    factor *= exp(-1.0 * phase.im);

    z4 = z_by_xphr(z3, factor, phase.re);

    return (z4);
}

//! Transform Gaussian beam parameter q1 to q2 according a an ABCD matrix

/*!
 * \param s ABCD matrix
 * \param q1 input Gaussian beam parameter
 * \param n1 index of refraction at q1
 * \param n2 index of refraction at q2
 */
complex_t q1_q2(ABCD_t s, complex_t q1, double n1, double n2) {
    complex_t q2, tmp_c;

    // sanity checks on inputs
    assert(n1 != 0.0);
    assert(n2 != 0.0);

    q2.re = s.A * q1.re / n1 + s.B;
    q2.im = s.A * q1.im / n1;
    tmp_c.re = s.C * q1.re / n1 + s.D;
    tmp_c.im = s.C * q1.im / n1;

    if (s.C == 0.0 && s.D == 0) {
        bug_error("division by zero (C, D=0)");
    }

    if (ceq(tmp_c, complex_0)) {
        bug_error("division by zero");
    }

    q2 = div_complex(q2, tmp_c);
    q2 = z_by_x(q2, n2);


    return (q2);
}

//! Multiply two ABCD matrices together

/*!
 * \param s1 first input ABCD matrix
 * \param s2 second input ABCD matrix
 */
ABCD_t multiply_ABCD(ABCD_t s1, ABCD_t s2) {
    ABCD_t s;

    s.A = s1.A * s2.A + s1.B * s2.C;
    s.B = s1.A * s2.B + s1.B * s2.D;
    s.C = s1.C * s2.A + s1.D * s2.C;
    s.D = s1.C * s2.B + s1.D * s2.D;

    return (s);
}

//! Find the inverse of an ABCD matrix

/*!
 * \param s1 input ABCD matrix
 */
ABCD_t inv_ABCD(ABCD_t s1) {
    ABCD_t s;
    double tmp_d;

    tmp_d = s1.A * s1.D - s1.C * s1.B;

    s.A = s1.D / tmp_d;
    s.B = -s1.B / tmp_d;
    s.C = -s1.C / tmp_d;
    s.D = s1.A / tmp_d;

    return (s);
}

//! Calculates `reverse' ABCD matrix. 

/*!
 * Beam coming from the `other' direction, 
 * type mirror : for reflection C -> -C *n2/n1 110903
 * type bs : for  transmission tangential A->1/A, D->1/D
 *
 * \param type the type of component; mirror or beam splitter
 * \param s1 ???
 * \param n ???
 *
 * \todo untested
 */
ABCD_t reverse_ABCD(int type, ABCD_t s1, double n) {
    ABCD_t s;

    bug_error("reverse_ABCD"); // not used anymore - if testing succeeds.

    switch (type) {
        case MIRROR:
            // now obsolete, will remove later 08/01/2007
            bug_error("Mirror reverse_ABCD");
            s.A = s1.A;
            s.B = s1.B;
            s.C = -s1.C * n;
            s.D = s1.D;
            break;
        case BEAMSPLITTER:
            if (s1.A == 0.0 || s1.D == 0.0) {
                bug_error("ABCD zero diagonal");
            }
            s.A = 1.0 / s1.A;
            s.B = s1.B;
            s.C = s1.C;
            s.D = 1.0 / s1.D;
            break;
        default:
            s = s1;
    }
    return (s);
}

//! Make an ABCD matrix

/*!
 * \param a element a of ABCD matrix
 * \param b element b of ABCD matrix
 * \param c element c of ABCD matrix
 * \param d element d of ABCD matrix
 */
ABCD_t make_ABCD(double a, double b, double c, double d) {
    ABCD_t s;

    s.A = a;
    s.B = b;
    s.C = c;
    s.D = d;

    return (s);
}

complex_t **_bnmk = NULL;

void alloc_bnmk(){
    if(!inter.tem_is_set)
        gerror("Cannot compute HG to LG coefficients when tem has not been set");
                
    if (_bnmk != NULL)
        bug_error("Already initialised bnmk variable");
    
    int n = inter.tem;
    int m = inter.tem;
    
    int num_fields = (int) (n + 1) * (m + 2) / 2;
    int N = n + m;
    
    _bnmk = (complex_t **) calloc(num_fields + 1, sizeof (complex_t *));
    
    if(_bnmk == NULL)
        gerror("Memory allocation for bnmk matrix failed");
    
    int i;
    
    for (i = 0; i < num_fields; i++) {
        // coupling coefficients
        _bnmk[i] = (complex_t *) calloc(N + 1, sizeof (complex_t));
        
        if(_bnmk[i] == NULL)
                gerror("Memory allocation for bnmk matrix failed");
    }
}

void free_bnmk(){
    if (_bnmk == NULL)
        bug_error("bnmk coefficients have not been allocated yet");
    
    int n = inter.tem;
    int m = inter.tem;
    
    int num_fields = (int) (n + 1) * (m + 2) / 2;
    int i;
    
    for (i = 0; i < num_fields; i++) {
        // coupling coefficients
        free(_bnmk[i]);
        _bnmk[i] = NULL;
    }
    
    free(_bnmk);
    _bnmk = NULL;
}

double nchoosek(int n, int k){
    if(k>n)
        return 0.0;
    else
        return gsl_sf_choose(n,k); 
}

double Pabn_0(int a, int b, int n){
    double rtn = 0;
    int j;
    
    for(j=0; j<=n; j++){
        rtn += nchoosek(n+a,j) * nchoosek(n+b,n-j) * gsl_pow_int(-1, n-j);
    }
    
    return rtn / gsl_pow_int(2,n);
}

void generate_bnmk(){
    alloc_bnmk();
    
    int _n = inter.tem;
    int _m = inter.tem;
    int num_fields = (int) (_n + 1) * (_m + 2) / 2;
    int i=0,k=0;
    
    double *k_fac = (double*) calloc(_n+_m+1, sizeof(double));
    
    
    for (i = 0; i < num_fields; i++) {
        int n,m;
        get_tem_modes_from_field_index(&n, &m, i);
        int N = n+m;
        int TwoPowN = pow(2,N);
        int nfac = fac(inter.tem);
        double sqrFac =  1.0 / (TwoPowN*nfac*nfac);
    
        for (k = 0; k <= N; k++){ 
            // Only need to compute the k factorials once the
            // first time round then we can reuse them
            if (i == 0){
                k_fac[k] = fac(k);
            }
            
            _bnmk[i][k].re = sqrt(fac(N-k) * k_fac[k] * sqrFac);
            _bnmk[i][k].re *= pow(-2,k) * Pabn_0(n-k, m-k, k);
        }
    }
    
    free(k_fac);
    k_fac = NULL;
}

double bnmk(int n, int m, int k){
    
    int N = n+m;
    double sqrFac =  1.0 / (pow(2,N)*fac(n)*fac(m));
    
    double rtn = sqrt(fac(N-k) * fac(k) * sqrFac);
    rtn *= pow(-2,k) * Pabn_0(n-k, m-k, k);
    
    return rtn;
}

/*
 * Converts a given set of hg amplitudes, defined in the usual Finesse
 * manner of a 1D array, into an array of LG amplitudes 
 */
void hg_2_lg(complex_t *A_hg, complex_t *A_lg){
    int p,l,i,j,n,m;
        
    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    
    for (i = 0; i < num_fields; i++) {
        get_tem_modes_from_field_index(&n, &m, i);
        
        int N = n+m;
                
        for(j=0; j<=N; j++){
            complex_t tmp = complex_0;
        
            l = 2*j-N;
            p = (N-abs(l))/2;
            double sqrFac =  1.0 / (pow(2,N)*fac(abs(l)+p)*fac(p));
            double bnmk = sqrt(fac(N-m) * fac(m) * sqrFac);
            bnmk *= pow(-2,m) * Pabn_0(abs(l)+p-m, p-m, m);
    
            // compute coupling
            tmp = z_pl_z(tmp, z_by_x(pow_complex( (l>0)? complex_i : complex_mi, m, 1), bnmk));            
            
            if(inter.debug & 16384)
                debug_msg("nm:%i%i -> lp:%i%i = %s\n",n,m,l,p,complex_form15(tmp));
            
            // get amount of mode HG_{N-k, k}
            tmp = z_by_z(tmp, A_hg[i]);
            
            // apply -1^p factor
            if( p % 2 == 1) // if p is odd
                tmp = z_by_x(tmp, -1.0);

            int ix = get_field_index_from_tem_LG(p,l);
            A_lg[ix] = tmp;
        }
    }
}

/*
 * Converts a given set of LG amplitudes, defined in the usual Finesse
 * manner of a 1D array, into an array of HG amplitudes 
 */
void lg_2_hg(complex_t *A_lg, complex_t *A_hg){
    int p,l,n,m,i,j;
        
    int num_fields = (int) (inter.tem + 1) * (inter.tem + 2) / 2;
    
    for (i = 0; i < num_fields; i++) {
        get_tem_modes_from_LG_field_index(&p, &l, i);
        n = abs(l) + p;
        m = p;
        
        int N = 2*p + abs(l);
        
        for(j=0; j<=N; j++){            
            double bnmk = sqrt(fac(N-j) * fac(j) / (pow(2,N)*fac(n)*fac(m)));
            bnmk *= pow(-2,j) * Pabn_0(n-j, m-j, j);
            
            int cp = 1;
            
            // apply -1^p factor
            if( p % 2 == 1) // if p is odd
                cp = -1;
            
            complex_t tmp2 = z_by_x(pow_complex( (l>0)? complex_mi : complex_i, j, 1), bnmk * cp);
            
            if(inter.debug & 16384)
                debug_msg("lg:%i%i -> nm:%i%i = %s\n",l,p,N-j,j,complex_form15(tmp2));
            
            int ix = get_field_index_from_tem(N-j, j);
            A_hg[ix] = z_pl_z(A_hg[ix], z_by_z(tmp2, A_lg[i]));
        }
    }
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

// $Id$

/*!
 * \file kat_gnu.h
 * \brief Header file for gnuplot-related routines in Finesse
 * 
 * @section Copyright notice
 *
 *  This file is part of the interferometer simulation Finesse
 *  http://www.gwoptics.org/finesse
 *
 *  Copyright (C) 1999 onwards Andreas Freise
 *  with parts of the code written by Daniel Brown, Paul Cochrane
 *  and Gerhard Heinzel.
 *
 *  This program is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License version 3 as published
 *  by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 *  Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KAT_GNU_H
#define KAT_GNU_H

char *otypename(int i);
void gnufile(int step, FILE *fp, char *fname, bool test_file);
void matlabfile(FILE *fp, char *fname);
void pythonfile(FILE *fp, char *fname);
void python_file_outputs(FILE *fp);
void plot3Dpython(FILE *fp, char *xy1, char *xy2);
void plot2Dpython(FILE *fp, char *xy1, char *xy2);
void print_python_usage(FILE *fp, char *python_file_str);
void print_python_3Dconvert(FILE *fp);
void print_python_pdf(FILE *fp, char *python_file_str);
void setpythonaxisprops(FILE *fp, int ax_number, char *xy1);
void print_python_global_settings(FILE *fp);
void setmatlabaxisprops(FILE *fp, char *XY1, char *xy1);
void plot2Dmatlab(FILE *fp, int startcol);
void plot3Dmatlab(FILE *fp, int startcol);
void write_matlab_usage(FILE *fp, char *matlab_func_str);
void write_nargout_check(FILE *fp, int is_splot);
void write3Dfunction(FILE *fp);

#endif // KAT_GNU_H

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 *
 * vim: tabstop=2 expandtab shiftwidth=2:
 */

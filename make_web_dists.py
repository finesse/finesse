print("""
This tool will create the web distribution packages for Finesse from the
Anaconda packages created from each platform. Please update the links in
this file to point to the correct Anaconda packages for extracting the
binaries:

    https://anaconda.org/gwoptics/finesse/files
""")

#conda_win32 = "https://anaconda.org/gwoptics/finesse/2.2/download/win-32/finesse-2.2-hf88ea13_11.tar.bz2"
conda_win64 = "https://anaconda.org/gwoptics/finesse/2.3.1/download/win-64/finesse-2.3.1-6.tar.bz2"
#conda_linux32 = "https://anaconda.org/gwoptics/finesse/2.2/download/linux-32/finesse-2.2-h69580f5_11.tar.bz2"
conda_linux64 = "https://anaconda.org/gwoptics/finesse/2.3.1/download/linux-64/finesse-2.3.1-2.tar.bz2"
conda_osx64 = "https://anaconda.org/gwoptics/finesse/2.3.1/download/osx-64/finesse-2.3.1-2.tar.bz2"

version = "2.3.1"

conda_pkgs = [
    #conda_win32,
    conda_win64,
    #conda_linux32,
    conda_linux64,
    conda_osx64
]

output_pkgs = [
    #"kat-" +version + "-win_32bit",
    "kat-" +version + "-win_64bit",
    #"kat-" +version + "-linux_32bit",
    "kat-" +version + "-linux_64bit",
    "kat-" +version + "-osx_64bit"
]

for _ in conda_pkgs:
    assert(version in _)

#assert("win-32" in conda_win32)
assert("win-64" in conda_win64)
#assert("linux-32" in conda_linux32)
assert("linux-64" in conda_linux64)
assert("osx-64" in conda_osx64)

import tempfile
import tarfile
import shutil
import os
import requests


outputdir = os.getcwd()

for pkg, out in zip(conda_pkgs, output_pkgs):
    tmpdir = tempfile.TemporaryDirectory()
    print("Creating", pkg, ", output at", out)
    os.chdir(tmpdir.name)
    os.makedirs('Finesse')
    #print(f"tempdir: {tmpdir.name}")
    #print(f"Retrieving {pkg}")
    r = requests.get(pkg)
    with open('pkg.tar.bz2', 'wb') as outfile:
        outfile.write(r.content)

    tar = tarfile.open('pkg.tar.bz2', "r:bz2")

    for _ in tar:
        if _.isfile() and 'bin' in _.name:
            tar.extract(_, tmpdir.name)
            path, f = os.path.split(_.name)
            shutil.move(_.name, os.path.join("Finesse/",f))
            os.removedirs   (path)
    
    shutil.copyfile(os.path.join(outputdir, "CHANGES"),       os.path.join(tmpdir.name,"Finesse/CHANGES"))
    shutil.copyfile(os.path.join(outputdir, "INSTALL"),       os.path.join(tmpdir.name,"Finesse/INSTALL"))
    shutil.copyfile(os.path.join(outputdir, "README.md"),     os.path.join(tmpdir.name,"Finesse/README.md"))
    shutil.copyfile(os.path.join(outputdir, "LICENSES"),      os.path.join(tmpdir.name,"Finesse/LICENSES"))
    shutil.copyfile(os.path.join(outputdir, "test.kat"),      os.path.join(tmpdir.name,"Finesse/test.kat"))
    shutil.copyfile(os.path.join(outputdir, "test_plot.kat"), os.path.join(tmpdir.name,"Finesse/test_plot.kat"))
    
    if "win" in pkg:
        shutil.copyfile(os.path.join(outputdir, "SetEnv.exe"),      os.path.join(tmpdir.name, "Finesse/SetEnv.exe"))
        shutil.copyfile(os.path.join(outputdir, "findGnuplot.bat"), os.path.join(tmpdir.name, "Finesse/findGnuplot.bat"))
        shutil.copyfile(os.path.join(outputdir, "install.bat"),     os.path.join(tmpdir.name, "Finesse/install.bat"))
    
    os.remove('pkg.tar.bz2')
    
    shutil.make_archive(os.path.join(outputdir, out), "zip")
    
    tmpdir.cleanup()

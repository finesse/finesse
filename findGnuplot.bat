@setlocal enableextensions enabledelayedexpansion
@echo off

:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------


CD %~dp0
echo ---------------------------------------
echo Looking for gnuplot on your computer...
FOR /F "usebackq tokens=3*" %%A IN (`REG QUERY "HKEY_CLASSES_ROOT\gnuplot\shell\open\command"`) DO (
    set appdir=%%A %%B
    set appdir=!appdir:~0,-4!
    )
    
IF DEFINED appdir (
    ECHO Found GNUPLOT %appdir%!
    ECHO Adding to end of kat.ini...
    echo GNUCOMMAND '%appdir%' >> kat.ini
) ELSE (
    ECHO No GNUPLOT installation could be
    ECHO found. Please find it manually or 
    ECHO and add to the GNUCOMMAND and WGNUCOMMAND
    ECHO to the kat.ini file. Or install gnuplot
    ECHO using the installer
    ECHO http://sourceforge.net/projects/gnuplot/files/latest/download
    ECHO and then re-run findGnuplot.bat.
)
echo ---------------------------------------
pause